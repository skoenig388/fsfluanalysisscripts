%Code checks if FLU trials were run correctly or not...blue quaddle name
%was wrong in Igor's/Reider's kiosk for a few months so checking if task
%was run correctly cuz a color choice was duplicated instead.

clar
%log_dir = 'Z:\DATA_kiosk\Reider\FLU Task\';
log_dir = 'Z:\DATA_kiosk\Igor\FLU Task\';

trial_name = '__TrialData.txt';
trialdef_name = '__trialdef_on_trial_';

%% Feature Dimensions and values
shapes = {'S01', 'S02', 'S03', 'S04', 'S05', 'S06', 'S07', 'S08', 'S09'};
patterns = {'P01', 'P02', 'P03', 'P04', 'P05', 'P06', 'P07', 'P08', 'P09'};

%patternless_colors = {'C6070014_6070014', 'C6070059_6070059', 'C6070106_6070106', 'C6070148_6070148', 'C6070194_6070194', 'C6070287_6070287', 'C6070335_6070335','C6070240_6070240'};
patternless_colors = {'C7070014_7070014', 'C7070059_7070059', 'C7070106_7070106', 'C7070148_7070148', 'C7070194_7070194','C7070240_7070240','C7070286_7070286', 'C7070335_7070335'};
gray_pattern_color = 'C7000000_5000000';

arms = {'A00_E01', 'A00_E02','A00_E03', 'A01_E00', 'A01_E01', 'A01_E02', 'A01_E03', 'A02_E00', 'A02_E01', 'A02_E02', 'A02_E03'};
%%

ls = dir(log_dir);
for file = 1:length(ls)
    first_bad_trial = true;
    if contains(ls(file).name,'FLU')
        disp(['Processing file #' num2str(file) '/' num2str(length(ls))])
        filename = ls(file).name;
        trial_dir = [log_dir filename '\RuntimeData\TrialData\'];
        trial_data = readtable([trial_dir filename trial_name]);
        if trial_data(end,:).Epoch4_Duration == -1
            %last trial was never completed
            trial_data(end,:) = [];
        end
        num_trials = size(trial_data,1);
        
        for t = 1:num_trials
            TrialDef = ReadJsonFile([trial_dir filename trialdef_name num2str(t) '.json']);
            if length(TrialDef.relevantObjects) ~= 3
                continue
            else
                feature_values = NaN(3,5);
                for stim = 1:3
                    [feature_values(stim,:),neutralVals] = ParseQuaddleName(TrialDef.relevantObjects(stim).StimName,patternless_colors,arms);
                end
                for dim = 1:5
                   if  ~all(feature_values(:,dim) == 0)
                       if length(unique(feature_values(:,dim))) ~= 3
                           if first_bad_trial
                               disp([filename ' bad trial with less than 3 unqiue Quaddles'])
                               first_bad_trial = false;
                           end
                       end
                   end
                end
            end              
        end
    end
end