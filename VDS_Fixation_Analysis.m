% %Writen 2/5/19 Seth Koenig

%clar

log_dir = 'Z:\MonkeyData\Wotan\VDS\';
sess_file_name = 'VDS__Wotan__19_02_2019__12_05_51';

Fs = 600;
[Blow,Alow] = butter(8,120/(Fs/2),'low');

screenX = 1920;
screenY = 1080;

session_data = ImportFLU_DataV2(sess_file_name,log_dir);


%% Align Eye-Data Python-Eyetracker to Frames/Epochs
trial_aligned_Python_eye_data = cell(1,session_data.num_trials);
for t = 1:session_data.num_trials
    
    
    ITI_epoch_start = [session_data.trial_data.Epoch0_StartFrame(t) session_data.trial_data.Epoch0_StartTimeAbsolute(t) session_data.trial_data.Epoch0_StartTimeRelative(t)];
    Reward_epoch_start =  [session_data.trial_data.Epoch7_StartFrame(t) session_data.trial_data.Epoch7_StartTimeAbsolute(t) session_data.trial_data.Epoch7_StartTimeRelative(t)];
    Reward_epoch_duration = session_data.trial_data.Epoch7_Duration(t);
    
    this_trials_frame_ind = find(session_data.frame_data.TrialInExperiment == t);
    reward_epoch_end_frame = session_data.frame_data.Frame(this_trials_frame_ind(end));

    trial_frame_start = ITI_epoch_start(1);
    trial_end_frame = reward_epoch_end_frame;
    
    trial_Unity_time_start = ITI_epoch_start(2);
    trail_Unity_end_time = Reward_epoch_start(2)+Reward_epoch_duration;
    
    UDP_start_index = find(session_data.UDP_recv_data.Frame == trial_frame_start);
    if length(UDP_start_index) > 1
        UDP_start_index = UDP_start_index(1);
        disp('Found Multiple UDP Start Messages')
    elseif isempty(UDP_start_index)
        if abs(all_UDP_recv_data{t}(1,1)-trial_frame_start) <= 1
            UDP_start_index = 1;
            disp('1 frame offset with UDP')
        else
            disp('Cant find Trial Start Frame in UDP')
            continue
        end
    end
    
    
    UDP_end_index = find(session_data.UDP_recv_data.Frame == trial_end_frame);
    if length(UDP_end_index) > 1
        UDP_end_index = UDP_end_index(1);
        disp('Found Multiple UDP End Messages')
    elseif isempty(UDP_end_index)
        if (trial_end_frame-session_data.UDP_recv_data.Frame(end)) == 1
            disp('UDP end frame 1 frame behind trial data')
            UDP_end_index = length(session_data.UDP_recv_data.Frame);
        else
            disp('Cant find Trial End Frame in UDP')
            continue
        end
    end
    
    %verify frame start time is within 1 frame across files
    if abs(session_data.UDP_recv_data.FrameStartUnity(UDP_start_index)- trial_Unity_time_start) > 0.025
        disp('Fixation Epochs Start: UDP and Frame Start times are off :(')
    end
    if abs(session_data.UDP_recv_data.FrameStartUnity(UDP_end_index)- trail_Unity_end_time) > 0.025
        disp('Fixation Epochs Start: UDP and Frame Start times are off :(')
    end
    
    Eye_tracker_time_stamp_start = session_data.UDP_recv_data.EyeTrackerTimeStamp(UDP_start_index);
    if isnan(Eye_tracker_time_stamp_start)
        disp('What')
    elseif isempty(Eye_tracker_time_stamp_start)
        disp('What')
    end
    
    Eye_tracker_time_stamp_end = session_data.UDP_recv_data.EyeTrackerTimeStamp(UDP_end_index);
    if isnan(Eye_tracker_time_stamp_end)
        disp('What')
    elseif isempty(Eye_tracker_time_stamp_end)
        disp('What')
    end
    
    try
        
        if size(dir(frame_data_dir2),1) == 3 %only 1 frame data file
            python_ind  = find(all_pythonEye_data_time(:,2) >= Eye_tracker_time_stamp_start & ...
                all_pythonEye_data_time(:,2) <= Eye_tracker_time_stamp_end);
            
            trial_aligned_Python_eye_data{t} = [all_pythonEye_data_time(python_ind,:) ...
                all_pythonEye_data_XY(python_ind,:)];
        else
            python_ind  = find(all_pythonEye_data_time{t}(:,2) >= Eye_tracker_time_stamp_start & ...
                all_pythonEye_data_time{t}(:,2) <= Eye_tracker_time_stamp_end);
            trial_aligned_Python_eye_data{t} = [all_pythonEye_data_time{t}(python_ind,:) ...
                all_pythonEye_data_XY{t}(python_ind,:)];
        end
    catch
        disp('Eye Data mismatch or alignment issue')
    end
end
%% Plot Unity and Python Trial Aligned Data


% for t = 1:session_data.num_trials
%   
%     if isempty(trial_aligned_Python_eye_data{t})
%         continue
%     end
%     blink_end = find(all_epoch{t} == 1);
%     blink_end = blink_end(end);
%     fixation_end = find(all_epoch{t} == 2);
%     fixation_end = fixation_end(end);
%     
%     selection_period = findgaps(find(all_epoch{t} == 5));
%     selection_period = selection_period(end,:);%only the last selection period truly counts
%     selection_period(selection_period == 0) = [];
%     selection_start = selection_period(1);
%     feedback_start = find(all_epoch{t} == 6);
%     feedback_start = feedback_start(1);
%     
%     selected_Quaddle_num = str2double(session_data.trial_data.SelectedObjectID{t}(4:end));
%     selected_Quaddle_location_x = round(session_data.trial_def{t}.relevantObjects(selected_Quaddle_num).StimScreenLocation.x);
%     selected_Quaddle_location_y = round(session_data.trial_def{t}.relevantObjects(selected_Quaddle_num).StimScreenLocation.y);
%     
%     python_time = trial_aligned_Python_eye_data{t}(:,2)/10^6;
%     unity_time = all_xy{t}(:,1)/10^6;
%     
%     figure
%     subplot(3,3,[1 2 3 4 5 6])
%     hold on
%     
%     p(1) = plot([unity_time(blink_end) unity_time(blink_end)],[-0.2 1.2],'k--');
%     plot([unity_time(fixation_end) unity_time(fixation_end)],[-0.2 1.2],'k--');
%     plot([unity_time(blink_end) unity_time(fixation_end)],[0.5 0.5],'k--')
% 
% 
%     p(2) = plot([unity_time(selection_start) unity_time(selection_start)],[-0.2 1.2],'r--');
%     plot([unity_time(feedback_start) unity_time(feedback_start)],[-0.2 1.2],'r--');
%     plot([unity_time(selection_start) unity_time(feedback_start)],...
%         [selected_Quaddle_location_x/screenX selected_Quaddle_location_x/screenX],'r--');
%     plot([unity_time(selection_start) unity_time(feedback_start)],...
%         [1-selected_Quaddle_location_y/screenY 1-selected_Quaddle_location_y/screenY],'r--');
%     
%     p(3) = plot(python_time,trial_aligned_Python_eye_data{t}(:,5));
%     p(4) = plot(python_time,trial_aligned_Python_eye_data{t}(:,6));
%     p(5) = plot(python_time,trial_aligned_Python_eye_data{t}(:,7));
%     p(6) = plot(python_time,trial_aligned_Python_eye_data{t}(:,8));
%     
%     p(7) = plot(unity_time,all_xy{t}(:,2)./screenX); 
%     p(8) = plot(unity_time,1-all_xy{t}(:,3)./screenY); 
% 
%     hold off
%     ylim([-0.25 1.25])
%     xlim([min(python_time) max(python_time)])
%     xlabel('Time from Trial Start (seconds)')
%     ylabel('Eye Position')
%     legend(p,'Central Fixation','Quaddle Selection','PyLx','PyLy','PyRx','PyRy','UniX','UniY')
%    
%     subplot(3,3,[7 8 9])
%     plot(unity_time(1:end-1),diff(unity_time))
%     hold on
%     yl = ylim;
%     plot([unity_time(blink_end) unity_time(blink_end)],[yl(1) yl(2)],'k--');
%     plot([unity_time(fixation_end) unity_time(fixation_end)],[yl(1) yl(2)],'k--');
%     plot([unity_time(selection_start) unity_time(selection_start)],[yl(1) yl(2)],'r--');
%     plot([unity_time(feedback_start) unity_time(feedback_start)],[yl(1) yl(2)],'r--');
%     hold off
%     xlabel('Time From Trial Start (seconds)')
%     ylabel('Frame Duration (seconds)')
%     xlim([min(python_time) max(python_time)])
%     box off
%     
%     subtitle([sess_file_name ': Eye Alignment Trial #' num2str(t)]);
%     
%     pause(1)
%     close
% end
%%
all_unity_time = [];
all_python_time = [];
for t = 1:session_data.num_trials
    all_unity_time = [all_unity_time; all_xy{t}(:,1)];
    all_python_time = [all_python_time; all_pythonEye_data_time{t}];
end

figure
plot(diff(all_unity_time)/10^3)
xlabel('Sample #')
ylabel('Difference in Eye tracker time stamp (ms)')
%%
average_unity_time = zeros(1,500);
average_unity_counts = zeros(1,500);
for t = 1:session_data.num_trials
    unity_time = diff(all_xy{t}(:,1)/10^6);
    if length(unity_time) > 500
        unity_time = unity_time(1:500);
    end
    average_unity_time(1:length(unity_time)) = average_unity_time(1:length(unity_time))+unity_time';
    average_unity_counts(1:length(unity_time)) = average_unity_counts(1:length(unity_time))+1;
    
end
%%
average_unity_time = average_unity_time./average_unity_counts;