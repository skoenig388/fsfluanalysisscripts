function [FLU_performance_by_num_irrel,FLU_search_duration_by_num_irrel,...
    LP_by_num_irrel,LP_aligned_FLU_performance_by_num_irrel,...
    FS_performance_by_num_irrel,FS_search_duration_by_num_irrel] = ...
    CalculateBlockPerformancebyNumIrrelDim_FLU2FS(session_data,FLU_FS_blocks,...
    min_FLU_block_len,min_FS_block_len,last_trial_window,...
    forward_smoothing_window,learning_point_threshold,forward_back_trials)
%modified from CalculateBlockPerformancebyNumIrrelDim by Seth Koenig 5/20/19

%Inputs:
% 1) session_data across all sessions
% 2) FLU_FS_blocks:completed FLU(1) and FS(2) blocks
% 3) min_FLU_block_len: miniumum # of trials in a FLU block
% 4) min_FS_block_Len: minimum # of trials in FS block
% 5) last_trial_window: # of trials to look at end of block
% 6) forward_smoothing_window: window width for forward smoothing filter
% 7) learning_point_threshold: proportion correct to consider having "learned"
% 8) forward_back_trials: for performance aligned LP, how far forward and back to look

%expected_numD = [3 6 9 12];
expected_numD = [3 5 7];


if nargin < 4
    error('Not enough input arguments')
elseif nargin < 5
    last_trial_window = 10;
elseif nargin < 6
    last_trial_window = 10;
    forward_smoothing_window = 10;
elseif nargin < 7
    last_trial_window = 10;
    forward_smoothing_window = 10;
    learning_point_threshold = 0.8;
elseif nargin < 8
    last_trial_window = 10;
    forward_smoothing_window = 10;
    learning_point_threshold = 0.8;
    forward_back_trials = 5;
end

if learning_point_threshold > 1
    %assuming thrteshold is meant to be proportion correct not 100%
    learning_point_threshold = learning_point_threshold/100;
end

num_blocks = length(FLU_FS_blocks);
block = session_data.trial_data.Block;
block = block + 1;%index starts at 0 in C++
rewarded = strcmpi(session_data.trial_data.isRewarded,'true');
search_duration = Calculate_FLU_Search_Duration(session_data);


%uses a little bit more space but makes indexing for LP easier so....
FLU_performance_by_num_irrel = cell(1,3);
FLU_search_duration_by_num_irrel = cell(1,3);
LP_by_num_irrel = cell(1,3);
LP_aligned_FLU_performance_by_num_irrel = cell(1,3);

%divide by numD
FS_performance_by_num_irrel = cell(length(expected_numD),3);
FS_search_duration_by_num_irrel = cell(length(expected_numD),3);

for num_irrel = 1:3
    FLU_performance_by_num_irrel{num_irrel} = NaN(num_blocks,min_FLU_block_len+last_trial_window);
    FLU_search_duration_by_num_irrel{num_irrel} = NaN(num_blocks,min_FLU_block_len+last_trial_window);
    LP_by_num_irrel{num_irrel} = NaN(1,num_blocks);
    LP_aligned_FLU_performance_by_num_irrel{num_irrel} = NaN(num_blocks,2*forward_back_trials+1);

    for numD = 1:length(expected_numD)
        FS_performance_by_num_irrel{numD,num_irrel} = NaN(num_blocks,min_FS_block_len);
        FS_search_duration_by_num_irrel{numD,num_irrel} = NaN(num_blocks,min_FS_block_len);
    end
end

number_of_irrelevant_dimensions = ParseBlockDef4NumIrrel(session_data.block_def);
FLU_blocks = find(FLU_FS_blocks == 1);

if any( FLU_FS_blocks == -1)%has fam blocks
    FS_index_to_add = 2;
else
    FS_index_to_add = 1;%doesn't have fam blocks
end

for b = 1:length(FLU_blocks)
    this_FLU_block = FLU_blocks(b);
    blockind = find(block == this_FLU_block);
    if isempty(blockind)
        error('Why is this FLU block empty?')
    elseif length(blockind) < min_FLU_block_len
        error('Why is this FLU block too short?')
    end
    
    this_number_irrel = number_of_irrelevant_dimensions(this_FLU_block)+1;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%---Data for FLU Block---%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %---Get Performance---%
    blockrewarded = rewarded(blockind(1):blockind(end));
    blockrewarded = [blockrewarded(end-(last_trial_window-1):end); blockrewarded];
    FLU_performance_by_num_irrel{this_number_irrel}(b,:)= blockrewarded(1:min_FLU_block_len+last_trial_window);
    
    %---Get Search Duration---%
    blocksearchdur = search_duration(blockind(1):blockind(end));
    blocksearchdur = [blocksearchdur(end-(last_trial_window-1):end); blocksearchdur];
    FLU_search_duration_by_num_irrel{this_number_irrel}(b,:) = blocksearchdur(1:min_FLU_block_len+last_trial_window);
    
    %---Get Learning Point---%
    blockrewarded = rewarded(blockind(1):blockind(end));
    trials_in_block = length(blockrewarded);
    lp = FindLp(blockrewarded, 'slidingwindow', forward_smoothing_window, learning_point_threshold);
    if ~isempty(lp) && ~isnan(lp) && lp ~= length(blockrewarded)
        LP_by_num_irrel{this_number_irrel} = [LP_by_num_irrel{this_number_irrel} lp];
        
        if lp <= forward_back_trials
            start_index = forward_back_trials-lp+1;
            LP_aligned_FLU_performance_by_num_irrel{this_number_irrel}(b,start_index+1:end) = blockrewarded(1:lp+forward_back_trials);
        elseif lp >= trials_in_block-forward_back_trials
            end_index = (lp+forward_back_trials)-trials_in_block;
            LP_aligned_FLU_performance_by_num_irrel{this_number_irrel}(b,1:end-end_index) = blockrewarded(lp-forward_back_trials:end);
        else
            LP_aligned_FLU_performance_by_num_irrel{this_number_irrel}(b,:) = blockrewarded(lp-forward_back_trials:lp+forward_back_trials);
        end
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%---Data for FS Block---%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if this_FLU_block+FS_index_to_add > length(FLU_FS_blocks)
        continue
    elseif FLU_FS_blocks(this_FLU_block+FS_index_to_add) == 2
        this_FS_block = this_FLU_block+FS_index_to_add;
        FS_blockind = find(block == this_FS_block);
        if isempty(FS_blockind)
            error('Why is this FS block empty?')
        elseif length(FS_blockind) < min_FS_block_len
            error('Why is this FS block too short?')
        elseif length(FS_blockind) > min_FS_block_len+1%give 1 trial buffer
            error('Why are there so many trials in this FS block?')
        end
        
        FSblockrewarded = rewarded(FS_blockind(1):FS_blockind(end));
        FSblocksearchdur = search_duration(FS_blockind(1):FS_blockind(end));
        if length(FSblockrewarded) > min_FS_block_len-1
            selected_objects = session_data.trial_data.SelectedObjectID(FS_blockind(1):FS_blockind(end));
            emptys = find(cellfun(@isempty,selected_objects));
            FSblockrewarded(emptys) = [];
            FSblocksearchdur(emptys) = [];
            if length(FSblockrewarded) > min_FS_block_len
                error(['Why are there still more than ' num2str(min_FS_block_len) ' trials in FS block?'])
            end
        end
        for bk = 1:length(FSblockrewarded)
            if length(session_data.trial_def{FS_blockind(bk)}.relevantObjects) == 8
                %newere version with stimlui on concentric circle
                numD = 0;
                for stim = 1:8
                    if sum(session_data.trial_def{FS_blockind(bk)}.relevantObjects(stim).StimDimVals ~= 0) >= 1
                       numD = numD + 1;  
                    end
                end
                numD = numD - 1;%target doesn't count
            else
                %older version
                numD = length(session_data.trial_def{FS_blockind(bk)}.relevantObjects)-1;
            end
            numDindex = find(expected_numD == numD);
            if isempty(numDindex)
                error('Wrong # of Distractors?');
            else
                FS_performance_by_num_irrel{numDindex,this_number_irrel}(this_FS_block,bk) ...
                    = FSblockrewarded(bk);
                FS_search_duration_by_num_irrel{numDindex,this_number_irrel}(this_FS_block,bk) ...
                    = FSblocksearchdur(bk);
            end
        end
    end
end

%---remove excess NaNs---%
FLU_performance_by_num_irrel = laundry(FLU_performance_by_num_irrel);
FLU_search_duration_by_num_irrel = laundry(FLU_search_duration_by_num_irrel);
LP_by_num_irrel = laundry(LP_by_num_irrel);
LP_aligned_FLU_performance_by_num_irrel = laundry(LP_aligned_FLU_performance_by_num_irrel,1);
FS_performance_by_num_irrel = laundry(FS_performance_by_num_irrel,1);
FS_search_duration_by_num_irrel = laundry(FS_search_duration_by_num_irrel,1);
end