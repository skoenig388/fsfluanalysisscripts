function [block_performance_by_target_dimension,all_search_duration_by_target_dimension] = ...
    CalculateBlockPerformancebyDimension(session_data,goodblocks,min_block_len,...
    last_trial_window,plot_results)
%adapted from code else where, written 5/11/19

%Inputs:
% 1) session_data across all sessions
% 2) goodblocks: blocks with acceptable performance across all sessions
% 3) min_block_len: miniumum # of trials in a block
% 4) last_trial_window: # of trials to look at end of block

%Outputs:
% 1) block_performance: block transition-aligned performance by feature dimension
% 2) search_duration: search duration by feature dimension
% *) Figures with performance by feature dimension


if nargin < 2
    error('Not enough input arguments')
elseif nargin < 3
    min_block_len = 30;
    last_trial_window = 10;
    plot_results = true;
elseif nargin < 4
    last_trial_window = 10;
    plot_results = true;
elseif  nargin < 5
    plot_results = true;
end

num_sessions = length(session_data);
block_performance_by_target_dimension = cell(1,5);
all_search_duration_by_target_dimension = cell(1,5);
block_performance_by_target_dimension_and_distactor_dimension = cell(5,5);
all_search_duration_by_target_dimension_and_distactor_dimension = cell(5,5);

for sess = 1:num_sessions
    
    num_blocks = length(goodblocks{sess});
    
    block = session_data{sess}.trial_data.Block;
    block = block + 1;%index starts at 0 in C++
    rewarded = strcmpi(session_data{sess}.trial_data.isHighestProbReward,'true');
    search_duration = Calculate_FLU_Search_Duration(session_data{sess});
    
    block_performance = NaN(num_blocks,min_block_len+last_trial_window);
    block_search_duration =  NaN(num_blocks,min_block_len+last_trial_window);
    target_dimensions = NaN(num_blocks,1);
    distractor_dimensions = NaN(num_blocks,2);
    
    
    for b = 1:num_blocks
        blockind = find(block == goodblocks{sess}(b));
        if isempty(blockind)
            error('why is block empty?')
        elseif length(blockind) < min_block_len %didn't finish
            error('why is block too short?')
        end
        
        %---Get Target and Distractor Dims---%
        [ target_dimensions(b),~] = DetermineTargetDimension(session_data{sess},goodblocks{sess}(b));
        
        distractor_dims = find(session_data{sess}.block_def(goodblocks{sess}(b)).TrialDefs(1).relevantObjects(1).StimDimVals ~= 0);
        distractor_dims(distractor_dims == target_dimensions(b)) = [];
        distractor_dimensions(b,1:length(distractor_dims)) = distractor_dims;
        
        %---Get Performance---%
        blockrewarded = rewarded(blockind(1):blockind(end));
        blockrewarded = [blockrewarded(end-(last_trial_window-1):end); blockrewarded];
        if length(blockrewarded) > min_block_len+last_trial_window
            block_performance(b,1:min_block_len+last_trial_window) = blockrewarded(1:min_block_len+last_trial_window);
        else
            block_performance(b,1:length(blockrewarded)) = blockrewarded;
        end
        
        %---Get Search Duration---%
        blocksearchdur = search_duration(blockind(1):blockind(end));
        blocksearchdur = [blocksearchdur(end-(last_trial_window-1):end); blocksearchdur];
        block_search_duration(b,1:min_block_len+last_trial_window) = blocksearchdur(1:min_block_len+last_trial_window);
        
        %---Get Search Duration based on performance---%
        blockrewarded = rewarded(blockind(1):blockind(end));
        blockrewarded = [blockrewarded(end-(last_trial_window-1):end); blockrewarded];
        block_performance(b,1:min_block_len+last_trial_window) = blockrewarded(1:min_block_len+last_trial_window);
    end
    
    %---Consolodiate by Distractor and Target Dimensions---%
    for target_dim = 1:5
        these_target_blocks = find(target_dimensions == target_dim);
        block_performance_by_target_dimension{target_dim} = ...
            [block_performance_by_target_dimension{target_dim}; block_performance(these_target_blocks,:)];
        all_search_duration_by_target_dimension{target_dim} = ...
            [all_search_duration_by_target_dimension{target_dim}; block_search_duration(these_target_blocks,:)];
        for distractor_dim = 1:5
            if distractor_dim ~= target_dim
                these_target_distractor_blocks = find(target_dimensions == target_dim ...
                    & (distractor_dimensions(:,1) == distractor_dim | ...
                    distractor_dimensions(:,2) == distractor_dim));%take either so sort of doubling
                
                block_performance_by_target_dimension_and_distactor_dimension{target_dim,distractor_dim} = ...
                    [block_performance_by_target_dimension_and_distactor_dimension{target_dim,distractor_dim}; ...
                    block_performance(these_target_distractor_blocks,:)];
                
                all_search_duration_by_target_dimension_and_distactor_dimension{target_dim,distractor_dim} = ...
                    [all_search_duration_by_target_dimension_and_distactor_dimension{target_dim,distractor_dim}; ...
                    block_search_duration(these_target_distractor_blocks,:)];
            end
        end
    end
end

%---Plot Results by Target Dimension---%
if plot_results
    dimension_names = {'Shape','Pattern','Color','Texture','Arms'};
    target_legend_str = [];
    
    figure
    subplot(1,2,1)
    hold on
    plot([0.5 0.5],[0 100],'k--')
    plot([-10 min_block_len],[80 80],'k--')
    plot([-10 min_block_len],[33 33],'k--')
    plot([-10 min_block_len],[66 66],'k--')
    empty_dims = [];
    for target_dim = 1:5
        if ~isempty(block_performance_by_target_dimension{target_dim})
            pdim(target_dim) = plot(-(last_trial_window-1):min_block_len,100*FilterLearningCurves(nanmean(block_performance_by_target_dimension{target_dim})',3,2));
            target_legend_str = [target_legend_str {[dimension_names{target_dim} ', n = ' num2str(size(block_performance_by_target_dimension{target_dim},1))]}];
        else
            empty_dims = [empty_dims target_dim];
        end
    end
    pdim(target_dim+1) = plot(-(last_trial_window-1):min_block_len,100*nanmean(cell2mat(block_performance_by_target_dimension')),'k','linewidth',4);
    target_legend_str = [target_legend_str {['All, n = ' num2str(size(cell2mat(block_performance_by_target_dimension'),1))]}];
    hold off
    pdim(empty_dims) = [];
    legend(pdim,target_legend_str,'location','Southeast')
    ylim([0 100])
    xlabel('Trial # Relative To Switch')
    ylabel('Performance %')
    box off
    title('Average Performance by Target Dimension')
    
    subplot(1,2,2)
    hold on
    for target_dim = 1:5
        if ~isempty(all_search_duration_by_target_dimension{target_dim})
            plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_search_duration_by_target_dimension{target_dim})',3,2));
        end
    end
    plot(-(last_trial_window-1):min_block_len,nanmean(cell2mat(all_search_duration_by_target_dimension')),'k','linewidth',4);
    yl = ylim;
    plot([0.5 0.5],[yl(1) yl(2)],'k--')
    hold off
    xlabel('Trial # Relative To Switch')
    ylabel('Search Duration (sec)')
    box off
    title('Average Search Duration by Target Dimension')
    
    %---Plot Results by Target & Distractor Dimensions---%
    empty_dims = cellfun(@isempty,block_performance_by_target_dimension_and_distactor_dimension);
    if ~all(empty_dims(:))
        colorlines = viridis(25);
        target_distractor_legend_str = [];
        figure
        subplot(1,2,1)
        hold on
        plot([0.5 0.5],[0 100],'k--')
        plot([-10 min_block_len],[80 80],'k--')
        plot([-10 min_block_len],[33 33],'k--')
        plot([-10 min_block_len],[66 66],'k--')
        empty_dims = [];
        iter = 1;
        pddim = [];
        for target_dim = 1:5
            for distractor_dim = 1:5
                if size(block_performance_by_target_dimension_and_distactor_dimension{target_dim,distractor_dim},1) > 1
                    pddim(iter) = plot(-(last_trial_window-1):min_block_len,100*FilterLearningCurves(...
                        nanmean(block_performance_by_target_dimension_and_distactor_dimension{target_dim,distractor_dim})',3,2),'Color',colorlines(iter,:));
                    target_distractor_legend_str = [target_distractor_legend_str {['T: ' dimension_names{target_dim} ....
                        ' & D: ' dimension_names{distractor_dim} ', n = ' ...
                        num2str(size(block_performance_by_target_dimension_and_distactor_dimension{target_dim,distractor_dim},1))]}];
                else
                    empty_dims = [empty_dims iter];
                end
                iter = iter+1;
            end
        end
        hold off
        empty_dims(empty_dims > length(pddim)) = [];%if all of last plots are empty empty_dims will be longer than pddim
        pddim(empty_dims) = [];
        legend(pddim,target_distractor_legend_str,'location','Southeast')
        ylim([0 100])
        xlabel('Trial # Relative To Switch')
        ylabel('Performance %')
        box off
        title('Average Performance by Target & Distractor Dimensions')
        
        subplot(1,2,2)
        hold on
        iter = 1;
        for target_dim = 1:5
            for distractor_dim = 1:5
                if size(all_search_duration_by_target_dimension_and_distactor_dimension{target_dim,distractor_dim},1) > 1
                    plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(...
                        nanmean(all_search_duration_by_target_dimension_and_distactor_dimension{target_dim,distractor_dim})',3,2),'Color',colorlines(iter,:));
                end
                iter = iter+1;
            end
        end
        yl = ylim;
        plot([0.5 0.5],[yl(1) yl(2)],'k--')
        hold off
        xlabel('Trial # Relative To Switch')
        ylabel('Search Duration (sec)')
        box off
        title('Average Search Duration by Target & Distractor Dimensions')
    end
end
end