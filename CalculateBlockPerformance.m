function [block_performance,trial_count,extra_trial_count,block_end_performance,block_performance_prob] = ...
    CalculateBlockPerformance(session_data,goodblocks,min_block_len,last_trial_window)
%adapted from code else where, written 5/8/19

%Inputs:
% 1) session_data: all the session data
% 2) goodblocks: blocks with acceptable performance
% 3) min_block_len: miniumum # of trials in a block
% 4) last_trial_window: # of trials to look at end of block

%Outputs:
% 1) block_performance: block transition-aligned performance
% 2) trial_count: trials per block
% 3) extra_trial_count: # of extra trials per block compared to minium
% 4) block_end_performance, performance at end of block
% 5) block_performance_prob: if reward is probabilistic want to know
% performance on selecting correct even if not rewarded

if nargin < 2
    error('Not enough input arguments')
elseif nargin < 3
    min_block_len = 30;
    last_trial_window = 10;
elseif nargin < 4
    last_trial_window = 10;
end

num_blocks = length(goodblocks);
block_performance = NaN(num_blocks,min_block_len+last_trial_window);
block_performance_prob = NaN(num_blocks,min_block_len+last_trial_window);
trial_count = NaN(1,num_blocks);
extra_trial_count = NaN(1,num_blocks);
block_end_performance = NaN(1,num_blocks);

block = session_data.trial_data.Block;
block = block + 1;%index starts at 0 in C++
rewarded = strcmpi(session_data.trial_data.isRewarded,'true');
selected_rewarded = strcmpi(session_data.trial_data.isHighestProbReward,'true');%want both for plotting

for b = 1:num_blocks
    blockind = find(block == goodblocks(b));
    if isempty(blockind)
        error('why is block empty?')
    elseif length(blockind) < min_block_len%didn't finish
        error('why is block too short?')
    end
    
    %---Get Trial Counts---%
    if length(blockind) >= min_block_len && b ~= num_blocks %so there's a next block...most likely finsihed it
        trial_count(b) = length(blockind);
        extra_trial_count(b) = length(blockind)-session_data.block_def(goodblocks(b)).TrialRange(1);
    elseif b == num_blocks
        blockrewarded = rewarded(blockind(end-(last_trial_window-1)):blockind(end));
        if (sum(blockrewarded) >= 8 && length(blockind) >= session_data.block_def(goodblocks(b)).TrialRange(1))...
                || length(blockind) >= session_data.block_def(goodblocks(b)).TrialRange(2)
            trial_count(b) = length(blockind);
            extra_trial_count(b) = length(blockind)-session_data.block_def(goodblocks(b)).TrialRange(1);
        end
        %otherwise don't know if block was terminated properly or monkey just stopped working
    end

    %---Get Performance---%
    blockrewarded = rewarded(blockind(1):blockind(end));
    blockrewarded = [blockrewarded(end-(last_trial_window-1):end); blockrewarded];
    block_end_performance(b) = mean(blockrewarded(1:min_block_len));
    if length(blockrewarded) > min_block_len+last_trial_window
        block_performance(b,1:min_block_len+last_trial_window) = blockrewarded(1:min_block_len+last_trial_window);
    else
        block_performance(b,1:length(blockrewarded)) = blockrewarded;
    end
    if session_data.block_def(goodblocks(b)).RuleArray.RewardProb < 1 %probabilistic reward
        blockrewarded_prob = selected_rewarded(blockind(1):blockind(end));
        blockrewarded_prob = [blockrewarded_prob(end-(last_trial_window-1):end); blockrewarded_prob];
        if length(blockrewarded_prob) > min_block_len+last_trial_window
            block_performance_prob(b,1:min_block_len+last_trial_window) = blockrewarded_prob(1:min_block_len+last_trial_window);
        else
            block_performance_prob(b,1:length(blockrewarded_prob)) = blockrewarded_prob;
        end
    end
end

end