%Simple import data to get RTs and Performance


clear,clc,close all%fclose all




trial_name = '__TrialData.txt';
trialdef_name = '__trialdef_on_trial_';
block_name = '__block_defs.json';

monkeyname = 'Wotan';
log_dir = 'Z:\MonkeyData\Wotan\';
sess_file_names = {'FLU__Wotan__30_10_2018__12_26_15','FLU__Wotan__31_10_2018__12_56_17',...
    'FLU__Wotan__01_11_2018__13_00_01','FLU__Wotan__02_11_2018__13_29_23','FLU__Wotan__03_11_2018__13_12_22'};

% monkeyname = 'Frey';
% log_dir = 'Z:\MonkeyData\Frey\';
% sess_file_names = {'FLU__Frey__27_10_2018__12_13_57','FLU__Frey__29_10_2018__11_25_38',...
%     'FLU__Frey__30_10_2018__11_42_22','FLU__Frey__31_10_2018__12_11_10','FLU__Frey__01_11_2018__12_06_45'};

all_block_performance = NaN(10*length(sess_file_names),50);
all_block_search_duration = NaN(10*length(sess_file_names),50);
all_block_ind = 1;
for sess = 1:length(sess_file_names)
    
    trial_dir = [log_dir sess_file_names{sess} '\RuntimeData\TrialData\'];
    config_dir = [log_dir sess_file_names{sess} '\RuntimeData\ConfigCopy\'];
    trial_data = readtable([trial_dir sess_file_names{sess} trial_name]);
    block_def = ReadJsonFile([config_dir sess_file_names{sess} block_name]);
    
    search_duration = trial_data.Epoch4_Duration;
    rewarded = strcmpi(trial_data.isRewarded,'true');
    
    block = trial_data.Block;
    block = block + 1;%index starts at 0 in C++
    
    for b =2:2:max(block)
        blockind = find(block == b);
        if length(blockind) < 15
            continue
        end
        blockrewarded = rewarded(blockind(1)-10:blockind(end));
        blocksearchdur = search_duration(blockind(1)-10:blockind(end));
        
        if length(blockrewarded) > 50
            all_block_performance(all_block_ind,1:50) = blockrewarded(1:50);
            all_block_search_duration(all_block_ind,1:50) = blocksearchdur(1:50);
        else
            all_block_performance(all_block_ind,1:length(blockrewarded)) = blockrewarded;
            all_block_search_duration(all_block_ind,1:length(blocksearchdur)) = blocksearchdur;
        end
        
        all_block_ind = all_block_ind + 1;
    end
    
end
%% Remove extra NaNs
all_block_performance = laundry(all_block_performance);
all_block_search_duration = laundry(all_block_search_duration);
%%  Calculate Average Performance and Search Duration
total_num_blocks = size(all_block_performance,1);
num_not_nans = sum(~isnan(all_block_performance));

average_performance = nansum(all_block_performance)./num_not_nans;
average_search_duration = nansum(all_block_search_duration)./num_not_nans;
%% Plot Results

figure
subplot(1,2,1)
plot(-9:40,100*average_performance)
hold on
plot([0.5 0.5],[0 100],'k--')
plot([-10 40],[80 80],'r--')
hold off
xlabel('Trial # Relative To Switch')
ylabel('Performance %')
box off
title('Average Performance')

subplot(1,2,2)
plot(-9:40,average_search_duration)
yl = ylim;
hold on
plot([0.5 0.5],[yl(1) yl(2)],'k--')
hold off
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (seconds)')
box off
title('Average Search Duration')

subtitle(['Reversal Learning-' monkeyname ': N = ' num2str(total_num_blocks) ' learning blocks'])
