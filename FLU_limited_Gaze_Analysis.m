%Cut of from the Standard FLU_Limited_Anlaysis on 5/10/19 to focus on Gaze
%Data analyses seperately

tic
clar
num_shuffs = 1000;
import_option = 'all';

%average acceptable performance across blocks
miniumum_performance = 0.5;%set to 0 if you don't want to remove blocks
max_block_cut = 3;%of of 1st and last blocks to cut if performance is very bad
min_block_len = 30;%miniumum # of trials in a block
last_trial_window = 10;%window to look at before end of block

%%%%%%%%%%%%%%%%%
%%%---Wotan---%%%
%%%%%%%%%%%%%%%%%
%---Eye tracking w/ 0 irrelevant features---%
% monkeyname = 'Wotan';
% sess_file_names = {'CFS__Wotan__20_03_2019__12_46_17',...
%     'CFS__Wotan__21_03_2019__13_42_40','FLU__Wotan__22_03_2019__12_51_44',...
%     'FLU__Wotan__23_03_2019__15_57_44','FLU__Wotan__25_03_2019__12_20_35',...
%     'FLU__Wotan__26_03_2019__12_22_36','FLU__Wotan__27_03_2019__12_44_25'};

%---Eye tracking w/ 1 irrelevant feature---%
% monkeyname = 'Wotan';
% sess_file_names = {'FLU__Wotan__28_03_2019__12_38_16','FLU__Wotan__01_04_2019__13_18_43',...
%     'FLU__Wotan__03_04_2019__13_00_04',...
%     'FLU__Wotan__07_04_2019__12_33_56','FLU__Wotan__08_04_2019__13_05_27',...
%     'FLU__Wotan__10_04_2019__12_47_14','FLU__Wotan__11_04_2019__13_18_53',...
%     'FLU__Wotan__12_04_2019__12_57_02','FLU__Wotan__13_04_2019__13_26_54',...
%     };

%---Eye tracking w/ 1-2 irrelevant feature---%
% monkeyname = 'Wotan';
% sess_file_names = {'FLU__Wotan__16_04_2019__12_07_05','FLU__Wotan__18_04_2019__12_40_04',...
%     'FLU__Wotan__20_04_2019__13_55_27','FLU__Wotan__22_04_2019__13_54_04',...
%     'FLU__Wotan__23_04_2019__12_49_33','FLU__Wotan__25_04_2019__12_27_30',...
%     'FLU__Wotan__26_04_2019__12_55_02','FLU__Wotan__27_04_2019__13_32_07',...
%     'FLU__Wotan__29_04_2019__13_05_45','FLU__Wotan__30_04_2019__12_27_22',...
%     'FLU__Wotan__03_05_2019__12_55_04','FLU__Wotan__04_05_2019__13_53_06',...
% };

%---Eye tracking w/ 2 irrelevant feature---%
% monkeyname = 'Wotan';
% sess_file_names = {'FLU__Wotan__06_05_2019__12_35_51','FLU__Wotan__07_05_2019__13_22_29',...
%     'FLU__Wotan__08_05_2019__12_59_05','FLU__Wotan__09_05_2019__12_57_41',...
%     'FLU__Wotan__10_05_2019__13_00_14',...
%     };


% %---Eye tracking w/0,1, and 2 irrelevant features---%
% monkeyname = 'Wotan';
% sess_file_names = {'CFS__Wotan__20_03_2019__12_46_17',...
%     'CFS__Wotan__21_03_2019__13_42_40','FLU__Wotan__22_03_2019__12_51_44',...
%     'FLU__Wotan__23_03_2019__15_57_44','FLU__Wotan__25_03_2019__12_20_35',...
%     'FLU__Wotan__26_03_2019__12_22_36','FLU__Wotan__27_03_2019__12_44_25',...
%     'FLU__Wotan__28_03_2019__12_38_16','FLU__Wotan__01_04_2019__13_18_43',...
%     'FLU__Wotan__03_04_2019__13_00_04',...
%     'FLU__Wotan__07_04_2019__12_33_56','FLU__Wotan__08_04_2019__13_05_27',...
%     'FLU__Wotan__10_04_2019__12_47_14','FLU__Wotan__11_04_2019__13_18_53',...
%     'FLU__Wotan__12_04_2019__12_57_02','FLU__Wotan__13_04_2019__13_26_54',...
%     'FLU__Wotan__16_04_2019__12_07_05','FLU__Wotan__17_04_2019__11_59_02',...
%     'FLU__Wotan__18_04_2019__12_40_04','FS__Wotan__19_04_2019__12_31_28',...
%     'FLU__Wotan__20_04_2019__13_55_27','FLU__Wotan__22_04_2019__13_54_04',...
%     'FLU__Wotan__23_04_2019__12_49_33','FLU__Wotan__25_04_2019__12_27_30',...
%     'FLU__Wotan__26_04_2019__12_55_02','FLU__Wotan__27_04_2019__13_32_07',...
%     };

%% Import Session Data
num_sessions = length(sess_file_names);
session_data = cell(1,num_sessions);
which_monkey = NaN(1,num_sessions);
for sess =1:num_sessions
    which_monkey(sess) = DetermineWhichMonkey(sess_file_names{sess});
    
    disp(['Loading data from session #' num2str(sess) '/' num2str(length(sess_file_names))])
    log_dir = FindDataDirectory(sess_file_names{sess});
    if ~isempty(log_dir)
        session_data{sess} = ImportFLU_DataV2(sess_file_names{sess},log_dir,import_option);
    else
        error(['Cant Find Log Dir for '  sess_file_names{sess}])
    end
end

%% Calculate Averege Performance by Block and Remove "Bad" Blocks
all_good_blocks = cell(1,num_sessions);
all_block_performance = cell(1,num_sessions);
all_session_block_performance = NaN(num_sessions,25);
for sess = 1:num_sessions
    [all_good_blocks{sess},all_block_performance{sess}] = ...
        RemoveBadBlocks(session_data{sess},miniumum_performance,max_block_cut,min_block_len);
    all_session_block_performance(sess,1:length(all_block_performance{sess})) = ...
        all_block_performance{sess};
end

figure
subplot(1,2,1)
plot(nanmean(all_session_block_performance))
xlabel('Block #')
ylabel('Performance-Proportion Correct')
title('Average Performance by Block')
box off

subplot(1,2,2)
hist(all_session_block_performance(:),20)
xlabel('Performance-Proportion Correct')
ylabel('Block Count')
title('Distribution of Block Performance')
box off

subtitle([monkeyname ': Performance by Block #'])
box off
%% Calculate perofrmance across all blocks
all_block_performance = [];
all_trial_count = NaN(length(sess_file_names),25);
all_extra_trial_count = NaN(length(sess_file_names),25);
all_block_end_performance = NaN(length(sess_file_names),25);

for sess = 1:num_sessions
    [block_performance,trial_count,extra_trial_count,block_end_performance] = ...
        CalculateBlockPerformance(session_data{sess},all_good_blocks{sess},min_block_len,last_trial_window);
    
    all_block_performance = [all_block_performance; block_performance];
    all_trial_count(sess,1:length(trial_count)) = trial_count;
    all_extra_trial_count(sess,1:length(extra_trial_count)) = extra_trial_count;
    all_block_end_performance(sess,1:length(block_end_performance)) = block_end_performance;
end

figure
subplot(2,2,1)
hold on
plot([0.5 0.5],[0 100],'k--')
plot([-10 30],[80 80],'k--')
plot([-10 30],[33 33],'k--')
plot([-10 30],[66 66],'k--')
plot(-9:30,FilterLearningCurves(100*nanmean(all_block_performance)',3,2),'r')
plot(-9:30,100*nanmean(all_block_performance))
hold off
ylim([0 100])
xlabel('Trial # Relative To Switch')
ylabel('Performance (% Correct)')
box off
title(sprintf(['Average Performance-All blocks \n' ...
    'Average_{20-30} = ' num2str(100*mean(nanmean(all_block_performance(:,end-9:end))),3) '%%, ' ...
    'Average_{Last 10} = ' num2str(100*mean(nanmean(all_block_performance(:,1:10))),3) '%%']));

subplot(2,2,2)
histogram(all_trial_count(:),30:60)
xlabel('Trials in Block')
ylabel('Block Count')
title('Total Trial Count per Block')
box off

subplot(2,2,3)
histogram(all_extra_trial_count(:),0:30)
xlabel('Extra Trials in Block')
ylabel('Block Count')
title('Extra Trial Count per block')
box off

subplot(2,2,4)
histogram(100*all_block_end_performance(:),-5:10:95)
xlabel('Performance (% Correct)')
ylabel('Block Count')
box off
title('Peformance at End of Block')

subtitle(monkeyname)

%% Calculate Search Duration/Response Time Measurements for All Blocks
all_search_duration = [];
all_correct_search_duration = [];
all_incorrect_search_duration = [];
all_incorrect_last_trial_search_duration = [];
all_correct_last_trial_search_duration = [];

all_correct_n1_incorrect_search_duration = [];
all_correct_n1_correct_search_duration = [];
all_incorrect_n1_incorrect_search_duration = [];
all_incorrect_n1_correct_search_duration = [];

for sess = 1:num_sessions
    [block_search_duration,block_correct_search_duration,block_incorrect_search_duration,...
        block_incorrect_last_trial,block_correct_last_trial,...
        correct_incorrect_minus_one_search_duration,correct_correct_minus_one_search_duration,...
        incorrect_incorrect_minus_one_search_duration,incorrect_correct_minus_one_search_duration] = ...
        CalculateBlockSearchDuration(session_data{sess},all_good_blocks{sess},min_block_len,last_trial_window);
    
    all_search_duration = [all_search_duration; block_search_duration];
    all_correct_search_duration = [all_correct_search_duration; block_correct_search_duration];
    all_incorrect_search_duration = [all_incorrect_search_duration; block_incorrect_search_duration];
    all_incorrect_last_trial_search_duration = [all_incorrect_last_trial_search_duration; block_incorrect_last_trial];
    all_correct_last_trial_search_duration = [all_correct_last_trial_search_duration; block_correct_last_trial];
    
    all_correct_n1_incorrect_search_duration = [all_correct_n1_incorrect_search_duration; correct_incorrect_minus_one_search_duration];
    all_correct_n1_correct_search_duration = [all_correct_n1_correct_search_duration; correct_correct_minus_one_search_duration];
    all_incorrect_n1_incorrect_search_duration = [all_incorrect_n1_incorrect_search_duration; incorrect_incorrect_minus_one_search_duration];
    all_incorrect_n1_correct_search_duration = [all_incorrect_n1_correct_search_duration; incorrect_correct_minus_one_search_duration];
end

figure
subplot(2,2,1)
hold on
plot(-9:30,FilterLearningCurves(nanmean(all_search_duration)',3,2),'r')
plot(-9:30,nanmean(all_search_duration))
yl = ylim;
plot([0.5 0.5],[yl(1) yl(2)],'k--')
hold off
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (sec)')
box off
title(sprintf(['Average Search Duration-All blocks \n' ...
    'Average_{20-30} = ' num2str(mean2(all_search_duration(:,end-9:end)),3) ' sec, ' ...
    'Average_{Last 10} = ' num2str(mean2(all_search_duration(:,1:10)),3) ' sec']))


subplot(2,2,2)
hold on
plot(-9:30,FilterLearningCurves(nanmean(all_search_duration)',3,2),'k')
plot(-9:30,FilterLearningCurves(nanmean(all_correct_search_duration)',3,2),'g')
plot(-9:30,FilterLearningCurves(nanmean(all_incorrect_search_duration)',3,2),'r')
plot(-9:30,FilterLearningCurves(nanmean(all_incorrect_last_trial_search_duration)',3,2),'b')
plot(-9:30,FilterLearningCurves(nanmean(all_correct_last_trial_search_duration)',3,2),'m')
yl = ylim;
plot([0.5 0.5],[yl(1) yl(2)],'k--')
hold off
legend('All','Correct','Incorrect','Incorrect Last Trial','Correct Last Trial')
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (sec)')
box off
title('Search Duration by Performance')

subplot(2,2,3)
hold on
plot(-9:30,FilterLearningCurves(nanmean(all_search_duration)',3,2),'k')
plot(-9:30,FilterLearningCurves(nanmean(all_correct_n1_correct_search_duration)',3,2),'g')
plot(-9:30,FilterLearningCurves(nanmean(all_correct_n1_incorrect_search_duration)',3,2),'b')
plot(-9:30,FilterLearningCurves(nanmean(all_incorrect_n1_correct_search_duration)',3,2),'m')
plot(-9:30,FilterLearningCurves(nanmean(all_incorrect_n1_incorrect_search_duration)',3,2),'r')
yl = ylim;
plot([0.5 0.5],[yl(1) yl(2)],'k--')
hold off
legend('All','Correct & N-1 Correct','Correct & N-1 Incorrect','Incorrect & N-1 Correct','Incorrect & N-1 Incorrect')
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (sec)')
box off
title('Search Duration by Performance')

subplot(2,2,4)
bar([nanmean(all_search_duration(:)),...
    nanmean(all_correct_search_duration(:)),nanmean(all_correct_n1_correct_search_duration(:)),nanmean(all_correct_n1_incorrect_search_duration(:)),...
    nanmean(all_incorrect_search_duration(:)),nanmean(all_incorrect_n1_correct_search_duration(:)),nanmean(all_incorrect_n1_incorrect_search_duration(:))])
xticks(1:7)
xticklabels({'All',...
    'All Correct','Correct & N-1 Correct','Correct & N-1 InCorrect',...
    'All Incorrect','Incorrect & N-1 Correct','Incorrect & N-1 Incorrect'})
xtickangle(30)
ylim(yl)
ylabel('Search Duration (sec)')
box off
title('Search Duration by Performance Across All Trials')

subtitle(monkeyname)

%% Eye tracking data analysis if collected eye data

trial_aligned_Python_eye_data = cell(1,num_sessions);
for sess = 1:num_sessions
    if ~isempty(session_data{sess}.gaze_data)
        trial_aligned_Python_eye_data{sess} = GetTrialAlignedPythonGazeData(session_data{sess});
    end
end
%%
target_looking_time = cell(1,num_sessions);
looking_at_highest_rewarded_item =  cell(1,num_sessions);
looking_intra_extra = cell(1,num_sessions);
for sess = 1:num_sessions
    if  ~isempty(session_data{sess}.gaze_data)
        
        [pixelsize,cmsize] = ReadMonitordetails(session_data{sess}.configdata.monitorDetails);
        screenX = pixelsize(1);
        screenY = pixelsize(2);
        
        
        block = session_data{sess}.trial_data.Block;
        block = block + 1;%index starts at 0 in C++
        
        target_looking_time{sess} = NaN(max(block),40);
        search_durs{sess} = NaN(max(block),40);
        looking_at_highest_rewarded_item{sess} = NaN(max(block),40);
        looking_intra_extra{sess} = NaN(max(block),1);
        
        all_block_ind = 1;
        
        
        for b =2:max(block)
            if contains(session_data{sess}.block_def(b).BlockID,'Intra')
                intra_extra = 1;
            elseif contains(session_data{sess}.block_def(b).BlockID,'Extra')
                intra_extra = 2;
            else
                error('blockID not recongized')
            end
            
            blockind = find(block == b);
            if length(blockind) < 15
                continue
            elseif length(blockind) > 30
                blockind = blockind(1:30);
            end
            blockind = [blockind(end-9:end); blockind];
            
            trial_nums = session_data{sess}.trial_data.TrialInExperiment(blockind);
            if length(blockind) > 40
                disp('Now')
            end
            for t = 1:length(trial_nums)
                
                select_rad = session_data{sess}.trial_data.ObjectselectionRadius(trial_nums(t));
                
                num_objs = length(session_data{sess}.trial_def{t}.relevantObjects);
                object_locations = NaN(2,num_objs);
                for o = 1:num_objs
                    object_locations(1,o) = round(session_data{sess}.trial_def{trial_nums(t)}.relevantObjects(o).StimScreenLocation.x);
                    object_locations(2,o) = round(session_data{sess}.trial_def{trial_nums(t)}.relevantObjects(o).StimScreenLocation.y);
                end
                
                stim_array = -1*ones(screenY,screenX);
                [yy,xx] = find(stim_array == -1);
                xyind = find(stim_array == -1);
                for o = 1:num_objs
                    rind = find(sqrt((object_locations(1,o)-xx).^2+(object_locations(2,o)-yy).^2) <= select_rad);
                    if o == 1
                        stim_array(xyind(rind)) = 11; %%make it more obviuos which is the target for
                    else
                        stim_array(xyind(rind)) = o;
                    end
                end
                
                frame_trial_ind = find(session_data{sess}.frame_data.TrialInExperiment == trial_nums(t));
                frame_events = session_data{sess}.frame_data.TrialEpoch(frame_trial_ind);
                
                
                frame_times = session_data{sess}.frame_data.FrameStartUnity(frame_trial_ind);
                frame_times_ET = session_data{sess}.frame_data.EyetrackerTimeStamp(frame_trial_ind);
                
                search_ind = find(frame_events == 5);
                if isempty(search_ind)
                    continue
                end
                
                search_start_ind = search_ind(1)-5;
                
                search_gaps = findgaps(search_ind);
                search_end_time_ind = search_gaps(end,1);
                
                eyetime = trial_aligned_Python_eye_data{sess}{trial_nums(t)}(:,2);
                if isempty(eyetime)
                    continue
                end
                
                
                t1 = find(eyetime <= frame_times_ET(search_start_ind));
                t1 = t1(end);
                t2 = find(eyetime > frame_times_ET(search_end_time_ind));
                if isempty(t2)
                    t2 = length(eyetime);
                end
                t2 = t2(1);
                
                %Left eye
                eyex = round(screenX*trial_aligned_Python_eye_data{sess}{trial_nums(t)}(t1:t2,5));
                eyey = round(screenY*(1-trial_aligned_Python_eye_data{sess}{trial_nums(t)}(t1:t2,6)));
                [eyex,eyey] = cleanEye(eyex,eyey,screenX,screenY,1);
                
                if isempty(eyex)
                    %try right eye
                    eyex = trial_aligned_Python_eye_data{sess}{trial_nums(t)}(t1:t2,7);
                    eyey = trial_aligned_Python_eye_data{sess}{trial_nums(t)}(t1:t2,8);
                    [eyex,eyey] = cleanEye(eyex,eyey,screenX,screenY,1);
                    if isempty(eyex)
                        disp(['Session #' num2str(sess) ', where is the eye data for trial ' num2str(t) '?'])
                        continue
                    end
                end
                
                stim_array_ind = sub2ind([screenY screenX],eyey,eyex);
                looking_time = stim_array(stim_array_ind);
                cumulative_looking_time = zeros(1,4);
                cumulative_looking_time(1) = sum(looking_time == -1);
                cumulative_looking_time(2) = sum(looking_time == 11);
                cumulative_looking_time(3) = sum(looking_time == 2);
                cumulative_looking_time(4) = sum(looking_time == 3);
                
                target_looking_time{sess}(all_block_ind,t) = cumulative_looking_time(2)/sum(cumulative_looking_time(2:end));
                search_durs{sess}(all_block_ind,t) = frame_times(search_end_time_ind)-frame_times(search_start_ind);
                
                looking_time_gaps = findgaps(looking_time);
                highest_reward = NaN;
                for gp = 1:size(looking_time_gaps)
                    current_gap = looking_time_gaps(gp,:);
                    current_gap(current_gap == 0) = [];
                    if all(current_gap == -1) %then looking at background
                        continue
                    else
                        if length(current_gap) > 6
                            if all(current_gap == 11)
                                highest_reward = 1;
                                break
                            else
                                highest_reward = 0;
                                break
                            end
                        else
                            continue
                        end
                    end
                end
                looking_at_highest_rewarded_item{sess}(all_block_ind,t) = highest_reward;
            end
            looking_intra_extra{sess}(all_block_ind) = intra_extra;
            
            
            all_block_ind = all_block_ind + 1;
        end
    end
end

%% Combine data across sets

all_target_looking_time = [];
all_looking_at_highest_rewarded_item = [];

intra_target_looking_time = [];
intra_looking_at_highest_rewarded_item = [];

extra_target_looking_time = [];
extra_looking_at_highest_rewarded_item = [];

for sess = 1:num_sessions
    if  ~isempty(session_data{sess}.gaze_data)
        all_target_looking_time = [all_target_looking_time; target_looking_time{sess}];
        all_looking_at_highest_rewarded_item = [all_looking_at_highest_rewarded_item; looking_at_highest_rewarded_item{sess}];
        
        intra_target_looking_time = [intra_target_looking_time; target_looking_time{sess}(looking_intra_extra{sess} == 1,:)];
        intra_looking_at_highest_rewarded_item = [intra_looking_at_highest_rewarded_item; looking_at_highest_rewarded_item{sess}(looking_intra_extra{sess} == 1,:)];
        
        extra_target_looking_time = [extra_target_looking_time; target_looking_time{sess}(looking_intra_extra{sess} == 2,:)];
        extra_looking_at_highest_rewarded_item = [extra_looking_at_highest_rewarded_item; looking_at_highest_rewarded_item{sess}(looking_intra_extra{sess} == 2,:)];
    end
end
%%
if ~isempty(all_target_looking_time)
    figure
    subplot(2,2,1)
    hold on
    plot(-9:30,100*average_performance);
    plot(-9:30,100*nanmean(all_target_looking_time));
    plot(-9:30,100*nanmean(all_looking_at_highest_rewarded_item));
    plot([0.5 0.5],[0 100],'k--')
    plot([-9 30],[80 80],'k--')
    plot([-9 30],[33 33 ],'k--')
    hold off
    xlabel('Trial # Relative To Switch')
    ylabel('Percent (%)')
    box off
    title('Raw Data All Blocks')
    
    legend({'Performance','Looking Time @ Target','1st Saccade to Target'},'Location','SouthEast')
    
    subplot(2,2,2)
    hold on
    plot(-9:30,FilterLearningCurves(100*average_performance',3,2));
    plot(-9:30,FilterLearningCurves(100*nanmean(all_target_looking_time)',3,2));
    plot(-9:30,FilterLearningCurves(100*nanmean(all_looking_at_highest_rewarded_item)',3,2));
    plot([0.5 0.5],[0 100],'k--')
    plot([-9 30],[80 80],'k--')
    plot([-9 30],[33 33 ],'k--')
    hold off
    xlabel('Trial # Relative To Switch')
    ylabel('Percent (%)')
    box off
    title('Smoothed Data All Blocks')
    
    subplot(2,2,3)
    hold on
    plot(-9:30,FilterLearningCurves(100*average_intra_performance',3,2));
    plot(-9:30,FilterLearningCurves(100*nanmean(intra_target_looking_time)',3,2));
    plot(-9:30,FilterLearningCurves(100*nanmean(intra_looking_at_highest_rewarded_item)',3,2));
    plot([0.5 0.5],[0 100],'k--')
    plot([-9 30],[80 80],'k--')
    plot([-9 30],[33 33 ],'k--')
    hold off
    xlabel('Trial # Relative To Switch')
    ylabel('Percent (%)')
    box off
    title(['Smoothed Data Intra-Dimensional Shifts (n = ' num2str(total_num_intra_blocks) ' Blocks'])
    
    subplot(2,2,4)
    hold on
    plot(-9:30,FilterLearningCurves(100*average_extra_performance',3,2));
    plot(-9:30,FilterLearningCurves(100*nanmean(extra_target_looking_time)',3,2));
    plot(-9:30,FilterLearningCurves(100*nanmean(extra_looking_at_highest_rewarded_item)',3,2));
    plot([0.5 0.5],[0 100],'k--')
    plot([-9 30],[80 80],'k--')
    plot([-9 30],[33 33 ],'k--')
    hold off
    xlabel('Trial # Relative To Switch')
    ylabel('Percent (%)')
    box off
    title(['Smoothed Data Extra-Dimensional Shifts (n = ' num2str(total_num_intra_blocks) ' Blocks'])
    
    subtitle(['Shift Learning-' monkeyname ': N = ' num2str(total_num_blocks) ' learning blocks'])
    
    
    figure
    hold on
    [hAx,hLine1,hLine2] = plotyy(-9:30,FilterLearningCurves(100*average_performance',3,2),-9:30,FilterLearningCurves(100*nanmean(all_target_looking_time)',3,2));
    plot([0.5 0.5],[0 100],'k--')
    hold off
    ylabel(hAx(1),'Performance (% Correct)') % left y-axis
    ylabel(hAx(2),'% Looking TIme') % right y-axis
    xlabel('Trial # Relative To Switch')
    box off
    title('Smoothed Data All Blocks')
    
    ylim(hAx(1), [0 100]);
    set(hAx(1), 'YTick',[0:10:100])
    ylim(hAx(2), [20 60]);
    set(hAx(2), 'YTick',[20:10:60])
    
end