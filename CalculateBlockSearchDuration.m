function [all_search_duration,correct_search_duration,incorrect_search_duration,...
    incorrect_minus_one_search_duration,correct_minus_one_search_duration,...
    correct_n_minus_one_incorrect_search_duration,correct_n_minus_one_correct_search_duration,...
    incorrect_n_minus_one_incorrect_search_duration,incorrect_n_minus_one_correct_search_duration] ...
    = CalculateBlockSearchDuration(session_data,goodblocks,min_block_len,last_trial_window)
%adapted from code else where, written 5/8/19

%Inputs:
% 1) session_data: all the session data
% 2) goodblocks: blocks with acceptable performance
% 3) min_block_len: miniumum # of trials in a block
% 4) last_trial_window: # of trials to look at end of block

%Outputs:
% 1) search_duration: block transition-aligned search duration
% 2) correct_search_duration: block transition-aligned search duration for correct trials
% 3) incorrect_search_duration: block transition-aligned search duration for incorrect trials
% 4) incorrect_minus_one_search_duration: block transition-aligned search duration for trials preceeded by an incorrect trial
% 5) correct_n_minus_one_incorrect_search_duration: block transition-aligned search duration for correct trial preceeded by an incorrect trial
% 6) correct_n_minus_one_correct_search_duration: block transition-aligned search duration for correct trial preceeded by an correct trial
% 7) incorrect_n_minus_one_incorrect_search_duration: block transition-aligned search duration for incorrect trials preceeded by an incorrect trial
% 8) incorrect_n_minus_one_correct_search_duration: block transition-aligned search duration for incorrect trials preceeded by an correct trial

if nargin < 2
    error('Not enough input arguments')
elseif nargin < 3
    min_block_len = 30;
    last_trial_window = 10;
elseif nargin < 4
    last_trial_window = 10;
end

num_blocks = length(goodblocks);
all_search_duration = NaN(num_blocks,min_block_len+last_trial_window);
correct_search_duration = NaN(num_blocks,min_block_len+last_trial_window);
incorrect_search_duration = NaN(num_blocks,min_block_len+last_trial_window);
incorrect_minus_one_search_duration = NaN(num_blocks,min_block_len+last_trial_window);
correct_minus_one_search_duration = NaN(num_blocks,min_block_len+last_trial_window);
correct_n_minus_one_incorrect_search_duration = NaN(num_blocks,min_block_len+last_trial_window);
correct_n_minus_one_correct_search_duration = NaN(num_blocks,min_block_len+last_trial_window);
incorrect_n_minus_one_incorrect_search_duration = NaN(num_blocks,min_block_len+last_trial_window);
incorrect_n_minus_one_correct_search_duration = NaN(num_blocks,min_block_len+last_trial_window);

block = session_data.trial_data.Block;
block = block + 1;%index starts at 0 in C++
rewarded = strcmpi(session_data.trial_data.isRewarded,'true');
search_duration = Calculate_FLU_Search_Duration(session_data);

for b = 1:num_blocks
    blockind = find(block == goodblocks(b));
    if isempty(blockind)
        error('why is block empty?')
    elseif length(blockind) < 15%didn't finish
        error('why is block too short?')
    end
    
    %---Get Search Duration---%
    blocksearchdur = search_duration(blockind(1):blockind(end));
    blocksearchdur = [blocksearchdur(end-(last_trial_window-1):end); blocksearchdur];

    if length(blocksearchdur) > min_block_len+last_trial_window
        all_search_duration(b,1:min_block_len+last_trial_window) = blocksearchdur(1:min_block_len+last_trial_window);
    else
        all_search_duration(b,1:length(blocksearchdur)) = blocksearchdur;
    end
    
    %---Get Search Duration based on performance---%
    blockrewarded = rewarded(blockind(1):blockind(end));
    blockrewarded = [blockrewarded(end-(last_trial_window-1):end); blockrewarded];
    if blockind(1) == 1
        prior_block_trial_rewarded = false;
    else
        prior_block_trial_rewarded = rewarded(blockind(1)-1); %trial 1 should not exist since don't analyze block 1
    end
    
    blocksearchdur_cor = blocksearchdur;
    blocksearchdur_cor(~blockrewarded) = NaN;
    if length(blocksearchdur_cor) > min_block_len+last_trial_window
        correct_search_duration(b,1:min_block_len+last_trial_window) = blocksearchdur_cor(1:min_block_len+last_trial_window);
    else
        correct_search_duration(b,1:length(blocksearchdur_cor)) = blocksearchdur_cor;
    end
    
    blocksearchdur_incor = blocksearchdur;
    blocksearchdur_incor(blockrewarded) = NaN;
    if length(blocksearchdur_incor) > min_block_len+last_trial_window
        incorrect_search_duration(b,1:min_block_len+last_trial_window) = blocksearchdur_incor(1:min_block_len+last_trial_window);
    else
        incorrect_search_duration(b,1:length(blocksearchdur_incor)) = blocksearchdur_incor;
    end
    
    blocksearchdur_incorm1 = blocksearchdur;
    blocksearchdur_incorm1([~prior_block_trial_rewarded; ~blockrewarded(1:end-1)]) = NaN;
    if length(blocksearchdur_incorm1) > min_block_len+last_trial_window
        incorrect_minus_one_search_duration(b,1:min_block_len+last_trial_window) = blocksearchdur_incorm1(1:min_block_len+last_trial_window);
    else
        incorrect_minus_one_search_duration(b,1:length(blocksearchdur_incorm1)) = blocksearchdur_incorm1;
    end
    
    blocksearchdur_corm1 = blocksearchdur;
    blocksearchdur_corm1([prior_block_trial_rewarded; blockrewarded(1:end-1)]) = NaN;
    if length(blocksearchdur_corm1) > min_block_len+last_trial_window
        correct_minus_one_search_duration(b,1:min_block_len+last_trial_window) = blocksearchdur_corm1(1:min_block_len+last_trial_window);
    else
        correct_minus_one_search_duration(b,1:length(blocksearchdur_corm1)) = blocksearchdur_corm1;
    end
   
    blocksearchdur_corr1corr = blocksearchdur;
    blocksearchdur_corr1corr([~prior_block_trial_rewarded; ~blockrewarded(1:end-1)] & ~blockrewarded) = NaN;
    if length(blocksearchdur_corr1corr) > min_block_len+last_trial_window
        correct_n_minus_one_correct_search_duration(b,1:min_block_len+last_trial_window) = blocksearchdur_corr1corr(1:min_block_len+last_trial_window);
    else
        correct_n_minus_one_correct_search_duration(b,1:length(blocksearchdur_corr1corr)) = blocksearchdur_corr1corr;
    end
    
    blocksearchdur_corr1incorr = blocksearchdur;
    blocksearchdur_corr1incorr([prior_block_trial_rewarded; blockrewarded(1:end-1)] & ~blockrewarded) = NaN;
    if length(blocksearchdur_corr1incorr) > min_block_len+last_trial_window
        correct_n_minus_one_incorrect_search_duration(b,1:min_block_len+last_trial_window) = blocksearchdur_corr1incorr(1:min_block_len+last_trial_window);
    else
        correct_n_minus_one_incorrect_search_duration(b,1:length(blocksearchdur_corr1incorr)) = blocksearchdur_corr1incorr;
    end

    blocksearchdur_incorr1corr = blocksearchdur;
    blocksearchdur_incorr1corr([~prior_block_trial_rewarded; ~blockrewarded(1:end-1)] & blockrewarded) = NaN;
    if length(blocksearchdur_corr1corr) > min_block_len+last_trial_window
        incorrect_n_minus_one_correct_search_duration(b,1:min_block_len+last_trial_window) = blocksearchdur_incorr1corr(1:min_block_len+last_trial_window);
    else
        incorrect_n_minus_one_correct_search_duration(b,1:length(blocksearchdur_incorr1corr)) = blocksearchdur_incorr1corr;
    end

    blocksearchdur_incorr1incorr = blocksearchdur;
    blocksearchdur_incorr1incorr([prior_block_trial_rewarded; blockrewarded(1:end-1)] & blockrewarded) = NaN;
    if length(blocksearchdur_incorr1incorr) > min_block_len+last_trial_window
        incorrect_n_minus_one_incorrect_search_duration(b,1:min_block_len+last_trial_window) = blocksearchdur_incorr1incorr(1:min_block_len+last_trial_window);
    else
        incorrect_n_minus_one_incorrect_search_duration(b,1:length(blocksearchdur_incorr1incorr)) = blocksearchdur_incorr1incorr;
    end

end
end