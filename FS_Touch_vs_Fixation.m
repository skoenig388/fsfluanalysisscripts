%Written Seth Koenig 3/8/19
%code looks at search duration for touch vs fixation for comparable sets
clar

log_dir = 'Z:\MonkeyData\Wotan\Feature Search Task\';


sess_file_names = {'FS__Wotan__03_10_2018__12_04_19','FS__Wotan__21_02_2019__13_30_26',... %Shape Set 1 vs Shape Set 1
    'FS__Wotan__12_10_2018__13_07_31','FS__Wotan__20_02_2019__12_05_08',... %Color set 1 vs color set 1
    'FS__Wotan__10_10_2018__14_00_28','FS__Wotan__22_02_2019__13_46_30',... %pattern set 2 vs pattern set 1, didn't finish pattern set 1 on touch
    'FS__Wotan__06_10_2018__15_07_21','FS__Wotan__25_02_2019__10_17_00',... %arm set 3 vs arm set 3
    };
session_type = {'touch','gaze',...
    'touch','gaze',...
    'touch','gaze',...
    'touch','gaze',...
    };

session_dimension = {'Shape','Shape',...
    'Color','Color',...
    'Pattern','Pattern',...
    'Arm','Arm',...
    };

%% Import Data (takes a while)
%session_data = cell(1,length(sess_file_names));
for sess = 1:length(sess_file_names)
    disp(['Importing Session # ' num2str(sess) '/' num2str(length(sess_file_names))]);
    session_data{sess} = ImportFLU_DataV2(sess_file_names{sess},log_dir);
end
%% Get Search Duration by across all sets and blocks
all_trial_num_objects = cell(1,length(sess_file_names));
all_trial_search_durations = cell(1,length(sess_file_names));
search_dur_diff = [];
for sess = 1:length(sess_file_names)
    num_trials = session_data{sess}.num_trials;
    all_trial_num_objects{sess} = NaN(1,num_trials);
    all_trial_search_durations{sess} = NaN(1,num_trials);
    for t =1:num_trials
        num_objs = length(session_data{sess}.trial_def{t}.relevantObjects);
        all_trial_num_objects{sess}(t) = num_objs;
        
        search_start_time = session_data{sess}.trial_data.Epoch3_StartTimeRelative(t)+session_data{sess}.trial_data.baselineDuration(t);
        if any(contains(session_data{sess}.trial_data.Properties.VariableNames,'ObjectSelectionDuration'))
            search_end_time =  session_data{sess}.trial_data.Epoch7_StartTimeRelative(t)...
                -session_data{sess}.trial_data.fBDuration(t)-session_data{sess}.trial_data.ObjectSelectionDuration(t);
        else
            search_end_time =  session_data{sess}.trial_data.Epoch7_StartTimeRelative(t)...
                -session_data{sess}.trial_data.fBDuration(t)-session_data{sess}.trial_data.choiceToFBDuration(t);
        end
        if search_end_time < search_start_time
            disp('What')
        end
        
        if  strcmpi(session_data{sess}.trial_data.isRewarded(t),'true')
            all_trial_search_durations{sess}(t) = search_end_time-search_start_time;
        end
        if ~any(contains(session_data{sess}.trial_data.Properties.VariableNames,'ObjectSelectionDuration'))
            search_duration = session_data{sess}.trial_data.Epoch4_Duration(t);
            search_duration2 = search_end_time-search_start_time;
            %disp(num2str(search_duration2-search_duration));
            search_dur_diff = [search_dur_diff search_duration2-search_duration];
        end
    end
end
%% Get average search duration by type and number of distractors
numD = cell2mat(all_trial_num_objects);
numD = unique(numD) ;

search_dur_by_numd_and_type = cell(2,length(numD));
for sess = [1:2 5:length(sess_file_names)]
    if strcmpi(session_type{sess},'gaze')
        type_ind = 2;
    elseif strcmpi(session_type{sess},'touch')
        type_ind = 1;
    else
        error('Unrecognized Session Type')
    end
    
    for nD = 1:length(numD)
        these_trials = find(all_trial_num_objects{sess} == numD(nD));
        search_dur_by_numd_and_type{type_ind,nD} = [search_dur_by_numd_and_type{type_ind,nD}   all_trial_search_durations{sess}(these_trials)];
    end
end
%%
avg_search_dur_by_numd_and_type = NaN(2,length(numD));
for type = 1:2
    for nD = 1:length(numD)
        avg_search_dur_by_numd_and_type(type,nD) = nanmean(search_dur_by_numd_and_type{type,nD});
    end
end

%%
figure
bar(1:length(numD),avg_search_dur_by_numd_and_type')
xticks(1:length(numD))
xticklabels({'0','1','3','5','9','12'})
xlabel('# of Distractors')
ylabel('Average Search Duration (sec)')
box off
legend('Touch','Gaze')
title('Search Duration by # of Distractors and Selection Modality')
%%
avg_search_durs_by_sess_and_numD = NaN(length(sess_file_names),length(numD));
for sess = 1:length(sess_file_names)
    for nD = 1:length(numD)
        these_trials = find(all_trial_num_objects{sess} == numD(nD));
        avg_search_durs_by_sess_and_numD(sess,nD) = nanmean(all_trial_search_durations{sess}(these_trials));
    end
end

figure
bar(1:length(numD),avg_search_durs_by_sess_and_numD')
xticklabels({'0','1','3','5','9','12'})
xlabel('# of Distractors')
ylabel('Average Search Duration (sec)')
box off
title('Search Duration by Sess #')

legend_str = [];
for sess = 1:length(sess_file_names)
    legend_str = [legend_str {[session_type{sess} '__' session_dimension{sess}]}];
end
legend(legend_str)


