%Simple import data to get RTs and Performance


clear,clc,close all%fclose all

trial_name = '__TrialData.txt';
trialdef_name = '__trialdef_on_trial_';
block_name = '__block_defs.json';


%---1 irrelevant feature---%
% monkeyname = 'Wotan';
% sess_file_names = {'FLU__Wotan__03_12_2018__15_18_08','FLU__Wotan__04_12_2018__10_27_26',...
%     'FLU__Wotan__05_12_2018__11_23_55','FLU__Wotan__08_12_2018__12_02_13',...
%     'FLU__Wotan__09_12_2018__13_43_05','FLU__Wotan__10_12_2018__15_48_08',...
%     'FLU__Wotan__11_12_2018__11_20_40','FLU__Wotan__12_12_2018__11_19_32',...
%     'FLU__Wotan__13_12_2018__11_20_58','FLU__Wotan__14_12_2018__12_01_09',...
%     'FLU__Wotan__07_01_2019__14_04_19','FLU__Wotan__08_01_2019__14_28_14'};


% monkeyname = 'Frey';
% sess_file_names = {'FLU__Frey__05_12_2018__10_36_22','FLU__Frey__08_12_2018__11_12_37',...
%     'FLU__Frey__09_12_2018__12_52_19','FLU__Frey__10_12_2018__14_53_12',...
%     'FLU__Frey__11_12_2018__10_39_42','FLU__Frey__12_12_2018__10_22_42',...
%     'FLU__Frey__14_12_2018__11_13_08','FLU__Frey__07_01_2019__13_12_01',...
%     'FLU__Frey__08_01_2019__13_34_08','FLU__Frey__09_01_2019__12_38_47',...
%     'FLU__Frey__11_01_2019__14_15_23','FLU__Frey__12_01_2019__14_42_58',...
%     'FLU__Frey__14_01_2019__11_48_24'};

%---2 irrelevant features---%
monkeyname = 'Wotan';
sess_file_names = {'FLU__Wotan__11_01_2019__15_10_41','FLU__Wotan__14_01_2019__12_44_40',...
    'FLU__Wotan__16_01_2019__13_29_45','FLU__Wotan__17_01_2019__14_27_36',...
    'FLU__Wotan__18_01_2019__12_09_39','FLU__Wotan__21_01_2019__12_36_27',...
    'FLU__Wotan__23_01_2019__12_40_21','FLU__Wotan__27_01_2019__13_47_33'};

% monkeyname = 'Frey';
% sess_file_names = {'FLU__Frey__16_01_2019__12_30_58','FLU__Frey__18_01_2019__11_23_45',...
%     'FLU__Frey__23_01_2019__11_31_30','FLU__Frey__24_01_2019__12_02_47',...
%     'FLU__Frey__25_01_2019__10_39_23','FLU__Frey__27_01_2019__12_49_34'};



%% All blocks

all_block_performance = NaN(10*length(sess_file_names),40);
all_block_search_duration = NaN(10*length(sess_file_names),40);
all_block_ind = 1;

stim_location = NaN(4,5000);
stim_num = 1;
for sess = 1:length(sess_file_names)
    
    if contains(sess_file_names{sess},'Frey')
        log_dir = 'Z:\MonkeyData\Frey\';
    elseif contains(sess_file_names{sess},'Wotan')
        log_dir = 'Z:\MonkeyData\Wotan\';
    end
    
    trial_dir = [log_dir sess_file_names{sess} '\RuntimeData\TrialData\'];
    config_dir = [log_dir sess_file_names{sess} '\RuntimeData\ConfigCopy\'];
    trial_data = readtable([trial_dir sess_file_names{sess} trial_name]);
    block_def = ReadJsonFile([config_dir sess_file_names{sess} block_name]);
    
    search_duration = trial_data.Epoch4_Duration;
    rewarded = strcmpi(trial_data.isRewarded,'true');
    
    block = trial_data.Block;
    block = block + 1;%index starts at 0 in C++
    
    
    for b =2:max(block)
        
        for t= 1:length(block_def(b).TrialDefs)
            
            stim_location(1,stim_num) = block_def(b).TrialDefs(t).relevantObjects(1).StimLocation.x;
            stim_location(2,stim_num) = block_def(b).TrialDefs(t).relevantObjects(1).StimLocation.z;
            
            stim_location(3,stim_num) = block_def(b).TrialDefs(t).relevantObjects(2).StimLocation.x;
            stim_location(4,stim_num) = block_def(b).TrialDefs(t).relevantObjects(2).StimLocation.z;
            
            stim_num = stim_num+1;
        end
        blockind = find(block == b);
        blockrewarded = rewarded(blockind(1)-10:blockind(end));
        blocksearchdur = search_duration(blockind(1)-10:blockind(end));
        
        if length(blockind) < 15
            continue
        end
        blocksearchdur(blocksearchdur >= 2.5) = NaN;%timed out or not paying attention
        blocksearchdur(blocksearchdur <= 0) = NaN;
        
        
        if length(blockrewarded) > 40
            all_block_performance(all_block_ind,1:40) = blockrewarded(1:40);
            all_block_search_duration(all_block_ind,1:40) = blocksearchdur(1:40);
        else
            all_block_performance(all_block_ind,1:length(blockrewarded)) = blockrewarded;
            all_block_search_duration(all_block_ind,1:length(blocksearchdur)) = blocksearchdur;
        end
        
        all_block_ind = all_block_ind + 1;
    end
    
end
% Remove extra NaNs
all_block_performance = laundry(all_block_performance);
all_block_search_duration = laundry(all_block_search_duration);
% Calculate Average Performance and Search Duration
total_num_blocks = size(all_block_performance,1);
num_not_nans = sum(~isnan(all_block_performance));

average_performance = nansum(all_block_performance)./num_not_nans;
average_search_duration = nansum(all_block_search_duration)./num_not_nans;

%% Intra- vs Extra-Dimensional Shifts

all_intra_block_performance = NaN(10*length(sess_file_names),40);
all_intra_block_search_duration = NaN(10*length(sess_file_names),40);
all_intra_block_ind = 1;

all_extra_block_performance = NaN(10*length(sess_file_names),40);
all_extra_block_search_duration = NaN(10*length(sess_file_names),40);
all_extra_block_ind = 1;
for sess = 1:length(sess_file_names)
    
    if contains(sess_file_names{sess},'Frey')
        log_dir = 'Z:\MonkeyData\Frey\';
    elseif contains(sess_file_names{sess},'Wotan')
        log_dir = 'Z:\MonkeyData\Wotan\';
    end
    
    
    trial_dir = [log_dir sess_file_names{sess} '\RuntimeData\TrialData\'];
    config_dir = [log_dir sess_file_names{sess} '\RuntimeData\ConfigCopy\'];
    trial_data = readtable([trial_dir sess_file_names{sess} trial_name]);
    block_def = ReadJsonFile([config_dir sess_file_names{sess} block_name]);
    
    search_duration = trial_data.Epoch4_Duration;
    rewarded = strcmpi(trial_data.isRewarded,'true');
    
    block = trial_data.Block;
    block = block + 1;%index starts at 0 in C++
    
    for b =2:max(block)
        
        if contains(block_def(b).BlockID,'Intra')
            intra_extra = 1;
        elseif contains(block_def(b).BlockID,'Extra')
            intra_extra = 2;
        else
            error('blockID not recongized')
        end
        blockind = find(block == b);
        blockrewarded = rewarded(blockind(1)-10:blockind(end));
        blocksearchdur = search_duration(blockind(1)-10:blockind(end));
        
        if length(blockind) < 15
            continue
        end
        blocksearchdur(blocksearchdur >= 2.5) = NaN;%timed out or not paying attention
        blocksearchdur(blocksearchdur < 0) = NaN;
        
        if intra_extra == 1
            if length(blockrewarded) > 40
                all_intra_block_performance(all_intra_block_ind,1:40) = blockrewarded(1:40);
                all_intra_block_search_duration(all_intra_block_ind,1:40) = blocksearchdur(1:40);
            else
                all_intra_block_performance(all_intra_block_ind,1:length(blockrewarded)) = blockrewarded;
                all_intra_block_search_duration(all_intra_block_ind,1:length(blocksearchdur)) = blocksearchdur;
            end
            all_intra_block_ind = all_intra_block_ind + 1;
        else
            if length(blockrewarded) > 40
                all_extra_block_performance(all_extra_block_ind,1:40) = blockrewarded(1:40);
                all_extra_block_search_duration(all_extra_block_ind,1:40) = blocksearchdur(1:40);
            else
                all_extra_block_performance(all_extra_block_ind,1:length(blockrewarded)) = blockrewarded;
                all_extra_block_search_duration(all_extra_block_ind,1:length(blocksearchdur)) = blocksearchdur;
            end
            all_extra_block_ind = all_extra_block_ind + 1;
        end
    end
end
% Remove extra NaNs
all_intra_block_performance = laundry(all_intra_block_performance);
all_intra_block_search_duration = laundry(all_intra_block_search_duration);
all_extra_block_performance = laundry(all_extra_block_performance);
all_extra_block_search_duration = laundry(all_extra_block_search_duration);

% Calculate Average Performance and Search Duration
total_num_intra_blocks = size(all_intra_block_performance,1);
num_not_nans_intra = sum(~isnan(all_intra_block_performance));
total_num_extra_blocks = size(all_extra_block_performance,1);
num_not_nans_extra = sum(~isnan(all_extra_block_performance));

average_intra_performance = nansum(all_intra_block_performance)./num_not_nans_intra;
average_intra_search_duration = nansum(all_intra_block_search_duration)./num_not_nans_intra;
average_extra_performance = nansum(all_extra_block_performance)./num_not_nans_extra;
average_extra_search_duration = nansum(all_extra_block_search_duration)./num_not_nans_extra;
% %% Intra- vs Extra-Dimensional Shifts, divided previous dimension type
% 
% %For intra, divide by whether distractor dimension is the same or different
% all_same_intra_block_performance = NaN(10*length(sess_file_names),40);
% all_same_intra_block_search_duration = NaN(10*length(sess_file_names),40);
% all_same_intra_block_ind = 1;
% 
% all_diff_intra_block_performance = NaN(10*length(sess_file_names),40);
% all_diff_intra_block_search_duration = NaN(10*length(sess_file_names),40);
% all_diff_intra_block_ind = 1;
% 
% %For extra, divide by whether distractor dimension is the same, different,
% %or the target dimension from the last block i.e. reverse
% all_same_extra_block_performance = NaN(10*length(sess_file_names),40);
% all_same_extra_block_search_duration = NaN(10*length(sess_file_names),40);
% all_same_extra_block_ind = 1;
% 
% all_diff_extra_block_performance = NaN(10*length(sess_file_names),40);
% all_diff_extra_block_search_duration = NaN(10*length(sess_file_names),40);
% all_diff_extra_block_ind = 1;
% 
% all_reverse_extra_block_performance = NaN(10*length(sess_file_names),40);
% all_reverse_extra_block_search_duration = NaN(10*length(sess_file_names),40);
% all_reverse_extra_block_ind = 1;
% 
% for sess = 1:length(sess_file_names)
%     
%     if contains(sess_file_names{sess},'Frey')
%         log_dir = 'Z:\MonkeyData\Frey\';
%     elseif contains(sess_file_names{sess},'Wotan')
%         log_dir = 'Z:\MonkeyData\Wotan\';
%     end
%     
%     trial_dir = [log_dir sess_file_names{sess} '\RuntimeData\TrialData\'];
%     config_dir = [log_dir sess_file_names{sess} '\RuntimeData\ConfigCopy\'];
%     trial_data = readtable([trial_dir sess_file_names{sess} trial_name]);
%     block_def = ReadJsonFile([config_dir sess_file_names{sess} block_name]);
%     
%     search_duration = trial_data.Epoch4_Duration;
%     rewarded = strcmpi(trial_data.isRewarded,'true');
%     
%     block = trial_data.Block;
%     block = block + 1;%index starts at 0 in C++
%     
%     for b =2:max(block)
%         
%         this_block_active_dims = find(cellfun(@numel,block_def(b).ActiveFeatureTemplate) > 1);
%         last_block_active_dims = find(cellfun(@numel,block_def(b-1).ActiveFeatureTemplate) > 1);
%         this_block_target_dim =  find(block_def(b).RuleArray.RelevantFeatureTemplate ~= -1);
%         last_block_target_dim =  find(block_def(b-1).RuleArray.RelevantFeatureTemplate ~= -1);
%         this_block_irrel_dim = this_block_active_dims(this_block_active_dims ~= this_block_target_dim);
%         last_block_irrel_dim = last_block_active_dims(last_block_active_dims ~= last_block_target_dim);
%         
%         if contains(block_def(b).BlockID,'Intra')
%             intra_extra = 1;
%             if this_block_irrel_dim == last_block_irrel_dim
%                 same_diff = 1;
%             else
%                 same_diff = 2;
%             end
%         elseif contains(block_def(b).BlockID,'Extra')
%             intra_extra = 2;
%             
%             if this_block_irrel_dim == last_block_irrel_dim
%                 same_diff = 1;
%             elseif this_block_irrel_dim == last_block_target_dim
%                 same_diff = 3;
%             elseif  this_block_target_dim == last_block_irrel_dim
%                 same_diff = 3;
%             else
%                 same_diff = 2;
%             end
%         else
%             error('blockID not recongized')
%         end
%         blockind = find(block == b);
%         blockrewarded = rewarded(blockind(1)-10:blockind(end));
%         blocksearchdur = search_duration(blockind(1)-10:blockind(end));
%         
%         if length(blockind) < 15
%             continue
%         end
%         blocksearchdur(blocksearchdur >= 2.5) = NaN;%timed out or not paying attention
%         blocksearchdur(blocksearchdur < 0) = NaN;
%         
%         if intra_extra == 1
%             if same_diff == 1
%                 if length(blockrewarded) > 40
%                     all_same_intra_block_performance(all_same_intra_block_ind,1:40) = blockrewarded(1:40);
%                     all_same_intra_block_search_duration(all_same_intra_block_ind,1:40) = blocksearchdur(1:40);
%                 else
%                     all_same_intra_block_performance(all_same_intra_block_ind,1:length(blockrewarded)) = blockrewarded;
%                     all_same_intra_block_search_duration(all_same_intra_block_ind,1:length(blocksearchdur)) = blocksearchdur;
%                 end
%                 all_same_intra_block_ind = all_same_intra_block_ind + 1;
%             elseif same_diff == 2
%                 if length(blockrewarded) > 40
%                     all_diff_intra_block_performance(all_diff_intra_block_ind,1:40) = blockrewarded(1:40);
%                     all_diff_intra_block_search_duration(all_diff_intra_block_ind,1:40) = blocksearchdur(1:40);
%                 else
%                     all_diff_intra_block_performance(all_diff_intra_block_ind,1:length(blockrewarded)) = blockrewarded;
%                     all_diff_intra_block_search_duration(all_diff_intra_block_ind,1:length(blocksearchdur)) = blocksearchdur;
%                 end
%                 all_diff_intra_block_ind = all_diff_intra_block_ind + 1;
%             end
%         else
%             if same_diff == 1
%                 if length(blockrewarded) > 40
%                     all_same_extra_block_performance(all_same_extra_block_ind,1:40) = blockrewarded(1:40);
%                     all_same_extra_block_search_duration(all_same_extra_block_ind,1:40) = blocksearchdur(1:40);
%                 else
%                     all_same_extra_block_performance(all_same_extra_block_ind,1:length(blockrewarded)) = blockrewarded;
%                     all_same_extra_block_search_duration(all_same_extra_block_ind,1:length(blocksearchdur)) = blocksearchdur;
%                 end
%                 all_same_extra_block_ind = all_same_extra_block_ind + 1;
%             elseif same_diff == 2
%                 if length(blockrewarded) > 40
%                     all_diff_extra_block_performance(all_diff_extra_block_ind,1:40) = blockrewarded(1:40);
%                     all_diff_extra_block_search_duration(all_diff_extra_block_ind,1:40) = blocksearchdur(1:40);
%                 else
%                     all_diff_extra_block_performance(all_diff_extra_block_ind,1:length(blockrewarded)) = blockrewarded;
%                     all_diff_extra_block_search_duration(all_diff_extra_block_ind,1:length(blocksearchdur)) = blocksearchdur;
%                 end
%                 all_diff_extra_block_ind = all_diff_extra_block_ind + 1;
%             elseif same_diff == 3
%                 if length(blockrewarded) > 40
%                     all_reverse_extra_block_performance(all_reverse_extra_block_ind,1:40) = blockrewarded(1:40);
%                     all_reverse_extra_block_search_duration(all_reverse_extra_block_ind,1:40) = blocksearchdur(1:40);
%                 else
%                     all_reverse_extra_block_performance(all_reverse_extra_block_ind,1:length(blockrewarded)) = blockrewarded;
%                     all_reverse_extra_block_search_duration(all_reverse_extra_block_ind,1:length(blocksearchdur)) = blocksearchdur;
%                 end
%                all_reverse_extra_block_ind = all_reverse_extra_block_ind + 1;
%             end
%         end
%     end
% end
% 
% % Remove extra NaNs
% all_same_intra_block_performance = laundry(all_same_intra_block_performance);
% all_same_intra_block_search_duration = laundry(all_same_intra_block_search_duration);
% all_diff_intra_block_performance = laundry(all_diff_intra_block_performance);
% all_diff_intra_block_search_duration = laundry(all_diff_intra_block_search_duration);
% all_same_extra_block_performance = laundry(all_same_extra_block_performance);
% all_same_extra_block_search_duration = laundry(all_same_extra_block_search_duration);
% all_diff_extra_block_performance = laundry(all_diff_extra_block_performance);
% all_diff_extra_block_search_duration = laundry(all_diff_extra_block_search_duration);
% all_reverse_extra_block_performance = laundry(all_reverse_extra_block_performance);
% all_reverse_extra_block_search_duration = laundry(all_reverse_extra_block_search_duration);
% 
% % Calculate Average Performance and Search Duration
% total_num_same_intra_blocks = size(all_same_intra_block_performance,1);
% total_num_diff_intra_blocks = size(all_diff_intra_block_performance,1);
% total_num_same_extra_blocks = size(all_same_extra_block_performance,1);
% total_num_diff_extra_blocks = size(all_diff_extra_block_performance,1);
% total_num_reverse_extra_blocks = size(all_reverse_extra_block_performance,1);
% 
% num_not_nans_same_intra = sum(~isnan(all_same_intra_block_performance));
% num_not_nans_diff_intra = sum(~isnan(all_diff_intra_block_performance));
% num_not_nans_same_extra = sum(~isnan(all_same_extra_block_performance));
% num_not_nans_diff_extra = sum(~isnan(all_diff_extra_block_performance));
% num_not_nans_reverse_extra = sum(~isnan(all_reverse_extra_block_performance));
% 
% if total_num_same_intra_blocks ~= 1
%     average_same_intra_performance = nansum(all_same_intra_block_performance)./num_not_nans_same_intra;
%     average_same_intra_search_duration = nansum(all_same_intra_block_search_duration)./num_not_nans_same_intra;
% else
%    average_same_intra_performance = all_same_intra_block_performance;
%    average_same_intra_search_duration = all_same_intra_block_search_duration;
% end
% average_diff_intra_performance = nansum(all_diff_intra_block_performance)./num_not_nans_diff_intra;
% average_diff_intra_search_duration = nansum(all_diff_intra_block_search_duration)./num_not_nans_diff_intra;
% average_same_extra_performance = nansum(all_same_extra_block_performance)./num_not_nans_same_extra;
% average_same_extra_search_duration = nansum(all_same_extra_block_search_duration)./num_not_nans_same_extra;
% average_diff_extra_performance = nansum(all_diff_extra_block_performance)./num_not_nans_diff_extra;
% average_diff_extra_search_duration = nansum(all_diff_extra_block_search_duration)./num_not_nans_diff_extra;
% average_reverse_extra_performance = nansum(all_reverse_extra_block_performance)./num_not_nans_reverse_extra;
% average_reverse_extra_search_duration = nansum(all_reverse_extra_block_search_duration)./num_not_nans_reverse_extra;
%% Basic Plot Results

figure
subplot(2,2,1)
plot(-9:30,100*average_performance)
hold on
plot([0.5 0.5],[0 100],'k--')
plot([-10 30],[80 80],'r--')
plot([-10 30],[50 50],'b--')
hold off
ylim([40 100])
xlabel('Trial # Relative To Switch')
ylabel('Performance %')
box off
title('All Switches: Average Performance')

asd = average_search_duration';
asd = [asd(10:-1:1); asd; asd(end:-1:end-9)];
asd = filtfilt(1/3*ones(1,3),1,asd);
asd = asd(11:end-10);

subplot(2,2,2)
plot(-9:30,average_search_duration)
yl = ylim;
hold on
plot([0.5 0.5],[yl(1) yl(2)],'k--')
plot(-9:30,asd,'r')
hold off
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (seconds)')
box off
title('All Switches: Average Search Duration')



subplot(2,2,3)
plot(-9:30,100*average_intra_performance)
hold on
plot(-9:30,100*average_extra_performance)
plot([0.5 0.5],[0 100],'k--')
plot([-10 30],[80 80],'k--')
plot([-10 30],[50 50],'k--')
hold off
ylim([40 100])
xlabel('Trial # Relative To Switch')
ylabel('Performance %')
box off
title(['Intra(n = ' num2str(total_num_intra_blocks) ')- vs Extradimensional (n = ' num2str(total_num_extra_blocks)...
    ') Shifts: Average Performance'])
legend('Intra','Extra')

asd = average_search_duration';
asd = [asd(10:-1:1); asd; asd(end:-1:end-9)];
asd = filtfilt(1/3*ones(1,3),1,asd);
asd = asd(11:end-10);

subplot(2,2,4)
plot(-9:30,average_intra_search_duration)
hold on
plot(-9:30,average_extra_search_duration)
yl = ylim;
plot([0.5 0.5],[yl(1) yl(2)],'k--')
hold off
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (seconds)')
box off
title(['Intra(n = ' num2str(total_num_intra_blocks) ')- vs Extradimensional (n = ' num2str(total_num_extra_blocks)...
    ') Shifts: Average Search Duration'])


subtitle(['Shift Learning-' monkeyname ': N = ' num2str(total_num_blocks) ' learning blocks'])

% %% Plot results by Same and Different
% 
% if ~isnan(average_same_intra_performance)
%     asid = average_same_intra_performance';
%     asid = [asid(10:-1:1); asid; asid(end:-1:end-9)];
%     asid = filtfilt(1/4*ones(1,4),1,asid);
%     asid = asid(11:end-10);
% else
%     asid = NaN(1,40);
% end
% 
% adid = average_diff_intra_performance';
% adid = [adid(10:-1:1); adid; adid(end:-1:end-9)];
% adid = filtfilt(1/4*ones(1,4),1,adid);
% adid = adid(11:end-10);
% 
% ases = average_same_extra_performance';
% ases = [ases(10:-1:1); ases; ases(end:-1:end-9)];
% ases = filtfilt(1/4*ones(1,4),1,ases);
% ases = ases(11:end-10);
% 
% ased = average_diff_extra_performance';
% ased = [ased(10:-1:1); ased; ased(end:-1:end-9)];
% ased = filtfilt(1/4*ones(1,4),1,ased);
% ased = ased(11:end-10);
% 
% ared = average_reverse_extra_performance';
% ared = [ared(10:-1:1); ared; ared(end:-1:end-9)];
% ared = filtfilt(1/4*ones(1,4),1,ared);
% ared = ared(11:end-10);
% 
% 
% figure
% subplot(1,2,1)
% hold on
% plot(-9:30,100*asid)
% plot(-9:30,100*adid)
% plot(-9:30,100*ases)
% plot(-9:30,100*ased)
% plot(-9:30,100*ared)
% plot([0.5 0.5],[0 100],'k--')
% plot([-10 30],[80 80],'r--')
% plot([-10 30],[50 50],'b--')
% hold off
% ylim([40 100])
% xlabel('Trial # Relative To Switch')
% ylabel('Performance %')
% box off
% title('Performance by Various Switch Types')
% legend('Intra-Same','Intra-Diff','Extra-Same','Extra-Diff','Extra-Reverse')
% 
% if ~isnan(average_same_intra_search_duration)
%     asid = average_same_intra_search_duration';
%     asid = [asid(10:-1:1); asid; asid(end:-1:end-9)];
%     asid = filtfilt(1/4*ones(1,4),1,asid);
%     asid = asid(11:end-10);
% else
%     asid = NaN(1,40);
% end
% 
% adid = average_diff_intra_search_duration';
% adid = [adid(10:-1:1); adid; adid(end:-1:end-9)];
% adid = filtfilt(1/4*ones(1,4),1,adid);
% adid = adid(11:end-10);
% 
% ases = average_same_extra_search_duration';
% ases = [ases(10:-1:1); ases; ases(end:-1:end-9)];
% ases = filtfilt(1/4*ones(1,4),1,ases);
% ases = ases(11:end-10);
% 
% ased = average_diff_extra_search_duration';
% ased = [ased(10:-1:1); ased; ased(end:-1:end-9)];
% ased = filtfilt(1/4*ones(1,4),1,ased);
% ased = ased(11:end-10);
% 
% ared = average_reverse_extra_search_duration';
% ared = [ared(10:-1:1); ared; ared(end:-1:end-9)];
% ared = filtfilt(1/4*ones(1,4),1,ared);
% ared = ared(11:end-10);
% 
% subplot(1,2,2)
% hold on
% plot(-9:30,asid)
% plot(-9:30,adid)
% plot(-9:30,ases)
% plot(-9:30,ased)
% plot(-9:30,ared)
% yl = ylim;
% plot([0.5 0.5],[yl(1) yl(2)],'k--')
% hold off
% xlabel('Trial # Relative To Switch')
% ylabel('Search Duration (seconds)')
% box off
% title('Search Duration by Various Switch Types')
% 
% subtitle(['Shift Learning-' monkeyname ': N = ' num2str(total_num_blocks) ' learning blocks'])

%%
stim_location = laundry(stim_location);

locs = NaN(3,4);
locs(1,1) = sum(stim_location(1,:) == 4 & stim_location(2,:) == 4) + sum(stim_location(3,:) == 4 & stim_location(4,:) == 4);
locs(1,2) = sum(stim_location(1,:) == -4 & stim_location(2,:) == 4) +  sum(stim_location(3,:) == -4 & stim_location(4,:) == 4);
locs(1,3) = sum(stim_location(1,:) == 4 & stim_location(2,:) == -4) + sum(stim_location(3,:) == 4 & stim_location(4,:) == -4);
locs(1,4) = sum(stim_location(1,:) == -4 & stim_location(2,:) == -4) + sum(stim_location(3,:) == -4 & stim_location(4,:) == -4);

locs(2,1) = sum(stim_location(1,:) == 4 & stim_location(2,:) == 4);
locs(2,2) = sum(stim_location(1,:) == -4 & stim_location(2,:) == 4);
locs(2,3) = sum(stim_location(1,:) == 4 & stim_location(2,:) == -4);
locs(2,4) = sum(stim_location(1,:) == -4 & stim_location(2,:) == -4);

locs(3,1) = sum(stim_location(3,:) == 4 & stim_location(4,:) == 4);
locs(3,2) = sum(stim_location(3,:) == -4 & stim_location(4,:) == 4);
locs(3,3) = sum(stim_location(3,:) == 4 & stim_location(4,:) == -4);
locs(3,4) = sum(stim_location(3,:) == -4 & stim_location(4,:) == -4);

locpct = locs;
locpct(1,:) = 100*locs(1,:)/sum(locs(1,:));
locpct(2,:) = 100*locs(2,:)/sum(locs(2,:));
locpct(3,:) = 100*locs(3,:)/sum(locs(3,:));
locpct