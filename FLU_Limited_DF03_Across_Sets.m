%Written by Seth Konig 3/2/19
%Similar to FLU_DF04_Combined_Data_Across_Sets but updated to work with
%newer scripts and new task structure

tic
clar
num_shuffs = 1000;
import_option = 'slim';

%average acceptable performance across blocks
max_block_count = 30;
miniumum_performance = 0.5;%set to 0 if you don't want to remove blocks
max_block_cut = 3;%of of 1st and last blocks to cut if performance is very bad
min_block_len = 30;%miniumum # of trials in a block
last_trial_window = 10;%window to look at before end of block

learning_point_threshold = 80;
forward_back_trials = 7;
forward_smoothing_window = 10; %size of the smoothing window for forward filter

%data import options
pre_processed_data_dir = '\Processed_Data\'; %for pre-saved session data files
reimport_new = false;%will import session data that is already processsed

max_time_out = 5*60;%seconds how long an acceptable break is
%%
%%%%%%%%%%%%%%%%
%%%---Frey---%%%
%%%%%%%%%%%%%%%%

%---0 irrelevant feature---%
% monkeyname = 'Frey';
% sess_file_names = {'FLU__Frey__15_02_2019__11_59_59','FLU__Frey__16_02_2019__12_31_20',...
%     'FLU__Frey__18_02_2019__11_09_46','FLU__Frey__25_02_2019__10_59_03','FLU__Frey__26_02_2019__11_09_32',...
%     'FLU__Frey__28_02_2019__10_31_32','FLU__Frey__01_03_2019__11_40_45','FLU__Frey__02_03_2019__12_50_48',...
%     'FLU__Frey__07_03_2019__11_00_10','FLU__Frey__08_03_2019__10_15_22','FLU__Frey__09_03_2019__10_41_20',...
%     'FLU__Frey__11_03_2019__11_39_47','FLU__Frey__12_03_2019__10_02_12','FLU__Frey__13_03_2019__10_21_17'};


%---1 Irrelevant Feature Dim---%
% monkeyname = 'Frey';
% sess_file_names = {'FLU__Frey__14_03_2019__10_21_27','FLU__Frey__05_04_2019__10_56_11',...
%     'FLU__Frey__08_04_2019__11_28_39','FLU__Frey__18_04_2019__10_30_20',...
%     'FLU__Frey__19_04_2019__10_36_02','FLU__Frey__22_04_2019__12_08_08',...
%     'FLU__Frey__23_04_2019__10_38_39','FLU__Frey__24_04_2019__10_51_15',...
%     'FLU__Frey__25_04_2019__10_22_35','FLU__Frey__29_04_2019__11_12_34',...
%     'FLU__Frey__01_05_2019__10_36_51','FLU__Frey__03_05_2019__10_43_53',...
%     'FLU__Frey__04_05_2019__11_45_06','FLU__Frey__06_05_2019__10_26_26',...
% };

%---1-2 Irrelevant Feature Dim w/ Extra Large Quaddles @ size 0.7---%
% monkeyname = 'Frey';
% sess_file_names = {'FLU__Frey__07_05_2019__10_27_39','FLU__Frey__08_05_2019__10_48_55',...
%     'FLU__Frey__09_05_2019__10_40_43','FLU__Frey__10_05_2019__10_23_23',...
%     'FLU__Frey__11_05_2019__11_50_02','FLU__Frey__13_05_2019__10_38_53',...
%     };

%---1-2 Irrelevant w/ Quaddle @ size 0.6---%
%"best" sets
% monkeyname = 'Frey';
% sess_file_names = {'FLU__Frey__16_05_2019__10_26_03','FLU__Frey__17_05_2019__10_50_06',...
%     'FLU__Frey__17_05_2019__11_32_34','FLU__Frey__18_05_2019__12_42_56',...
%     'FLU__Frey__20_05_2019__10_09_21','FLU__Frey__21_05_2019__10_08_25',...
%     'FLU__Frey__22_05_2019__10_38_25','FLU__Frey__24_05_2019__07_42_54',...
%     'FLU__Frey__28_05_2019__10_06_06','FLU__Frey__31_05_2019__11_23_06',...
%     'FLU__Frey__03_06_2019__10_36_30','FLU__Frey__04_06_2019__11_01_08',...
%     'FLU__Frey__06_06_2019__10_21_22','FLU__Frey__07_06_2019__10_42_01',...
%     };

%%%%%%%%%%%%%%%%%
%%%---Wotan---%%%
%%%%%%%%%%%%%%%%%
%---Eye tracking w/ 0 irrelevant features---%
% monkeyname = 'Wotan';
% sess_file_names = {'CFS__Wotan__20_03_2019__12_46_17',...
%     'CFS__Wotan__21_03_2019__13_42_40','FLU__Wotan__22_03_2019__12_51_44',...
%     'FLU__Wotan__23_03_2019__15_57_44','FLU__Wotan__25_03_2019__12_20_35',...
%     'FLU__Wotan__26_03_2019__12_22_36','FLU__Wotan__27_03_2019__12_44_25'};

%---Eye tracking w/ 1 irrelevant feature---%
% monkeyname = 'Wotan';
% sess_file_names = {'FLU__Wotan__28_03_2019__12_38_16','FLU__Wotan__01_04_2019__13_18_43',...
%     'FLU__Wotan__03_04_2019__13_00_04',...
%     'FLU__Wotan__07_04_2019__12_33_56','FLU__Wotan__08_04_2019__13_05_27',...
%     'FLU__Wotan__10_04_2019__12_47_14','FLU__Wotan__11_04_2019__13_18_53',...
%     'FLU__Wotan__12_04_2019__12_57_02','FLU__Wotan__13_04_2019__13_26_54',...
%     };

%---Eye tracking w/ 1-2 irrelevant feature---%
% monkeyname = 'Wotan';
% sess_file_names = {'FLU__Wotan__16_04_2019__12_07_05','FLU__Wotan__18_04_2019__12_40_04',...
%     'FLU__Wotan__20_04_2019__13_55_27','FLU__Wotan__22_04_2019__13_54_04',...
%     'FLU__Wotan__23_04_2019__12_49_33','FLU__Wotan__25_04_2019__12_27_30',...
%     'FLU__Wotan__26_04_2019__12_55_02','FLU__Wotan__27_04_2019__13_32_07',...
%     'FLU__Wotan__29_04_2019__13_05_45','FLU__Wotan__30_04_2019__12_27_22',...
%     'FLU__Wotan__03_05_2019__12_55_04','FLU__Wotan__04_05_2019__13_53_06',...
% };

%---Eye tracking w/ 2 irrelevant feature---%
% monkeyname = 'Wotan';
% sess_file_names = {'FLU__Wotan__06_05_2019__12_35_51','FLU__Wotan__07_05_2019__13_22_29',...
%     'FLU__Wotan__08_05_2019__12_59_05','FLU__Wotan__09_05_2019__12_57_41',...
%     'FLU__Wotan__10_05_2019__13_00_14','FLU__Wotan__11_05_2019__13_47_27',...
%     'FLU__Wotan__13_05_2019__12_42_34','FLU__Wotan__14_05_2019__13_17_05',...
%     };


%---Eye tracking w/0,1, and 2 irrelevant features---%
% monkeyname = 'Wotan 0-2 Irrel';
% sess_file_names = {'CFS__Wotan__20_03_2019__12_46_17',...
%     'CFS__Wotan__21_03_2019__13_42_40','FLU__Wotan__22_03_2019__12_51_44',...
%     'FLU__Wotan__23_03_2019__15_57_44','FLU__Wotan__25_03_2019__12_20_35',...
%     'FLU__Wotan__26_03_2019__12_22_36','FLU__Wotan__27_03_2019__12_44_25',...
%     'FLU__Wotan__28_03_2019__12_38_16','FLU__Wotan__01_04_2019__13_18_43',...
%     'FLU__Wotan__03_04_2019__13_00_04',...
%     'FLU__Wotan__07_04_2019__12_33_56','FLU__Wotan__08_04_2019__13_05_27',...
%     'FLU__Wotan__10_04_2019__12_47_14','FLU__Wotan__11_04_2019__13_18_53',...
%     'FLU__Wotan__12_04_2019__12_57_02','FLU__Wotan__13_04_2019__13_26_54',...
%     'FLU__Wotan__16_04_2019__12_07_05','FLU__Wotan__17_04_2019__11_59_02',...
%     'FLU__Wotan__18_04_2019__12_40_04','FS__Wotan__19_04_2019__12_31_28',...
%     'FLU__Wotan__20_04_2019__13_55_27','FLU__Wotan__22_04_2019__13_54_04',...
%     'FLU__Wotan__23_04_2019__12_49_33','FLU__Wotan__25_04_2019__12_27_30',...
%     'FLU__Wotan__26_04_2019__12_55_02','FLU__Wotan__27_04_2019__13_32_07',...
%     'FLU__Wotan__29_04_2019__13_05_45','FLU__Wotan__30_04_2019__12_27_22',...
%     'FLU__Wotan__03_05_2019__12_55_04','FLU__Wotan__04_05_2019__13_53_06',...
%     ...
%     'FLU__Wotan__06_05_2019__12_35_51','FLU__Wotan__07_05_2019__13_22_29',...
%     'FLU__Wotan__08_05_2019__12_59_05','FLU__Wotan__09_05_2019__12_57_41',...
%     'FLU__Wotan__10_05_2019__13_00_14','FLU__Wotan__11_05_2019__13_47_27',...
%     'FLU__Wotan__13_05_2019__12_42_34','FLU__Wotan__14_05_2019__13_17_05',...
%     };


%%%%%%%%%%%%%%%%%%
%%%---Reider---%%%
%%%%%%%%%%%%%%%%%%
%---0 Irrelevant---%
% monkeyname = 'Reider';
% sess_file_names = {'FLU__Reider__21_03_2019__11_15_49','FLU__Reider__27_03_2019__09_04_07',...
%     'FLU__Reider__28_03_2019__08_11_58','FLU__Reider__01_04_2019__10_31_48',...
%     'FLU__Reider__03_04_2019__09_48_55','FLU__Reider__04_04_2019__08_59_18',...
%     'FLU__Reider__05_04_2019__10_40_14','FLU__Reider__09_04_2019__11_33_04',...
% };

%---1 Irrelevant---%
%his last 10 sets
% monkeyname = 'Reider';
% sess_file_names = {'FLU__Reider__11_06_2019__09_15_28','FLU__Reider__12_06_2019__10_25_54',...
%    'FLU__Reider__13_06_2019__07_09_34','FLU__Reider__14_06_2019__07_55_03',...
%    'FLU__Reider__18_06_2019__10_42_21','FLU__Reider__19_06_2019__09_19_18',...
%    'FLU__Reider__20_06_2019__07_29_07','FLU__Reider__21_06_2019__07_53_45',...
%    'FLU__Reider__24_06_2019__09_51_06','FLU__Reider__25_06_2019__08_00_47',...
%    };

%his 11 "best" sets
% monkeyname = 'Reider';
% sess_file_names = {'FLU__Reider__18_04_2019__09_32_55','FLU__Reider__30_04_2019__09_23_55',...
%     'FLU__Reider__02_05_2019__09_53_28','FLU__Reider__13_05_2019__09_23_29',...
%     'FLU__Reider__14_05_2019__08_23_29','FLU__Reider__23_05_2019__07_54_19',...
%     'FLU__Reider__24_05_2019__07_24_23','FLU__Reider__31_05_2019__07_53_35',...
%     'FLU__Reider__03_06_2019__07_21_26','FLU__Reider__18_06_2019__10_42_21',...
%     'FLU__Reider__20_06_2019__07_29_07',...
%     };

%---1-2 Irrelevant---%
% monkeyname = 'Reider';
% sess_file_names = {'FLU__Reider__26_06_2019__07_24_53','FLU__Reider__27_06_2019__07_50_28',...
%     'FLU__Reider__28_06_2019__09_20_05','FLU__Reider__11_07_2019__08_24_00',...
%     'FLU__Reider__25_07_2019__07_14_31','FLU__Reider__26_07_2019__08_04_21',... 
% };

% %---0-2 Irrelevant---%
%monkeyname = 'Reider';
% sess_file_names = {'FLU__Reider__12_09_2019__10_58_57','FLU__Reider__13_09_2019__11_07_03',...
%     'FLU__Reider__16_09_2019__07_30_22','FLU__Reider__19_09_2019__11_51_26',...
%     'FLU__Reider__25_09_2019__10_56_57','FLU__Reider__26_09_2019__10_48_56',...
%     'FLU__Reider__30_09_2019__10_09_32','FLU__Reider__02_10_2019__10_41_48'...
%     'FLU__Reider__03_10_2019__10_26_22','FLU__Reider__04_10_2019__10_17_08',...
%     'FLU__Reider__10_10_2019__10_25_21','FLU__Reider__11_10_2019__10_33_32',...
%     };


%%%%%%%%%%%%%%%%
%%%---Igor---%%%
%%%%%%%%%%%%%%%%
%---0 Irrelevant---%
% monkeyname = 'Igor';
% sess_file_names = {'FLU__Igor__29_03_2019__08_57_59','FLU__Igor__01_04_2019__09_00_46',...
%     'FLU__Igor__02_04_2019__08_21_16','FLU__Igor__03_04_2019__08_04_24',...
%     'FLU__Igor__05_04_2019__08_59_11','FLU__Igor__08_04_2019__09_14_38'};

%---1 Irrelevant---%
% monkeyname = 'Igor';
% sess_file_names = {'FLU__Igor__10_04_2019__09_04_06','FLU__Igor__23_04_2019__12_07_06',...
%     'FLU__Igor__29_04_2019__07_08_33','FLU__Igor__30_04_2019__11_51_59',...
%     'FLU__Igor__02_05_2019__07_33_24','FLU__Igor__06_05_2019__08_14_07',...
%     'FLU__Igor__09_05_2019__09_32_57','FLU__Igor__13_05_2019__07_16_44',...
%     'FLU__Igor__15_05_2019__09_33_33',...
%     };

%---1-2 Irrelevant---%
% monkeyname = 'Igor';
% sess_file_names = {'FLU__Igor__23_05_2019__10_11_31','FLU__Igor__24_05_2019__09_58_28',...
%     'FLU__Igor__29_05_2019__10_34_46','FLU__Igor__07_06_2019__08_02_25',...
%     'FLU__Igor__14_06_2019__10_00_53','FLU__Igor__18_06_2019__08_51_21',...
%     'FLU__Igor__19_06_2019__07_28_31',...
% };

%---2 Irrelevant---%
% monkeyname = 'Igor';
% sess_file_names = {'FLU__Igor__24_06_2019__07_54_46','FLU__Igor__26_06_2019__09_28_13',...
%     'FLU__Igor__28_06_2019__07_21_58','FLU__Igor__03_07_2019__09_51_04',...
%     'FLU__Igor__09_07_2019__12_06_22','FLU__Igor__12_07_2019__10_41_06',...
%     'FLU__Igor__13_07_2019__10_33_29',...
%     };

%sets after switching to new kiosk to look for reduced spatial biases
% monkeyname = 'Igor';
% sess_file_names = {'FLU__Igor__03_07_2019__09_51_04','FLU__Igor__04_07_2019__12_38_24',...
%     'FLU__Igor__05_07_2019__08_52_02','FLU__Igor__08_07_2019__12_05_04',...
%     'FLU__Igor__09_07_2019__12_06_22','FLU__Igor__10_07_2019__10_41_14',...
%     'FLU__Igor__11_07_2019__10_47_15'};

    
%%%%%%%%%%%%%%%%%%
%%%---Sindri---%%%
%%%%%%%%%%%%%%%%%%
%---0 irrelevant----%
% monkeyname = 'Sindri';
% sess_file_names = {'FLU__Sindri__27_03_2019__08_20_29',...
%     'FLU__Sindri__28_03_2019__08_06_39','FLU__Sindri__29_03_2019__09_05_46',...
%     'FLU__Sindri__04_04_2019__08_52_30',...
%     'FLU__Sindri__05_04_2019__10_06_16','FLU__Sindri__08_04_2019__10_14_16',...
%     'FLU__Sindri__10_04_2019__09_08_13','FLU__Sindri__10_04_2019__09_08_13',...};

%---1 irrelevant---%
% monkeyname = 'Sindri';
% sess_file_names = {'FLU__Sindri__16_04_2019__11_51_56','FLU__Sindri__02_05_2019__08_15_09',...
%     'FLU__Sindri__03_05_2019__07_17_49','FLU__Sindri__08_05_2019__08_02_46',...
%     'FLU__Sindri__10_05_2019__07_19_02','FLU__Sindri__15_05_2019__07_31_05',...
%     'FLU__Sindri__21_05_2019__09_43_17','FLU__Sindri__22_05_2019__08_43_27',...
%     'FLU__Sindri__23_05_2019__07_49_41','FLU__Sindri__24_05_2019__07_19_07',...
%     'FLU__Sindri__28_05_2019__09_34_43','FLU__Sindri__03_06_2019__07_28_42',...
%     'FLU__Sindri__04_06_2019__09_35_07','FLU__Sindri__19_06_2019__07_32_29',...
% };

%---1-2 irrelevant---%
% monkeyname = 'Sindri';
% sess_file_names = {'FLU__Sindri__24_06_2019__07_50_17','FLU__Sindri__26_06_2019__07_15_16',...
%     'FLU__Sindri__02_07_2019__09_53_53','FLU__Sindri__11_07_2019__08_35_55',...
%     'FLU__Sindri__19_07_2019__07_33_01','FLU__Sindri__24_07_2019__07_26_31',...
%     'FLU__Sindri__27_07_2019__11_00_36',...
% };

%%%%%%%%%%%%%%%%
%%%---Bard---%%%
%%%%%%%%%%%%%%%%
%---0 irrelevant---%
% monkeyname = 'Bard';
% sess_file_names = {'FLU__Bard__03_04_2019__10_09_40',...
%     'FLU__Bard__04_04_2019__10_40_09','FLU__Bard__05_04_2019__09_07_58',...
%     'FLU__Bard__09_04_2019__10_38_44',...
%     'FLU__Bard__11_04_2019__11_28_50','FLU__Bard__15_04_2019__09_55_12',...
%     'FLU__Bard__16_04_2019__09_39_53','FLU__Bard__17_04_2019__10_33_13',...
%     'FLU__Bard__18_04_2019__09_34_07','FLU__Bard__19_04_2019__09_12_27',...
%     };

%---1 irrelevant---%
% monkeyname = 'Bard';
% sess_file_names = {'FLU__Bard__23_04_2019__09_51_00','FLU__Bard__04_06_2019__11_26_32',...
%     'FLU__Bard__12_06_2019__10_24_18','FLU__Bard__13_06_2019__09_17_48',...
%     'FLU__Bard__26_06_2019__09_13_15','FLU__Bard__27_06_2019__09_45_34',...
%     'FLU__Bard__01_07_2019__11_34_08',...
%     };


% %---1-2 Irrel---%
% % monkeyname = 'Bard';
% % sess_file_names = {'FLU__Bard__09_07_2019__12_17_08',...
% %     'FLU__Bard__11_07_2019__10_45_25','FLU__Bard__13_07_2019__12_15_08',...
% %     'FLU__Bard__15_07_2019__09_46_57','FLU__Bard__16_07_2019__11_16_13',...
% %     'FLU__Bard__18_07_2019__12_44_55','FLU__Bard__26_07_2019__10_08_26',...
% %     'FLU__Bard__27_07_2019__13_17_36',...
% % };
% 
% monkeyname = 'Bard';
% sess_file_names = {'FLU__Bard__04_09_2019__11_37_22','FLU__Bard__05_09_2019__11_41_19',...
%     'FLU__Bard__06_09_2019__10_09_49','FLU__Bard__09_09_2019__10_34_52',...
%     'FLU__Bard__03_09_2019__11_25_39','FLU__Bard__10_09_2019__09_51_53',...
%     'FLU__Bard__11_09_2019__11_56_04','FLU__Bard__12_09_2019__11_44_02'};

%---0-2 Irrelevant (mastery sets)---%
monkeyname = 'Bard';
sess_file_names = {'FLU__Bard__04_10_2019__10_11_12','FLU__Bard__08_10_2019__11_27_55',...
    'FLU__Bard__09_10_2019__11_07_16','FLU__Bard__14_10_2019__12_04_34',...
    'FLU__Bard__15_10_2019__12_20_19',...
    };


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%---Multiple Monkeys---%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% monkeyname = 'All 0 Irreleveant';
% sess_file_names = {'FLU__Frey__15_02_2019__11_59_59','FLU__Frey__16_02_2019__12_31_20',...
%     'FLU__Frey__18_02_2019__11_09_46','FLU__Frey__25_02_2019__10_59_03','FLU__Frey__26_02_2019__11_09_32',...
%     'FLU__Frey__28_02_2019__10_31_32','FLU__Frey__01_03_2019__11_40_45','FLU__Frey__02_03_2019__12_50_48',...
%     'FLU__Frey__07_03_2019__11_00_10','FLU__Frey__08_03_2019__10_15_22','FLU__Frey__09_03_2019__10_41_20',...
%     'FLU__Frey__11_03_2019__11_39_47','FLU__Frey__12_03_2019__10_02_12','FLU__Frey__13_03_2019__10_21_17',...
%     'CFS__Wotan__20_03_2019__12_46_17','CFS__Wotan__21_03_2019__13_42_40','FLU__Wotan__22_03_2019__12_51_44',...
%     'FLU__Wotan__23_03_2019__15_57_44','FLU__Wotan__25_03_2019__12_20_35',...
%     'FLU__Sindri__27_03_2019__08_20_29',...
%     'FLU__Sindri__28_03_2019__08_06_39','FLU__Sindri__29_03_2019__09_05_46',...
%     'FLU__Sindri__04_04_2019__08_52_30',...
%     'FLU__Sindri__05_04_2019__10_06_16','FLU__Sindri__08_04_2019__10_14_16',...
%     'FLU__Sindri__10_04_2019__09_08_13','FLU__Sindri__10_04_2019__09_08_13',...
%     'FLU__Igor__29_03_2019__08_57_59','FLU__Igor__01_04_2019__09_00_46',...
%     'FLU__Igor__02_04_2019__08_21_16','FLU__Igor__03_04_2019__08_04_24',...
%     'FLU__Igor__05_04_2019__08_59_11','FLU__Igor__08_04_2019__09_14_38',...
%     'FLU__Reider__21_03_2019__11_15_49','FLU__Reider__27_03_2019__09_04_07',...
%     'FLU__Reider__28_03_2019__08_11_58','FLU__Reider__01_04_2019__10_31_48',...
%     'FLU__Reider__03_04_2019__09_48_55','FLU__Reider__04_04_2019__08_59_18',...
%     'FLU__Reider__05_04_2019__10_40_14','FLU__Reider__09_04_2019__11_33_04',...
%     'FLU__Bard__03_04_2019__10_09_40',...
%     'FLU__Bard__04_04_2019__10_40_09','FLU__Bard__05_04_2019__09_07_58',...
%     'FLU__Bard__09_04_2019__10_38_44',...
%     'FLU__Bard__11_04_2019__11_28_50','FLU__Bard__15_04_2019__09_55_12',...
%     'FLU__Bard__16_04_2019__09_39_53','FLU__Bard__17_04_2019__10_33_13',...
%     'FLU__Bard__18_04_2019__09_34_07','FLU__Bard__19_04_2019__09_12_27',...
%    };

% monkeyname = 'All 1 Irreleveant';
% sess_file_names = {...
%     'FLU__Wotan__28_03_2019__12_38_16','FLU__Wotan__01_04_2019__13_18_43',...
%     'FLU__Wotan__03_04_2019__13_00_04',...
%     'FLU__Wotan__07_04_2019__12_33_56','FLU__Wotan__08_04_2019__13_05_27',...
%     'FLU__Wotan__10_04_2019__12_47_14','FLU__Wotan__11_04_2019__13_18_53',...
%     'FLU__Wotan__12_04_2019__12_57_02','FLU__Wotan__13_04_2019__13_26_54',...
%     ...
%     'FLU__Frey__14_03_2019__10_21_27','FLU__Frey__05_04_2019__10_56_11',...
%     'FLU__Frey__08_04_2019__11_28_39','FLU__Frey__18_04_2019__10_30_20',...
%     'FLU__Frey__19_04_2019__10_36_02','FLU__Frey__22_04_2019__12_08_08',...
%     'FLU__Frey__23_04_2019__10_38_39','FLU__Frey__24_04_2019__10_51_15',...
%     'FLU__Frey__25_04_2019__10_22_35','FLU__Frey__29_04_2019__11_12_34',...
%     'FLU__Frey__01_05_2019__10_36_51','FLU__Frey__03_05_2019__10_43_53',...
%     'FLU__Frey__04_05_2019__11_45_06','FLU__Frey__06_05_2019__10_26_26',...
%     ...
%     'FLU__Igor__10_04_2019__09_04_06','FLU__Igor__23_04_2019__12_07_06',...
%     'FLU__Igor__29_04_2019__07_08_33','FLU__Igor__30_04_2019__11_51_59',...
%     'FLU__Igor__02_05_2019__07_33_24','FLU__Igor__06_05_2019__08_14_07',...
%     'FLU__Igor__09_05_2019__09_32_57','FLU__Igor__13_05_2019__07_16_44',...
%     'FLU__Igor__15_05_2019__09_33_33',...
%     ...
%     'FLU__Sindri__16_04_2019__11_51_56','FLU__Sindri__02_05_2019__08_15_09',...
%     'FLU__Sindri__03_05_2019__07_17_49','FLU__Sindri__08_05_2019__08_02_46',...
%     'FLU__Sindri__10_05_2019__07_19_02','FLU__Sindri__15_05_2019__07_31_05',...
%     'FLU__Sindri__21_05_2019__09_43_17','FLU__Sindri__22_05_2019__08_43_27',...
%     'FLU__Sindri__23_05_2019__07_49_41','FLU__Sindri__24_05_2019__07_19_07',...
%     'FLU__Sindri__28_05_2019__09_34_43','FLU__Sindri__03_06_2019__07_28_42',...
%     'FLU__Sindri__04_06_2019__09_35_07','FLU__Sindri__19_06_2019__07_32_29',...
%         ...
%     'FLU__Reider__12_09_2019__10_58_57','FLU__Reider__13_09_2019__11_07_03',...
%     'FLU__Reider__16_09_2019__07_30_22','FLU__Reider__19_09_2019__11_51_26',...
%     'FLU__Reider__25_09_2019__10_56_57','FLU__Reider__26_09_2019__10_48_56',...
%     'FLU__Reider__27_09_2019__10_56_31','FLU__Reider__30_09_2019__10_09_32',...
%     'FLU__Reider__01_10_2019__12_41_37','FLU__Reider__02_10_2019__10_41_48',...
%     'FLU__Reider__03_10_2019__10_26_22','FLU__Reider__04_10_2019__10_17_08',...
%     'FLU__Reider__07_10_2019__10_16_20',...
%     'FLU__Reider__09_10_2019__10_13_37','FLU__Reider__10_10_2019__10_25_21',...
%     'FLU__Reider__11_10_2019__10_33_32',...
%     ...
%     'FLU__Bard__23_04_2019__09_51_00','FLU__Bard__04_06_2019__11_26_32',...
%     'FLU__Bard__12_06_2019__10_24_18','FLU__Bard__13_06_2019__09_17_48',...
%     'FLU__Bard__26_06_2019__09_13_15','FLU__Bard__27_06_2019__09_45_34',...
%     'FLU__Bard__01_07_2019__11_34_08',...
%     };

% monkeyname = 'All 1-2 Irreleveant';
% sess_file_names = {'FLU__Wotan__16_04_2019__12_07_05','FLU__Wotan__18_04_2019__12_40_04',...
%     'FLU__Wotan__20_04_2019__13_55_27','FLU__Wotan__22_04_2019__13_54_04',...
%     'FLU__Wotan__23_04_2019__12_49_33','FLU__Wotan__25_04_2019__12_27_30',...
%     'FLU__Wotan__26_04_2019__12_55_02','FLU__Wotan__27_04_2019__13_32_07',...
%     'FLU__Wotan__29_04_2019__13_05_45','FLU__Wotan__30_04_2019__12_27_22',...
%     'FLU__Wotan__03_05_2019__12_55_04','FLU__Wotan__04_05_2019__13_53_06',...
%     ...
%     'FLU__Igor__23_05_2019__10_11_31','FLU__Igor__24_05_2019__09_58_28',...
%     'FLU__Igor__29_05_2019__10_34_46','FLU__Igor__07_06_2019__08_02_25',...
%     'FLU__Igor__14_06_2019__10_00_53','FLU__Igor__18_06_2019__08_51_21',...
%     'FLU__Igor__19_06_2019__07_28_31',...
%     ...
%     'FLU__Frey__07_05_2019__10_27_39','FLU__Frey__08_05_2019__10_48_55',...
%     'FLU__Frey__09_05_2019__10_40_43','FLU__Frey__10_05_2019__10_23_23',...
%     'FLU__Frey__11_05_2019__11_50_02','FLU__Frey__13_05_2019__10_38_53',...
%     ...
%     'FLU__Sindri__24_06_2019__07_50_17','FLU__Sindri__26_06_2019__07_15_16',...
%     'FLU__Sindri__02_07_2019__09_53_53','FLU__Sindri__11_07_2019__08_35_55',...
%     'FLU__Sindri__19_07_2019__07_33_01','FLU__Sindri__24_07_2019__07_26_31',...
%     'FLU__Sindri__27_07_2019__11_00_36',...
%};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%-0 to 2 Irrelevants Sets---%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% monkeyname = 'All 0-2 Irrel';
% sess_file_names = {...
%     'FLU__Wotan__09_08_2019__12_45_49','FLU__Wotan__10_08_2019__13_59_45',...
%     'FLU__Wotan__12_08_2019__12_43_36','FLU__Wotan__13_08_2019__12_59_30',...
%     'FLU__Wotan__14_08_2019__13_11_27','FLU__Wotan__15_08_2019__13_33_35',...
%     'FLU__Wotan__16_08_2019__15_03_07','FLU__Wotan__17_08_2019__14_10_48',...
%     'FLU__Wotan__22_08_2019__12_01_22','FLU__Wotan__23_08_2019__11_55_25',...
%     'FLU__Wotan__24_08_2019__12_29_54','FLU__Wotan__25_08_2019__12_34_05',...
%     'FLU__Wotan__26_08_2019__11_30_10','FLU__Wotan__29_08_2019__12_16_49',...
%     'FLU__Wotan__30_08_2019__11_57_51','FLU__Wotan__05_09_2019__13_34_52',...
%     'FLU__Wotan__06_09_2019__13_14_40',...
%     ...
%     'FLU__Frey__29_08_2019__11_00_16','FLU__Frey__30_08_2019__10_41_35',...
%     'FLU__Frey__03_09_2019__11_37_33','FLU__Frey__04_09_2019__10_17_17',...
%     'FLU__Frey__05_09_2019__12_06_33','FLU__Frey__06_09_2019__11_59_25',...
%     'FLU__Frey__07_09_2019__11_38_47','FLU__Frey__09_09_2019__10_51_46',...
%     ...
%     'CFS__Igor__02_10_2019__08_58_56','CFS__Igor__03_10_2019__08_42_51',...
%     'CFS__Igor__04_10_2019__08_33_24','FLU__Igor__07_10_2019__08_43_30',...
%     'FLU__Igor__08_10_2019__09_04_22','FLU__Igor__09_10_2019__08_38_13',...
%     'FLU__Igor__10_10_2019__08_42_58','FLU__Igor__11_10_2019__08_50_11',...
%     'FLU__Igor__15_10_2019__08_49_50',...
%     ...
%     };

%%
%% Import Session Data
num_sessions = length(sess_file_names);
session_data = cell(1,num_sessions);
which_monkey = NaN(1,num_sessions);
for sess =1:num_sessions
    which_monkey(sess) = DetermineWhichMonkey(sess_file_names{sess});
    
    disp(['Loading data from session #' num2str(sess) '/' num2str(num_sessions)])
    log_dir = FindDataDirectory(sess_file_names{sess});
    if ~isempty(log_dir)
        session_data_file_name = [log_dir sess_file_names{sess} ...
            pre_processed_data_dir sess_file_names{sess} '_session_data.mat'];
        if ~exist(session_data_file_name,'file') && ~reimport_new
            session_data{sess} = ImportFLU_DataV2(sess_file_names{sess},log_dir,import_option);
        else
            load(session_data_file_name,'all_session_data');
            session_data{sess} = all_session_data;
        end
    else
        error(['Cant Find Log Dir for '  sess_file_names{sess}])
    end
end

%% Calculate Averege Performance by Block and Remove "Bad" Blocks
all_good_blocks = cell(1,num_sessions);
all_block_performance = cell(1,num_sessions);
all_session_block_performance = NaN(num_sessions,max_block_count);
for sess = 1:num_sessions
    [all_good_blocks{sess},all_block_performance{sess}] = ...
        RemoveBadBlocks(session_data{sess},miniumum_performance,max_block_cut,min_block_len,max_time_out);
    all_session_block_performance(sess,1:length(all_block_performance{sess})) = ...
        all_block_performance{sess};
end

figure
subplot(1,2,1)
plot(nanmean(all_session_block_performance))
xlabel('Block #')
ylabel('Performance-Proportion Correct')
title('Average Performance by Block')
box off

subplot(1,2,2)
hist(all_session_block_performance(:),20)
xlabel('Performance-Proportion Correct')
ylabel('Block Count')
title('Distribution of Block Performance')
box off

subtitle([monkeyname ': Performance by Block #'])
box off
%% Calculate perofrmance across all blocks
all_block_performance = [];
all_trial_count = NaN(length(sess_file_names),max_block_count);
all_extra_trial_count = NaN(length(sess_file_names),max_block_count);
all_block_end_performance = NaN(length(sess_file_names),max_block_count);

for sess = 1:num_sessions
    [block_performance,trial_count,extra_trial_count,block_end_performance] = ...
        CalculateBlockPerformance(session_data{sess},all_good_blocks{sess},min_block_len,last_trial_window);
    
    all_block_performance = [all_block_performance; block_performance];
    all_trial_count(sess,1:length(trial_count)) = trial_count;
    all_extra_trial_count(sess,1:length(extra_trial_count)) = extra_trial_count;
    all_block_end_performance(sess,1:length(block_end_performance)) = block_end_performance;
end

figure
subplot(2,2,1)
hold on
plot([0.5 0.5],[0 100],'k--')
plot([-(last_trial_window-1) min_block_len],[80 80],'k--')
plot([-(last_trial_window-1) min_block_len],[33 33],'k--')
plot([-(last_trial_window-1) min_block_len],[66 66],'k--')
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_block_performance)',3,2),'r')
plot(-(last_trial_window-1):min_block_len,100*nanmean(all_block_performance))
hold off
ylim([0 100])
xlabel('Trial # Relative To Switch')
ylabel('Performance (% Correct)')
box off
title(sprintf(['Average Performance-All blocks \n' ...
    'Average_{20-30} = ' num2str(100*mean(nanmean(all_block_performance(:,end-9:end))),3) '%%, ' ...
    'Average_{Last 10} = ' num2str(100*mean(nanmean(all_block_performance(:,1:10))),3) '%%']));

subplot(2,2,2)
histogram(all_trial_count(:),30:60)
xlabel('Trials in Block')
ylabel('Block Count')
title('Total Trial Count per Block')
box off

subplot(2,2,3)
histogram(all_extra_trial_count(:),0:30)
xlabel('Extra Trials in Block')
ylabel('Block Count')
title('Extra Trial Count per block')
box off

subplot(2,2,4)
histogram(100*all_block_end_performance(:),-5:10:95)
xlabel('Performance (% Correct)')
ylabel('Block Count')
box off
title('Peformance at End of Block')

subtitle(monkeyname)

%% Calculate Search Duration/Response Time Measurements for All Blocks
all_search_duration = [];
all_correct_search_duration = [];
all_incorrect_search_duration = [];
all_incorrect_last_trial_search_duration = [];
all_correct_last_trial_search_duration = [];

all_correct_n1_incorrect_search_duration = [];
all_correct_n1_correct_search_duration = [];
all_incorrect_n1_incorrect_search_duration = [];
all_incorrect_n1_correct_search_duration = [];

for sess = 1:num_sessions
    [block_search_duration,block_correct_search_duration,block_incorrect_search_duration,...
        block_incorrect_last_trial,block_correct_last_trial,...
        correct_incorrect_minus_one_search_duration,correct_correct_minus_one_search_duration,...
        incorrect_incorrect_minus_one_search_duration,incorrect_correct_minus_one_search_duration] = ...
        CalculateBlockSearchDuration(session_data{sess},all_good_blocks{sess},min_block_len,last_trial_window);
    
    all_search_duration = [all_search_duration; block_search_duration];
    all_correct_search_duration = [all_correct_search_duration; block_correct_search_duration];
    all_incorrect_search_duration = [all_incorrect_search_duration; block_incorrect_search_duration];
    all_incorrect_last_trial_search_duration = [all_incorrect_last_trial_search_duration; block_incorrect_last_trial];
    all_correct_last_trial_search_duration = [all_correct_last_trial_search_duration; block_correct_last_trial];
    
    all_correct_n1_incorrect_search_duration = [all_correct_n1_incorrect_search_duration; correct_incorrect_minus_one_search_duration];
    all_correct_n1_correct_search_duration = [all_correct_n1_correct_search_duration; correct_correct_minus_one_search_duration];
    all_incorrect_n1_incorrect_search_duration = [all_incorrect_n1_incorrect_search_duration; incorrect_incorrect_minus_one_search_duration];
    all_incorrect_n1_correct_search_duration = [all_incorrect_n1_correct_search_duration; incorrect_correct_minus_one_search_duration];
end

figure
subplot(2,2,1)
hold on
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_search_duration)',3,2),'r')
plot(-(last_trial_window-1):min_block_len,nanmean(all_search_duration))
yl = ylim;
plot([0.5 0.5],[yl(1) yl(2)],'k--')
hold off
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (sec)')
box off
title(sprintf(['Average Search Duration-All blocks \n' ...
    'Average_{20-30} = ' num2str(mean2(all_search_duration(:,end-9:end)),3) ' sec, ' ...
    'Average_{Last 10} = ' num2str(mean2(all_search_duration(:,1:10)),3) ' sec']))


subplot(2,2,2)
hold on
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_search_duration)',3,2),'k')
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_correct_search_duration)',3,2),'g')
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_incorrect_search_duration)',3,2),'r')
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_incorrect_last_trial_search_duration)',3,2),'b')
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_correct_last_trial_search_duration)',3,2),'m')
yl = ylim;
plot([0.5 0.5],[yl(1) yl(2)],'k--')
hold off
legend('All','Correct','Incorrect','Incorrect Last Trial','Correct Last Trial')
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (sec)')
box off
title('Search Duration by Performance')

subplot(2,2,3)
hold on
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_search_duration)',3,2),'k')
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_correct_n1_correct_search_duration)',3,2),'g')
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_correct_n1_incorrect_search_duration)',3,2),'b')
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_incorrect_n1_correct_search_duration)',3,2),'m')
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_incorrect_n1_incorrect_search_duration)',3,2),'r')
yl = ylim;
plot([0.5 0.5],[yl(1) yl(2)],'k--')
hold off
legend('All','Correct & N-1 Correct','Correct & N-1 Incorrect','Incorrect & N-1 Correct','Incorrect & N-1 Incorrect')
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (sec)')
box off
title('Search Duration by Performance')

subplot(2,2,4)
bar([nanmean(all_search_duration(:)),...
    nanmean(all_correct_search_duration(:)),nanmean(all_correct_n1_correct_search_duration(:)),nanmean(all_correct_n1_incorrect_search_duration(:)),...
    nanmean(all_incorrect_search_duration(:)),nanmean(all_incorrect_n1_correct_search_duration(:)),nanmean(all_incorrect_n1_incorrect_search_duration(:))])
xticks(1:7)
xticklabels({'All',...
    'All Correct','Correct & N-1 Correct','Correct & N-1 InCorrect',...
    'All Incorrect','Incorrect & N-1 Correct','Incorrect & N-1 Incorrect'})
xtickangle(30)
ylim(yl)
ylabel('Search Duration (sec)')
box off
title('Search Duration by Performance Across All Trials')

subtitle(monkeyname)
%% Calculate Performance for Intra-vs Extra-dimensional shifts and Cued vs Uncued Shifts
all_intra_performance = [];
all_extra_performance = [];

all_cued_intra_performance = [];
all_cued_extra_performance = [];
all_uncued_intra_performance = [];
all_uncued_extra_performance = [];

all_intra_search_duration = [];
all_extra_search_duration = [];

all_cued_intra_search_duration = [];
all_cued_extra_search_duration = [];
all_uncued_intra_search_duration = [];
all_uncued_extra_search_duration = [];

for sess = 1:num_sessions
    [intra_extra_block_performance,intra_extra_search_duration,...
        intra_extra_shift,cued_uncued_shift] = CalculateBlockPerformanceIntra_vs_Extra_Dim(...
        session_data{sess},all_good_blocks{sess},min_block_len,last_trial_window);
    
    all_intra_performance = [all_intra_performance; intra_extra_block_performance(intra_extra_shift == 1,:)];
    all_extra_performance = [all_extra_performance; intra_extra_block_performance(intra_extra_shift == 2,:)];
    
    all_cued_intra_performance = [all_cued_intra_performance; intra_extra_block_performance(intra_extra_shift == 1 & cued_uncued_shift == 1,:)];
    all_cued_extra_performance = [all_cued_extra_performance; intra_extra_block_performance(intra_extra_shift == 2 & cued_uncued_shift == 1,:)];
    all_uncued_intra_performance = [all_uncued_intra_performance; intra_extra_block_performance(intra_extra_shift == 1 & cued_uncued_shift == 0,:)];
    all_uncued_extra_performance = [all_uncued_extra_performance; intra_extra_block_performance(intra_extra_shift == 2 & cued_uncued_shift == 0,:)];
    
    all_intra_search_duration = [all_intra_search_duration; intra_extra_search_duration(intra_extra_shift == 1,:)];
    all_extra_search_duration = [all_extra_search_duration; intra_extra_search_duration(intra_extra_shift == 2,:)];
    
    all_cued_intra_search_duration = [all_cued_intra_search_duration; intra_extra_search_duration(intra_extra_shift == 1 & cued_uncued_shift == 1,:)];
    all_cued_extra_search_duration = [all_cued_extra_search_duration; intra_extra_search_duration(intra_extra_shift == 2 & cued_uncued_shift == 1,:)];
    all_uncued_intra_search_duration = [all_uncued_intra_search_duration; intra_extra_search_duration(intra_extra_shift == 1 & cued_uncued_shift == 0,:)];
    all_uncued_extra_search_duration = [all_uncued_extra_search_duration; intra_extra_search_duration(intra_extra_shift == 2 & cued_uncued_shift == 0,:)];
    
end

if size(all_cued_intra_search_duration,1) ~= 0 && size(all_uncued_intra_search_duration,1) ~= 0
    %---Plots for Intradimensional vs Extradimensional---%
    figure
    subplot(2,2,1)
    hold on
    plot([0.5 0.5],[0 100],'k--')
    plot([-(last_trial_window-1) min_block_len],[80 80],'k--')
    plot([-(last_trial_window-1) min_block_len],[33 33],'k--')
    plot([-(last_trial_window-1) min_block_len],[66 66],'k--')
    p1 = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_intra_performance)',3,2),'r');
    p2 = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_extra_performance)',3,2),'b');
    hold off
    ylim([0 100])
    xlabel('Trial # Relative To Switch')
    ylabel('Performance (% Correct)')
    box off
    title('Performance: Intra vs Extra-Dim Shifts')
    legend([p1 p2],{'Intra','Extra'},'Location','SouthEast')
    
    subplot(2,2,3)
    plot(-(last_trial_window-1):min_block_len,FilterLearningCurves((100*nanmean(all_extra_performance)-100*nanmean(all_intra_performance))',3,2),'m');
    hold on
    yl = ylim;
    plot([0 0],[yl(1) yl(2)],'k--')
    hold off
    xlabel('Trial # Relative To Switch')
    ylabel('Performance (% Correct)')
    box off
    title('Performance Difference: Extra-Intra')
    
    subplot(2,2,2)
    hold on
    plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_intra_search_duration)',3,2),'r');
    plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_extra_search_duration)',3,2),'b');
    yl = ylim;
    plot([0 0],[yl(1) yl(2)],'k--')
    hold off
    xlabel('Trial # Relative To Switch')
    ylabel('Search Duration (sec)')
    box off
    title('Search Duration: Intra vs Extra-Dim Shifts')
    
    subplot(2,2,4)
    plot(-(last_trial_window-1):min_block_len,FilterLearningCurves((nanmean(all_extra_search_duration)-nanmean(all_intra_search_duration))',3,1),'m');
    hold on
    yl = ylim;
    plot([0 0],[yl(1) yl(2)],'k--')
    hold off
    xlabel('Trial # Relative To Switch')
    ylabel('Search Duration (Sec)')
    box off
    title('Search Duration Difference: Extra-Intra')
    
    subtitle(monkeyname)
    
    %---Plots for Cued vs Uncued & Intradimensional vs Extradimensional---%
    figure
    subplot(2,2,1)
    hold on
    plot([0.5 0.5],[0 100],'k--')
    plot([-(last_trial_window-1) min_block_len],[80 80],'k--')
    plot([-(last_trial_window-1) min_block_len],[33 33],'k--')
    plot([-(last_trial_window-1) min_block_len],[66 66],'k--')
    p1 = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_cued_intra_performance)',3,2),'r');
    p2 = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_uncued_intra_performance)',3,2),'m');
    p3= plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_cued_extra_performance)',3,2),'b');
    p4 = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_uncued_extra_performance)',3,2),'g');
    hold off
    ylim([0 100])
    xlabel('Trial # Relative To Switch')
    ylabel('Performance (% Correct)')
    box off
    title('Performance: Intra/Extra & Cued/Uncued Shifts')
    legend([p1 p2 p3 p4],{'Cued Intra','Uncued Intra','Cued Extra','Uncued Extra'},'Location','SouthEast')
    
    subplot(2,2,3)
    hold on
    plot(-(last_trial_window-1):min_block_len,FilterLearningCurves((100*nanmean(all_cued_intra_performance)-100*nanmean(all_uncued_intra_performance))',3,1),'c');
    plot(-(last_trial_window-1):min_block_len,FilterLearningCurves((100*nanmean(all_cued_extra_performance)-100*nanmean(all_uncued_extra_performance))',3,1),'k');
    yl = ylim;
    plot([0 0],[yl(1) yl(2)],'k--')
    hold off
    xlabel('Trial # Relative To Switch')
    ylabel('Performance (% Correct)')
    box off
    title('Performance Difference: Cued-Uncued')
    legend({'Intra','Extra'},'Location','SouthEast')
    
    subplot(2,2,2)
    hold on
    p1 = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_cued_intra_search_duration)',3,2),'r');
    p2 = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_uncued_intra_search_duration)',3,2),'m');
    p3= plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_cued_extra_search_duration)',3,2),'b');
    p4 = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_uncued_extra_search_duration)',3,2),'g');
    yl = ylim;
    plot([0 0],[yl(1) yl(2)],'k--')
    hold off
    xlabel('Trial # Relative To Switch')
    ylabel('Search Duration (sec)')
    box off
    title('Search Duration: Intra/Extra & Cued/Uncued Shifts')
    
    subplot(2,2,4)
    hold on
    plot(-(last_trial_window-1):min_block_len,FilterLearningCurves((nanmean(all_cued_intra_search_duration)-nanmean(all_uncued_intra_search_duration))',3,1),'c');
    plot(-(last_trial_window-1):min_block_len,FilterLearningCurves((nanmean(all_cued_extra_search_duration)-nanmean(all_uncued_extra_search_duration))',3,1),'k');
    yl = ylim;
    plot([0 0],[yl(1) yl(2)],'k--')
    hold off
    xlabel('Trial # Relative To Switch')
    ylabel('Search Duration (sec)')
    box off
    title('Search Duration Difference: Cued-Uncued')
    
    subtitle(monkeyname)
end
%% Stats shift type intra-vs-extra
% stat_win = [4 20]+10;%trials 4-20 after the shift igore 1st since could be at chance
% observed_diff = sum(average_extra_performance(stat_win(1):stat_win(2))-average_intra_performance(stat_win(1):stat_win(2)));
% observed_diff_time = FilterLearningCurves(average_extra_performance',3,2)-FilterLearningCurves(average_intra_performance',3,2);
%
% shuffled_diff = NaN(1,num_shuffs);
% shuffled_diff_time = NaN(num_shuffs,40);
% all_shift_data = [all_extra_block_performance; all_intra_block_performance];
% all_shift_ind = [ones(total_num_extra_blocks,1); 2*ones(total_num_intra_blocks,1)];
% for shuff = 1:num_shuffs
%     shuff_ind = all_shift_ind(randperm(length(all_shift_ind)));
%     shuff_intra = nanmean(all_shift_data(shuff_ind == 1,:));
%     shuff_extra = nanmean(all_shift_data(shuff_ind == 2,:));
%     shuffled_diff(shuff) = sum(shuff_extra(stat_win(1):stat_win(2))-shuff_intra(stat_win(1):stat_win(2)));
%
%     shuffled_diff_time(shuff,:) = FilterLearningCurves(shuff_extra',3,2)-FilterLearningCurves(shuff_intra',3,2);
% end
% prct_intra_extra = 100*sum(observed_diff > shuffled_diff)/num_shuffs;
%
% time_95pct = NaN(40,1);
% for tp = 1:40
%     time_95pct(tp) = prctile(shuffled_diff_time(:,tp),95);
% end
%
% sig_time_points = find(observed_diff_time > time_95pct);
% sig_time_points(sig_time_points <= 14) = [];

%% Plot performance and search duration by monkey
[block_performance_by_monkey,search_duration_by_monkey] = ...
    CalculateBlockPerformancebyMonkey(session_data,all_good_blocks,min_block_len,...
    last_trial_window,which_monkey);

%% Plot performance and search duration by target and distractor dimensions
[block_performance_by_dim,search_duration_by_dim] = ...
    CalculateBlockPerformancebyDimension(session_data,all_good_blocks,min_block_len,...
    last_trial_window);

%% Learning Point anlayses
all_Learning_Points = [];
all_LP_aligned_block_performance = [];
all_LP_aligned_search_duration = [];
all_block_performance_LP_less_10 = [];
all_block_performance_LP_less_20 = [];
all_block_performance_LP_less_30 = [];
all_block_performance_LP_greater30 = [];
all_block_performance_no_LP = [];
for sess = 1:num_sessions
    [LP_aligned_block_performance,LP_aligned_search_duration,Learning_Points,...
        block_performance_LP_less_10,block_performance_LP_less_20,block_performance_LP_less_30,...
        block_performance_LP_greater30,block_performance_no_LP] = ...
        CalculateLearningPoint(session_data{sess},all_good_blocks{sess},min_block_len,last_trial_window,...
        forward_smoothing_window,learning_point_threshold,forward_back_trials);
    
    all_Learning_Points = [all_Learning_Points Learning_Points];
    all_LP_aligned_block_performance = [all_LP_aligned_block_performance; LP_aligned_block_performance];
    all_LP_aligned_search_duration = [all_LP_aligned_search_duration; LP_aligned_search_duration];
    
    all_block_performance_LP_less_10 = [all_block_performance_LP_less_10; block_performance_LP_less_10];
    all_block_performance_LP_less_20 = [all_block_performance_LP_less_20; block_performance_LP_less_20];
    all_block_performance_LP_less_30 = [all_block_performance_LP_less_30; block_performance_LP_less_30];
    all_block_performance_LP_greater30 = [all_block_performance_LP_greater30; block_performance_LP_greater30];
    all_block_performance_no_LP = [all_block_performance_no_LP; block_performance_no_LP];
end
all_Learning_Points = laundry(all_Learning_Points);
all_LP_aligned_block_performance = laundry(all_LP_aligned_block_performance);

LP_perform_legend_str = [];
LP_perform_legend_name = {'LP <= 10','LP > 10 & LP <= 20','LP > 20 & LP <= 30','LP > 30','LP DNE'};
for LPtype = 1:length(LP_perform_legend_name)
    if LPtype == 1
        n_str = size(all_block_performance_LP_less_10,1);
    elseif LPtype == 2
        n_str = size(all_block_performance_LP_less_20,1);
    elseif LPtype == 3
        n_str = size(all_block_performance_LP_less_30,1);
    elseif LPtype == 4
        n_str = size(all_block_performance_LP_greater30,1);
    elseif LPtype == 5
        n_str = size(all_block_performance_no_LP,1);
    end
    LP_perform_legend_str{LPtype} = [LP_perform_legend_name{LPtype} ', n = ' num2str(n_str)];
end

figure
subplot(2,2,1)
plot([0 0],[0 100],'k--')
hold on
plot([-forward_back_trials forward_back_trials],[33 33],'k')
plot(-forward_back_trials:forward_back_trials,100*nanmean(all_LP_aligned_block_performance))
hold off
xlabel('Trial from LP')
ylabel('Performance (% Correct)')
box off
axis square
xlim([-forward_back_trials forward_back_trials])
ylim([0 100])
title(['All Switches: Backward Learning Curve, n = ' num2str(size(all_LP_aligned_block_performance,1))])

subplot(2,2,2)
hist(all_Learning_Points,1:max(all_Learning_Points))
xlabel('LP Tria#')
ylabel('Block Count')
axis square
box off
title('All Switches: Learning Point Distribution')

subplot(2,2,3)
plot(-forward_back_trials:forward_back_trials,nanmean(all_LP_aligned_search_duration))
yl = ylim;
hold on
plot([0 0],[yl(1) yl(2)],'k--')
hold off
xlabel('Trial from LP')
ylabel('Search Duaration (sec)')
box off
axis square
xlim([-forward_back_trials forward_back_trials])
title('All Switches: Backwards Search Duration Curve')

subplot(2,2,4)
hold on
plot([0.5 0.5],[0 100],'k--')
plot([-(last_trial_window-1) min_block_len],[33 33],'k--')
plot([-(last_trial_window-1) min_block_len],[66 66],'k--')
p(1) = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_block_performance_LP_less_10)',3,2),'g');
p(2) = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_block_performance_LP_less_20)',3,2),'b');
p(3) = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_block_performance_LP_less_30)',3,2),'m');
p(4) = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_block_performance_LP_greater30)',3,2),'r');
p(5) = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_block_performance_no_LP)',3,2),'c');
hold off
box off
grid on
axis square
ylim([0 100])
xlabel('Trial # Relative To Switch')
ylabel('Performance (% Correct)')
title('Performance: by "Learning Rate"')
legend(p,LP_perform_legend_str,'Location','NorthEastOutside')

subtitle(monkeyname)

%% Plot Performacne by # of Irrelevant Feature Dimensions
all_LP_by_num_irrel = cell(1,3);
all_LP_aligned_block_performance_by_num_irrel = cell(1,3);
all_block_performance_by_num_irrel = cell(1,3);
all_search_duration_by_num_irrel = cell(1,3);
median_lps = NaN(num_sessions,3);
prop_unlearned_blocks = NaN(num_sessions,3);
for sess = 1:num_sessions
    [block_performance_by_num_irrel,search_duration_by_num_irrel,...
        LP_by_num_irrel,LP_aligned_block_performance_by_num_irrel] = ...
        CalculateBlockPerformancebyNumIrrelDim(session_data{sess},all_good_blocks{sess},min_block_len,last_trial_window,...
        forward_smoothing_window,learning_point_threshold,forward_back_trials);
    
    for num_irrel = 1:3
        median_lps(sess,num_irrel) = nanmedian(LP_by_num_irrel{num_irrel});
        prop_unlearned_blocks(sess,num_irrel) = sum(isnan(LP_by_num_irrel{num_irrel}))/length(LP_by_num_irrel{num_irrel});
        all_LP_by_num_irrel{num_irrel} = [all_LP_by_num_irrel{num_irrel} LP_by_num_irrel{num_irrel}];
        all_LP_aligned_block_performance_by_num_irrel{num_irrel} = ...
            [all_LP_aligned_block_performance_by_num_irrel{num_irrel}; LP_aligned_block_performance_by_num_irrel{num_irrel}];
        all_block_performance_by_num_irrel{num_irrel} = [all_block_performance_by_num_irrel{num_irrel}; ...
            block_performance_by_num_irrel{num_irrel}];
        all_search_duration_by_num_irrel{num_irrel} = [all_search_duration_by_num_irrel{num_irrel}; ...
            search_duration_by_num_irrel{num_irrel}];
    end
end

n_irrel = NaN(1,3);
for num_irrel = 1:3
    n_irrel(num_irrel) = size(all_block_performance_by_num_irrel{num_irrel},1);
end

%---Run Simple ANOVA on LP distributions---%
mean_all_lps = mean(median_lps);
sem_all_lps = std(median_lps)./sqrt(num_sessions);
[p_LP,tbl,stats] = anova1(median_lps);
multcompare(stats)


%---Plot Results by Number of irrelevant featurs
figure
subplot(2,3,1)
hold on
plot([0.5 0.5],[0 100],'k--')
plot([-(last_trial_window-1) min_block_len],[33 33],'k--')
plot([-(last_trial_window-1) min_block_len],[66 66],'k--')
plot([-(last_trial_window-1) min_block_len],[80 80],'k--')
for num_irrel = 1:3
    if n_irrel(num_irrel) > 0
        pnirr(num_irrel) = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_block_performance_by_num_irrel{num_irrel})',3,2));
    else
        pnirr(num_irrel) = plot(0,0);
    end
end
hold off
box off
ylim([0 100])
xlabel('Trial # Relative To Switch')
ylabel('Performance (% Correct)')
title('Performance by # of Irrelevant Dimensions')
legend(pnirr,{['0, n = ' num2str(n_irrel(1))],['1, n = ' num2str(n_irrel(2))],...
    ['2, n = ' num2str(n_irrel(3))]},'Location','SouthEast')

subplot(2,3,2)
hold on
for num_irrel = 1:3
    if n_irrel(num_irrel) > 0
        plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_search_duration_by_num_irrel{num_irrel})',3,2));
    else
        plot(0,1)
    end
end
yl = ylim;
plot([0.5 0.5],[yl(1) yl(2)],'k--')
hold off
box off
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (sec)')
title('Search Duration by # of Irrelevant Dimensions')

subplot(2,3,4)
hold on
plot([0 0],[0 100],'k--')
plot([-forward_back_trials forward_back_trials],[33 33],'k--')
plot([-forward_back_trials forward_back_trials],[66 66],'k--')
for num_irrel = 1:3
    plot(-forward_back_trials:forward_back_trials,100*nanmean(all_LP_aligned_block_performance_by_num_irrel{num_irrel}))
end
ylim([0 100])
xlim([-forward_back_trials forward_back_trials])
hold off
box off
xlabel('Trial from LP')
ylabel('Performance (% Correct)')
title('Backward Learning Curves by # of Irrelevant Dimensions')

subplot(2,3,5)
hold on
for num_irrel = 1:3
    n_dist = hist(all_LP_by_num_irrel{num_irrel},1:2:60);
    n_dist = n_dist./sum(n_dist);
    plot(1:2:60,n_dist);
end
hold off
box off
xlabel('LP trial #')
ylabel('Proportion of Blocks')
title('LP Distribution # of Irrelevant Dimensions')

subplot(2,3,3)
bar(mean_all_lps)
hold on
errorbar(mean_all_lps,sem_all_lps,'k','linestyle','none')
hold off
box off
ylabel('LP')
xlabel('Num Irrel')
xticks(1:3)
xticklabels({'0','1','2'})
title(['Mean and Std, ANOVA p = ' num2str(p_LP,3)])

subplot(2,3,6)
bar(mean(prop_unlearned_blocks))
hold on
errorbar(mean(prop_unlearned_blocks),std(prop_unlearned_blocks)./sqrt(num_sessions),'k','linestyle','none')
hold off
box off
ylabel('Proportion of Blocks')
xlabel('Num Irrel')
xticks(1:3)
xticklabels({'0','1','2'})
title('Proprotion of Unlearned blocks')
%%
% Bootstrapping Stats For number of irrel feature dims
stat_win = [4 20]+last_trial_window;%trials 4-20 after the shift igore 1st since could be at chance

observed_diff = NaN(3,3);
observed_pct = NaN(3,3);
for irrel = 1:3
    for irrel2 = 1:3
        if irrel > irrel2
           observed_diff(irrel,irrel2) = sum(mean(all_block_performance_by_num_irrel{irrel}(:,stat_win(1):stat_win(2)))-...
               mean(all_block_performance_by_num_irrel{irrel2}(:,stat_win(1):stat_win(2))));
           
           all_data = [all_block_performance_by_num_irrel{irrel}(:,stat_win(1):stat_win(2)); ...
               all_block_performance_by_num_irrel{irrel2}(:,stat_win(1):stat_win(2))];
           all_data_ind = [ones(size(all_block_performance_by_num_irrel{irrel},1),1);...
               2*ones(size(all_block_performance_by_num_irrel{irrel2},1),1)];
           shuffled_diff = NaN(1,num_shuffs);
           for shuff = 1:num_shuffs
               shuff_ind = all_data_ind(randperm(length(all_data_ind)));
               shuff1 = nanmean(all_data(shuff_ind == 1,:));
               shuff2 = nanmean(all_data(shuff_ind == 2,:));
               shuffled_diff(shuff) = sum(shuff1-shuff2);
           end
           
           observed_pct(irrel,irrel2) = 100*sum(observed_diff(irrel,irrel2) < shuffled_diff)/num_shuffs;
        end
    end
end
%%
figure
imagesc(observed_pct,'AlphaData',~isnan(observed_pct))
xticks(1:3)
yticks(1:3)
xticklabels({'0','1','2'})
yticklabels({'0','1','2'})
caxis([50 100])
%% Look for Choice Biases: Spatial & Feature

[spatial_choice_baises,feature_choice_baises] = CalculateChoiceBiases(...
    session_data,min_block_len,forward_smoothing_window,learning_point_threshold);
subtitle(monkeyname)

%%
toc