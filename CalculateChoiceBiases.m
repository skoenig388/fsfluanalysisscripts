function [spatial_stickiness,location_of_selections,feature_stickiness] = ...
    CalculateChoiceBiases( session_data,min_block_len,forward_smoothing_window,learning_point_threshold)
%Written by Seth Konig 5/17/19

%Inputs:
% 1) session_data: all the session data
% 2) min_block_len: miniumum # of trials in a block
% 3) forward_smoothing_window: window width for forward smoothing filter
% 4) learning_point_threshold: proportion correct to consider having "learned"

%Outputs:
% 0) Figure showing main results
% 1) spatial_stickiness: 0's and 1's representing whether choice on that
%   trial was in the same location as the last trial
% 2) location_of_selections: location indeces of chosen item
% 3) feature_stickiness: 0's and 1's reprsenting whether choice on that
%   trial had the same features as the last trial
%***all are broken down by performance
% a) all trials
% b) pre-LP trials
% c) post-LP trials
% d) trials from blocks with no LP

if learning_point_threshold > 1
    %assuming thrteshold is meant to be proportion correct not 100%
    learning_point_threshold = learning_point_threshold/100;
end

num_sessions = length(session_data);

%row 1: all trials
%row 2: pre-LP
%row 3: post-LP
%row 4: No LP
location_of_selections = cell(num_sessions,4); %which location chosen
spatial_stickiness = cell(num_sessions,4);%whether same location chosen as last trial
ideal_spatial_stickiness = cell(num_sessions,4);%if reward is in same location as last trial
feature_choice_baises = cell(num_sessions,4);%whether same features were chosen as last trial
ideal_feature_choice_baises = cell(num_sessions,4);%if rewarded features are same as last trial

feature_stickiness = cell(num_sessions,4);%whether selected features are same as last trials
ideal_feature_stickiness = cell(num_sessions,4);%if rewarded features are the same as last trial
for sess = 1:num_sessions
    if isfield(session_data{sess}.configdata,'View2DNumLocations')
        num_locations = session_data{sess}.configdata.View2DNumLocations;
        num_locations = str2double(num_locations);
        if num_locations == 4
            all_locations = [4 4;-4 4; 4 -4; -4 -4];
        elseif num_locations == 8
            all_locations = [4 4;-4 4; 4 -4; -4 -4; 0 5.65; 0 -5.65; 5.65 0; -5.65 0];
        else
            error('Need to specify locations')
        end
    else
        num_locations = 4;
        all_locations = [4 4;-4 4; 4 -4; -4 -4];
    end
    
    num_trials = session_data{sess}.num_trials;
    for type = 1:4
        location_of_selections{sess,type} = NaN(1,num_trials);
        spatial_stickiness{sess,type} = NaN(1,num_trials);
        ideal_spatial_stickiness{sess,type} = NaN(1,num_trials);
        feature_choice_baises{sess,type} = NaN(1,num_trials);
        ideal_feature_choice_baises{sess,type} = NaN(1,num_trials);
        
        feature_stickiness{sess,type} = NaN(1,num_trials);
        ideal_feature_stickiness{sess,type} = NaN(1,num_trials);
    end
    
    %want to analyze all "completed" blocks because bad performance trials could
    %show very apparent biases that we may not clealry see in "good" blocks
    block = session_data{sess}.trial_data.Block;
    block = block + 1;%index starts at 0 in C++
    num_blocks = max(block);
    rewarded = strcmpi(session_data{sess}.trial_data.isRewarded,'true');
    for b = 1:num_blocks
        blockind = find(block == b);
        if length(blockind) < min_block_len%didn't finish
            continue
        end
        
        %---Get Learning Point---%
        blockrewarded = rewarded(blockind(1):blockind(end));
        lp = FindLp(blockrewarded, 'slidingwindow', forward_smoothing_window, learning_point_threshold);
        for trial =1:length(blockind)
            t = blockind(trial);
            if trial == 1
                continue
            end
            selected_stim = session_data{sess}.trial_data.SelectedObjectID{t};
            last_selected_stim = session_data{sess}.trial_data.SelectedObjectID{t-1};
            
            if ~isempty(last_selected_stim) && ~isempty(selected_stim)
                selected_objetnum = str2double(selected_stim(4:end));
                stim_location = session_data{sess}.trial_def{t}.relevantObjects(selected_objetnum).StimLocation;
                stim_location_ind = find(stim_location.x == all_locations(:,1) & ...
                    stim_location.z == all_locations(:,2));
                
                last_selected_objetnum = str2double(last_selected_stim(4:end));
                last_stim_location = session_data{sess}.trial_def{t-1}.relevantObjects(last_selected_objetnum).StimLocation;
                last_stim_location_ind = find(last_stim_location.x == all_locations(:,1) & ...
                    last_stim_location.z == all_locations(:,2));
                
                reward_stim_location = session_data{sess}.trial_def{t}.relevantObjects(1).StimLocation;
                reward_stim_location_ind = find(reward_stim_location.x == all_locations(:,1) & ...
                    reward_stim_location.z == all_locations(:,2));
                
                last_reward_stim_location = session_data{sess}.trial_def{t-1}.relevantObjects(1).StimLocation;
                last_reward_stim_location_ind = find(last_reward_stim_location.x == all_locations(:,1) & ...
                    last_reward_stim_location.z == all_locations(:,2));
                
                
                
                selected_stim_vals = session_data{sess}.trial_def{t}.relevantObjects(selected_objetnum).StimDimVals;
                last_selected_stim_vals = session_data{sess}.trial_def{t-1}.relevantObjects(last_selected_objetnum).StimDimVals;
                observed_num_overlapping_features = sum(last_selected_stim_vals == selected_stim_vals & selected_stim_vals ~= 0);
                
                reward_stim_vals = session_data{sess}.trial_def{t}.relevantObjects(1).StimDimVals;
                last_reward_stim_vals = session_data{sess}.trial_def{t-1}.relevantObjects(1).StimDimVals;
                ideal_num_overlapping_features = sum(last_reward_stim_vals == reward_stim_vals & reward_stim_vals ~= 0);
                    
                %---For All Trials---%
                location_of_selections{sess,1}(t) = stim_location_ind;
                if last_stim_location_ind == stim_location_ind
                    spatial_stickiness{sess,1}(t) = 1;
                else
                    spatial_stickiness{sess,1}(t) = 0;
                end
                
                if reward_stim_location_ind == last_reward_stim_location_ind
                    ideal_spatial_stickiness{sess,1}(t) = 1;
                else
                    ideal_spatial_stickiness{sess,1}(t) = 0;
                end
                
                feature_stickiness{sess,1}(t) = observed_num_overlapping_features;
                ideal_feature_stickiness{sess,1}(t) = ideal_num_overlapping_features;
                
                %---Performance Based---%
                type = [];
                if isnan(lp) %no lp
                    type = 4;
                elseif trial < lp
                    type = 2;
                elseif trial > lp
                    type = 3;
                elseif trial == lp %include in post-lp for now
                    type = 2;
                end
                
                location_of_selections{sess,type}(t) = stim_location_ind;
                if last_stim_location_ind == stim_location_ind
                    spatial_stickiness{sess,type}(t) = 1;
                else
                    spatial_stickiness{sess,type}(t) = 0;
                end
                
                if reward_stim_location_ind == last_reward_stim_location_ind
                    ideal_spatial_stickiness{sess,type}(t) = 1;
                else
                    ideal_spatial_stickiness{sess,type}(t) = 0;
                end
                
                feature_stickiness{sess,type}(t) = observed_num_overlapping_features;
                ideal_feature_stickiness{sess,type}(t) = ideal_num_overlapping_features;
            end
        end
    end
end

%% Average within Sessions
all_ideal_spatial_stickiness = NaN(num_sessions,4);
all_spatial_stickiness = NaN(num_sessions,4);
all_ideal_feature_stickiness = NaN(num_sessions,4);
all_feature_stickiness = NaN(num_sessions,4);
all_feature_stickiness_dist = NaN(num_sessions,4,4);
all_ideal_feature_stickiness_dist = NaN(num_sessions,4,4);
max_loc_ind = max(max(cellfun(@max,location_of_selections)));
all_location_of_selections = NaN(num_sessions,4,max_loc_ind);
for sess = 1:num_sessions
    for type = 1:4
        all_ideal_spatial_stickiness(sess,type) = sum(ideal_spatial_stickiness{sess,type} == 1)/sum(~isnan(ideal_spatial_stickiness{sess,type}));
        all_spatial_stickiness(sess,type) = sum(spatial_stickiness{sess,type} == 1)/sum(~isnan(spatial_stickiness{sess,type}));
        all_ideal_feature_stickiness(sess,type) = nanmean(ideal_feature_stickiness{sess,type});
        all_feature_stickiness(sess,type) = nanmean(feature_stickiness{sess,type});
        
        for num_overlap = 0:3
            all_feature_stickiness_dist(sess,type,num_overlap+1) = sum(feature_stickiness{sess,type} == num_overlap)./sum(~isnan(feature_stickiness{sess,type}));
            all_ideal_feature_stickiness_dist(sess,type,num_overlap+1) = sum(ideal_feature_stickiness{sess,type} == num_overlap)./sum(~isnan(feature_stickiness{sess,type}));;
        end
        
        for loc = 1:max_loc_ind
            all_location_of_selections(sess,type,loc) = sum(location_of_selections{sess,type} == loc)./sum(~isnan(location_of_selections{sess,type}));
        end
    end
end
%% Claculate Average and Stds Across Sessions

avg_spatial_stickiness = NaN(4,2);%type by observed vs ideal
sem_spatial_stickiness = NaN(4,2);%type by observed vs ideal

avg_feature_stickiness = NaN(4,2);%type by observed vs ideal
sem_feature_stickiness = NaN(4,2);%type by observed vs ideal

avg_feature_stickiness_dist = NaN(4,4);%for observed
avg_ideal_feature_stickiness_dist = NaN(4,4);%for ideal
sem_feature_stickiness_dist = NaN(4,4);
sem_ideal_feature_stickiness_dist = NaN(4,4);

avg_spatial_biases = NaN(4,max_loc_ind);%type by location
sem_spatial_biases = NaN(4,max_loc_ind);%type by location

for type = 1:4
    avg_spatial_stickiness(type,1) = nanmean(all_spatial_stickiness(:,type));
    sem_spatial_stickiness(type,1) = nanstd(all_spatial_stickiness(:,type))./sqrt(num_sessions);
    avg_spatial_stickiness(type,2) = nanmean(all_ideal_spatial_stickiness(:,type));
    sem_spatial_stickiness(type,2) = nanstd(all_ideal_spatial_stickiness(:,type))./sqrt(num_sessions);
    
    avg_feature_stickiness(type,1) = nanmean(all_feature_stickiness(:,type));
    sem_feature_stickiness(type,1) = nanstd(all_feature_stickiness(:,type))./sqrt(num_sessions);
    avg_feature_stickiness(type,2) = nanmean(all_ideal_feature_stickiness(:,type));
    sem_feature_stickiness(type,2) = nanstd(all_ideal_feature_stickiness(:,type))./sqrt(num_sessions);
    
    for loc = 1:max_loc_ind
        avg_spatial_biases(type,loc) = nanmean(all_location_of_selections(:,type,loc));
        sem_spatial_biases(type,loc) = nanstd(all_location_of_selections(:,type,loc))./sqrt(num_sessions);
    end
    
    for num_overlap = 0:3
        avg_feature_stickiness_dist(type,num_overlap+1) = nanmean(all_feature_stickiness_dist(:,type,num_overlap+1));
        sem_feature_stickiness_dist(type,num_overlap+1) = nanstd(all_feature_stickiness_dist(:,type,num_overlap+1))./sqrt(num_sessions);
        avg_ideal_feature_stickiness_dist(type,num_overlap+1) = nanmean(all_ideal_feature_stickiness_dist(:,type,num_overlap+1));
        sem_ideal_feature_stickiness_dist(type,num_overlap+1) = nanstd(all_ideal_feature_stickiness_dist(:,type,num_overlap+1))./sqrt(num_sessions);
    end
end

figure
subplot(2,2,1)
errorb(avg_spatial_stickiness,sem_spatial_stickiness)
hold on
plot([0 5],[1/max_loc_ind 1/max_loc_ind],'k--')
hold off
xticks(1:4)
xticklabels({'All Trials','Pre-LP','Post-LP','No LP'})
ylabel('Prob Choosing Same Location')
title('Spatial Stickiness')
box off
legend({'Observed','Ideal'},'location','northeastoutside')

subplot(2,2,2)
errorb(avg_spatial_biases',sem_spatial_biases')
hold on
if max_loc_ind == 4
    plot([0 5],[1/max_loc_ind 1/max_loc_ind],'k--')
elseif max_loc_ind == 8
    plot([0 9],[1/max_loc_ind 1/max_loc_ind],'k--')
end
hold off
xlabel('Selected Quaddle Location')
if max_loc_ind == 4
    xticks(1:4)
    xticklabels({'UR','UL','LR','LL'})
    xticks(1:8);
elseif max_loc_ind == 8
   xticklabels({'UR','UL','LR','LL','U','Down','R','L'})
end
ylabel('Prob Choosing Location')
title('Spatial Biases')
box off
legend({'All Trials','Pre-LP','Post-LP','No LP'},'location','northeastoutside')

subplot(2,2,3)
errorb(avg_feature_stickiness,sem_feature_stickiness)
xticks(1:4)
xticklabels({'All Trials','Pre-LP','Post-LP','No LP'})
ylabel('Prob Choosing Same Features')
title('Feature Stickiness')
box off
legend({'Observed','Ideal'},'location','northeastoutside')

subplot(2,2,4)
errorb(avg_feature_stickiness_dist',sem_feature_stickiness_dist')
hold on
plot(1:4,mean(avg_ideal_feature_stickiness_dist),'r*')
hold off
xticks(1:4)
xticklabels({'0','1','2','3'})
xlabel('Number of Shared Features')
ylabel('Prob Choosing Same Features')
title('Feature Stickiness')
box off
legend({'All Trials','Pre-LP','Post-LP','No LP','Ideal'},'location','northeastoutside')
title('Feature Stickiness: # of Shared Features')

end