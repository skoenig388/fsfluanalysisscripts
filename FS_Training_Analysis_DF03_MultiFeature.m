%Scripts to Process Feature Search (FS) training
%written by Seth Konig 10/25/18

clear,clc,close all%fclose all

session_names = {'FS__Frey__12_10_2018__12_27_13','FS__Frey__13_10_2018__09_44_17','FS__Frey__14_10_2018__14_51_03',...
    'FS__Frey__16_10_2018__13_04_28','FS__Frey__17_10_2018__12_36_22','FS__Frey__18_10_2018__14_20_28',...
    'FS__Frey__18_10_2018__14_20_28',...
    'FS__Wotan__18_10_2018__15_05_37','FS__Wotan__19_10_2018__11_30_52',...
    'FS__Wotan__20_10_2018__15_02_27','FS__Wotan__20_10_2018__15_09_28','FS__Wotan__21_10_2018__13_31_37',...
    'FS__Igor__07_02_2019__10_29_54','FS__Igor__08_02_2019__08_51_46','FS__Igor__11_02_2019__08_59_45',...
    'FS__Igor__12_02_2019__10_26_44','FS__Igor__13_02_2019__10_17_58',...
    'FS__Reider__12_02_2019__09_33_24','FS__Reider__13_02_2019__08_57_40','FS__Reider__14_02_2019__10_31_59',...
    'FS__Reider__15_02_2019__09_11_41','FS__Reider__18_02_2019__10_21_47',...
    'FS__Bard__13_02_2019__09_54_13','FS__Bard__14_02_2019__09_31_07','FS__Bard__15_02_2019__09_18_28',...
    'FS__Bard__18_02_2019__09_08_06','FS__Bard__19_02_2019__11_30_11','FS__Bard__19_02_2019__11_45_45',...
    'FS__Sindri__20_02_2019__09_10_38','FS__Sindri__21_02_2019__10_05_48','FS__Sindri__22_02_2019__09_53_38',...
    'FS__Sindri__25_02_2019__09_05_50','FS__Sindri__26_02_2019__09_52_55',...
    };

%% Import Session Data
session_data = cell(1,length(session_names));
which_monkey = NaN(1,length(session_names));
for sn = 1:length(session_names)
    session_data{sn} = ImportFLU_Data(session_names{sn});
    session_data{sn} = ImportFLU_Data(session_names{sn});
    if contains(session_names{sn},'Bard')
        which_monkey(sn) = 1;
    elseif contains(session_names{sn},'Frey')
        which_monkey(sn) = 2;
    elseif contains(session_names{sn},'Igor')
        which_monkey(sn) = 3;
    elseif contains(session_names{sn},'Reider')
        which_monkey(sn) = 4;
    elseif contains(session_names{sn},'Sindri')
        which_monkey(sn) = 5;
    elseif contains(session_names{sn},'Wotan')
        which_monkey(sn) = 6;
    else
        error('Name Not found')
    end
end

%% Plot performance and search duration by trial #...probably not that useful
trial_count = NaN(1,length(session_names));
session_performance = cell(1,length(session_names));
for sn = 1:length(session_names)
    
    trial_count(sn) = size(session_data{sn}.trial_data,1);
    
    search_duration = session_data{sn}.trial_data.Epoch4_Duration;
    sd = search_duration;
    sd = [sd(10:-1:1); sd; sd(end:-1:end-9)];
    sd = filtfilt(1/10*ones(1,10),1,sd);
    sd = sd(10:end-10);
    
    session_performance{sn}.search_duration = search_duration;
    session_performance{sn}.smoothed_search_duration = sd;
    
    trial_duration = session_data{sn}.trial_data.TrialTime;
    td = trial_duration;
    td = [td(10:-1:1); td; td(end:-1:end-9)];
    td = filtfilt(1/10*ones(1,10),1,td);
    td = td(10:end-10);
    
    session_performance{sn}.trial_duration = trial_duration;
    session_performance{sn}.smoothed_trial_duration = td;
    
    rewarded = strcmpi(session_data{sn}.trial_data.isRewarded,'true');
    rwd = 100*rewarded;
    rwd = [rwd(10:-1:1); rwd; rwd(end:-1:end-9)];
    rwd = filtfilt(1/10*ones(1,10),1,rwd);
    rwd = rwd(10:end-10);
    
    session_performance{sn}.rewarded = rewarded;
    session_performance{sn}.smoothed_rewarded = rwd;
    
end

performance_by_trial_number = NaN(length(session_names),floor(median(trial_count)));
search_dur_by_trial_number = NaN(length(session_names),floor(median(trial_count)));

for sn = 1:length(session_names)
    
    if trial_count(sn) >=  floor(median(trial_count))
        performance_by_trial_number(sn,1:end) = session_performance{sn}.rewarded(1:floor(median(trial_count)));
        search_dur_by_trial_number(sn,1:end) = session_performance{sn}.search_duration(1:floor(median(trial_count)));
    else
        performance_by_trial_number(sn,1:trial_count(sn)) = session_performance{sn}.rewarded;
        search_dur_by_trial_number(sn,1:trial_count(sn)) = session_performance{sn}.search_duration;
    end
end

perform = nanmean(performance_by_trial_number);
perform = [perform(10:-1:1) perform perform(end:-1:end-9)];
perform = filtfilt(1/10*ones(1,10),1,perform);
perform = perform(10:end-10);


search = nanmean(search_dur_by_trial_number);
search = [search(10:-1:1) search search(end:-1:end-9)];
search = filtfilt(1/10*ones(1,10),1,search);
search = search(10:end-10);

figure
subplot(1,2,1)
plot(100*perform);
xlabel('Trial #')
ylabel('Performance %')
title('Smoothed Performance Across Sessions')
box off
axis square

subplot(1,2,2)
plot(search);
xlabel('Trial #')
ylabel('Search Duration (seconds)')
title('Smoothed Search Duration Across Sessions')
box off
axis square

%% Get Search Duration and Performance by Dimension
dims = {'Shape','Pattern','Color','Arm'};
dim_count = ones(1,4);
average_performanceMulti1D =  NaN(length(dims),50);
average_search_durationMulti1D =  NaN(length(dims),50);

stim_location = [];
stim_search_time = [];
for sn = 1:length(session_names)
    
    block_nums = session_data{sn}.trial_data.Block;%index starts at 0 in C++
    for b = 1:max(block_nums)+1
        if size(session_data{sn}.block_def(b).TrialDefs(1).relevantObjects,1) == 1
            continue%fam block
        end
        
        target_stimulus =  session_data{sn}.block_def(b).TrialDefs(1).relevantObjects(1).StimName;
        
        [dimsVals,neutralVals] = ParseQuaddleName(target_stimulus);
        if ~neutralVals(1)
            dim_index = 1;
        elseif ~neutralVals(2)
            dim_index = 2;
        elseif  ~neutralVals(3)
            dim_index = 3;
        elseif  ~neutralVals(5)
            dim_index = 4;
        else
            disp('We didnt use texture')
        end
        
        
        these_trials =  find(session_data{sn}.trial_data.Block == b-1);
        if isempty (these_trials)%if session was not finished then may then may have started task next day where left off
            continue
        end
        
        for t = 1:length(these_trials)
            rwd = session_performance{sn}.rewarded(these_trials(t));
            if rwd == 0
                continue
            end
            search_dur = session_performance{sn}.search_duration(these_trials(t));
            stimloc = session_data{sn}.trial_def{these_trials(t)}.relevantObjects(1).StimScreenLocation;
            
            stim_location = [stim_location; [stimloc.x stimloc.y]];
            stim_search_time = [stim_search_time; search_dur];
        end
        
        rewarded = session_performance{sn}.rewarded(these_trials);
        search_durs = session_performance{sn}.search_duration(these_trials);
        search_durs = search_durs(rewarded == 1);
        
        average_performanceMulti1D(dim_index,dim_count(dim_index)) = mean(rewarded);
        average_search_durationMulti1D(dim_index,dim_count(dim_index)) = mean(search_durs);
        dim_count(dim_index) = dim_count(dim_index)+1;
    end
end

std_performanceMulti1D = nanstd(average_performanceMulti1D');
std_search_durationMulti1D = nanstd(average_search_durationMulti1D');

average_performanceMulti1D = nanmean(average_performanceMulti1D');
average_search_durationMulti1D = nanmean(average_search_durationMulti1D');

%% Get Search Duration and Performance by Dimension for each monkey
dims = {'Shape','Pattern','Color','Arm'};
session_count = zeros(max(which_monkey),length(dims));
average_performanceMulti1Dbymonk =  zeros(max(which_monkey),length(dims));
average_search_durationMulti1Dbymonk = zeros(max(which_monkey),length(dims));

for sn = 1:length(session_names)
    
    block_nums = session_data{sn}.trial_data.Block;%index starts at 0 in C++
    for b = 1:max(block_nums)+1
        if size(session_data{sn}.block_def(b).TrialDefs(1).relevantObjects,1) == 1
            continue%fam block
        end
        
        target_stimulus =  session_data{sn}.block_def(b).TrialDefs(1).relevantObjects(1).StimName;
        
        [dimsVals,neutralVals] = ParseQuaddleName(target_stimulus);
        if ~neutralVals(1)
            dim_index = 1;
        elseif ~neutralVals(2)
            dim_index = 2;
        elseif  ~neutralVals(3)
            dim_index = 3;
        elseif  ~neutralVals(5)
            dim_index = 4;
        else
            disp('We didnt use texture')
        end
        
        these_trials =  find(session_data{sn}.trial_data.Block == b-1);
        if isempty (these_trials)%if session was not finished then may then may have started task next day where left off
            continue
        end
        
        rewarded = session_performance{sn}.rewarded(these_trials);
        search_durs = session_performance{sn}.search_duration(these_trials);
        search_durs = search_durs(rewarded == 1);
        
        session_count(which_monkey(sn),dim_index) = session_count(which_monkey(sn),dim_index)+1;
        average_performanceMulti1Dbymonk(which_monkey(sn),dim_index) =  average_performanceMulti1Dbymonk(which_monkey(sn),dim_index)+ nanmean(rewarded);
        average_search_durationMulti1Dbymonk(which_monkey(sn),dim_index) = average_search_durationMulti1Dbymonk(which_monkey(sn),dim_index)+ nanmean(search_durs);
        
    end
end

average_performanceMulti1Dbymonk = average_performanceMulti1Dbymonk./session_count;
average_search_durationMulti1Dbymonk = average_search_durationMulti1Dbymonk./session_count;


%% Plot Search Duration and Performance by Monkey for Each Feature Dimensions

figure
subplot(1,2,1)
bar(100*average_performanceMulti1Dbymonk')
hold off
xticks(1:length(dims))
xticklabels(dims)
xlabel('# of Distractors')
ylabel('Peformance %')
box off
ylim([90 100])
title('Across Session/Blocks Performance')
axis square
legend({'Bard','Frey','Igor','Reider','Sindri','Wotan'},'Location','SouthEast')

subplot(1,2,2)
bar(average_search_durationMulti1Dbymonk')
xlabel('Dimension')
xticks(1:4)
xticklabels(dims)
ylabel('Search Duration (seconds)')
box off
title('Across Session/Blocks Search Duration')
ylim([0.5 1])

%% Plot Search Duration and Performance by Feature Dimension
figure
subplot(1,2,1)
bar(100*average_performanceMulti1D')
hold on
errorbar(1:length(dims),100*average_performanceMulti1D,100*std_performanceMulti1D./sqrt(dim_count),'k','LineStyle','none')
hold off
xticks(1:length(dims))
xticklabels(dims)
xlabel('# of Distractors')
ylabel('Peformance %')
box off
ylim([90 100])
title('Across Session/Blocks Performance')
axis square

subplot(1,2,2)
bar(average_search_durationMulti1D')
hold on
errorbar(1:length(dims), average_search_durationMulti1D, std_search_durationMulti1D./sqrt(dim_count),'k','LineStyle','none');
hold off
xticks(1:length(dims))
xticklabels(dims)
xlabel('# of Distractors')
ylabel('Search Duration (seconds)')
box off
ylim([0.5 1])
title('Across Session/Blocks Search Duration')
axis square

if length(session_names) == 33
    subtitle('DF03 MultiSets: Combined Data Across All Monkeys')
else
    subtitle('DF03 MultiSets: Monkey??')
end
save('1D_Multi_FSTraining','average_performanceMulti1D','average_search_durationMulti1D');

%% Plot Search Duration by Stimulus Location
all_x_locs = unique(stim_location(:,1));
all_y_locs = unique(stim_location(:,2));

all_search_durs = zeros(length(all_y_locs),length(all_x_locs));
eccentricity = zeros(length(all_y_locs),length(all_x_locs));
for y = 1:length(all_y_locs)
    for x = 1:length(all_x_locs)
        ind = find(stim_location(:,1) == all_x_locs(x) & stim_location(:,2) == all_y_locs(y));
        all_search_durs(y,x) = mean(stim_search_time(ind));
        eccentricity(y,x) = 100*ceil(sqrt((all_y_locs(y)-1080/2).^2 + (all_x_locs(x)-1920/2).^2)/100);
    end
end

all_eccentricity = unique(eccentricity);
search_dur_by_eccentricity = NaN(1,length(all_eccentricity));
for e = 1:length(all_eccentricity)
    ind = find(eccentricity == all_eccentricity(e));
    search_dur_by_eccentricity(e) = mean(all_search_durs(ind));
end
%remove center point
search_dur_by_eccentricity(1) = [];
all_eccentricity(1) = [];

figure
subplot(2,1,1)
imAlpha = ones(size(all_search_durs));
imAlpha(isnan(all_search_durs))=0;
imagesc(all_search_durs,'AlphaData',imAlpha);
colorbar
colormap('viridis')
axis equal
axis off
title('Search Duration (seconds) by Stimulus Location')

subplot(2,1,2)
plot(all_eccentricity,search_dur_by_eccentricity,'ko-')
xlabel('Eccentricity (pixels)')
ylabel('Search Duration (seconds)')
box off
axis square
p = polyfit(all_eccentricity',search_dur_by_eccentricity,1);
title(['Slope = ' num2str(100*1000*p(1),3) ' ms/100 pixel'])
