function [block_avg_peformance,FLU_FS_blocks] = FLU2FS_Avg_Block_Performance(session_data,...
    min_FLU_block_len,min_FS_block_len)
%Code modified from RemoveBadBlocks by Seth Konig 5/20/19

%Inputs:
% 1) session_data: all the session data
% 2) min_FLU_block_len: miniumum # of trials in a FLU block
% 3) min_FS_block_Len: minimum # of trials in FS block

%Outputs:
%**ignores blocks that don't have the minimum # of trials completed
% 1) block_avg_peformance: average proportion correct by block
% 2) FLU_FS_blocks: 1 for FLU blocks 2 for FS blocks

block = session_data.trial_data.Block;
block = block + 1;%index starts at 0 in C++
num_blocks = max(block);
rewarded = strcmpi(session_data.trial_data.isRewarded,'true');

%---Get Average Block Performance---%
FLU_FS_blocks = NaN(1,num_blocks);
block_avg_peformance = NaN(1,num_blocks);
for b =1:max(block)
    blockind = find(block == b);
    
    blockname = session_data.block_def(b).BlockID;
    if contains(blockname,'FLU')
        FLU_or_FS_block = 1;
    elseif contains(blockname,'FS')
        FLU_or_FS_block = 2;
    elseif contains(blockname,'Fam')
        FLU_or_FS_block = - 1;
    else
        error('Block Name Not recognized')
    end
    
    if isempty(blockind)
        continue
    elseif length(blockind) < min_FLU_block_len && FLU_or_FS_block == 1
        continue
    elseif length(blockind) < min_FS_block_len && FLU_or_FS_block == 2
        continue
    end
    
    FLU_FS_blocks(b) = FLU_or_FS_block;

    blockrewarded = rewarded(blockind(1):blockind(end));
    block_avg_peformance(b) = sum(blockrewarded)/length(blockrewarded);
end
end