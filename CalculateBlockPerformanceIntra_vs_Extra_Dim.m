function [intra_extra_block_performance,intra_extra_search_duration,...
    intra_extra_shift,cued_uncued_shift,intra_extra_num_irrel] = CalculateBlockPerformanceIntra_vs_Extra_Dim(...
    session_data,goodblocks,min_block_len,last_trial_window)
%adapted from code else where, written 5/8/19

%Inputs:
% 1) session_data: all the session data
% 2) goodblocks: blocks with acceptable performance
% 3) min_block_len: miniumum # of trials in a block
% 4) last_trial_window: # of trials to look at end of block

%Outputs:
% 1) block_performance: block transition-aligned performance
% 2) search_duration: block transition-aligned search duratio
% 3) intra_extra_shift: 1 for intra, 2 for extra
% 4) cued_uncued_shift: 1 for cued, 0 for uncued
%Outputs are divided by intra- vs extra-dimensional shifts as well whether
%rule change was "cued" or not by change in feature dimension on the screen

if nargin < 2
    error('Not enough input arguments')
elseif nargin < 3
    min_block_len = 30;
    last_trial_window = 10;
elseif nargin < 4
    last_trial_window = 10;
end

block = session_data.trial_data.Block;
block = block + 1;%index starts at 0 in C++
rewarded = strcmpi(session_data.trial_data.isHighestProbReward,'true');
search_duration = Calculate_FLU_Search_Duration(session_data);

number_of_irrelevant_dimensions = ParseBlockDef4NumIrrel(session_data.block_def);

num_blocks = length(goodblocks);
intra_extra_block_performance = NaN(num_blocks,min_block_len+last_trial_window);
intra_extra_search_duration = NaN(num_blocks,min_block_len+last_trial_window);
intra_extra_shift = NaN(1,num_blocks);
cued_uncued_shift = NaN(1,num_blocks);
for b = 1:num_blocks
    blockind = find(block == goodblocks(b));
    if isempty(blockind)
        error('why is block empty?')
    elseif length(blockind) < 15%didn't finish
        error('why is block too short?')
    end
    
    intra_extra_num_irrel(b) = number_of_irrelevant_dimensions(goodblocks(b));
    
    %---Get Performance---%
    blockrewarded = rewarded(blockind(1):blockind(end));
    blockrewarded = [blockrewarded(end-(last_trial_window-1):end); blockrewarded];
    blocksearchdur = search_duration(blockind(1):blockind(end));
    blocksearchdur = [blocksearchdur(end-(last_trial_window-1):end); blocksearchdur];
    
    if length(blockrewarded) > min_block_len+last_trial_window
        intra_extra_block_performance(b,1:min_block_len+last_trial_window) = blockrewarded(1:min_block_len+last_trial_window);
        intra_extra_search_duration(b,1:min_block_len+last_trial_window) = blocksearchdur(1:min_block_len+last_trial_window);
    else
        intra_extra_block_performance(b,1:length(blockrewarded)) = blockrewarded;
        intra_extra_search_duration(b,1:length(blocksearchdur)) = blocksearchdur;
    end
    
    if contains(session_data.block_def(goodblocks(b)).BlockID,'Intra')
        intra_extra_shift(b) = 1;
    elseif contains(session_data.block_def(goodblocks(b)).BlockID,'Extra')
        intra_extra_shift(b) = 2;
    elseif contains(session_data.block_def(goodblocks(b)).BlockID,'First')
        continue
    else
        error('blockID not recongized')
    end
    
    prev_dims = find(session_data.block_def(goodblocks(b)-1).TrialDefs(1).relevantObjects(1).StimDimVals ~= 0);
    cur_dims = find(session_data.block_def(goodblocks(b)).TrialDefs(1).relevantObjects(1).StimDimVals ~= 0);
    
    if length(prev_dims) == length(cur_dims)%for same # of feature dimensions
        if all(prev_dims == cur_dims) %uncued
            cued_uncued_shift(b) = 0;
        else %cued
            cued_uncued_shift(b) = 1;
        end
    else
        if length(prev_dims) ~= length(cur_dims) %different number of feature dimensions means cued
            cued_uncued_shift(b) = 1;
        else
            error('Unknown Change')
        end
    end
end