%Feature Search Multiple regresssion analysis
% Written Seth Konig 4/12/19

clar 


%---For 2D FS----%
% task_type = '2D';
% session_names = {'FS__Frey__19_10_2018__10_53_42','FS__Frey__20_10_2018__14_18_09','FS__Frey__21_10_2018__12_50_15',...
%     'FS__Wotan__22_10_2018__11_20_06','FS__Wotan__23_10_2018__12_24_11','FS__Wotan__24_10_2018__12_13_16',...
%     'FS__Igor__14_02_2019__09_25_35','FS__Igor__15_02_2019__10_14_14','FS__Igor__18_02_2019__09_04_07',...
%     'FS__Reider__19_02_2019__09_00_04','FS__Reider__20_02_2019__10_29_15','FS__Reider__21_02_2019__10_18_39',...
%     'FS__Bard__20_02_2019__10_03_53','FS__Bard__21_02_2019__08_47_37','FS__Bard__22_02_2019__08_57_27',...
%     'FS__Sindri__27_02_2019__10_22_55','FS__Sindri__28_02_2019__09_23_40','FS__Sindri__01_03_2019__10_50_08',...
%     };


%---For 3D FS----%
task_type = '3D';
session_names = {'FS__Frey__22_10_2018__10_33_30','FS__Frey__23_10_2018__11_44_06','FS__Frey__24_10_2018__11_29_39',...
    'FS__Wotan__25_10_2018__12_33_20','FS__Wotan__26_10_2018__12_02_12','FS__Wotan__27_10_2018__13_07_35',...
    'FS__Igor__19_02_2019__10_15_04','FS__Igor__20_02_2019__09_07_57','FS__Igor__21_02_2019__08_55_18',...
    'FS__Reider__22_02_2019__08_54_02','FS__Reider__25_02_2019__10_15_40','FS__Reider__26_02_2019__10_07_42',...
    'FS__Bard__25_02_2019__10_07_22','FS__Bard__26_02_2019__09_06_15','FS__Bard__27_02_2019__09_19_23',...
    'FS__Sindri__04_03_2019__10_23_41','FS__Sindri__05_03_2019__09_22_56','FS__Sindri__06_03_2019__10_14_11'......
    };



num_sessions = length(session_names);
session_data = cell(1,num_sessions);
which_monkey = NaN(1,num_sessions);
for sn = 1:length(session_names)
    session_data{sn} = ImportFLU_Data(session_names{sn});
    if contains(session_names{sn},'Bard')
        which_monkey(sn) = 1;
    elseif contains(session_names{sn},'Frey')
        which_monkey(sn) = 2;
    elseif contains(session_names{sn},'Igor')
        which_monkey(sn) = 3;
    elseif contains(session_names{sn},'Reider')
        which_monkey(sn) = 4;
    elseif contains(session_names{sn},'Sindri')
        which_monkey(sn) = 5;
    elseif contains(session_names{sn},'Wotan')
        which_monkey(sn) = 6;
    else
        error('Name Not found')
    end
end

%%  Feature Dimensions and values for name parsing
shapes = {'S01', 'S02', 'S03', 'S04', 'S05', 'S06', 'S07', 'S08', 'S09'};
patterns = {'P01', 'P02', 'P03', 'P04', 'P05', 'P06', 'P07', 'P08', 'P09'};

patternless_colors = {'C6070014_6070014', 'C6070059_6070059', 'C6070106_6070106', 'C6070148_6070148', 'C6070194_6070194', 'C6070287_6070287', 'C6070335_6070335'};
patterned_colors =   {'C7070014_5000000', 'C7070059_5000000', 'C7070106_5000000', 'C7070148_5000000', 'C7070194_5000000', 'C7070286_5000000', 'C7070335_5000000'};
gray_pattern_color = 'C7000000_5000000';

arms = {'A00_E01', 'A00_E02','A00_E03', 'A01_E00', 'A01_E01', 'A01_E02', 'A01_E03', 'A02_E00', 'A02_E01', 'A02_E02', 'A02_E03'};

%%
all_r_2 = NaN(num_sessions,2);
all_f_stats = NaN(num_sessions,2);
all_p_vals = NaN(num_sessions,2);
all_b2s = NaN(num_sessions,4);
for sess = 1:num_sessions
    
    disp(['Processing Session #' num2str(sess)])
    
    trial_index = 1;
    all_search_durations = NaN(600,1);
    target_eccentricity = NaN(600,1);%/100 pixels
    all_num_distractors = NaN(600,1);
    distractor_type_count = NaN(600,2);
    
    num_trials = size(session_data{sess}.trial_data,1);
    for t =num_trials:-1:1
        if strcmpi(session_data{sess}.trial_data.isRewarded{t},'false')
            continue
        end
        
        num_objs = length(session_data{sess}.trial_def{t}.relevantObjects);
        if num_objs == 1 %familiarization trials
            continue
        end
        
        %only way to get time of final selection here is to look back from reward period
        search_start_time = session_data{sess}.trial_data.Epoch3_StartTimeRelative(t)+session_data{sess}.trial_data.baselineDuration(t);
        search_end_time =  session_data{sess}.trial_data.Epoch7_StartTimeRelative(t)...
            -session_data{sess}.trial_data.Epoch6_Duration(t)-session_data{sess}.trial_data.Epoch5_Duration(t);
        search_duration = search_end_time-search_start_time;
        if search_duration > 5 || search_duration < 0.25
            continue
        end
        all_search_durations(trial_index) = search_duration;
        all_num_distractors(trial_index) = num_objs-1;
        
        target_x = round(session_data{sess}.trial_def{t}.relevantObjects(1).StimScreenLocation.x);
        target_y = round(session_data{sess}.trial_def{t}.relevantObjects(1).StimScreenLocation.y);
        target_eccentricity(trial_index) = sqrt((target_x-1920/2).^2 + (target_y-1080/2).^2)/100;
        
        for o = 2:num_objs
            dimVals= ParseQuaddleName(session_data{sess}.trial_def{t}.relevantObjects(o).StimName,patternless_colors,arms);
            if dimVals(2) ~= 0%patterned object
                dimVals= ParseQuaddleName(session_data{sess}.trial_def{t}.relevantObjects(o).StimName,patterned_colors,arms);
            end
            allStimDimVals(o,:) = dimVals;
        end
        
        presences = zeros(1,2);
        for o = 1:num_objs
            if all(allStimDimVals(o,:) == 0)
                presences(1) = presences(1)+1;
            else
                presences(2) = presences(2)+1;
            end
        end
        distractor_type_count(trial_index,:) = presences;

       
        trial_index = trial_index+1;
    end
    
    
    %% Remove Excess NaNs
    all_search_durations(trial_index:end,:) = [];
    target_eccentricity(trial_index:end,:) = [];
    all_num_distractors(trial_index:end,:) = [];
    distractor_type_count(trial_index:end,:) = [];
    
    %% Fit linear regression models
    
    %---Model 1: # distractors and eccentricity---%
    X = [ones(size(all_search_durations)) target_eccentricity all_num_distractors];
    [b1,bint1,r1,rint1,stats1] = regress(all_search_durations,X);
    all_r_2(sess,1) = stats1(1);
    all_f_stats(sess,1) = stats1(2);
    all_p_vals(sess,1) = stats1(3);
    
    
    %---Model 2: Target Eccentricity & Distractor Type---%
    X = [ones(size(all_search_durations)) target_eccentricity distractor_type_count];
    [b5,bint5,r5,rint5,stats5] = regress(all_search_durations,X);
    all_r_2(sess,2) = stats5(1);
    all_f_stats(sess,2) = stats5(2);
    all_p_vals(sess,2) = stats5(3);
    all_b2s(sess,:) = b5';

end
%%
figure
subplot(2,2,1)
bar(mean(all_r_2))
hold on
errorbar(mean(all_r_2),std(all_r_2)./sqrt(num_sessions),'k','LineStyle','none');
hold off
xlabel('Model #')
ylabel('R^2')
title('Model Fit: R^2')
box off

subplot(2,2,2)
bar(mean(all_f_stats))
hold on
errorbar(mean(all_f_stats),std(all_f_stats)./sqrt(num_sessions),'k','LineStyle','none');
hold off
xlabel('Model #')
ylabel('F-statistic')
title('Model Fit: F-statistic')
box off

subplot(2,2,3)
bar(mean(-log10(all_p_vals)))
hold on
errorbar(mean(-log10(all_p_vals)),std(-log10(all_p_vals))./sqrt(num_sessions),'k','LineStyle','none');
hold off
xlabel('Model #')
ylabel('-log_{10} p-value')
title('Model Fit: p-value')
box off

labels = {'Eccentricity','Neutral','1D Decomp'};
[~,si] = sort(mean(all_b2s(:,2:end)));
subplot(2,2,4)
bar(1000*mean(all_b2s(:,si+1)))
hold on
errorbar(1000*mean(all_b2s(:,si+1)),1000*std(all_b2s(:,si+1))./sqrt(num_sessions),'k','LineStyle','none');
hold off
xticks(1:length(b5)-1)
xticklabels(labels(si));
xtickangle(30)
ylabel('Effect on Search Duration (ms)')
box off
title('Model #5')

%%
labels = {'Eccentricity','Neutral','1D Decomp'};
[~,si] = sort(mean(all_b2s(:,2:end)));
figure
bar(1000*mean(all_b2s(:,si+1)))
hold on
errorbar(1000*mean(all_b2s(:,si+1)),1000*std(all_b2s(:,si+1))./sqrt(num_sessions),'k','LineStyle','none');
hold off
xticks(1:length(b5)-1)
xticklabels(labels(si));
xtickangle(30)
ylabel('Effect on Search Duration (ms)')
box off
title(['Best Model for ' task_type ', r^2 = ' num2str(mean(all_r_2(:,2)),3)])