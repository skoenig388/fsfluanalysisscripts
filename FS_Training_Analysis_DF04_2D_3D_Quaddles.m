%Scripts to Process Feature Search (FS) training
%written by Seth Konig 10/25/18

%clear,clc,close all%fclose all


session_names = {'FS__Frey__19_10_2018__10_53_42','FS__Frey__20_10_2018__14_18_09','FS__Frey__21_10_2018__12_50_15',...
    'FS__Frey__22_10_2018__10_33_30','FS__Frey__23_10_2018__11_44_06','FS__Frey__24_10_2018__11_29_39',...
    'FS__Wotan__22_10_2018__11_20_06','FS__Wotan__23_10_2018__12_24_11','FS__Wotan__24_10_2018__12_13_16',...
    'FS__Wotan__25_10_2018__12_33_20','FS__Wotan__26_10_2018__12_02_12','FS__Wotan__27_10_2018__13_07_35',...
    'FS__Igor__14_02_2019__09_25_35','FS__Igor__15_02_2019__10_14_14','FS__Igor__18_02_2019__09_04_07',...
    'FS__Igor__19_02_2019__10_15_04','FS__Igor__20_02_2019__09_07_57','FS__Igor__21_02_2019__08_55_18',...
    'FS__Reider__19_02_2019__09_00_04','FS__Reider__20_02_2019__10_29_15','FS__Reider__21_02_2019__10_18_39',...
    'FS__Reider__22_02_2019__08_54_02','FS__Reider__25_02_2019__10_15_40','FS__Reider__26_02_2019__10_07_42',...
    'FS__Bard__20_02_2019__10_03_53','FS__Bard__21_02_2019__08_47_37','FS__Bard__22_02_2019__08_57_27',...
    'FS__Bard__25_02_2019__10_07_22','FS__Bard__26_02_2019__09_06_15','FS__Bard__27_02_2019__09_19_23',...
    'FS__Sindri__27_02_2019__10_22_55','FS__Sindri__28_02_2019__09_23_40','FS__Sindri__01_03_2019__10_50_08',...
    'FS__Sindri__04_03_2019__10_23_41','FS__Sindri__05_03_2019__09_22_56','FS__Sindri__06_03_2019__10_14_11'......
    };

session_type = {'2D','2D','2D',...
    '3D','3D','3D',...
    '2D','2D','2D',...
    '3D','3D','3D',...
    '2D','2D','2D',...
    '3D','3D','3D',...
    '2D','2D','2D',...
    '3D','3D','3D',...
    '2D','2D','2D',...
    '3D','3D','3D',...
    '2D','2D','2D',...
    '3D','3D','3D',...
    };

session_data = cell(1,length(session_names));
which_monkey = NaN(1,length(session_names));
for sn = 1:length(session_names)
    session_data{sn} = ImportFLU_Data(session_names{sn});
    if contains(session_names{sn},'Bard')
        which_monkey(sn) = 1;
    elseif contains(session_names{sn},'Frey')
        which_monkey(sn) = 2;
    elseif contains(session_names{sn},'Igor')
        which_monkey(sn) = 3;
    elseif contains(session_names{sn},'Reider')
        which_monkey(sn) = 4;
    elseif contains(session_names{sn},'Sindri')
        which_monkey(sn) = 5;
    elseif contains(session_names{sn},'Wotan')
        which_monkey(sn) = 6;
    else
        error('Name Not found')
    end
end

%%
trial_count = NaN(1,length(session_names));
session_performance = cell(1,length(session_names));
for sn = 1:length(session_names)
    
    trial_count(sn) = size(session_data{sn}.trial_data,1);
    
    search_duration = session_data{sn}.trial_data.Epoch4_Duration;
    sd = search_duration;
    sd = [sd(10:-1:1); sd; sd(end:-1:end-9)];
    sd = filtfilt(1/10*ones(1,10),1,sd);
    sd = sd(10:end-10);
    
    session_performance{sn}.search_duration = search_duration;
    session_performance{sn}.smoothed_search_duration = sd;
    
    trial_duration = session_data{sn}.trial_data.TrialTime;
    td = trial_duration;
    td = [td(10:-1:1); td; td(end:-1:end-9)];
    td = filtfilt(1/10*ones(1,10),1,td);
    td = td(10:end-10);
    
    session_performance{sn}.trial_duration = trial_duration;
    session_performance{sn}.smoothed_trial_duration = td;
    
    rewarded = strcmpi(session_data{sn}.trial_data.isRewarded,'true');
    rwd = 100*rewarded;
    rwd = [rwd(10:-1:1); rwd; rwd(end:-1:end-9)];
    rwd = filtfilt(1/10*ones(1,10),1,rwd);
    rwd = rwd(10:end-10);
    
    session_performance{sn}.rewarded = rewarded;
    session_performance{sn}.smoothed_rewarded = rwd;
    
end

%%
performance_by_trial_number = NaN(length(session_names),floor(median(trial_count)));
search_dur_by_trial_number = NaN(length(session_names),floor(median(trial_count)));

for sn = 1:length(session_names)
    
    if trial_count(sn) >=  floor(median(trial_count))
        performance_by_trial_number(sn,1:end) = session_performance{sn}.rewarded(1:floor(median(trial_count)));
        search_dur_by_trial_number(sn,1:end) = session_performance{sn}.search_duration(1:floor(median(trial_count)));
    else
        performance_by_trial_number(sn,1:trial_count(sn)) = session_performance{sn}.rewarded;
        search_dur_by_trial_number(sn,1:trial_count(sn)) = session_performance{sn}.search_duration;
    end
end

%%

perform = nanmean(performance_by_trial_number);
perform = [perform(10:-1:1) perform perform(end:-1:end-9)];
perform = filtfilt(1/10*ones(1,10),1,perform);
perform = perform(10:end-10);


search = nanmean(search_dur_by_trial_number);
search = [search(10:-1:1) search search(end:-1:end-9)];
search = filtfilt(1/10*ones(1,10),1,search);
search = search(10:end-10);

figure
subplot(1,2,1)
plot(perform);
xlabel('Trial #')
ylabel('Performance %')
title('Smoothed Performance Across Sessions')
box off
axis square

subplot(1,2,2)
plot(search);
xlabel('Trial #')
ylabel('Search Duration (seconds)')
title('Smoothed Search Duration Across Sessions')
box off
axis square

%%
num_distractors = [0 1 3 5 9 12];
dims = {'2D','3D'};
average_performance2D3D =  NaN(length(dims),length(num_distractors));
average_search_duration2D3D =  NaN(length(dims),length(num_distractors));
error_selected_stim = [];
for d = 1:length(dims)
    these_sets = find(contains(session_type,dims{d}));
    
    performance =  NaN(length(these_sets),length(num_distractors));
    search_duration = NaN(length(these_sets),length(num_distractors));
    
    for set = 1:length(these_sets)
        block_nums = session_data{these_sets(set)}.trial_data.Block;%index starts at 0 in C++
        for b = 1:max(block_nums)+1
            if contains(session_data{sn}.block_def(b).BlockID,'D0')
                block_ind = 1;
            elseif  contains(session_data{these_sets(set)}.block_def(b).BlockID,'D12')
                block_ind = 6;
            elseif  contains(session_data{these_sets(set)}.block_def(b).BlockID,'D1')
                block_ind = 2;
            elseif  contains(session_data{these_sets(set)}.block_def(b).BlockID,'D3')
                block_ind = 3;
            elseif  contains(session_data{these_sets(set)}.block_def(b).BlockID,'D5')
                block_ind = 4;
            elseif  contains(session_data{these_sets(set)}.block_def(b).BlockID,'D9')
                block_ind = 5;
            else
                disp('BlockID not recognized')
            end
            
            if block_ind ~= b
                disp('block # mismatch for some reason')
            end
            
            these_trials =  find(session_data{these_sets(set)}.trial_data.Block == b-1);
            
            for t = 1:length(these_trials)
                rwd = session_performance{these_sets(set)}.rewarded(these_trials(t));
                if rwd == 1
                    %want incorrect trials so not correct
                    continue
                end
                selectedID =  session_data{these_sets(set)}.trial_data.SelectedObjectID{these_trials(t)};
                if isempty(selectedID)
                    %nothing selected/timed out
                    continue
                end
                stimind = str2double(selectedID(4:end));
                selectedStim = session_data{these_sets(set)}.trial_def{these_trials(t)}.relevantObjects(stimind).StimName;
                
                [dimsVals,neutralVals] = ParseQuaddleName(selectedID);
                if all(neutralVals)
                    error_selected_stim = [error_selected_stim 0];
                else
                    error_selected_stim = [error_selected_stim 1];
                end
            end
            
            rewarded = session_performance{these_sets(set)}.rewarded(these_trials);
            search_durs = session_performance{these_sets(set)}.search_duration(these_trials);
            search_durs = search_durs(rewarded == 1);
            
            performance(set,block_ind) = mean(rewarded);
            search_duration(set,block_ind) = mean(search_durs);
        end
    end
    average_performance2D3D(d,:)  = nanmean(performance);
    average_search_duration2D3D(d,:) =  nanmean(search_duration);
end
%% Get Search Duration by Set so can compute by monkey
all_search_durations = NaN(length(session_names),length(num_distractors));
for set = 1:length(session_names)
    for b = 1:max(block_nums)+1
        if contains(session_data{sn}.block_def(b).BlockID,'D0')
            block_ind = 1;
        elseif  contains(session_data{sn}.block_def(b).BlockID,'D12')
            block_ind = 6;
        elseif  contains(session_data{sn}.block_def(b).BlockID,'D1')
            block_ind = 2;
        elseif  contains(session_data{sn}.block_def(b).BlockID,'D3')
            block_ind = 3;
        elseif  contains(session_data{sn}.block_def(b).BlockID,'D5')
            block_ind = 4;
        elseif  contains(session_data{sn}.block_def(b).BlockID,'D9')
            block_ind = 5;
        else
            disp('BlockID not recognized')
        end
        
        if block_ind ~= b
            disp('block # mismatch for some reason')
        end
        
        these_trials =  find(session_data{set}.trial_data.Block == b-1);
        search_durs = session_data{set}.trial_data.Epoch4_Duration(these_trials);
        all_search_durations(set,block_ind) = nanmean(search_durs);
    end
end
%% without 1D data

dims = {'2D','3D'};

figure
subplot(2,2,1)
bar(100*average_performance2D3D')
xticks(1:length(num_distractors))
xticklabels(num2cell(num_distractors))
xlabel('# of Distractors')
ylabel('Peformance %')
box off
%legend(dims)
ylim([75 100])
title('Search Duration: Dimension vs. # Distractors')

subplot(2,2,2)
bar(average_search_duration2D3D')
xticks(1:length(num_distractors))
xticklabels(num2cell(num_distractors))
xlabel('# of Distractors')
ylabel('Search Duration (seconds)')
box off
legend(dims,'Location','NorthEastOutside')
ylim([0.5 1.25])
title('Search Duration: Dimension vs. # Distractors')

average_slope = NaN(1,length(dims));
for d = 1:length(dims)
    p = polyfit(num_distractors,average_search_duration2D3D(d,:),1);
    average_slope(d) = p(1);
end

subplot(2,2,4)
bar(1000*average_slope)
xticks(1:length(dims))
xticklabels(dims)
xlabel('Dimension')
ylabel('Slope (Search Duration (ms)/Distractor)')
title('Slope: Effect of # of Distractors')
box off

search_slope_by_monkey = NaN(length(dims),max(which_monkey));
for monk = 1:max(which_monkey)
    for dim = 1:length(dims)
        this_monkey_this_dim = find(strcmpi(dims{dim},session_type) == 1 & which_monkey == monk);
        search_durs_this_monkey = all_search_durations(this_monkey_this_dim,:);
        if size(search_durs_this_monkey,1) == 1
             p = polyfit(num_distractors,search_durs_this_monkey,1);
        else
            p = polyfit(num_distractors,nanmean(search_durs_this_monkey),1);
        end
        search_slope_by_monkey(dim,monk) = p(1);
    end
end

subplot(2,2,3)
bar(1000*search_slope_by_monkey')
box off
xticks(1:max(which_monkey))
xticklabels({'Bard','Frey','Igor','Reider','Sindri','Wotan'})
ylabel('Slope (Search Duration (ms)/Distractor)')
title('Slope by Monkey')

%% with 1D data present
dims = {'1D','2D','3D'};
load('1DFSTraining.mat');

average_performance = [mean(average_performance1D); average_performance2D3D];
average_search_duration = [mean(average_search_duration1D); average_search_duration2D3D];


figure
subplot(2,2,1)
bar(100*average_performance')
xticks(1:length(num_distractors))
xticklabels(num2cell(num_distractors))
xlabel('# of Distractors')
ylabel('Peformance %')
box off
%legend(dims)
ylim([75 100])
title('Search Duration: Dimension vs. # Distractors')

subplot(2,2,2)
bar(average_search_duration')
xticks(1:length(num_distractors))
xticklabels(num2cell(num_distractors))
xlabel('# of Distractors')
ylabel('Search Duration (seconds)')
box off
legend(dims)
ylim([0.5 1.25])
title('Search Duration: Dimension vs. # Distractors')

average_slope = NaN(1,length(dims));
for d = 1:length(dims)
    p = polyfit(num_distractors,average_search_duration(d,:),1);
    average_slope(d) = p(1);
end

subplot(2,2,4)
bar(1000*average_slope)
xticks(1:length(dims))
xticklabels(dims)
xlabel('Dimension')
ylabel('Slope (Search Duration (ms)/Distractor)')
title('Slope: Effect of # of Distractors')
box off

%%
save('2D3D_FSTraining','average_performance2D3D','average_search_duration2D3D');
