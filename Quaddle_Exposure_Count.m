%Determine number of Times, Sessions, and Blocks a monkey experienced a
%Quaddle.
%Written by Seth Koenig 11/23/18

clar

monkeyname = 'Wotan';
log_dir = 'Z:\MonkeyData\Wotan\';

% monkeyname = 'Frey';
% log_dir = 'Z:\MonkeyData\Frey\';

task_list = {'FLU Task'};%,'Feature Search Task','Context Feature Search Task','VDM','VDS'};

shapes = {'S00','S01', 'S02', 'S03', 'S04', 'S05', 'S06', 'S07', 'S08', 'S09'};
patterns = {'P00','P01', 'P02', 'P03', 'P04', 'P05', 'P06', 'P07', 'P08', 'P09'};
patternless_colors = {'C6000000_6000000', 'C7070014_7070014', 'C7070059_7070059', 'C7070106_7070106', 'C7070148_7070148', 'C7070194_7070194', 'C7070286_7070286', 'C7070335_7070335','C7070240_7070240'};
patterned_colors =   {'C7000000_5000000', 'C7070014_5000000', 'C7070059_5000000', 'C7070106_5000000', 'C7070148_5000000', 'C7070194_5000000', 'C7070286_5000000', 'C7070335_5000000','C7070240_5000000'};
arms = {'A00_E00','A01_E00', 'A02_E00','A00_E01', 'A00_E02', 'A00_E03','A01_E01', 'A02_E01','A01_E02', 'A01_E03','A02_E02','A02_E03'};

total_usage = zeros(5,12);
sess_usage = zeros(5,12);

for task = 1:length(task_list)
    file_list = dir([log_dir task_list{task} '\']);
    
    for f = 1:length(file_list)
        if ~contains(file_list(f).name,'.')
            disp(['Importing Session ' num2str(f) '/' num2str(length(file_list))]);
            session_data = ImportFLU_DataV2(file_list(f).name,[log_dir task_list{task} '\'],'slim');
            block = session_data.trial_data.Block;
            block = block + 1;%index starts at 0 in C++
            
            trial_count = length(block);
            trial_usage = zeros(5,12);
            for t = 1:trial_count
                
                for stim = 1:length(session_data.trial_def{t}.relevantObjects)
                    
                    stim_name = session_data.trial_def{t}.relevantObjects(stim).StimName;
                    
                    dimVals= ParseQuaddleName(stim_name,patternless_colors,arms);
                    if dimVals(2) ~= 0%patterned object
                        dimVals= ParseQuaddleName(stim_name,patterned_colors,arms);
                    end

                    dimVals(3) = dimVals(3)-1;
                    dimVals(5) = dimVals(5)-1;
                                   
                    if all(dimVals <= 0)
                        disp(stim_name)
                        continue
                    end
                    
                    trial_usage(1,dimVals(1)+1) = trial_usage(1,dimVals(1)+1)+1;
                    trial_usage(2,dimVals(2)+1) =  trial_usage(2,dimVals(2)+1)+ 1;
                    trial_usage(3,dimVals(3)+1) = trial_usage(3,dimVals(3)+1)+1;
                    trial_usage(4,dimVals(4)+1) =  trial_usage(4,dimVals(4)+1)+1;
                    trial_usage(5,dimVals(5)+1) =  trial_usage(5,dimVals(5)+1)+1;
                end
            end
            
            sess_usage(trial_usage ~= 0) = sess_usage(trial_usage ~= 0)+1;
            total_usage = total_usage + trial_usage;
            
        end
    end
end
%% Check that they are 0 and then fill in non-existent colors/patterns with NaNs
if all(total_usage(1,length(shapes)+1:end) ~= 0)
    error('Why are there shape values where there shouldnt be?')
else
    total_usage(1,length(shapes)+1:end) = NaN;
end

if all(total_usage(2,length(patterns)+1:end) ~= 0)
    error('Why are there pattern values where there shouldnt be?')
else
    total_usage(2,length(patterns)+1:end) = NaN;
end

if all(total_usage(3,length(patternless_colors)+1:end) ~= 0)
    error('Why are there color values where there shouldnt be?')
else
    total_usage(3,length(patternless_colors)+1:end) = NaN;
end

if all(total_usage(4,2:end) ~= 0)
    error('Why are there texture values where there shouldnt be?')
else
    total_usage(4,2:end) = NaN;
end

if all(sess_usage(1,length(shapes)+1:end) ~= 0)
    error('Why are there shape values where there shouldnt be?')
else
    sess_usage(1,length(shapes)+1:end) = NaN;
end

if all(sess_usage(2,length(patterns)+1:end) ~= 0)
    error('Why are there pattern values where there shouldnt be?')
else
    sess_usage(2,length(patterns)+1:end) = NaN;
end

if all(sess_usage(3,length(patternless_colors)+1:end) ~= 0)
    error('Why are there color values where there shouldnt be?')
else
    sess_usage(3,length(patternless_colors)+1:end) = NaN;
end

if all(sess_usage(4,2:end) ~= 0)
    error('Why are there texture values where there shouldnt be?')
else
    sess_usage(4,2:end) = NaN;
end


%arms should all be used
%% Total Usage
figure
subplot(2,2,1)
imagesc(total_usage,'AlphaData',~isnan(total_usage))
xlabel('Feature Value #')
yticks(1:5)
yticklabels({'Shape','Pattern','Color','Texture','Arms'})
colorbar
colormap('viridis')
title('Total Usage')
box off


subplot(2,2,3)
imagesc(total_usage(:,2:end),'AlphaData',~isnan(total_usage(:,2:end)))
xlabel('Feature Value #')
yticks(1:5)
yticklabels({'Shape','Pattern','Color','Texture','Arms'})
colorbar
colormap('viridis')
title('Total Non-Neutral Usage')
box off

subplot(2,2,2)
bar(total_usage(:,1))
xticks(1:5)
xticklabels({'Shape','Pattern','Color','Texture','Arms'})
box off
ylabel('# Trials')
title('Total Neutral Usage')

subplot(2,2,4)
bar(total_usage(:,2:end))
xlabel('Feature Value #')
xticks(1:5)
xticklabels({'Shape','Pattern','Color','Texture','Arms'})
ylabel('# Trials')
title('Total Usage')
box off

subtitle('Total Usage')

%% Total Session Usage
figure
subplot(2,2,1)
imagesc(sess_usage,'AlphaData',~isnan(sess_usage))
xlabel('Feature Value #')
yticks(1:5)
yticklabels({'Shape','Pattern','Color','Texture','Arms'})
colorbar
colormap('viridis')
title('Total Usage')
box off


subplot(2,2,3)
imagesc(sess_usage(:,2:end),'AlphaData',~isnan(sess_usage(:,2:end)))
xlabel('Feature Value #')
yticks(1:5)
yticklabels({'Shape','Pattern','Color','Texture','Arms'})
colorbar
colormap('viridis')
title('Total Non-Neutral Usage')
box off

subplot(2,2,2)
bar(sess_usage(:,1))
xticks(1:5)
xticklabels({'Shape','Pattern','Color','Texture','Arms'})
box off
ylabel('# Sessions')

title('Total Neutral Usage')

subplot(2,2,4)
bar(sess_usage(:,2:end))
xlabel('Feature Value #')
xticks(1:5)
xticklabels({'Shape','Pattern','Color','Texture','Arms'})
title('Total Usage')
ylabel('# Sessions')

box off

subtitle('Total Session Usage')