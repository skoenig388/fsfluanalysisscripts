%Written by Seth Koenig 4/15/19
%Code combines data cross multiple sessions

clar
import_option = 'all';

num_distractor_Quaddles = [3 5 9 12];

% monkeyname = 'Chase and Seth';
% sess_file_names = {'FLU__Seth__15_04_2019__11_38_05','FLU__Seth__15_04_2019__10_44_37',...
%     'FLU__Chase__15_04_2019__10_56_44'};

% monkeyname = 'Frey & Wotan & Reider';
% sess_file_names = {'FLU__Frey__17_04_2019__11_15_06','FS__Frey__18_04_2019__12_04_55',...
%     'FLU__Wotan__17_04_2019__13_16_43','FS__Wotan__18_04_2019__13_43_46',...
%     'FLU__Reider__18_04_2019__11_32_53'};


% monkeyname = 'Reigor';
% sess_file_names = {'FLU__Reider__18_04_2019__11_32_53','FS__Igor__22_04_2019__10_00_08',...
%     'FS__Igor__23_04_2019__14_09_50'};

% monkeyname = 'Frey';
% sess_file_names = {'FLU__Frey__17_04_2019__11_15_06','FS__Frey__18_04_2019__12_04_55',...
%     'FS__Frey__21_04_2019__12_41_36','FS__Frey__21_04_2019__12_53_28',...
%     'FS__Frey__21_04_2019__13_05_02','FS__Frey__21_04_2019__13_16_45',...
%     'FS__Frey__21_04_2019__13_28_32','FS__Frey__22_04_2019__11_55_40'};

% monkeyname = 'Wotan';
% sess_file_names = {'FLU__Wotan__17_04_2019__13_16_43','FS__Wotan__18_04_2019__13_43_46',...
%     'FS__Wotan__21_04_2019__13_54_13','FS__Wotan__21_04_2019__14_03_32',...
%     'FS__Wotan__21_04_2019__14_12_54','FS__Wotan__21_04_2019__14_22_24',...
%     'FS__Wotan__21_04_2019__14_32_05','FS__Wotan__22_04_2019__13_44_20'};

monkeyname = 'All';
sess_file_names = {'FLU__Wotan__17_04_2019__13_16_43','FS__Wotan__18_04_2019__13_43_46',...
    'FS__Wotan__21_04_2019__13_54_13','FS__Wotan__21_04_2019__14_03_32',...
    'FS__Wotan__21_04_2019__14_12_54','FS__Wotan__21_04_2019__14_22_24',...
    'FS__Wotan__21_04_2019__14_32_05','FS__Wotan__22_04_2019__13_44_20',...
    'FLU__Frey__17_04_2019__11_15_06','FS__Frey__18_04_2019__12_04_55',...
    'FS__Frey__21_04_2019__12_41_36','FS__Frey__21_04_2019__12_53_28',...
    'FS__Frey__21_04_2019__13_05_02','FS__Frey__21_04_2019__13_16_45',...
    'FS__Frey__21_04_2019__13_28_32','FS__Frey__22_04_2019__11_55_40',...
    'FLU__Reider__18_04_2019__11_32_53','FS__Igor__22_04_2019__10_00_08',...
    'FS__Igor__23_04_2019__14_09_50'};
%% Import Session Data
num_sessions = length(sess_file_names);
session_data = cell(1,num_sessions);
for sess =1:num_sessions
    disp(['Loading data from session #' num2str(sess) '/' num2str(length(sess_file_names))])
    log_dir = FindDataDirectory(sess_file_names{sess});
    %log_dir = 'Z:\MonkeyData\HumanFSLumen\';
    if ~isempty(log_dir)
        session_data{sess} = ImportFLU_DataV2(sess_file_names{sess},log_dir,import_option);
    else
        error(['Cant Find Log Dir for '  sess_file_names{sess}])
    end
end

%% Determine Search Duration by luminance & # of Distractors

all_search_durations = cell(1,num_sessions);

for sess = 1:num_sessions
    all_search_durations{sess} = cell(3,length(num_distractor_Quaddles));

    if isfield(session_data{sess}.configdata,'objectSelectionMode')
        if strcmpi(session_data{sess}.configdata.objectSelectionMode,'2')
            search_start_time = session_data{sess}.trial_data.Epoch3_StartTimeRelative+session_data{sess}.trial_data.baselineDuration;
            search_end_time =  session_data{sess}.trial_data.Epoch7_StartTimeRelative...
                -session_data{sess}.trial_data.Epoch6_Duration-session_data{sess}.trial_data.Epoch5_Duration;
            search_duration = search_end_time-search_start_time;
        else
            search_duration = session_data{sess}.trial_data.Epoch4_Duration;
        end
    else
        search_duration = session_data{sess}.trial_data.Epoch4_Duration;
    end
    rewarded = strcmpi(session_data{sess}.trial_data.isRewarded,'true');

    num_trials = size(session_data{sess}.trial_data,1);
    trial_names = cell(1,num_trials);
    trial_numD_Lum = NaN(num_trials,4);
    for t = 1:num_trials
        trial_names{t} = session_data{sess}.trial_def{t}.TrialName;
        tc = trial_names{t}(10:end);
        newStr = split(tc,'_');
        if length(newStr) == 2
            trial_numD_Lum(1) = str2double(newStr{1});%trial #
            trial_numD_Lum(2) = str2double(newStr{2});%lumen & numD
            if  trial_numD_Lum(2) < 10
                trial_numD_Lum(3) = trial_numD_Lum(2);%numD index
                trial_numD_Lum(4) = 1;%lumen index 50
            elseif  trial_numD_Lum(2) < 100
                trial_numD_Lum(3) = trial_numD_Lum(2)/10;%numD index
                trial_numD_Lum(4) = 2;%lumen index 60
            else
                trial_numD_Lum(3) = trial_numD_Lum(2)/100;%numD index
                trial_numD_Lum(4) = 3;%lumen index 70
            end
        elseif length(newStr) > 2
            error('NumD & Lumen Split Incorrectly')
        end
    end

    for t = 1:num_trials
        if ~isnan(trial_numD_Lum(3)) && rewarded(t)
            all_search_durations{sess}{trial_numD_Lum(4),trial_numD_Lum(3)} = [...
                all_search_durations{sess}{trial_numD_Lum(4),trial_numD_Lum(3)} search_duration(t)];
        end
    end
end
%% Calculate average search duration for each session for each lumen level and numD
average_all_search_durations = cell(1,num_sessions);
for sess = 1:num_sessions
    average_all_search_durations{sess} = NaN(3,length(num_distractor_Quaddles));
    for lum  = 1:3
        for numD = 1:length(num_distractor_Quaddles)
            average_all_search_durations{sess}(lum,numD) = mean(all_search_durations{sess}{lum,numD});
        end
    end
end

%% Calculate mean and s.e.m. of search duration across sessions for each lumen level and numD
all_mean_search_durs = NaN(3,length(num_distractor_Quaddles));
all_sem_search_durs = NaN(3,length(num_distractor_Quaddles));

for lum  = 1:3
    for numD = 1:length(num_distractor_Quaddles)
        all_vals = NaN(1,num_sessions);
        for sess = 1:num_sessions
            all_vals(sess) =  average_all_search_durations{sess}(lum,numD);
        end
        all_mean_search_durs(lum,numD) = mean(all_vals);
        all_sem_search_durs(lum,numD) = std(all_vals)./sqrt(num_sessions);
    end
end

%% Calculate Slope for individual sessions
all_slopes = NaN(num_sessions,3);
all_intercepts = NaN(num_sessions,3);
for sess = 1:num_sessions
    for lum = 1:3
        p = polyfit(num_distractor_Quaddles,average_all_search_durations{sess}(lum,:),1);
        all_slopes(sess,lum) = p(1);
        all_intercepts(sess,lum) = p(2);
    end
end
%% Plot results

figure
subplot(2,2,1)
plot(num_distractor_Quaddles,1000*all_mean_search_durs,'o-')
hold on
errorbar(num_distractor_Quaddles,1000*all_mean_search_durs(1,:),1000*all_sem_search_durs(1,:),'k','LineStyle','none')
errorbar(num_distractor_Quaddles,1000*all_mean_search_durs(2,:),1000*all_sem_search_durs(2,:),'k','LineStyle','none')
errorbar(num_distractor_Quaddles,1000*all_mean_search_durs(3,:),1000*all_sem_search_durs(3,:),'k','LineStyle','none')
hold off
legend({'50','60','70'},'location','SouthEast')
xlabel('# Distractors')
ylabel('Search Duration in ms')
xlim([2 13])
box off
axis square
title('Average Search Duration by Luminance & # of Distractors')

subplot(2,2,2)
bar(1000*mean(all_slopes))
hold on
errorbar(1:3,1000*mean(all_slopes),1000*std(all_slopes)./sqrt(num_sessions),'k','LineStyle','none')
hold off
xticks(1:3)
xticklabels({'50','60','70'})
xlabel('Luminance Level')
ylabel('Slope: ms/Distractor')
box off
axis square
title('Average Search Efficiency by Luminance')

subplot(2,2,3)
bar(1000*mean(all_intercepts))
hold on
errorbar(1:3,1000*mean(all_intercepts),1000*std(all_intercepts)./sqrt(num_sessions),'k','LineStyle','none')
hold off
xticks(1:3)
xticklabels({'50','60','70'})
xlabel('Luminance Level')
ylabel('Intercept')
box off
axis square
title('Average Search Intercept by Luminance')
yl = ylim;
minyl = 10*floor(min(1000*mean(all_intercepts)-1.2*1000*std(all_intercepts))/10);
ylim([minyl yl(2)])

subtitle(monkeyname)

%% Eye tracking data analysis if collected eye data
trial_aligned_Python_eye_data = cell(1,num_sessions);
for sess = 1:num_sessions
    if ~isempty(session_data{sess}.gaze_data)
        trial_aligned_Python_eye_data{sess} = GetTrialAlignedPythonGazeData(session_data{sess});
    end
end
%% Analyzes Pupil response to Stimulus Array by luminance & distractor count

luminance_level = NaN(num_sessions,120);
num_distractors = NaN(num_sessions,120);

%epoch aligned pupil responses
fixation_pupil_response = cell(1,num_sessions);
baseline_pupil_response = cell(1,num_sessions);
pupil_stim_array_response = cell(1,num_sessions);
pupil_selection_response = cell(1,num_sessions);
pupil_feedback_response = cell(1,num_sessions);
pupil_reward_response = cell(1,num_sessions);

for sess = 1:num_sessions
    if  ~isempty(session_data{sess}.gaze_data)
        [pixelsize,cmsize] = ReadMonitordetails(session_data{sess}.configdata.monitorDetails);
        screenX = pixelsize(1);
        screenY = pixelsize(2);
        
        block = session_data{sess}.trial_data.Block;
        block = block + 1;%index starts at 0 in C++
        
        trial_ind = 1;
        
        fixation_pupil_response{sess} = NaN(length(block),250);
        baseline_pupil_response{sess} = NaN(length(block),250);
        pupil_stim_array_response{sess} = NaN(length(block),300);
        pupil_selection_response{sess} = NaN(length(block),400);
        pupil_feedback_response{sess} = NaN(length(block),350);
        pupil_reward_response{sess} = NaN(length(block),350);
        for b =2:max(block)%1st block should be fam block
            blockind = find(block == b);
            trial_nums = session_data{sess}.trial_data.TrialInExperiment(blockind);
            for t = 1:length(trial_nums)
                
                trial_name = session_data{sess}.trial_def{trial_nums(t)}.TrialName(10:end);
                newStr = split(trial_name,'_');
                trial_numD_Lum = [];
                if length(newStr) == 2
                    trial_numD_Lum(1) = str2double(newStr{1});%trial #
                    trial_numD_Lum(2) = str2double(newStr{2});%lumen & numD
                    if  trial_numD_Lum(2) < 10
                        trial_numD_Lum(3) = trial_numD_Lum(2);%numD index
                        trial_numD_Lum(4) = 1;%lumen index 50
                    elseif  trial_numD_Lum(2) < 100
                        trial_numD_Lum(3) = trial_numD_Lum(2)/10;%numD index
                        trial_numD_Lum(4) = 2;%lumen index 60
                    else
                        trial_numD_Lum(3) = trial_numD_Lum(2)/100;%numD index
                        trial_numD_Lum(4) = 3;%lumen index 70
                    end
                elseif length(newStr) > 2
                    error('NumD & Lumen Split Incorrectly')
                end
                
                luminance_level(sess,trial_ind) = trial_numD_Lum(4);
                num_distractors(sess,trial_ind) = trial_numD_Lum(3);
                
                frame_trial_ind = find(session_data{sess}.frame_data.TrialInExperiment == trial_nums(t));
                frame_events = session_data{sess}.frame_data.TrialEpoch(frame_trial_ind);
                
                frame_times = session_data{sess}.frame_data.FrameStartUnity(frame_trial_ind);
                frame_times_ET = session_data{sess}.frame_data.EyetrackerTimeStamp(frame_trial_ind);
                
                fix_period = find(frame_events == 2);
                baseline_period = find(frame_events == 3);
                search_period = find(frame_events == 4);
                selection_period = find(frame_events == 5);
                selection_period = findgaps(selection_period);
                final_selection_period = selection_period(end,:);
                final_selection_period(final_selection_period == 0) = [];
                feedback_period = find(frame_events == 6);
                reward_period = find(frame_events == 7);
                
                if isempty(feedback_period)
                    %never finished the trial
                    continue
                end
                %Left & Right pupil
                eyetime = trial_aligned_Python_eye_data{sess}{trial_nums(t)}(:,2);
                Lpupil = trial_aligned_Python_eye_data{sess}{trial_nums(t)}(:,7);
                Rpupil = trial_aligned_Python_eye_data{sess}{trial_nums(t)}(:,8);
                %take the best pupil data
                if sum(isnan(Lpupil)) > sum(isnan(Rpupil))
                    pupil = Rpupil;
                else
                    pupil = Lpupil;
                end
                
                %---Get Fixation Aligned Pupil---%
                t1 = find(eyetime <= frame_times_ET(fix_period(1))-200);%200 ms before
                if isempty(t1)
                    continue
                end
                t1 = t1(end);
                t2 = find(eyetime > frame_times_ET(fix_period(end)));
                if isempty(t2)
                    t2 = length(eyetime);
                end
                t2 = t2(1);
                
                fixation_pupil = pupil(t1:t2);
                baseline_fix_pupil = nanmean(fixation_pupil(end-99:end));
                fixation_pupil = fixation_pupil./baseline_fix_pupil;
                if length(fixation_pupil) >= 250
                    fixation_pupil_response{sess}(trial_ind,:) = fixation_pupil(end-249:end);
                else
                    missing = 250-length(fixation_pupil);
                    fixation_pupil_response{sess}(trial_ind,missing+1:end) = fixation_pupil;
                end
                
                
                %---Get Baseline Aligned Pupil---%
                t1 = find(eyetime <= frame_times_ET(baseline_period(1)));
                if isempty(t1)
                    continue
                end
                t1 = t1(end);
                t2 = find(eyetime > frame_times_ET(baseline_period(end)));
                if isempty(t2)
                    t2 = length(eyetime);
                end
                t2 = t2(1);
                
                baseline_pupil = pupil(t1:t2)./baseline_fix_pupil;
                if length(baseline_pupil) >= 250
                    baseline_pupil_response{sess}(trial_ind,:) = baseline_pupil(1:250);
                else
                    baseline_pupil_response{sess}(trial_ind,1:length(baseline_pupil)) = baseline_pupil;
                end
                
                %---Get Search Array Aligned Pupil---%
                t1 = find(eyetime <= frame_times_ET(search_period(1)));
                if isempty(t1)
                    continue
                end
                t1 = t1(end);
                t2 = find(eyetime > frame_times_ET(search_period(1)+80));
                if isempty(t2)
                    t2 = length(eyetime);
                end
                t2 = t2(1);
                
                array_pupil = pupil(t1:t2)./baseline_fix_pupil;
                if length(array_pupil) >= 300
                    pupil_stim_array_response{sess}(trial_ind,:) = array_pupil(1:300);
                else
                    pupil_stim_array_response{sess}(trial_ind,1:length(array_pupil)) = array_pupil;
                end
                
                %---Get Selection Aligned Pupil---%
                t1 = find(eyetime <= frame_times_ET(final_selection_period(1)-50));
                if isempty(t1)
                    continue
                end
                t1 = t1(end);
                if search_period(1)+250 <= length(frame_times_ET)
                    t2 = find(eyetime > frame_times_ET(final_selection_period(end)));%250 ms after start
                else
                    t2 = find(eyetime > frame_times_ET(end));
                end
                if isempty(t2)
                    t2 = length(eyetime);
                end
                t2 = t2(1);
                
                selection_pupil = pupil(t1:t2)./baseline_fix_pupil;
                if length(selection_pupil) >= 400
                    pupil_selection_response{sess}(trial_ind,:) = selection_pupil(1:400);
                else
                    pupil_selection_response{sess}(trial_ind,1:length(selection_pupil)) = selection_pupil;
                end
                
                %---Get Feedback Aligned Pupil---%
                t1 = find(eyetime <= frame_times_ET(feedback_period(1)-50));
                if isempty(t1)
                    continue
                end
                t1 = t1(end);
                t2 = find(eyetime > frame_times_ET(feedback_period(end)));
                if isempty(t2)
                    t2 = length(eyetime);
                end
                t2 = t2(1);
                
                feedback_pupil = pupil(t1:t2)./baseline_fix_pupil;
                if length(feedback_pupil) >= 350
                    pupil_feedback_response{sess}(trial_ind,:) = feedback_pupil(1:350);
                else
                    pupil_feedback_response{sess}(trial_ind,1:length(feedback_pupil)) = feedback_pupil;
                end
                
                %---Get Reward Aligned Pupil---%
                t1 = find(eyetime <= frame_times_ET(reward_period(1)-3));
                if isempty(t1)
                    continue
                end
                t1 = t1(end);
                t2 = find(eyetime > frame_times_ET(reward_period(end)));
                if isempty(t2)
                    t2 = length(eyetime);
                end
                t2 = t2(1);
                
                reward_pupil = pupil(t1:t2)./baseline_fix_pupil;
                if length(reward_pupil) >= 350
                    pupil_reward_response{sess}(trial_ind,:) = reward_pupil(1:350);
                else
                    pupil_reward_response{sess}(trial_ind,1:length(reward_pupil)) = reward_pupil;
                end
                
                trial_ind = trial_ind+1;
            end
        end
    end
end

%---Get Averages Across Sessions---%
avg_pupil_fixation_across_sessions = NaN(num_sessions,250);
avg_pupil_baseline_across_sessions = NaN(num_sessions,250);
avg_pupil_array_across_sessions = NaN(num_sessions,300);
avg_pupil_selection_across_sessions = NaN(num_sessions,400);
avg_pupil_feedback_across_sessions = NaN(num_sessions,350);
avg_pupil_reward_across_sessions = NaN(num_sessions,350);
for sess = 1:num_sessions
    avg_pupil_fixation_across_sessions(sess,:) = nanmean(fixation_pupil_response{sess});
    avg_pupil_baseline_across_sessions(sess,:) = nanmean(baseline_pupil_response{sess});
    avg_pupil_array_across_sessions(sess,:) = nanmean(pupil_stim_array_response{sess});
    avg_pupil_selection_across_sessions(sess,:) = nanmean(pupil_selection_response{sess});
    avg_pupil_feedback_across_sessions(sess,:) = nanmean(pupil_feedback_response{sess});
    avg_pupil_reward_across_sessions(sess,:) = nanmean(pupil_reward_response{sess});
end

%---Organize Pupil Response for Selection and Search Period by Luminance & # Distractors---%
pupil_array_response_by_numD_and_lum = cell(3,length(num_distractor_Quaddles));
pupil_selection_response_by_numD_and_lum = cell(3,length(num_distractor_Quaddles));%numD should have little affect
for sess = 1:num_sessions
    for lum = 1:3
        for numD = 1:length(num_distractor_Quaddles)
            these_trials = find(luminance_level(sess,:) == lum & num_distractors(sess,:) == numD);
            pupil_array_response_by_numD_and_lum{lum,numD} = [pupil_array_response_by_numD_and_lum{lum,numD};....
                pupil_stim_array_response{sess}(these_trials,:)];
            pupil_selection_response_by_numD_and_lum{lum,numD} = [pupil_selection_response_by_numD_and_lum{lum,numD};....
                pupil_selection_response{sess}(these_trials,:)];
        end
    end
end

pupil_array_response_by_lum = cell(1,3);
pupil_selection_response__lum = cell(1,3);%numD should have little affect
for sess = 1:num_sessions
    for lum = 1:3
        these_trials = find(luminance_level(sess,:) == lum);
        pupil_array_response_by_lum{lum} = [pupil_array_response_by_lum{lum};....
            pupil_stim_array_response{sess}(these_trials,:)];
        pupil_selection_response__lum{lum} = [pupil_selection_response__lum{lum};....
            pupil_selection_response{sess}(these_trials,:)];
    end
end

figure
plot((1:250)./600,mean(avg_pupil_fixation_across_sessions),'k')
hold on
plot((251:500)./600,mean(avg_pupil_baseline_across_sessions),'b')
plot((501:800)./600,mean(avg_pupil_array_across_sessions),'r')
plot((751:1150)./600,mean(avg_pupil_selection_across_sessions),'c');
plot((1151:1500)./600,mean(avg_pupil_feedback_across_sessions),'m');
plot((1501:1850)./600,mean(avg_pupil_reward_across_sessions),'g')
box off
ylabel('Normalized Pupil')
xlabel('Trial Time (sec)')
legend({'Fixation','Baseline','Search','Selection','Feedback','Reward'},'location','NorthEast')
subtitle(monkeyname)

figure
subplot(2,2,1)
hold on
for lum = 1:3
    plot((501:800)./600,nanmean(pupil_array_response_by_lum{lum}))
end
hold off
xlabel('Time from Search Array On (sec)')
ylabel('Normalized Pupil')
legend({'50','60','70'})

subplot(2,2,2)
hold on
for lum = 1:3
    plot((751:1150)./600,nanmean(pupil_selection_response__lum{lum}))
end
hold off
xlabel('Time from Selection (sec)')
ylabel('Normalized Pupil')
legend({'50','60','70'})

colorlines = parula(15);
subplot(2,2,3)
hold on
legend_str = [];
lum_level = {'50','60','70'};
iter = 1;
for lum = 1:3
    for numD = 1:length(num_distractor_Quaddles)
        plot((501:800)./600,nanmean(pupil_array_response_by_numD_and_lum{lum,numD}),'Color',colorlines(iter,:))
        legend_str = [legend_str {[lum_level{lum} ', ' num2str(num_distractor_Quaddles(numD))]}];
        iter = iter+1;
    end
end
hold off
xlabel('Time from Search Array On (sec)')
ylabel('Normalized Pupil')
legend(legend_str)

subplot(2,2,4)
hold on
iter = 1;
for lum = 1:3
    for numD = 1:length(num_distractor_Quaddles)
        plot((751:1150)./600,nanmean(pupil_selection_response_by_numD_and_lum{lum,numD}),'Color',colorlines(iter,:))
         iter = iter+1;
    end
end
hold off
xlabel('Time from Selection (sec)')
ylabel('Normalized Pupil')
legend(legend_str)

subtitle(monkeyname)