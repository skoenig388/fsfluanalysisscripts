%Simple import data to get RTs and Performance


clear,clc,close all%fclose all




trial_name = '__TrialData.txt';
trialdef_name = '__trialdef_on_trial_';
block_name = '__block_defs.json';
%
% monkeyname = 'Wotan';
% sess_file_names = {'FLU__Wotan__20_11_2018__10_47_16','FLU__Wotan__21_11_2018__10_57_12',...
%                    'FLU__Wotan__22_11_2018__12_58_45','FLU__Wotan__23_11_2018__13_48_36',...
%                    'FLU__Wotan__27_11_2018__12_25_21','FLU__Wotan__28_11_2018__14_46_10',...
%                    'FLU__Wotan__30_11_2018__11_44_41','FLU__Wotan__01_12_2018__13_58_02'};
% %

% monkeyname = 'Frey';
% sess_file_names = {'FLU__Frey__23_11_2018__12_57_55','FLU__Frey__27_11_2018__11_26_21',...
%                    'FLU__Frey__26_11_2018__10_30_47',...
%                    'FLU__Frey__28_11_2018__11_38_44','FLU__Frey__30_11_2018__10_55_54',...
%                    'FLU__Frey__01_12_2018__13_07_49','FLU__Frey__03_12_2018__14_28_47'};

% 
monkeyname = 'Combined Frey & Wotan';
sess_file_names = {'FLU__Wotan__20_11_2018__10_47_16','FLU__Wotan__21_11_2018__10_57_12',...
                   'FLU__Wotan__22_11_2018__12_58_45','FLU__Wotan__23_11_2018__13_48_36',...
                   'FLU__Wotan__27_11_2018__12_25_21','FLU__Wotan__28_11_2018__14_46_10',...
                   'FLU__Wotan__30_11_2018__11_44_41','FLU__Wotan__01_12_2018__13_58_02',...
                   'FLU__Frey__23_11_2018__12_57_55','FLU__Frey__27_11_2018__11_26_21',...
                   'FLU__Frey__26_11_2018__10_30_47',...
                   'FLU__Frey__28_11_2018__11_38_44','FLU__Frey__30_11_2018__10_55_54',...
                   'FLU__Frey__01_12_2018__13_07_49','FLU__Frey__03_12_2018__14_28_47'};


%% All blocks

all_block_performance = NaN(10*length(sess_file_names),40);
all_block_search_duration = NaN(10*length(sess_file_names),40);
all_block_ind = 1;

stim_location = NaN(4,5000);
stim_num = 1;
for sess = 1:length(sess_file_names)
    
    if contains(sess_file_names{sess},'Frey')
        log_dir = 'Z:\MonkeyData\Frey\';
    elseif contains(sess_file_names{sess},'Wotan')
        log_dir = 'Z:\MonkeyData\Wotan\';
    end
    
    trial_dir = [log_dir sess_file_names{sess} '\RuntimeData\TrialData\'];
    config_dir = [log_dir sess_file_names{sess} '\RuntimeData\ConfigCopy\'];
    trial_data = readtable([trial_dir sess_file_names{sess} trial_name]);
    block_def = ReadJsonFile([config_dir sess_file_names{sess} block_name]);
    
    search_duration = trial_data.Epoch4_Duration;
    rewarded = strcmpi(trial_data.isRewarded,'true');
    
    block = trial_data.Block;
    block = block + 1;%index starts at 0 in C++
    
    
    for b =2:max(block)
        
        for t= 1:length(block_def(b).TrialDefs)
            
            stim_location(1,stim_num) = block_def(b).TrialDefs(t).relevantObjects(1).StimLocation.x;
            stim_location(2,stim_num) = block_def(b).TrialDefs(t).relevantObjects(1).StimLocation.z;
            
            stim_location(3,stim_num) = block_def(b).TrialDefs(t).relevantObjects(2).StimLocation.x;
            stim_location(4,stim_num) = block_def(b).TrialDefs(t).relevantObjects(2).StimLocation.z;
            
            stim_num = stim_num+1;
        end
        blockind = find(block == b);
        blockrewarded = rewarded(blockind(1)-10:blockind(end));
        blocksearchdur = search_duration(blockind(1)-10:blockind(end));
        
        if length(blockind) < 15
            continue
        end
        blocksearchdur(blocksearchdur >= 2.5) = NaN;%timed out or not paying attention
        blocksearchdur(blocksearchdur < 0) = NaN;
        
        
        if length(blockrewarded) > 40
            all_block_performance(all_block_ind,1:40) = blockrewarded(1:40);
            all_block_search_duration(all_block_ind,1:40) = blocksearchdur(1:40);
        else
            all_block_performance(all_block_ind,1:length(blockrewarded)) = blockrewarded;
            all_block_search_duration(all_block_ind,1:length(blocksearchdur)) = blocksearchdur;
        end
        
        all_block_ind = all_block_ind + 1;
    end
    
end
% Remove extra NaNs
all_block_performance = laundry(all_block_performance);
all_block_search_duration = laundry(all_block_search_duration);
% Calculate Average Performance and Search Duration
total_num_blocks = size(all_block_performance,1);
num_not_nans = sum(~isnan(all_block_performance));

average_performance = nansum(all_block_performance)./num_not_nans;
average_search_duration = nansum(all_block_search_duration)./num_not_nans;

%% Intra- vs Extra-Dimensional Shifts

all_intra_block_performance = NaN(10*length(sess_file_names),40);
all_intra_block_search_duration = NaN(10*length(sess_file_names),40);
all_intra_block_ind = 1;

all_extra_block_performance = NaN(10*length(sess_file_names),40);
all_extra_block_search_duration = NaN(10*length(sess_file_names),40);
all_extra_block_ind = 1;
for sess = 1:length(sess_file_names)
    
    if contains(sess_file_names{sess},'Frey')
        log_dir = 'Z:\MonkeyData\Frey\';
    elseif contains(sess_file_names{sess},'Wotan')
        log_dir = 'Z:\MonkeyData\Wotan\';
    end
    
    
    trial_dir = [log_dir sess_file_names{sess} '\RuntimeData\TrialData\'];
    config_dir = [log_dir sess_file_names{sess} '\RuntimeData\ConfigCopy\'];
    trial_data = readtable([trial_dir sess_file_names{sess} trial_name]);
    block_def = ReadJsonFile([config_dir sess_file_names{sess} block_name]);
    
    search_duration = trial_data.Epoch4_Duration;
    rewarded = strcmpi(trial_data.isRewarded,'true');
    
    block = trial_data.Block;
    block = block + 1;%index starts at 0 in C++
    
    for b =2:max(block)
        
        if contains(block_def(b).BlockID,'Intra')
            intra_extra = 1;
        elseif contains(block_def(b).BlockID,'Extra')
            intra_extra = 2;
        else
            error('blockID not recongized')
        end
        blockind = find(block == b);
        blockrewarded = rewarded(blockind(1)-10:blockind(end));
        blocksearchdur = search_duration(blockind(1)-10:blockind(end));
        
        if length(blockind) < 15
            continue
        end
        blocksearchdur(blocksearchdur >= 2.5) = NaN;%timed out or not paying attention
        blocksearchdur(blocksearchdur < 0) = NaN;
        
        if intra_extra == 1
            if length(blockrewarded) > 40
                all_intra_block_performance(all_intra_block_ind,1:40) = blockrewarded(1:40);
                all_intra_block_search_duration(all_intra_block_ind,1:40) = blocksearchdur(1:40);
            else
                all_intra_block_performance(all_intra_block_ind,1:length(blockrewarded)) = blockrewarded;
                all_intra_block_search_duration(all_intra_block_ind,1:length(blocksearchdur)) = blocksearchdur;
            end
            all_intra_block_ind = all_intra_block_ind + 1;
        else
            if length(blockrewarded) > 40
                all_extra_block_performance(all_extra_block_ind,1:40) = blockrewarded(1:40);
                all_extra_block_search_duration(all_extra_block_ind,1:40) = blocksearchdur(1:40);
            else
                all_extra_block_performance(all_extra_block_ind,1:length(blockrewarded)) = blockrewarded;
                all_extra_block_search_duration(all_extra_block_ind,1:length(blocksearchdur)) = blocksearchdur;
            end
            all_extra_block_ind = all_extra_block_ind + 1;
        end
    end
end
% Remove extra NaNs
all_intra_block_performance = laundry(all_intra_block_performance);
all_intra_block_search_duration = laundry(all_intra_block_search_duration);
all_extra_block_performance = laundry(all_extra_block_performance);
all_extra_block_search_duration = laundry(all_extra_block_search_duration);

% Calculate Average Performance and Search Duration
total_num_intra_blocks = size(all_intra_block_performance,1);
num_not_nans_intra = sum(~isnan(all_intra_block_performance));
total_num_extra_blocks = size(all_extra_block_performance,1);
num_not_nans_extra = sum(~isnan(all_extra_block_performance));

average_intra_performance = nansum(all_intra_block_performance)./num_not_nans_intra;
average_intra_search_duration = nansum(all_intra_block_search_duration)./num_not_nans_intra;
average_extra_performance = nansum(all_extra_block_performance)./num_not_nans_extra;
average_extra_search_duration = nansum(all_extra_block_search_duration)./num_not_nans_extra;


%% Plot Results

figure
subplot(2,2,1)
plot(-9:30,100*average_performance)
hold on
plot([0.5 0.5],[0 100],'k--')
plot([-10 30],[80 80],'r--')
plot([-10 30],[50 50],'b--')
hold off
ylim([40 100])
xlabel('Trial # Relative To Switch')
ylabel('Performance %')
box off
title('All Switches: Average Performance')

asd = average_search_duration';
asd = [asd(10:-1:1); asd; asd(end:-1:end-9)];
asd = filtfilt(1/3*ones(1,3),1,asd);
asd = asd(11:end-10);

subplot(2,2,2)
plot(-9:30,average_search_duration)
yl = ylim;
hold on
plot([0.5 0.5],[yl(1) yl(2)],'k--')
plot(-9:30,asd,'r')
hold off
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (seconds)')
box off
title('All Switches: Average Search Duration')



subplot(2,2,3)
plot(-9:30,100*average_intra_performance)
hold on
plot(-9:30,100*average_extra_performance)
plot([0.5 0.5],[0 100],'k--')
plot([-10 30],[80 80],'k--')
plot([-10 30],[50 50],'k--')
hold off
ylim([40 100])
xlabel('Trial # Relative To Switch')
ylabel('Performance %')
box off
title(['Intra(n = ' num2str(total_num_intra_blocks) ')- vs Extradimensional (n = ' num2str(total_num_extra_blocks)...
    ') Shifts: Average Performance'])
legend('Intra','Extra')

asd = average_search_duration';
asd = [asd(10:-1:1); asd; asd(end:-1:end-9)];
asd = filtfilt(1/3*ones(1,3),1,asd);
asd = asd(11:end-10);

subplot(2,2,4)
plot(-9:30,average_intra_search_duration)
hold on
plot(-9:30,average_extra_search_duration)
yl = ylim;
plot([0.5 0.5],[yl(1) yl(2)],'k--')
hold off
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (seconds)')
box off
title(['Intra(n = ' num2str(total_num_intra_blocks) ')- vs Extradimensional (n = ' num2str(total_num_extra_blocks)...
    ') Shifts: Average Search Duration'])


subtitle(['Shift Learning-' monkeyname ': N = ' num2str(total_num_blocks) ' learning blocks'])
%%
stim_location = laundry(stim_location);

locs = NaN(3,4);
locs(1,1) = sum(stim_location(1,:) == 4 & stim_location(2,:) == 4) + sum(stim_location(3,:) == 4 & stim_location(4,:) == 4);
locs(1,2) = sum(stim_location(1,:) == -4 & stim_location(2,:) == 4) +  sum(stim_location(3,:) == -4 & stim_location(4,:) == 4);
locs(1,3) = sum(stim_location(1,:) == 4 & stim_location(2,:) == -4) + sum(stim_location(3,:) == 4 & stim_location(4,:) == -4);
locs(1,4) = sum(stim_location(1,:) == -4 & stim_location(2,:) == -4) + sum(stim_location(3,:) == -4 & stim_location(4,:) == -4);

locs(2,1) = sum(stim_location(1,:) == 4 & stim_location(2,:) == 4);
locs(2,2) = sum(stim_location(1,:) == -4 & stim_location(2,:) == 4);
locs(2,3) = sum(stim_location(1,:) == 4 & stim_location(2,:) == -4);
locs(2,4) = sum(stim_location(1,:) == -4 & stim_location(2,:) == -4);

locs(3,1) = sum(stim_location(3,:) == 4 & stim_location(4,:) == 4);
locs(3,2) = sum(stim_location(3,:) == -4 & stim_location(4,:) == 4);
locs(3,3) = sum(stim_location(3,:) == 4 & stim_location(4,:) == -4);
locs(3,4) = sum(stim_location(3,:) == -4 & stim_location(4,:) == -4);