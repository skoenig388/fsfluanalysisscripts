%Simple import data to get RTs and Performance
%Re-Written by Seth Koenig 3/8/19 to be cleaner and hopefully faster

clar

% monkeyname = 'Wotan';
% sess_file_names = {'FLU__Wotan__04_11_2018__12_25_57','FLU__Wotan__05_11_2018__13_19_07',...
%     'FLU__Wotan__06_11_2018__12_54_38','FLU__Wotan__07_11_2018__11_41_06',...
%     'FLU__Wotan__08_11_2018__10_41_47','FLU__Wotan__13_11_2018__11_06_25',...
%     'FLU__Wotan__15_11_2018__11_38_51','FLU__Wotan__17_11_2018__13_59_03',...
%     'FLU__Wotan__19_11_2018__11_45_33'};

% monkeyname = 'Frey';
% sess_file_names = {'FLU__Frey__02_11_2018__12_48_59','FLU__Frey__03_11_2018__12_17_59',...
%     'FLU__Frey__04_11_2018__11_41_03','FLU__Frey__05_11_2018__12_40_57',...
%     'FLU__Frey__08_11_2018__09_46_50','FLU__Frey__13_11_2018__10_21_50',...
%     'FLU__Frey__14_11_2018__13_16_53','FLU__Frey__20_11_2018__10_05_39',...
%     'FLU__Frey__21_11_2018__10_14_58','FLU__Frey__22_11_2018__12_09_02'};



% monkeyname = 'Combined Frey & Wotan';
% sess_file_names = {'FLU__Frey__02_11_2018__12_48_59','FLU__Frey__03_11_2018__12_17_59',...
%     'FLU__Frey__04_11_2018__11_41_03','FLU__Frey__05_11_2018__12_40_57',...
%     'FLU__Frey__08_11_2018__09_46_50','FLU__Frey__13_11_2018__10_21_50',...
%     'FLU__Frey__14_11_2018__13_16_53','FLU__Frey__20_11_2018__10_05_39',...
%     'FLU__Frey__21_11_2018__10_14_58','FLU__Frey__22_11_2018__12_09_02',...
%     'FLU__Wotan__04_11_2018__12_25_57','FLU__Wotan__05_11_2018__13_19_07',...
%     'FLU__Wotan__06_11_2018__12_54_38','FLU__Wotan__07_11_2018__11_41_06',...
%     'FLU__Wotan__08_11_2018__10_41_47','FLU__Wotan__13_11_2018__11_06_25',...
%     'FLU__Wotan__15_11_2018__11_38_51','FLU__Wotan__17_11_2018__13_59_03',...
%     'FLU__Wotan__19_11_2018__11_45_33'};

% monkeyname = 'Bard';
% sess_file_names = {'FLU__Bard__07_03_2019__10_10_54','FLU__Bard__08_03_2019__08_28_21'};

% monkeyname = 'Reider';
% sess_file_names = {'FLU__Reider__06_03_2019__09_48_53','FLU__Reider__07_03_2019__08_56_58',...
%     'FLU__Reider__08_03_2019__08_17_22'};

monkeyname = 'Igor';
% sess_file_names = {'FLU__Igor__01_03_2019__11_00_08','FLU__Igor__04_03_2019__08_56_29','FLU__Igor__05_03_2019__08_36_09',...
%     'FLU__Igor__06_03_2019__08_45_42','FLU__Igor__08_03_2019__09_40_22'};

monkeyname = 'All 6 monkeys';
sess_file_names = {'FLU__Frey__02_11_2018__12_48_59','FLU__Frey__03_11_2018__12_17_59',...
    'FLU__Frey__04_11_2018__11_41_03','FLU__Frey__05_11_2018__12_40_57',...
    'FLU__Frey__08_11_2018__09_46_50','FLU__Frey__13_11_2018__10_21_50',...
    'FLU__Frey__14_11_2018__13_16_53','FLU__Frey__20_11_2018__10_05_39',...
    'FLU__Frey__21_11_2018__10_14_58','FLU__Frey__22_11_2018__12_09_02',...
    'FLU__Wotan__04_11_2018__12_25_57','FLU__Wotan__05_11_2018__13_19_07',...
    'FLU__Wotan__06_11_2018__12_54_38','FLU__Wotan__07_11_2018__11_41_06',...
    'FLU__Wotan__08_11_2018__10_41_47','FLU__Wotan__13_11_2018__11_06_25',...
    'FLU__Wotan__15_11_2018__11_38_51','FLU__Wotan__17_11_2018__13_59_03',...
    'FLU__Wotan__19_11_2018__11_45_33',...
    'FLU__Bard__07_03_2019__10_10_54','FLU__Bard__08_03_2019__08_28_21',...
    'FLU__Bard__11_03_2019__08_29_25','FLU__Bard__12_03_2019__08_50_28',...
    'FLU__Bard__14_03_2019__14_30_31','FLU__Bard__22_03_2019__09_38_19',...
    'FLU__Bard__15_03_2019__09_17_51','FLU__Bard__18_03_2019__09_03_42',...
    'FLU__Sindri__14_03_2019__09_16_50','FLU__Sindri__15_03_2019__11_10_00',...
    'FLU__Sindri__18_03_2019__10_09_58','FLU__Sindri__19_03_2019__09_57_50',...
    'FLU__Sindri__20_03_2019__09_14_04','FLU__Sindri__21_03_2019__08_04_01',...
    'FLU__Sindri__22_03_2019__08_36_00','FLU__Sindri__25_03_2019__08_39_42',...
    'FLU__Reider__07_03_2019__08_56_58','FLU__Reider__08_03_2019__08_17_22',...
    'FLU__Reider__11_03_2019__08_48_17','FLU__Reider__12_03_2019__09_52_23',...
    'FLU__Reider__13_03_2019__08_14_29','FLU__Reider__14_03_2019__14_16_09',...
    'FLU__Reider__15_03_2019__11_31_30','FLU__Reider__18_03_2019__09_19_16',...
    'FLU__Igor__01_03_2019__11_00_08','FLU__Igor__04_03_2019__08_56_29',...
    'FLU__Igor__05_03_2019__08_36_09','FLU__Igor__06_03_2019__08_45_42',...
    'FLU__Igor__08_03_2019__09_40_22','FLU__Igor__11_03_2019__09_53_12',...
    'FLU__Igor__14_03_2019__09_10_21','FLU__Igor__15_03_2019__09_15_08',...
    };

%% Import Data
num_sessions = length(sess_file_names);
session_data = cell(1,num_sessions);
session_dimension = NaN(1,num_sessions);
which_monkey = NaN(1,num_sessions);
for sess = 1:num_sessions
    disp(['Impoting Data from Session #' num2str(sess) '/' num2str(num_sessions)])
    session_data{sess} = ImportFLU_Data(sess_file_names{sess});
   [dimVals,neutralVals] = ParseQuaddleName(session_data{sess}.trial_def{1}.relevantObjects(1).StimName);
    session_dimension(sess) = find(neutralVals == 0);
    which_monkey(sess) = DetermineWhichMonkey(sess_file_names{sess});
end
which_dimensions = unique(session_dimension);
%% Get Learning Curves and Search Durations
learning_curves_by_dimension = cell(1,max(which_dimensions));
search_duration_curves_by_dimension = cell(1,max(which_dimensions));

learning_curves_by_monkey = cell(1,max(which_monkey));
search_duration_curves_by_monkey = cell(1,max(which_monkey));

all_block_performance = NaN(10*length(sess_file_names),40);
all_block_search_duration = NaN(10*length(sess_file_names),40);
all_block_ind = 1;
for sess = 1:length(sess_file_names)
    
    this_dim = session_dimension(sess);
    this_monkey = which_monkey(sess);
    
    search_duration = session_data{sess}.trial_data.Epoch4_Duration;
    rewarded = strcmpi(session_data{sess}.trial_data.isRewarded,'true');
    
    block = session_data{sess}.trial_data.Block;
    block = block + 1;%index starts at 0 in C++
    
    for b =2:max(block)
        blockind = find(block == b);
        blockrewarded = rewarded(blockind(1)-10:blockind(end));
        blocksearchdur = search_duration(blockind(1)-10:blockind(end));
        
        if length(blockind) < 12
            continue
        end
        blocksearchdur(blocksearchdur >= 2.5) = NaN;%timed out or not paying attention
        blocksearchdur(blocksearchdur < 0.25) = NaN;%too fast to be real
        
        if length(blockrewarded) >= 40
            all_block_performance(all_block_ind,1:40) = blockrewarded(1:40);
            all_block_search_duration(all_block_ind,1:40) = blocksearchdur(1:40);
            
            search_duration_curves_by_dimension{this_dim} = [search_duration_curves_by_dimension{this_dim}; blocksearchdur(1:40)'];
            learning_curves_by_dimension{this_dim} = [learning_curves_by_dimension{this_dim}; blockrewarded(1:40)'];
            
            search_duration_curves_by_monkey{this_monkey} = [search_duration_curves_by_monkey{this_monkey}; blocksearchdur(1:40)'];
            learning_curves_by_monkey{this_monkey} = [learning_curves_by_monkey{this_monkey}; blockrewarded(1:40)'];
            
        else
            all_block_performance(all_block_ind,1:length(blockrewarded)) = blockrewarded;
            all_block_search_duration(all_block_ind,1:length(blocksearchdur)) = blocksearchdur;
            
            search_duration_curves_by_dimension{this_dim} = [search_duration_curves_by_dimension{this_dim};...
                [blocksearchdur; NaN(40-length(blockrewarded),1)]'];
            learning_curves_by_dimension{this_dim} = [learning_curves_by_dimension{this_dim}; ...
                [blockrewarded;  NaN(40-length(blockrewarded),1)]'];
            
            search_duration_curves_by_monkey{this_monkey} = [search_duration_curves_by_monkey{this_monkey};...
                [blocksearchdur; NaN(40-length(blockrewarded),1)]'];
            learning_curves_by_monkey{this_monkey} = [learning_curves_by_monkey{this_monkey}; ...
                [blockrewarded;  NaN(40-length(blockrewarded),1)]'];
        end
        
        all_block_ind = all_block_ind + 1;
    end
    
end
%% Plot Average Results
total_num_blocks = size(all_block_performance,1);
num_not_nans = sum(~isnan(all_block_performance));

average_performance = nansum(all_block_performance)./num_not_nans;
average_search_duration = nansum(all_block_search_duration)./num_not_nans;

figure
subplot(1,2,1)
plot(-9:30,100*average_performance)
hold on
plot([0.5 0.5],[0 100],'k--')
plot([-10 30],[80 80],'r--')
hold off
xlabel('Trial # Relative To Switch')
ylabel('Performance %')
box off
title('Average Performance')

asd = average_search_duration';
asd = [asd(10:-1:1); asd; asd(end:-1:end-9)];
asd = filtfilt(1/5*ones(1,5),1,asd);
asd = asd(11:end-10);

subplot(1,2,2)
plot(-9:30,average_search_duration)
yl = ylim;
hold on
plot([0.5 0.5],[yl(1) yl(2)],'k--')
plot(-9:30,FilterLearningCurves(average_search_duration',3),'r')
hold off
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (seconds)')
box off
title('Average Search Duration')

subtitle(['Reversal Learning-' monkeyname ': N = ' num2str(total_num_blocks) ' learning blocks'])

%% Plot Results by Feature Dimension

n_dim = cellfun(@(s) size(s,1),learning_curves_by_dimension);

figure
subplot(1,2,1)
hold on
for dim = 1:length(which_dimensions)
       plot(-9:30,100*FilterLearningCurves(nanmean(learning_curves_by_dimension{which_dimensions(dim)})',3))
end
plot(-9:30,100*average_performance,'k')
plot([0.5 0.5],[0 100],'k--')
plot([-10 30],[80 80],'r--')
hold off
xlabel('Trial # Relative To Switch')
ylabel('Performance %')
title('Average Performance')
legend({['Shape, n = ' num2str(n_dim(1))],['Pattern, n = ' num2str(n_dim(2))],...
    ['Color n, = ' num2str(n_dim(3))],['Arm, n = ' num2str(n_dim(5))], ...
    ['All n = ' num2str(total_num_blocks)]},'Location','SouthEast')

subplot(1,2,2)
hold on
for dim = 1:length(which_dimensions)
    %smooth because otherwise it's too noisy
    if ~isempty(search_duration_curves_by_dimension{which_dimensions(dim)})
         plot(-9:30,FilterLearningCurves(nanmean(search_duration_curves_by_dimension{which_dimensions(dim)})',3))
    end
end
yl = ylim;
plot([0.5 0.5],[yl(1) yl(2)],'k--')
hold off
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (seconds)')
title('Average Search Duration')

subtitle(['Feature Dimension-specific Reversal Learning Curves: ' monkeyname])

%% Plot Results by Feature Dimension
if length(unique(which_monkey)) > 1
    n_monkey = cellfun(@(s) size(s,1),learning_curves_by_monkey);
    
    figure
    subplot(1,2,1)
    hold on
    for monk = 1:length(learning_curves_by_monkey)
        if ~isempty(learning_curves_by_monkey{monk})
            plot(-9:30,100*FilterLearningCurves(nanmean(learning_curves_by_monkey{monk})',3))
        else
            plot([0 0],[0 0]);
        end
    end
    plot(-9:30,100*average_performance,'k')
    plot([0.5 0.5],[0 100],'k--')
    plot([-10 30],[80 80],'r--')
    hold off
    xlabel('Trial # Relative To Switch')
    ylabel('Performance %')
    title('Average Performance')
    legend({['Bard, n = ' num2str(n_monkey(1))],   ['Frey, n = ' num2str(n_monkey(2))],...
        ['Igor n, = ' num2str(n_monkey(3))],   ['Reider, n = ' num2str(n_monkey(4))],...
        ['Sindri n, = ' num2str(n_monkey(5))], ['Wotan, n = ' num2str(n_monkey(6))],...
        ['All n = ' num2str(total_num_blocks)]},'Location','SouthEast')
    
    subplot(1,2,2)
    hold on
    for monk = 1:length(search_duration_curves_by_monkey)
        %smooth because otherwise it's too noisy
        if ~isempty(search_duration_curves_by_monkey{monk})
            plot(-9:30,FilterLearningCurves(nanmean(search_duration_curves_by_monkey{monk})',3))
        else
            plot([])
        end
    end
    yl = ylim;
    plot([0.5 0.5],[yl(1) yl(2)],'k--')
    hold off
    xlabel('Trial # Relative To Switch')
    ylabel('Search Duration (seconds)')
    title('Smoothed Search Duration')
    
    subtitle('Monkey-specific Reversal Learning Curves')
    
end