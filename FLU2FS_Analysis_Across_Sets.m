%Written by Seth Konig 5/20/19 follows a lot of the FLU limited scripts

tic
clar
import_option = 'slim';

%average acceptable performance across blocks
miniumum_performance = 0.5;%set to 0 if you don't want to remove blocks
max_block_cut = 3;%of of 1st and last blocks to cut if performance is very bad
min_FLU_block_len = 25;%miniumum # of trials in a FLU block
max_FLU_block_len = 60;
last_trial_window = 10;%window to look at before end of block

%Learning Point (LP) variables
learning_point_threshold = 80;
forward_back_trials = 7;
forward_smoothing_window = 10; %size of the smoothing window for forward filter

%FS Parameters
%num_Distractors = [3 6 9 12];
num_Distractors = [3 5 7];
min_FS_performance = 6/9;%for analysis

%%%%%%%%%%%%%%%%
%%---Wotan---%%%
%%%%%%%%%%%%%%%%

%---FLU2FS with only 3 distractors---%
%didn't do very well
% monkeyname = 'Wotan';
% sess_file_names = {'FLU__Wotan__21_05_2019__12_32_17','FLU__Wotan__22_05_2019__13_15_46',...
%     'FLU__Wotan__23_05_2019__10_47_19','FLU__Wotan__24_05_2019__10_09_18',...
%     'FLU__Wotan__28_05_2019__12_10_28','FLU__Wotan__29_05_2019__14_25_43',...
%     'FLU__Wotan__30_05_2019__12_42_03','FLU__Wotan__31_05_2019__13_35_38'};

%---FLU2FS with 3 Re-familiarization Trials & up to 12 distractors---%
% monkeyname = 'Wotan';
% sess_file_names = {'FLU__Wotan__01_06_2019__15_04_11','FLU__Wotan__03_06_2019__12_35_47'};

%---FLU2FS v3 with 8 stimuli for 9 FS and 25-35 FLU trials, no Fam---%
%min_FS_block_len = 9;%miniumum # of trials in a FS block
% monkeyname = 'Wotan';
% sess_file_names = {'FLU2FS__Wotan__22_06_2019__12_57_48','FLU2FS__Wotan__24_06_2019__11_50_27',...
%     'FLU2FS__Wotan__25_06_2019__12_32_03','FLU2FS__Wotan__26_06_2019__11_54_38',...
%     'FLU2FS__Wotan__27_06_2019__10_44_26','FLU2FS__Wotan__01_07_2019__11_43_55',...
%     'FLU2FS__Wotan__02_07_2019__12_18_25','FLU2FS__Wotan__03_07_2019__13_01_27',...
%     'FLU2FS__Wotan__04_07_2019__11_57_48','FLU2FS__Wotan__05_07_2019__10_45_20',...
%     'FLU2FS__Wotan__08_07_2019__12_14_17','FLU2FS__Wotan__09_07_2019__11_16_52'};


%---FLU2FS v3 with 8 stimuli for 24 FS and 25-35 FLU trials, no Fam---%
% min_FS_block_len = 24;%miniumum # of trials in a FS block
% monkeyname = 'Wotan';
% sess_file_names = {'FLU2FS__Wotan__11_07_2019__13_08_24','FLU2FS__Wotan__12_07_2019__12_27_12'};

min_FS_block_len = 9;
monkeyname = 'Igor';
sess_file_names = {'FLU2FS__Igor__17_07_2019__10_57_58','FLU2FS__Igor__18_07_2019__10_34_30',...
    'FLU2FS__Igor__19_07_2019__07_29_39','FLU2FS__Igor__17_07_2019__10_57_58',...
    'FLU2FS__Igor__22_07_2019__10_25_50','FLU2FS__Igor__23_07_2019__08_40_31',...
    };


%% Import Session Data
num_sessions = length(sess_file_names);
session_data = cell(1,num_sessions);
which_monkey = NaN(1,num_sessions);
for sess =1:num_sessions
    which_monkey(sess) = DetermineWhichMonkey(sess_file_names{sess});

    disp(['Loading data from session #' num2str(sess) '/' num2str(length(sess_file_names))])
    log_dir = FindDataDirectory(sess_file_names{sess});
    if ~isempty(log_dir)
        session_data{sess} = ImportFLU_DataV2(sess_file_names{sess},log_dir,import_option);
    else
        error(['Cant Find Log Dir for '  sess_file_names{sess}])
    end
end

%% Calculate Averege Performance by Block and Remove "Bad" Blocks
all_FLU_FS_blocks = cell(1,num_sessions);
all_block_performance = cell(1,num_sessions);
all_FLU_block_performance = NaN(num_sessions,30);
all_FS_block_performance = NaN(num_sessions,30);
for sess = 1:num_sessions
    [all_block_performance{sess},all_FLU_FS_blocks{sess}] = FLU2FS_Avg_Block_Performance(session_data{sess},...
        min_FLU_block_len,min_FS_block_len);
    FLU_perform = all_block_performance{sess}(all_FLU_FS_blocks{sess} == 1);
    FS_perform = all_block_performance{sess}(all_FLU_FS_blocks{sess} == 2);
    all_FLU_block_performance(sess,1:length(FLU_perform)) = FLU_perform;
    all_FS_block_performance(sess,1:length(FS_perform)) = FS_perform;
end

figure
subplot(2,2,1)
if num_sessions > 1
    plot(nanmean(all_FLU_block_performance))
else
    plot(all_FLU_block_performance)
end
xlabel('Block #')
ylabel('Performance-Proportion Correct')
title('Average FLU Performance by Block')
box off

subplot(2,2,2)
hist(all_FLU_block_performance(:),20)
xlabel('Performance-Proportion Correct')
ylabel('Block Count')
title('Distribution of FLU Block Performance')
box off

subplot(2,2,3)
if num_sessions > 1
    plot(nanmean(all_FS_block_performance))
else
    plot(all_FS_block_performance)
end
xlabel('Block #')
ylabel('Performance-Proportion Correct')
title('Average FS Performance by Block')
box off

subplot(2,2,4)
hist(all_FS_block_performance(:),9)
xlabel('Performance-Proportion Correct')
ylabel('Block Count')
title('Distribution of FS Block Performance')
box off

subtitle([monkeyname ': Performance by Block #'])
box off

%% Calculate Averege Performance by number of irrelevant features

all_LP_by_num_irrel = cell(1,3);
all_LP_aligned_block_performance_by_num_irrel = cell(1,3);
all_FLU_block_performance_by_num_irrel = cell(1,3);
all_FLU_search_duration_by_num_irrel = cell(1,3);
all_FS_block_performance_by_num_irrel = cell(length(num_Distractors),3);
all_FS_search_duration_by_num_irrel = cell(length(num_Distractors),3);

average_performance_by_num_irrel_and_session = NaN(3,num_sessions);
average_search_duration_by_num_irrel_and_session = NaN(3,num_sessions);

for sess = 1:num_sessions
    [FLU_performance_by_num_irrel,FLU_search_duration_by_num_irrel,...
        LP_by_num_irrel,LP_aligned_FLU_performance_by_num_irrel,...
        FS_performance_by_num_irrel,FS_search_duration_by_num_irrel] = ...
        CalculateBlockPerformancebyNumIrrelDim_FLU2FS(session_data{sess},...
        all_FLU_FS_blocks{sess},min_FLU_block_len,min_FS_block_len,last_trial_window,...
        forward_smoothing_window,learning_point_threshold,forward_back_trials);
    
    for num_irrel = 1:3
        all_LP_by_num_irrel{num_irrel} = [all_LP_by_num_irrel{num_irrel} LP_by_num_irrel{num_irrel}];
        all_LP_aligned_block_performance_by_num_irrel{num_irrel} = ...
            [all_LP_aligned_block_performance_by_num_irrel{num_irrel}; LP_aligned_FLU_performance_by_num_irrel{num_irrel}];
        all_FLU_block_performance_by_num_irrel{num_irrel} = [all_FLU_block_performance_by_num_irrel{num_irrel}; ...
            FLU_performance_by_num_irrel{num_irrel}];
        all_FLU_search_duration_by_num_irrel{num_irrel} = [all_FLU_search_duration_by_num_irrel{num_irrel}; ...
            FLU_search_duration_by_num_irrel{num_irrel}];
        
        
        all_trial_performance = [];
        all_search_dur = [];
        for numD = 1:length(num_Distractors)
            all_FS_block_performance_by_num_irrel{numD,num_irrel} = [...
                all_FS_block_performance_by_num_irrel{numD,num_irrel}; ...
                FS_performance_by_num_irrel{numD,num_irrel}];
            all_FS_search_duration_by_num_irrel{numD,num_irrel} = [...
                all_FS_search_duration_by_num_irrel{numD,num_irrel}; ...
                FS_search_duration_by_num_irrel{numD,num_irrel}];
            
            all_trial_performance = [all_trial_performance;  FS_performance_by_num_irrel{numD,num_irrel}];
            all_search_dur = [all_search_dur; FS_search_duration_by_num_irrel{numD,num_irrel}];
        end
        
        average_performance_by_num_irrel_and_session(num_irrel,sess) = nanmean(all_trial_performance(:));
        average_search_duration_by_num_irrel_and_session(num_irrel,sess) = nanmean(all_search_dur(:));
    end
end

n_FLU_irrel = NaN(1,3);
for num_irrel = 1:3
    n_FLU_irrel(num_irrel) = size(all_FLU_block_performance_by_num_irrel{num_irrel},1);
end

%---Plot FLU Results by Number of irrelevant featurs
figure
subplot(2,2,1)
hold on
plot([0.5 0.5],[0 100],'k--')
plot([-(last_trial_window-1) min_FLU_block_len],[33 33],'k--')
plot([-(last_trial_window-1) min_FLU_block_len],[66 66],'k--')
plot([-(last_trial_window-1) min_FLU_block_len],[80 80],'k--')
for num_irrel = 1:3
    if n_FLU_irrel(num_irrel) > 0
        pnirr(num_irrel) = plot(-(last_trial_window-1):min_FLU_block_len,...
            FilterLearningCurves(100*nanmean(all_FLU_block_performance_by_num_irrel{num_irrel})',3,2));
    else
        pnirr(num_irrel) = plot(0,0);
    end
end
hold off
box off
ylim([0 100])
xlabel('Trial # Relative To Switch')
ylabel('Performance (% Correct)')
title('Performance by # of Irrelevant Dimensions')
legend(pnirr,{['0, n = ' num2str(n_FLU_irrel(1))],['1, n = ' num2str(n_FLU_irrel(2))],...
    ['2, n = ' num2str(n_FLU_irrel(3))]},'Location','SouthEast')

subplot(2,2,2)
hold on
for num_irrel = 1:3
    if n_FLU_irrel(num_irrel) > 0
        plot(-(last_trial_window-1):min_FLU_block_len,FilterLearningCurves(nanmean(all_FLU_search_duration_by_num_irrel{num_irrel})',3,2));
    else
        plot(0,1)
    end
end
yl = ylim;
plot([0.5 0.5],[yl(1) yl(2)],'k--')
hold off
box off
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (sec)')
title('Search Duration by # of Irrelevant Dimensions')

subplot(2,2,3)
hold on
plot([0 0],[0 100],'k--')
plot([-forward_back_trials forward_back_trials],[33 33],'k--')
plot([-forward_back_trials forward_back_trials],[66 66],'k--')
for num_irrel = 1:3
    plot(-forward_back_trials:forward_back_trials,100*nanmean(all_LP_aligned_block_performance_by_num_irrel{num_irrel}))
end
ylim([0 100])
xlim([-forward_back_trials forward_back_trials])
hold off
box off
xlabel('Trial from LP')
ylabel('Performance (% Correct)')
title('Backward Learning Curves by # of Irrelevant Dimensions')

subplot(2,2,4)
hold on
for num_irrel = 1:3
    n_dist = hist(all_LP_by_num_irrel{num_irrel},1:2:60);
    n_dist = n_dist./sum(n_dist);
    plot(1:2:60,n_dist);
end
hold off
box off
xlabel('LP trial #')
ylabel('Proportion of Blocks')
title('LP Distribution # of Irrelevant Dimensions')

%---Plot FS Results by Number of irrelevant featurs
average_performance_by_num_irrel = cell(1,3);
average_search_duration_by_num_irrel = cell(1,3);
for num_irrel = 1:3
    for numD = 1:length(num_Distractors)
        average_performance_by_num_irrel{num_irrel} = [...
            average_performance_by_num_irrel{num_irrel}; ...
            all_FS_block_performance_by_num_irrel{numD,num_irrel}];
        
        %only want search durations on correct trials
        these_search_durs = all_FS_search_duration_by_num_irrel{numD,num_irrel};
        these_search_durs(these_search_durs > 5) = NaN;
        these_search_durs(all_FS_block_performance_by_num_irrel{numD,num_irrel} == 0) = NaN;
        average_search_duration_by_num_irrel{num_irrel} = [...
            average_search_duration_by_num_irrel{num_irrel}; these_search_durs];
    end
end

figure
subplot(2,2,1)
hold on
for num_irrel = 1:3
    plot(100*nanmean(average_performance_by_num_irrel{num_irrel}))
end
hold off
legend({'0 irrel','1 irrel','2 irrel'},'Location','NorthEastOutside')
title('FS Performance by # Irrel')
ylabel('Performance (% Correct)')
xlabel('Trial #')

subplot(2,2,2)
hold on
for num_irrel = 1:3
    plot(nanmean(average_search_duration_by_num_irrel{num_irrel}))
end
hold off
title('FS Search Duration by # Irrel')
ylabel('Search Duration (sec)')
xlabel('Trial #')

subplot(2,2,3)
plot(100*average_performance_by_num_irrel_and_session')
hold on
plot(100*nanmean(average_performance_by_num_irrel_and_session),'k')
hold off
xlabel('Session #')
ylabel('Performance (% Correct)')
title('Performance by Session')
box off

subplot(2,2,4)
plot(average_search_duration_by_num_irrel_and_session')
xlabel('Session #')
ylabel('Search Duration (Sec)')
title('Search Duration by Session')
box off
%% Plot FS Performance by # of Distractors, of Irrelenvant Dimensions, Performance, etc.

Calculate_FLU2FS_FSonly_anlaysis(session_data,all_FLU_FS_blocks,...
    min_FLU_block_len,min_FS_block_len,min_FS_performance)

%% Calculate Performance for Intra-vs Extra-dimensional shifts and Cued vs Uncued Shifts
all_intra_performance = [];
all_extra_performance = [];
all_intra_search_duration = [];
all_extra_search_duration = [];

all_intra_extra_by_num_irrel_performance = cell(2,3);
all_intra_extra_by_num_irrel_search_duration = cell(2,3);

all_FLU_blocks = cell(1,num_sessions);
for sess = 1:num_sessions
    these_FLU_blocks = find(all_FLU_FS_blocks{sess} == 1);
    all_FLU_blocks{sess} = these_FLU_blocks;
    
    [intra_extra_block_performance,intra_extra_search_duration,...
        intra_extra_shift,cued_uncued_shift,intra_extra_num_irrel] = ...
        CalculateBlockPerformanceIntra_vs_Extra_Dim(...
        session_data{sess},these_FLU_blocks,min_FLU_block_len,last_trial_window);
    
    all_intra_performance = [all_intra_performance; intra_extra_block_performance(intra_extra_shift == 1,:)];
    all_extra_performance = [all_extra_performance; intra_extra_block_performance(intra_extra_shift == 2,:)];
    
    all_intra_search_duration = [all_intra_search_duration; intra_extra_search_duration(intra_extra_shift == 1,:)];
    all_extra_search_duration = [all_extra_search_duration; intra_extra_search_duration(intra_extra_shift == 2,:)];
    
    for intra_extra = 1:2
        for num_irrel = 0:2
            all_intra_extra_by_num_irrel_performance{intra_extra,num_irrel+1} = [all_intra_extra_by_num_irrel_performance{intra_extra,num_irrel+1};...
                intra_extra_block_performance(intra_extra_shift == intra_extra & intra_extra_num_irrel == num_irrel,:)];
            
            all_intra_extra_by_num_irrel_search_duration{intra_extra,num_irrel+1} = [all_intra_extra_by_num_irrel_search_duration{intra_extra,num_irrel+1};...
                intra_extra_search_duration(intra_extra_shift == intra_extra & intra_extra_num_irrel == num_irrel,:)];
        end
    end
end

%---Plots for Intradimensional vs Extradimensional---%
figure
subplot(2,2,1)
hold on
plot([0.5 0.5],[0 100],'k--')
plot([-(last_trial_window-1) min_FLU_block_len],[80 80],'k--')
plot([-(last_trial_window-1) min_FLU_block_len],[33 33],'k--')
plot([-(last_trial_window-1) min_FLU_block_len],[66 66],'k--')
p1 = plot(-(last_trial_window-1):min_FLU_block_len,FilterLearningCurves(100*nanmean(all_intra_performance)',3,2),'r');
p2 = plot(-(last_trial_window-1):min_FLU_block_len,FilterLearningCurves(100*nanmean(all_extra_performance)',3,2),'b');
hold off
ylim([0 100])
xlabel('Trial # Relative To Switch')
ylabel('Performance (% Correct)')
box off
title('Performance: Intra vs Extra-Dim Shifts')
legend([p1 p2],{'Intra','Extra'},'Location','SouthEast')

subplot(2,2,3)
plot(-(last_trial_window-1):min_FLU_block_len,FilterLearningCurves((100*nanmean(all_extra_performance)-100*nanmean(all_intra_performance))',3,2),'m');
hold on
yl = ylim;
plot([0 0],[yl(1) yl(2)],'k--')
plot([-(last_trial_window-1) min_FLU_block_len ],[0 0],'k--')
hold off
xlabel('Trial # Relative To Switch')
ylabel('Performance (% Correct)')
box off
title('Performance Difference: Extra-Intra')

subplot(2,2,2)
hold on
plot(-(last_trial_window-1):min_FLU_block_len,FilterLearningCurves(nanmean(all_intra_search_duration)',3,2),'r');
plot(-(last_trial_window-1):min_FLU_block_len,FilterLearningCurves(nanmean(all_extra_search_duration)',3,2),'b');
yl = ylim;
plot([0 0],[yl(1) yl(2)],'k--')
hold off
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (sec)')
box off
title('Search Duration: Intra vs Extra-Dim Shifts')

subplot(2,2,4)
plot(-(last_trial_window-1):min_FLU_block_len,FilterLearningCurves((nanmean(all_extra_search_duration)-nanmean(all_intra_search_duration))',3,1),'m');
hold on
yl = ylim;
plot([0 0],[yl(1) yl(2)],'k--')
plot([-(last_trial_window-1) min_FLU_block_len ],[0 0],'k--')
hold off
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (Sec)')
box off
title('Search Duration Difference: Extra-Intra')

subtitle(monkeyname)

%%
%---Plot FLU Performance for intra-vs-extra for different number of irrelevant features---%
clrs = ['rgb'];
styles = ['-:'];
figure
subplot(2,2,1)
legend_str = [];
hold on
for intra_extra = 1:2
    for num_irrel = 0:2
        plot(-(last_trial_window-1):min_FLU_block_len,FilterLearningCurves(100*nanmean(all_intra_extra_by_num_irrel_performance{intra_extra,num_irrel+1})',3,2),...
            [clrs(num_irrel+1) styles(intra_extra)]);
        if intra_extra == 1
            legend_str = [legend_str {['Intra, ' num2str(num_irrel) ' irrel']}];
        else
            legend_str = [legend_str {['Extra, ' num2str(num_irrel) ' irrel']}];
        end
    end
end
plot([0.5 0.5],[0 100],'k--')
plot([-(last_trial_window-1) min_FLU_block_len ],[33 33],'k--')
ylim([0 100])
hold off
xlabel('Trial # Relative To Switch')
ylabel('Performance (% Correct)')
legend(legend_str,'location','SouthEast')

subplot(2,2,3)
hold on
for num_irrel = 0:2
    plot(-(last_trial_window-1):min_FLU_block_len,FilterLearningCurves(...
        (100*nanmean(all_intra_extra_by_num_irrel_performance{2,num_irrel+1}) - ...
        100*nanmean(all_intra_extra_by_num_irrel_performance{1,num_irrel+1}))',3,2),...
        [clrs(num_irrel+1) styles(1)]);
end
yl = ylim;
plot([0.5 0.5],[yl(1) yl(2)],'k--')
plot([-(last_trial_window-1) min_FLU_block_len ],[0 0],'k--')
hold off
xlabel('Trial # Relative To Switch')
ylabel('Performance Difference (% Correct)')
box off
title('Performance Difference by #Irrel: Extra-Intra')

subplot(2,2,2)
legend_str = [];
hold on
for intra_extra = 1:2
    for num_irrel = 0:2
        plot(-(last_trial_window-1):min_FLU_block_len,FilterLearningCurves(nanmean(all_intra_extra_by_num_irrel_search_duration{intra_extra,num_irrel+1})',3,2),...
            [clrs(num_irrel+1) styles(intra_extra)]);
    end
end
yl = ylim;
plot([0.5 0.5],[yl(1) yl(2)],'k--')
hold off
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (sec)')
title('Search Duration by #Irrel & Extra vs Intra')


subplot(2,2,4)
hold on
for num_irrel = 0:2
    plot(-(last_trial_window-1):min_FLU_block_len,FilterLearningCurves(...
        (nanmean(all_intra_extra_by_num_irrel_search_duration{2,num_irrel+1}) - ...
        nanmean(all_intra_extra_by_num_irrel_search_duration{1,num_irrel+1}))',3,2),...
        [clrs(num_irrel+1) styles(1)]);
end
yl = ylim;
plot([0.5 0.5],[yl(1) yl(2)],'k--')
plot([-(last_trial_window-1) min_FLU_block_len ],[0 0],'k--')
hold off
xlabel('Trial # Relative To Switch')
ylabel('Search Duration Difference (sec)')
box off
title('Search Duration Difference by #Irrel: Extra-Intra')

subtitle(monkeyname)
%% Plot performance and search duration by target and distractor dimensions
[block_performance_by_monkey,search_duration_by_monkey] = ...
    CalculateBlockPerformancebyDimension(session_data,all_FLU_blocks,min_FLU_block_len,...
    last_trial_window);
