function varargout = FLUAnalysisGUI2(varargin)
% FLUANALYSISGUI2 MATLAB code for FLUAnalysisGUI2.fig
%      FLUANALYSISGUI2, by itself, creates a new FLUANALYSISGUI2 or raises the existing
%      singleton*.
%
%      H = FLUANALYSISGUI2 returns the handle to a new FLUANALYSISGUI2 or the handle to
%      the existing singleton*.
%
%      FLUANALYSISGUI2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FLUANALYSISGUI2.M with the given input arguments.
%
%      FLUANALYSISGUI2('Property','Value',...) creates a new FLUANALYSISGUI2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FLUAnalysisGUI2_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FLUAnalysisGUI2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FLUAnalysisGUI2

% Last Modified by GUIDE v2.5 14-Nov-2018 16:42:53

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @FLUAnalysisGUI2_OpeningFcn, ...
    'gui_OutputFcn',  @FLUAnalysisGUI2_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FLUAnalysisGUI2 is made visible.
function FLUAnalysisGUI2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FLUAnalysisGUI2 (see VARARGIN)

% Choose default command line output for FLUAnalysisGUI2
handles.selpath = '';
handles.output = hObject;
% Update handles structure
guidata(hObject, handles);
% UIWAIT makes FLUAnalysisGUI2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = FLUAnalysisGUI2_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isempty(handles.selpath)
    handles.selpath = uigetdir;
    guidata(hObject, handles);
else
    handles.selpath = uigetdir(handles.selpath);
end
analyze_data(hObject,handles);


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fid = fopen([handles.selpath '\' handles.sess_file_name '_Peformance.txt'],'w');
fprintf(fid,handles.full_performance_str);
fclose(fid);

% --- Executes during object creation, after setting all properties.
function axes1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: place code in OpeningFcn to populate axes1
handles.axes1 = findobj(gcf,'Tag','axes1');


% --- Executes during object creation, after setting all properties.
function axes2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: place code in OpeningFcn to populate axes2
handles.axes2 = findobj(gcf,'Tag','axes2');


% --- Executes during object creation, after setting all properties.
function axes3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: place code in OpeningFcn to populate axes3
handles.axes3 = findobj(gcf,'Tag','axes3');


% --- Executes during object creation, after setting all properties.
function text2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called



function analyze_data(hObject,handles)

%---Get File Paths---%
folderpath = handles.selpath;
dashes = strfind(folderpath,'\');

log_dir = folderpath(1:dashes(length(dashes)));
sess_file_name = folderpath(dashes(length(dashes))+1:end);

trial_dir = [log_dir sess_file_name '\RuntimeData\TrialData\'];
trial_name = '__TrialData.txt';
trialdef_name = '__trialdef_on_trial_';

config_dir = [log_dir sess_file_name '\RuntimeData\ConfigCopy\'];
block_name = '__block_defs.json';


%---Get Task Config File---%
%task determines how to do basic anlayses and which plots to make
list = dir(config_dir);
config_file_name = [];
for l = 1:length(list)
    if list(l).isdir
        continue
    else
        if contains(list(l).name,'BlockDef')
            config_file_name = [config_dir list(l).name];
            break
        end
    end
end
task_type = [];
if contains(config_file_name,'CFS')
    task_type = 'CFS';
elseif contains(config_file_name,'FLU2FS')
    task_type = 'FLU2FS';
elseif contains(config_file_name,'FLU')
    task_type = 'FLU';
elseif contains(config_file_name,'FS')
    task_type = 'FS';
elseif contains(config_file_name,'VDM') || contains(config_file_name,'VDS')
    task_type = 'VDM';
else
    task_type = 'VDM';%just default to VDM since plot is the simplest and makes no assumptions
end

%---Read in File Data---%
trial_data = readtable([trial_dir sess_file_name trial_name]);
block_def = ReadJsonFile([config_dir sess_file_name block_name]);

%---Get Basic Trial and Perforamnce Data---%
search_start_time = trial_data.Epoch3_StartTimeRelative+trial_data.baselineDuration;
search_end_time =  trial_data.Epoch7_StartTimeRelative...
    -trial_data.Epoch6_Duration-trial_data.Epoch5_Duration;
search_duration = search_end_time-search_start_time;

rewarded = strcmpi(trial_data.isRewarded,'true');
highest_reward = strcmpi(trial_data.isHighestProbReward,'true');
if all(rewarded == highest_reward)
    prob_reward = false;
else
    prob_reward = true;
end

block = trial_data.Block;
block = block + 1;%index starts at 0 in C++
num_blocks = max(block);

if isnan(search_duration(end)) || search_duration(end) == -1 %last trial wasn't initiated
    trial_count = length(search_duration)-1;
    rewarded = rewarded(1:end-1);
    search_duration = search_duration(1:end-1);
    block = block(1:end-1);
else
    trial_count = length(search_duration);
end

%---Smooth Performance and Search Duration Data for Plotting---%
sd = search_duration;
sd = [sd(10:-1:1); sd; sd(end:-1:end-9)];
sd = filtfilt(1/10*ones(1,10),1,sd);
sd = sd(10:end-10);

rwd = 100*rewarded;
rwd = [rwd(10:-1:1); rwd; rwd(end:-1:end-9)];
rwd = filtfilt(1/10*ones(1,10),1,rwd);
rwd = rwd(10:end-10);

%---Plot Smoothed Search Duration---%
axes(handles.axes1); % Switch current axes to axes1.
plot(sd)
hold on
yl = ylim;
for b = 2:max(block)
    blockind = find(block == b);
    if ~isempty(blockind)
        plot([blockind(1) blockind(1)],[yl(1) yl(2)],'k--')
    end
end
hold off
xlim([0 length(block)+1])
xlabel('Trial #')
ylabel('Search Duration (sec)')
title('Smoothed Search Duration')
box off


%---Make Plots for Different Tasks--%
switch task_type
    case {'FLU','FLU2FS'}
        average_performance = cell(1,2);
        average_search_duration = cell(1,2);
        
        %---Get # of Trials in Each Block---%
        min_num_trials = NaN(1,num_blocks);
        FLU_or_FS_block = NaN(1,num_blocks);
        for b = 1:num_blocks
            min_num_trials(b) = block_def(b).TrialRange(1);
            blockname = block_def(b).BlockID;
            if contains(blockname,'FLU')
                FLU_or_FS_block(b) = 1;
            elseif contains(blockname,'FS')
                FLU_or_FS_block(b) = 2;
            else
                FLU_or_FS_block(b) = NaN;
            end
        end
        min_FLU_trials = min_num_trials(FLU_or_FS_block == 1);
        min_FLU_trials = min(min_FLU_trials);
        num_FS_trials =  min_num_trials(FLU_or_FS_block == 2);
        num_FS_trials = min(num_FS_trials);
        
        %---Calculate Performance and Search Duration by Block---%
        all_block_performance_prob = NaN(num_blocks,min_FLU_trials+10);
        all_block_performance = cell(1,2);
        all_block_search_duration = cell(1,2);
        for task = 1:2
            if task == 1
                all_block_performance{task} = NaN(num_blocks,min_FLU_trials+10);
                all_block_search_duration{task} = NaN(num_blocks,min_FLU_trials+10);
            else
                all_block_performance{task} = NaN(num_blocks,num_FS_trials+2);%extra trial buffer
                all_block_search_duration{task} = NaN(num_blocks,num_FS_trials+2);
            end
        end
        
        for b =1:num_blocks
            if FLU_or_FS_block(b) == 1 %FLU blocks
                
                blockind = find(block == b);
                if length(blockind) < min_FLU_trials %too short to see learning curves or block not completed
                    continue
                end
                
                blockrewarded = rewarded(blockind(1):blockind(end));
                blockrewarded = [blockrewarded(end-9:end); blockrewarded];
                blocksearchdur = search_duration(blockind(1):blockind(end));
                blocksearchdur = [blocksearchdur(end-9:end); blocksearchdur];
                
                all_block_performance{1}(b,:) = blockrewarded(1:min_FLU_trials+10);
                all_block_search_duration{1}(b,:) = blocksearchdur(1:min_FLU_trials+10);
                
                blockrewarded_prob = highest_reward(blockind(1):blockind(end));
                blockrewarded_prob = [blockrewarded_prob(end-9:end); blockrewarded_prob];
                all_block_performance_prob(b,:) = blockrewarded_prob(1:min_FLU_trials+10);
            elseif FLU_or_FS_block(b) == 2 %FS blocks
                
                blockind = find(block == b);
                if length(blockind) < num_FS_trials %too short to see learning curves or block not completed
                    continue
                end
                
                blockrewarded = rewarded(blockind(1):blockind(end));
                blocksearchdur = search_duration(blockind(1):blockind(end));
                
                if blockrewarded > size(all_block_performance{2},2)%shouldn't really happen if monkey is trying
                    blockrewarded = blockrewarded(1:size(all_block_performance{2},1));
                    blocksearchdur = blocksearchdur(1:size(all_block_performance{2},1));
                end
                all_block_performance{2}(b,1:length(blockrewarded)) = blockrewarded;
                all_block_search_duration{2}(b,1:length(blocksearchdur)) = blocksearchdur;
            end
        end
        
        for task = 1:2
            average_performance{task} = nanmean(all_block_performance{task});
            average_search_duration{task} = nanmean(all_block_search_duration{task});
        end
        average_prob_performance = nanmean(all_block_performance_prob);
        
        %---Plot Average FLU Performance---%
        axes(handles.axes2); % Switch current axes to axes2.
        plot(-9:min_FLU_trials,100*average_performance{1})
        hold on
        if prob_reward
            plot(-9:min_FLU_trials,100*average_prob_performance)
        end
        plot([0.5 0.5],[0 100],'k--')
        plot([-10 min_FLU_trials],[80 80],'r--')
        plot([-10 min_FLU_trials],[50 50],'b--')
        plot([-10 min_FLU_trials],[33 33],'k--')
        hold off
        if prob_reward
            legend({'Raw','Corrected'},'Location','SouthEast')
        end
        xlim([-10 min_FLU_trials])
        ylim([0 100])
        xlabel('Trial # Relative To Switch')
        title('Average FLU Performance')
        ylabel('Performance (% Correct)')
        box off
        
        %---Plot Average FLU Search Duration---%
        axes(handles.axes3); % Switch current axes to axes3.
        plot(-9:min_FLU_trials,average_search_duration{1})
        yl = ylim;
        hold on
        plot([0.5 0.5],[yl(1) yl(2)],'k--')
        hold off
        xlabel('Trial # Relative To Switch')
        title('Average FLU Search Duration')
        ylabel('Search Duration (sec)')
        box off
        
    case {'FS','CFS'}
        average_performance = NaN(1,num_blocks);
        average_search_duration = NaN(1,num_blocks);
        for b =1:num_blocks
            blockind = find(block == b);
            if length(blockind) < 10
                continue
            end
            blockrewarded = rewarded(blockind(1):blockind(end));
            blocksearchdur = search_duration(blockind(1):blockind(end));
            
            average_performance(b) = mean(blockrewarded);
            average_search_duration(b) = mean(blocksearchdur);
        end
        
        %---Plot Performance by Block #---%
        axes(handles.axes2); % Switch current axes to axes2.
        plot(1:num_blocks,100*average_performance);
        xlim([0.5 num_blocks+0.5]);
        xlabel('Block #')
        title('Performance')
        ylabel('Performance (% Correct)')
        box off
        
        %---Plot Search Duration by Trial #---%
        axes(handles.axes3); % Switch current axes to axes3.
        plot(1:num_blocks,average_search_duration);
        xlim([0.5 num_blocks+0.5]);
        xlabel('Block #')
        ylabel('Search Duration (sec)')
        title('Search Duration')
        box off
        
    case {'VDM'}
        average_performance = rewarded;
        average_search_duration  = search_duration;
        
        %---Plot Performance by Trial #---%
        axes(handles.axes2); % Switch current axes to axes2.
        plot(1:length(average_performance),100*average_performance);
        xlim([0 length(average_performance)+1]);
        xlabel('Trial #')
        title('Performance')
        ylabel('Performance (% Correct)')
        box off
        
        %---Plot Search Duration by Trial #---%
        axes(handles.axes3); % Switch current axes to axes3.
        plot(1:length(average_search_duration),average_search_duration);
        xlim([0 length(average_search_duration) + 1]);
        xlabel('Trial #')
        title('Search Duration')
        ylabel('Search Duration (sec)')
        box off
end


%---Get Necessary Information For Performance Text---%
peformance_str = ['\t\t Completed ' num2str(trial_count) ' trials \n' ...
    '\t\t Completed ' num2str(max(block)) ' blocks \n' ...
    '\t Average Performance was ' num2str(100*sum(rewarded)/trial_count,3) '%% \n' ...
    '------------------------------------------------------------------'];

switch task_type
    case {'FLU'}
        block_perform_str = ['\n' ....
            'Average Performance on trials ' num2str(min_FLU_trials-10) '-' num2str(min_FLU_trials) ...
            ' in block = ' num2str(100*mean(average_performance{1}(end-9:end)),3) '%% \n' ...
            'Average Performance on last 10 trials in block = ' num2str(100*mean(average_performance{1}(1:10)),3) '%%' ...
            '\n'  '------------------------------------------------------------------' '\n'];
        
        if prob_reward
            block_perform_str = [block_perform_str 'Performance Corrected for Probabilistic Reward\n' ...
                'Average Performance on trials ' num2str(min_FLU_trials-10) '-' num2str(min_FLU_trials) ...
                ' in block = ' num2str(100*mean(average_prob_performance(end-9:end)),3) '%% \n' ...
                'Average Performance on last 10 trials in block = ' num2str(100*mean(average_prob_performance(1:10)),3) '%%' ...
                '\n'  '------------------------------------------------------------------' '\n'];
            
        end
    case {'FLU2FS'}
        FLU_perform_str = ['\n' ....
            'Average Performance on FLU trials ' num2str(min_FLU_trials-10) '-' num2str(min_FLU_trials) ...
            ' in block = ' num2str(100*mean(average_performance{1}(end-9:end)),3) '%% \n' ...
            'Average Performance on last 10 FLU trials in block = ' num2str(100*mean(average_performance{1}(1:10)),3) '%%' ...
            '\n'  '------------------------------------------------------------------' '\n'];
        
        FS_perform_str = ['Average Performance on FS trials ' num2str(100*nanmean(average_performance{2}(:)),3) '%%' ...
            '\n'  '------------------------------------------------------------------' '\n'];
        
        block_perform_str = [FLU_perform_str FS_perform_str];
        
    case {'FS','CFS'}
        block_perform_str = ['\n' ...
            'Average Performance on last Block = ' num2str(100*average_performance(end),3) '%% \n'...
            'Average Performance on last 10 trials = ' num2str(100*mean(rewarded(end-9:end)),3) '%%' ...
            '\n'  '------------------------------------------------------------------' '\n'];
        
    case {'VDM'}
        block_perform_str = ['\n' ...
            'Average Performance on last 10 trials = ' num2str(100*mean(rewarded(end-9:end)),3) '%%' ...
            '\n'  '------------------------------------------------------------------' '\n'];
end

%---Calculate Performance by Block---%
all_block_perform_str = [];
for b =1:max(block)
    blockind = find(block == b);
    blockrewarded = rewarded(blockind);
    all_block_perform_str = [all_block_perform_str '\n' 'Performance on ' block_def(b).BlockID ' was ' num2str(100*sum(blockrewarded)/length(blockrewarded),3) '%%'];
end

full_performance_str = ['\n\t\t' sess_file_name ' \n\n ' peformance_str block_perform_str all_block_perform_str];
set(handles.text2,'string',sprintf(full_performance_str));

handles.sess_file_name = sess_file_name;
handles.full_performance_str = full_performance_str;
guidata(hObject, handles);
return

function JsonData = ReadJsonFile(filename)
fid = fopen(filename);
raw = fread(fid,inf);
str = char(raw');
fclose(fid);
JsonData = jsondecode(str);


% --- Executes when figure1 is resized.
function figure1_SizeChangedFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
