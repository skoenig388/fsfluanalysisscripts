% %Written by Seth Konig 5/20/19 follows a lot of the FLU limited scripts
%
% tic
% clar
% import_option = 'all';
%
% %average acceptable performance across blocks
% miniumum_performance = 0.5;%set to 0 if you don't want to remove blocks
% max_block_cut = 3;%of of 1st and last blocks to cut if performance is very bad
% min_FLU_block_len = 25;%miniumum # of trials in a FLU block
% max_FLU_block_len = 60;
% min_FS_block_len = 9;%miniumum # of trials in a FS block
% last_trial_window = 10;%window to look at before end of block
%
% %Learning Point (LP) variables
% learning_point_threshold = 80;
% forward_back_trials = 7;
% forward_smoothing_window = 10; %size of the smoothing window for forward filter
%
% %FS Parameters
% num_Distractors = [3 5 7];
% min_FS_performance = 6/9;%for analysis
%
% %%%%%%%%%%%%%%%%
% %%---Wotan---%%%
% %%%%%%%%%%%%%%%%
%
% %---FLU2FS v3 with 8 stimuli for FS and FLU trials, no Fam---%
% monkeyname = 'Wotan';
% sess_file_names = {'FLU2FS__Wotan__22_06_2019__12_57_48','FLU2FS__Wotan__24_06_2019__11_50_27',...
%     'FLU2FS__Wotan__25_06_2019__12_32_03'};
%
% %% Import Session Data
% num_sessions = length(sess_file_names);
% session_data = cell(1,num_sessions);
% which_monkey = NaN(1,num_sessions);
% for sess =1:num_sessions
%     which_monkey(sess) = DetermineWhichMonkey(sess_file_names{sess});
%
%     disp(['Loading data from session #' num2str(sess) '/' num2str(length(sess_file_names))])
%     log_dir = FindDataDirectory(sess_file_names{sess});
%     if ~isempty(log_dir)
%         session_data{sess} = ImportFLU_DataV2(sess_file_names{sess},log_dir,import_option);
%     else
%         error(['Cant Find Log Dir for '  sess_file_names{sess}])
%     end
% end
%
% %% Calculate Averege Performance by Block and Remove "Bad" Blocks
% all_FLU_FS_blocks = cell(1,num_sessions);
% all_block_performance = cell(1,num_sessions);
% all_FLU_block_performance = NaN(num_sessions,min_FLU_block_len);
% all_FS_block_performance = NaN(num_sessions,min_FLU_block_len);
% for sess = 1:num_sessions
%     [all_block_performance{sess},all_FLU_FS_blocks{sess}] = FLU2FS_Avg_Block_Performance(session_data{sess},...
%         min_FLU_block_len,min_FS_block_len);
%     FLU_perform = all_block_performance{sess}(all_FLU_FS_blocks{sess} == 1);
%     FS_perform = all_block_performance{sess}(all_FLU_FS_blocks{sess} == 2);
%     all_FLU_block_performance(sess,1:length(FLU_perform)) = FLU_perform;
%     all_FS_block_performance(sess,1:length(FS_perform)) = FS_perform;
% end
%
% figure
% subplot(2,2,1)
% if num_sessions > 1
%     plot(nanmean(all_FLU_block_performance))
% else
%     plot(all_FLU_block_performance)
% end
% xlabel('Block #')
% ylabel('Performance-Proportion Correct')
% title('Average FLU Performance by Block')
% box off
%
% subplot(2,2,2)
% hist(all_FLU_block_performance(:),20)
% xlabel('Performance-Proportion Correct')
% ylabel('Block Count')
% title('Distribution of FLU Block Performance')
% box off
%
% subplot(2,2,3)
% if num_sessions > 1
%     plot(nanmean(all_FS_block_performance))
% else
%     plot(all_FS_block_performance)
% end
% xlabel('Block #')
% ylabel('Performance-Proportion Correct')
% title('Average FS Performance by Block')
% box off
%
% subplot(2,2,4)
% hist(all_FS_block_performance(:),9)
% xlabel('Performance-Proportion Correct')
% ylabel('Block Count')
% title('Distribution of FS Block Performance')
% box off
%
% subtitle([monkeyname ': Performance by Block #'])
% box off
%
% %% Calculate Averege Performance by number of irrelevant features
%
% all_LP_by_num_irrel = cell(1,3);
% all_LP_aligned_block_performance_by_num_irrel = cell(1,3);
% all_FLU_block_performance_by_num_irrel = cell(1,3);
% all_FLU_search_duration_by_num_irrel = cell(1,3);
% all_FS_block_performance_by_num_irrel = cell(length(num_Distractors),3);
% all_FS_search_duration_by_num_irrel = cell(length(num_Distractors),3);
%
% average_performance_by_num_irrel_and_session = NaN(3,num_sessions);
% average_search_duration_by_num_irrel_and_session = NaN(3,num_sessions);
%
% for sess = 1:num_sessions
%     [FLU_performance_by_num_irrel,FLU_search_duration_by_num_irrel,...
%         LP_by_num_irrel,LP_aligned_FLU_performance_by_num_irrel,...
%         FS_performance_by_num_irrel,FS_search_duration_by_num_irrel] = ...
%         CalculateBlockPerformancebyNumIrrelDim_FLU2FS(session_data{sess},...
%         all_FLU_FS_blocks{sess},min_FLU_block_len,min_FS_block_len,last_trial_window,...
%         forward_smoothing_window,learning_point_threshold,forward_back_trials);
%
%     for num_irrel = 1:3
%         all_LP_by_num_irrel{num_irrel} = [all_LP_by_num_irrel{num_irrel} LP_by_num_irrel{num_irrel}];
%         all_LP_aligned_block_performance_by_num_irrel{num_irrel} = ...
%             [all_LP_aligned_block_performance_by_num_irrel{num_irrel}; LP_aligned_FLU_performance_by_num_irrel{num_irrel}];
%         all_FLU_block_performance_by_num_irrel{num_irrel} = [all_FLU_block_performance_by_num_irrel{num_irrel}; ...
%             FLU_performance_by_num_irrel{num_irrel}];
%         all_FLU_search_duration_by_num_irrel{num_irrel} = [all_FLU_search_duration_by_num_irrel{num_irrel}; ...
%             FLU_search_duration_by_num_irrel{num_irrel}];
%
%
%         all_trial_performance = [];
%         all_search_dur = [];
%         for numD = 1:length(num_Distractors)
%             all_FS_block_performance_by_num_irrel{numD,num_irrel} = [...
%                 all_FS_block_performance_by_num_irrel{numD,num_irrel}; ...
%                 FS_performance_by_num_irrel{numD,num_irrel}];
%             all_FS_search_duration_by_num_irrel{numD,num_irrel} = [...
%                 all_FS_search_duration_by_num_irrel{numD,num_irrel}; ...
%                 FS_search_duration_by_num_irrel{numD,num_irrel}];
%
%             all_trial_performance = [all_trial_performance;  FS_performance_by_num_irrel{numD,num_irrel}];
%             all_search_dur = [all_search_dur; FS_search_duration_by_num_irrel{numD,num_irrel}];
%         end
%
%         average_performance_by_num_irrel_and_session(num_irrel,sess) = nanmean(all_trial_performance(:));
%         average_search_duration_by_num_irrel_and_session(num_irrel,sess) = nanmean(all_search_dur(:));
%     end
% end
%
% n_FLU_irrel = NaN(1,3);
% for num_irrel = 1:3
%     n_FLU_irrel(num_irrel) = size(all_FLU_block_performance_by_num_irrel{num_irrel},1);
% end
%
% %---Plot FLU Results by Number of irrelevant featurs
% figure
% subplot(2,2,1)
% hold on
% plot([0.5 0.5],[0 100],'k--')
% plot([-(last_trial_window-1) min_FLU_block_len],[33 33],'k--')
% plot([-(last_trial_window-1) min_FLU_block_len],[66 66],'k--')
% plot([-(last_trial_window-1) min_FLU_block_len],[80 80],'k--')
% for num_irrel = 1:3
%     if n_FLU_irrel(num_irrel) > 0
%         pnirr(num_irrel) = plot(-(last_trial_window-1):min_FLU_block_len,...
%             FilterLearningCurves(100*nanmean(all_FLU_block_performance_by_num_irrel{num_irrel})',3,2));
%     else
%         pnirr(num_irrel) = plot(0,0);
%     end
% end
% hold off
% box off
% ylim([0 100])
% xlabel('Trial # Relative To Switch')
% ylabel('Performance (% Correct)')
% title('Performance by # of Irrelevant Dimensions')
% legend(pnirr,{['0, n = ' num2str(n_FLU_irrel(1))],['1, n = ' num2str(n_FLU_irrel(2))],...
%     ['2, n = ' num2str(n_FLU_irrel(3))]},'Location','SouthEast')
%
% subplot(2,2,2)
% hold on
% for num_irrel = 1:3
%     if n_FLU_irrel(num_irrel) > 0
%         plot(-(last_trial_window-1):min_FLU_block_len,FilterLearningCurves(nanmean(all_FLU_search_duration_by_num_irrel{num_irrel})',3,2));
%     else
%         plot(0,1)
%     end
% end
% yl = ylim;
% plot([0.5 0.5],[yl(1) yl(2)],'k--')
% hold off
% box off
% xlabel('Trial # Relative To Switch')
% ylabel('Search Duration (sec)')
% title('Search Duration by # of Irrelevant Dimensions')
%
% subplot(2,2,3)
% hold on
% plot([0 0],[0 100],'k--')
% plot([-forward_back_trials forward_back_trials],[33 33],'k--')
% plot([-forward_back_trials forward_back_trials],[66 66],'k--')
% for num_irrel = 1:3
%     plot(-forward_back_trials:forward_back_trials,100*nanmean(all_LP_aligned_block_performance_by_num_irrel{num_irrel}))
% end
% ylim([0 100])
% xlim([-forward_back_trials forward_back_trials])
% hold off
% box off
% xlabel('Trial from LP')
% ylabel('Performance (% Correct)')
% title('Backward Learning Curves by # of Irrelevant Dimensions')
%
% subplot(2,2,4)
% hold on
% for num_irrel = 1:3
%     n_dist = hist(all_LP_by_num_irrel{num_irrel},1:2:60);
%     n_dist = n_dist./sum(n_dist);
%     plot(1:2:60,n_dist);
% end
% hold off
% box off
% xlabel('LP trial #')
% ylabel('Proportion of Blocks')
% title('LP Distribution # of Irrelevant Dimensions')
%
% %---Plot FS Results by Number of irrelevant featurs
% average_performance_by_num_irrel = cell(1,3);
% average_search_duration_by_num_irrel = cell(1,3);
% for num_irrel = 1:3
%     for numD = 1:length(num_Distractors)
%         average_performance_by_num_irrel{num_irrel} = [...
%             average_performance_by_num_irrel{num_irrel}; ...
%             all_FS_block_performance_by_num_irrel{numD,num_irrel}];
%
%         %only want search durations on correct trials
%         these_search_durs = all_FS_search_duration_by_num_irrel{numD,num_irrel};
%         these_search_durs(these_search_durs > 5) = NaN;
%         these_search_durs(all_FS_block_performance_by_num_irrel{numD,num_irrel} == 0) = NaN;
%         average_search_duration_by_num_irrel{num_irrel} = [...
%             average_search_duration_by_num_irrel{num_irrel}; these_search_durs];
%     end
% end
%
% figure
% subplot(2,2,1)
% hold on
% for num_irrel = 1:3
%     plot(100*nanmean(average_performance_by_num_irrel{num_irrel}))
% end
% hold off
% legend({'0 irrel','1 irrel','2 irrel'},'Location','NorthEastOutside')
% title('FS Performance by # Irrel')
% ylabel('Performance (% Correct)')
% xlabel('Trial #')
%
% subplot(2,2,2)
% hold on
% for num_irrel = 1:3
%     plot(nanmean(average_search_duration_by_num_irrel{num_irrel}))
% end
% hold off
% title('FS Search Duration by # Irrel')
% ylabel('Search Duration (sec)')
% xlabel('Trial #')
%
% subplot(2,2,3)
% plot(100*average_performance_by_num_irrel_and_session')
% hold on
% plot(100*nanmean(average_performance_by_num_irrel_and_session),'k')
% hold off
% xlabel('Session #')
% ylabel('Performance (% Correct)')
% title('Performance by Session')
% box off
%
% subplot(2,2,4)
% plot(average_search_duration_by_num_irrel_and_session')
% xlabel('Session #')
% ylabel('Search Duration (Sec)')
% title('Search Duration by Session')
% box off
%
% %% Eye tracking data analysis if collected eye data
%
% trial_aligned_Python_eye_data = cell(1,num_sessions);
% for sess = 1:num_sessions
%     if ~isempty(session_data{sess}.gaze_data)
%         trial_aligned_Python_eye_data{sess} = GetTrialAlignedPythonGazeData(session_data{sess});
%     end
% end

%%
FLU_neutral_looking_time = cell(1,num_sessions);
FS_neutral_looking_time = cell(1,num_sessions);
FLU_target_looking = cell(1,num_sessions);
FS_target_looking = cell(1,num_sessions);
for sess = 1:num_sessions
    if  ~isempty(session_data{sess}.gaze_data)
        
        [pixelsize,cmsize] = ReadMonitordetails(session_data{sess}.configdata.monitorDetails);
        screenX = pixelsize(1);
        screenY = pixelsize(2);
        
        
        block = session_data{sess}.trial_data.Block;
        block = block + 1;%index starts at 0 in C++
        
        FLU_neutral_looking_time{sess} = NaN(max(block),min_FLU_block_len);
        FS_neutral_looking_time{sess} = NaN(max(block),min_FS_block_len);
        FLU_target_looking{sess} = NaN(max(block),min_FLU_block_len);
        FS_target_looking{sess} = NaN(max(block),min_FS_block_len);
        
        for b =1:max(block)
            
            blockind = find(block == b);
            trial_nums = session_data{sess}.trial_data.TrialInExperiment(blockind);
            if all_FLU_FS_blocks{sess}(b) == 1%FLU Block
                if length(blockind) < min_FLU_block_len
                    continue
                else
                    trial_nums = trial_nums(1:min_FLU_block_len);
                end
            elseif all_FLU_FS_blocks{sess}(b) == 2%FS Block
                if length(blockind) < min_FS_block_len
                    continue
                else
                    trial_nums = trial_nums(1:min_FS_block_len);
                end
            end
            
            for t = 1:length(trial_nums)
                
                select_rad = session_data{sess}.trial_data.ObjectselectionRadius(trial_nums(t));
                
                num_objs = length(session_data{sess}.trial_def{t}.relevantObjects);
                object_locations = NaN(2,num_objs);
                for o = 1:num_objs
                    object_locations(1,o) = round(session_data{sess}.trial_def{trial_nums(t)}.relevantObjects(o).StimScreenLocation.x);
                    object_locations(2,o) = round(session_data{sess}.trial_def{trial_nums(t)}.relevantObjects(o).StimScreenLocation.y);
                end

                stim_array = -1*ones(screenY,screenX);
                [yy,xx] = find(stim_array == -1);
                xyind = find(stim_array == -1);
                for o = 1:num_objs
                    rind = find(sqrt((object_locations(1,o)-xx).^2+(object_locations(2,o)-yy).^2) <= select_rad);
                    if o == 1
                        stim_array(xyind(rind)) = 2; %%make it more obviuos which is the target for
                    elseif all(session_data{sess}.trial_def{trial_nums(t)}.relevantObjects(o).StimDimVals == 0)%neutral Quaddle
                        stim_array(xyind(rind)) = 0;
                    else
                        stim_array(xyind(rind)) = 1;
                    end
                end

                frame_trial_ind = find(session_data{sess}.frame_data.TrialInExperiment == trial_nums(t));
                frame_events = session_data{sess}.frame_data.TrialEpoch(frame_trial_ind);
                
                frame_times = session_data{sess}.frame_data.FrameStartUnity(frame_trial_ind);
                frame_times_ET = session_data{sess}.frame_data.EyetrackerTimeStamp(frame_trial_ind);
                
                search_ind = find(frame_events == 5);
                if isempty(search_ind)
                    continue
                end
                
                search_start_ind = search_ind(1)-5;
                
                search_gaps = findgaps(search_ind);
                search_end_time_ind = search_gaps(end,1);
                
                eyetime = trial_aligned_Python_eye_data{sess}{trial_nums(t)}(:,2);
                if isempty(eyetime)
                    continue
                end
                
                t1 = find(eyetime <= frame_times_ET(search_start_ind));
                t1 = t1(end);
                t2 = find(eyetime > frame_times_ET(search_end_time_ind));
                if isempty(t2)
                    t2 = length(eyetime);
                end
                t2 = t2(1);
                
                %Left eye
                eyex = round(screenX*trial_aligned_Python_eye_data{sess}{trial_nums(t)}(t1:t2,5));
                eyey = round(screenY*(1-trial_aligned_Python_eye_data{sess}{trial_nums(t)}(t1:t2,6)));
                [eyex,eyey] = cleanEye(eyex,eyey,screenX,screenY,1);
                
                if isempty(eyex)
                    %try right eye
                    eyex = trial_aligned_Python_eye_data{sess}{trial_nums(t)}(t1:t2,7);
                    eyey = trial_aligned_Python_eye_data{sess}{trial_nums(t)}(t1:t2,8);
                    [eyex,eyey] = cleanEye(eyex,eyey,screenX,screenY,1);
                    if isempty(eyex)
                        disp(['Session #' num2str(sess) ', where is the eye data for trial ' num2str(t) '?'])
                        continue
                    end
                end
                
                stim_array_ind = sub2ind([screenY screenX],eyey,eyex);
                looking_time = stim_array(stim_array_ind);
                
                cumulative_looking_time = zeros(1,4);
                cumulative_looking_time(1) = sum(looking_time == -1);%background
                cumulative_looking_time(2) = sum(looking_time == 2);%target
                cumulative_looking_time(3) = sum(looking_time == 1);%non-neutral distractor
                cumulative_looking_time(4) = sum(looking_time == 0);%neutral Quaddle
                
                %%
                if all_FLU_FS_blocks{sess}(b) == 1%FLU Block
                    FLU_target_looking{sess}(b,t) = cumulative_looking_time(2)/sum(cumulative_looking_time(2:end));
                    FLU_neutral_looking_time{sess}(b,t) = cumulative_looking_time(4)/sum(cumulative_looking_time(2:end));
                else
                    FS_target_looking{sess}(b,t) = cumulative_looking_time(2)/sum(cumulative_looking_time(2:end));
                    FS_neutral_looking_time{sess}(b,t) = cumulative_looking_time(4)/sum(cumulative_looking_time(2:end));
                end
            end
        end
    end
end

%% Combine data across sets

all_FLU_neutral_looking_time = [];
all_FS_neutral_looking_time = [];
all_FLU_target_looking = [];
all_FS_target_looking = [];

for sess = 1:num_sessions
    if  ~isempty(session_data{sess}.gaze_data)
        all_FLU_neutral_looking_time = [all_FLU_neutral_looking_time; FLU_neutral_looking_time{sess}];
        all_FLU_target_looking = [all_FLU_target_looking; FLU_target_looking{sess}];
        
        all_FS_neutral_looking_time = [all_FS_neutral_looking_time; FS_neutral_looking_time{sess}];
        all_FS_target_looking = [all_FS_target_looking; FS_target_looking{sess}];
    end
end
all_FLU_neutral_looking_time = laundry(all_FLU_neutral_looking_time);
all_FLU_target_looking = laundry(all_FLU_target_looking);
all_FS_neutral_looking_time = laundry(all_FS_neutral_looking_time);
all_FS_target_looking = laundry(all_FS_target_looking);
%%

average_performance = [];
for irrel = 1:3
    average_performance = [average_performance; all_FLU_block_performance_by_num_irrel{irrel}];
end
%%

figure
hold on
plot(-9:min_FLU_block_len,100*nanmean(average_performance));
plot(1:min_FLU_block_len,100*nanmean(all_FLU_target_looking));
plot(1:min_FLU_block_len,100*nanmean(all_FLU_neutral_looking_time));
plot([0.5 0.5],[0 100],'k--')
plot([-9 min_FLU_block_len],[80 80],'k--')
plot([-9 min_FLU_block_len],[33 33 ],'k--')
hold off
xlabel('Trial # Relative To Switch')
ylabel('Percent (%)')
box off
title('Raw Data All Blocks')

legend({'Performance','Looking Time @ Target','Looking Time @ Neutral'},'Location','SouthEast')