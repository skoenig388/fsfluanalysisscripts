function Calculate_FLU2FS_FSonly_anlaysis(session_data,all_FLU_FS_blocks,...
    min_FLU_block_len,min_FS_block_len,min_FS_performance)
%Written by Seth Konig 6/24/19

%Inputs:
% 1) session_data across all sessions
% 2) all_FLU_FS_blocks: determines which blocks are FLu and which are FS
% 3) min_FLU_block_len: miniumum # of trials in a FLU block
% 4) min_FS_performance: minimum perforamnce to count as "good" FS block

%expected_numD = [3 6 9 12];
expected_numD = [3 5 7];

num_sessions = length(session_data);

session_average_FLU_performance = NaN(num_sessions,3);
session_average_FLU_search_duration = NaN(num_sessions,3);
session_average_FS_performance = NaN(num_sessions,length(expected_numD),3);
session_average_FS_search_dur = NaN(num_sessions,length(expected_numD),3,2);

for sess = 1:num_sessions
    num_blocks = length(all_FLU_FS_blocks{sess});
    block = session_data{sess}.trial_data.Block;
    block = block + 1;%index starts at 0 in C++
    rewarded = strcmpi(session_data{sess}.trial_data.isRewarded,'true');
    search_duration = Calculate_FLU_Search_Duration(session_data{sess});
    
    %uses a little bit more space but makes indexing for LP easier so....
    FLU_performance_last_10_trials = NaN(num_blocks,3);
    FLU_search_duration_last_10_trials = NaN(num_blocks,3);
    
    %divide by numD,num irrel in FLU block, whether FS block was good, and
    %whether trial was rewarded
    FS_performance_by_num_irrel = cell(length(expected_numD),3);
    FS_search_duration_by_num_irrel = cell(length(expected_numD),3,2,2);
    
    for numD = 1:length(expected_numD)
        for num_irrel = 1:3
            for good = 1:2
                for rw = 1:2
                    FS_search_duration_by_num_irrel{numD,num_irrel,good,rw} = NaN(num_blocks,min_FS_block_len);
                end
            end
            FS_performance_by_num_irrel{numD,num_irrel} = NaN(num_blocks,min_FS_block_len);
        end
    end
    
    number_of_irrelevant_dimensions = ParseBlockDef4NumIrrel(session_data{sess}.block_def);
    FLU_blocks = find(all_FLU_FS_blocks{sess} == 1);
    
    for b = 1:length(FLU_blocks)
        this_FLU_block = FLU_blocks(b);
        blockind = find(block == this_FLU_block);
        if isempty(blockind)
            error('Why is this FLU block empty?')
        elseif length(blockind) < min_FLU_block_len
            error('Why is this FLU block too short?')
        end
        
        this_number_irrel = number_of_irrelevant_dimensions(this_FLU_block)+1;
        
        %---Get Performance on last 10 trials of FLU block---%
        blockrewarded = rewarded(blockind(1):blockind(end));
        FLU_performance_last_10_trials(b,this_number_irrel) = mean(blockrewarded(end-9:end));
        
        %---Get Search Duration on last 10 trials of FLU block---%
        blocksearchdur = search_duration(blockind(1):blockind(end));
        FLU_search_duration_last_10_trials(b,this_number_irrel) = mean(blocksearchdur(end-9:end));
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%---Data for FS Block---%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if this_FLU_block+1 > length(all_FLU_FS_blocks{sess})
            continue
        elseif all_FLU_FS_blocks{sess}(this_FLU_block+1) == 2
            this_FS_block = this_FLU_block+1;
            FS_blockind = find(block == this_FS_block);
            if isempty(FS_blockind)
                error('Why is this FS block empty?')
            elseif length(FS_blockind) < min_FS_block_len
                %block not finished
                continue
            elseif length(FS_blockind) > min_FS_block_len+1
                error('Why are there so many trials in this FS block?')
            end
            
            FSblockrewarded = rewarded(FS_blockind(1):FS_blockind(end));
            FSblocksearchdur = search_duration(FS_blockind(1):FS_blockind(end));
            if length(FSblockrewarded) > min_FS_block_len
                selected_objects = session_data{sess}.trial_data.SelectedObjectID(FS_blockind(1):FS_blockind(end));
                emptys = find(cellfun(@isempty,selected_objects));
                FSblockrewarded(emptys) = [];
                FSblocksearchdur(emptys) = [];
            end
            
            for bk = 1:length(FSblockrewarded)
                if length(session_data{sess}.trial_def{FS_blockind(bk)}.relevantObjects) == 8
                    %newere version with stimlui on concentric circle
                    numD = 0;
                    for stim = 1:8
                        if sum(session_data{sess}.trial_def{FS_blockind(bk)}.relevantObjects(stim).StimDimVals ~= 0) >= 1
                            numD = numD + 1;
                        end
                    end
                    numD = numD - 1;%target doesn't count
                else
                    %older version
                    numD = length(session_data{sess}.trial_def{FS_blockind(bk)}.relevantObjects)-1;
                end
                numDindex = find(expected_numD == numD);
                if isempty(numDindex)
                    error('Wrong # of Distractors?');
                else
                    FS_performance_by_num_irrel{numDindex,this_number_irrel}(this_FS_block,bk) ...
                        = FSblockrewarded(bk);
                    if sum(FSblockrewarded)/length(FSblockrewarded) >= min_FS_performance
                        if FSblockrewarded(bk)
                            FS_search_duration_by_num_irrel{numDindex,this_number_irrel,1,1}(this_FS_block,bk) ...
                                = FSblocksearchdur(bk);
                        else
                            FS_search_duration_by_num_irrel{numDindex,this_number_irrel,1,2}(this_FS_block,bk) ...
                                = FSblocksearchdur(bk);
                        end
                    else
                        if FSblockrewarded(bk)
                            FS_search_duration_by_num_irrel{numDindex,this_number_irrel,2,1}(this_FS_block,bk) ...
                                = FSblocksearchdur(bk);
                        else
                            FS_search_duration_by_num_irrel{numDindex,this_number_irrel,2,2}(this_FS_block,bk) ...
                                = FSblocksearchdur(bk);
                        end
                    end
                end
            end
        else
            disp('Why is there no FS block to anlayze?')
            continue
        end
    end
    
    %compact into average across sessions
    for num_irrel = 1:3
        session_average_FLU_performance(sess,num_irrel) = nanmean(FLU_performance_last_10_trials(:,num_irrel));
        session_average_FLU_search_duration(sess,num_irrel) = nanmean(FLU_search_duration_last_10_trials(:,num_irrel));
        for numD = 1:length(expected_numD)
            session_average_FS_performance(sess,numD,num_irrel) = nanmean(FS_performance_by_num_irrel{numD,num_irrel}(:));
            for good = 1:2
                for rw = 1:2
                    session_average_FS_search_dur(sess,numD,num_irrel,good,rw) = nanmean(FS_search_duration_by_num_irrel{numD,num_irrel,good,rw}(:));
                end
            end
        end
    end
end

%---Plot Main Results---%
figure
subplot(3,3,1)
errorb(0:2,100*mean(session_average_FLU_performance),100*std(session_average_FLU_performance)./sqrt(num_sessions))
box off
xticks([0:2])
xlabel('# of Irrelevant Dimensions')
ylabel('Average Performance')
title('FlU performance last 10 trials')
box off
ylim([75 105])

subplot(3,3,2)
errorb(0:2,mean(session_average_FLU_search_duration),std(session_average_FLU_search_duration)./sqrt(num_sessions))
box off
xticks([0:2])
xlabel('# of Irrelevant Dimensions')
ylabel('Search Duration (sec)')
title('FLU Search Duration last 10 trials')
box off

across_session_avg = NaN(3,length(expected_numD));
across_session_sem = NaN(3,length(expected_numD));

for num_irrel = 1:3
    for numD = 1:length(expected_numD)
        session_average_FS_performance(sess,numD,num_irrel) = nanmean(FS_performance_by_num_irrel{numD,num_irrel}(:));
        across_session_avg(num_irrel,numD) = mean(session_average_FS_performance(:,numD,num_irrel));
        across_session_sem(num_irrel,numD) = std(session_average_FS_performance(:,numD,num_irrel))./sqrt(num_sessions);
    end
end

clrs = 'rgb';
subplot(3,3,3)
hold on
for irrel = 1:3
    errorbar(expected_numD,100*across_session_avg(irrel,:),100*across_session_sem(irrel,:),clrs(irrel),'linewidth',2)
end
plot([expected_numD(1)-1 expected_numD(end)+1],[100/8 100/8],'k--')
hold off
box off
xlabel('# Distractors')
ylabel('Performance')
xticks([expected_numD])
ylim([0 100])
xlim([expected_numD(1)-1 expected_numD(end)+1])
legend({'0 irrel','1 irrel','2 irrel'},'location','northEastOutside')
title('FS Performance')

across_session_avg_correct_trials_good_blocks = NaN(3,length(expected_numD));
across_session_sem_correct_trials_good_blocks = NaN(3,length(expected_numD));

good = 1;
rw = 1;
for num_irrel = 1:3
    for numD = 1:length(expected_numD)
        across_session_avg_correct_trials_good_blocks(num_irrel,numD) = ...
            nanmean(session_average_FS_search_dur(:,numD,num_irrel,good,rw));
        across_session_sem_correct_trials_good_blocks(num_irrel,numD) = ...
            nanstd(session_average_FS_search_dur(:,numD,num_irrel,good,rw))./sqrt(num_sessions);
    end
end

clrs = 'rgb';
subplot(3,3,4)
hold on
for irrel = 1:3
    errorbar(expected_numD,across_session_avg_correct_trials_good_blocks(irrel,:),...
        across_session_sem_correct_trials_good_blocks(irrel,:),clrs(irrel),'linewidth',2)
end
hold off
box off
xlabel('# Distractors')
ylabel('Search Duration (sec)')
xticks([expected_numD])
xlim([expected_numD(1)-1 expected_numD(end)+1])
title('FS SD Correct Trials Good FS Blocks')

subplot(3,3,7)
hold on
for irrel = 1:2
    plot(expected_numD,across_session_avg_correct_trials_good_blocks(irrel,:)-across_session_avg_correct_trials_good_blocks(3,:))
end
plot(expected_numD,across_session_avg_correct_trials_good_blocks(2,:)-across_session_avg_correct_trials_good_blocks(1,:))
hold off
legend({'2 vs 0','2 vs 1','1 vs 0'},'location','Northeastoutside')
ylabel('Search Duration (sec)')
xlabel('# Distractors')
xticks([expected_numD])
xlim([expected_numD(1)-1 expected_numD(end)+1])
title('Difference in Search Duration by # Irrel')

across_session_avg_incorrect_trials_good_blocks = NaN(3,length(expected_numD));
across_session_sem_incorrect_trials_good_blocks = NaN(3,length(expected_numD));

good = 1;
rw = 2;
for num_irrel = 1:3
    for numD = 1:length(expected_numD)
        across_session_avg_incorrect_trials_good_blocks(num_irrel,numD) = ...
            nanmean(session_average_FS_search_dur(:,numD,num_irrel,good,rw));
        across_session_sem_incorrect_trials_good_blocks(num_irrel,numD) = ...
            nanstd(session_average_FS_search_dur(:,numD,num_irrel,good,rw))./sqrt(num_sessions);
    end
end

clrs = 'rgb';
subplot(3,3,5)
hold on
for irrel = 1:3
    errorbar(expected_numD,across_session_avg_incorrect_trials_good_blocks(irrel,:),...
        across_session_sem_incorrect_trials_good_blocks(irrel,:),clrs(irrel),'linewidth',2)
end
hold off
box off
xlabel('# Distractors')
ylabel('Search Duration (sec)')
xticks([expected_numD])
xlim([expected_numD(1)-1 expected_numD(end)+1])
title('FS SD Incorrect Trials Good FS Blocks')

across_session_avg_correct_trials_bad_blocks = NaN(3,length(expected_numD));
across_session_sem_correct_trials_bad_blocks = NaN(3,length(expected_numD));

good = 2;
rw = 1;
for num_irrel = 1:3
    for numD = 1:length(expected_numD)
        across_session_avg_correct_trials_bad_blocks(num_irrel,numD) = ...
            nanmean(session_average_FS_search_dur(:,numD,num_irrel,good,rw));
        across_session_sem_correct_trials_bad_blocks(num_irrel,numD) = ...
            nanstd(session_average_FS_search_dur(:,numD,num_irrel,good,rw))./sqrt(num_sessions);
    end
end

clrs = 'rgb';
subplot(3,3,6)
hold on
for irrel = 1:3
    errorbar(expected_numD,across_session_avg_correct_trials_bad_blocks(irrel,:),...
        across_session_sem_correct_trials_bad_blocks(irrel,:),clrs(irrel),'linewidth',2)
end
hold off
box off
xlabel('# Distractors')
ylabel('Search Duration (sec)')
xticks([expected_numD])
xlim([expected_numD(1)-1 expected_numD(end)+1])
title('FS SD Correct Trials Bad FS Blocks')


across_session_avg_incorrect_trials_bad_blocks = NaN(3,length(expected_numD));
across_session_sem_incorrect_trials_bad_blocks = NaN(3,length(expected_numD));

good = 2;
rw = 2;
for num_irrel = 1:3
    for numD = 1:length(expected_numD)
        across_session_avg_incorrect_trials_bad_blocks(num_irrel,numD) = ...
            nanmean(session_average_FS_search_dur(:,numD,num_irrel,good,rw));
        across_session_sem_incorrect_trials_bad_blocks(num_irrel,numD) = ...
            nanstd(session_average_FS_search_dur(:,numD,num_irrel,good,rw))./sqrt(num_sessions);
    end
end

clrs = 'rgb';
subplot(3,3,9)
hold on
for irrel = 1:3
    errorbar(expected_numD,across_session_avg_incorrect_trials_bad_blocks(irrel,:),...
        across_session_sem_incorrect_trials_bad_blocks(irrel,:),clrs(irrel),'linewidth',2)
end
hold off
box off
xlabel('# Distractors')
ylabel('Search Duration (sec)')
xticks([expected_numD])
xlim([expected_numD(1)-1 expected_numD(end)+1])
title('FS SD Incorrect Trials Bad FS Blocks')

end