%Written by Seth Konig 3/2/19
%Similar to FLU_DF04_Combined_Data_Across_Sets but updated to work with
%newer scripts and new task structure

tic
clar
num_shuffs = 1000;
import_option = 'slim';

%average acceptable performance across blocks
max_block_count = 30;
miniumum_performance = 0.5;%set to 0 if you don't want to remove blocks
max_block_cut = 3;%of of 1st and last blocks to cut if performance is very bad
min_block_len = 30;%miniumum # of trials in a block
last_trial_window = 10;%window to look at before end of block

learning_point_threshold = 80;
forward_back_trials = 7;
forward_smoothing_window = 10; %size of the smoothing window for forward filter

%data import options
pre_processed_data_dir = '\Processed_Data\'; %for pre-saved session data files
reimport_new = false;%will import session data that is already processsed

max_time_out = 5*60;%seconds how long an acceptable break is
%%

%%%%%%%%%%%%%%%%%%
%%%---Reider---%%%
%%%%%%%%%%%%%%%%%%
%---0 Irrelevant, deterministic Reward---%
% monkeyname = 'Reider';
% sess_file_names = {'FLU__Reider__21_03_2019__11_15_49','FLU__Reider__27_03_2019__09_04_07',...
%     'FLU__Reider__28_03_2019__08_11_58','FLU__Reider__01_04_2019__10_31_48',...
%     'FLU__Reider__03_04_2019__09_48_55','FLU__Reider__04_04_2019__08_59_18',...
%     'FLU__Reider__05_04_2019__10_40_14','FLU__Reider__09_04_2019__11_33_04',...
% };

%---1 Irrelevant, deterministic Reward---%
% monkeyname = 'Reider';
% sess_file_names = {'FLU__Reider__18_04_2019__09_32_55','FLU__Reider__30_04_2019__09_23_55',...
%     'FLU__Reider__02_05_2019__09_53_28','FLU__Reider__13_05_2019__09_23_29',...
%     'FLU__Reider__14_05_2019__08_23_29','FLU__Reider__23_05_2019__07_54_19',...
%     'FLU__Reider__24_05_2019__07_24_23','FLU__Reider__31_05_2019__07_53_35',...
%     'FLU__Reider__03_06_2019__07_21_26','FLU__Reider__18_06_2019__10_42_21',...
%     'FLU__Reider__20_06_2019__07_29_07',...
%     };

%---0 Irrelevant, prob reward 0.85---%
% monkeyname = 'Reider';
% sess_file_names = {'FLU__Reider__14_10_2019__10_40_22','FLU__Reider__15_10_2019__10_28_04',...
%     'FLU__Reider__16_10_2019__10_27_07','FLU__Reider__17_10_2019__10_33_09'};


%%%%%%%%%%%%%%%%
%%%---Frey---%%%
%%%%%%%%%%%%%%%%

%---0 irrelevant deterministic reward---%
% monkeyname = 'Frey';
% sess_file_names = {'FLU__Frey__15_02_2019__11_59_59','FLU__Frey__16_02_2019__12_31_20',...
%     'FLU__Frey__18_02_2019__11_09_46','FLU__Frey__25_02_2019__10_59_03','FLU__Frey__26_02_2019__11_09_32',...
%     'FLU__Frey__28_02_2019__10_31_32','FLU__Frey__01_03_2019__11_40_45','FLU__Frey__02_03_2019__12_50_48',...
%     'FLU__Frey__07_03_2019__11_00_10','FLU__Frey__08_03_2019__10_15_22','FLU__Frey__09_03_2019__10_41_20',...
%     'FLU__Frey__11_03_2019__11_39_47','FLU__Frey__12_03_2019__10_02_12','FLU__Frey__13_03_2019__10_21_17'};

%---0 Irrelevant, prob reward 0.85---%
% monkeyname = 'Frey';
% sess_file_names = {'FLU2FS__Frey__16_10_2019__11_29_49','FLU2FS__Frey__17_10_2019__10_33_41',...
%     };

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%---Combined Across Monkeys---%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
monkeyname = 'Frey + Redier';
sess_file_names = {'FLU2FS__Frey__16_10_2019__11_29_49','FLU2FS__Frey__17_10_2019__10_33_41',...
    'FLU__Reider__14_10_2019__10_40_22','FLU__Reider__15_10_2019__10_28_04',...
    'FLU__Reider__16_10_2019__10_27_07','FLU__Reider__17_10_2019__10_33_09',...
    };

sess_file_names = {'FLU__Reider__17_10_2019__10_33_09'};

%% Import Session Data
num_sessions = length(sess_file_names);
session_data = cell(1,num_sessions);
which_monkey = NaN(1,num_sessions);
for sess =1:num_sessions
    which_monkey(sess) = DetermineWhichMonkey(sess_file_names{sess});
    
    disp(['Loading data from session #' num2str(sess) '/' num2str(num_sessions)])
    log_dir = FindDataDirectory(sess_file_names{sess});
    if ~isempty(log_dir)
        session_data_file_name = [log_dir sess_file_names{sess} ...
            pre_processed_data_dir sess_file_names{sess} '_session_data.mat'];
        if ~exist(session_data_file_name,'file') && ~reimport_new
            session_data{sess} = ImportFLU_DataV2(sess_file_names{sess},log_dir,import_option);
        else
            load(session_data_file_name,'all_session_data');
            session_data{sess} = all_session_data;
        end
    else
        error(['Cant Find Log Dir for '  sess_file_names{sess}])
    end
end

%% Calculate Averege Performance by Block and Remove "Bad" Blocks
all_good_blocks = cell(1,num_sessions);
all_block_performance = cell(1,num_sessions);
all_session_block_performance = NaN(num_sessions,max_block_count);
for sess = 1:num_sessions
    [all_good_blocks{sess},all_block_performance{sess}] = ...
        RemoveBadBlocks(session_data{sess},miniumum_performance,max_block_cut,min_block_len,max_time_out);
    all_session_block_performance(sess,1:length(all_block_performance{sess})) = ...
        all_block_performance{sess};
end

figure
subplot(1,2,1)
plot(nanmean(all_session_block_performance))
xlabel('Block #')
ylabel('Performance-Proportion Correct')
title('Average Performance by Block')
box off

subplot(1,2,2)
hist(all_session_block_performance(:),20)
xlabel('Performance-Proportion Correct')
ylabel('Block Count')
title('Distribution of Block Performance')
box off

subtitle([monkeyname ': Performance by Block #'])
box off
%% Calculate perofrmance across all blocks
all_block_performance = [];
all_block_performance_prob = [];

all_trial_count = NaN(length(sess_file_names),max_block_count);
all_extra_trial_count = NaN(length(sess_file_names),max_block_count);
all_block_end_performance = NaN(length(sess_file_names),max_block_count);

for sess = 1:num_sessions
    [block_performance,trial_count,extra_trial_count,block_end_performance,block_performance_prob] = ...
        CalculateBlockPerformance(session_data{sess},all_good_blocks{sess},min_block_len,last_trial_window);
    
    all_block_performance = [all_block_performance; block_performance];
    all_block_performance_prob = [all_block_performance_prob; block_performance_prob];
    all_trial_count(sess,1:length(trial_count)) = trial_count;
    all_extra_trial_count(sess,1:length(extra_trial_count)) = extra_trial_count;
    all_block_end_performance(sess,1:length(block_end_performance)) = block_end_performance;
end

figure
subplot(2,2,1)
hold on
plot(-(last_trial_window-1):min_block_len,100*nanmean(all_block_performance))
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_block_performance)',3,2),'r')
plot(-(last_trial_window-1):min_block_len,100*nanmean(all_block_performance_prob),'g')
plot([0.5 0.5],[0 100],'k--')
plot([-(last_trial_window-1) min_block_len],[80 80],'k--')
plot([-(last_trial_window-1) min_block_len],[33 33],'k--')
plot([-(last_trial_window-1) min_block_len],[66 66],'k--')
hold off
legend({'Raw','Smoothed','Corrected'},'location','SouthEast')
ylim([0 100])
xlabel('Trial # Relative To Switch')
ylabel('Performance (% Correct)')
box off
if isempty(all_block_performance_prob)
title(sprintf(['Average Performance-All blocks \n' ...
    'Average_{20-30} = ' num2str(100*mean(nanmean(all_block_performance(:,end-9:end))),3) '%%, ' ...
    'Average_{Last 10} = ' num2str(100*mean(nanmean(all_block_performance(:,1:10))),3) '%%']));
else
    title(sprintf(['Average Performance-All blocks \n' ...
    'Average_{20-30} = ' num2str(100*mean(nanmean(all_block_performance_prob(:,end-9:end))),3) '%%, ' ...
    'Average_{Last 10} = ' num2str(100*mean(nanmean(all_block_performance_prob(:,1:10))),3) '%%']));
end
subplot(2,2,2)
histogram(all_trial_count(:),30:60)
xlabel('Trials in Block')
ylabel('Block Count')
title('Total Trial Count per Block')
box off

subplot(2,2,3)
histogram(all_extra_trial_count(:),0:30)
xlabel('Extra Trials in Block')
ylabel('Block Count')
title('Extra Trial Count per block')
box off

subplot(2,2,4)
histogram(100*all_block_end_performance(:),-5:10:95)
xlabel('Performance (% Correct)')
ylabel('Block Count')
box off
title('Peformance at End of Block')

subtitle(monkeyname)

%% Learning Point anlayses
all_Learning_Points = [];
all_LP_aligned_block_performance = [];
all_LP_aligned_search_duration = [];
all_block_performance_LP_less_10 = [];
all_block_performance_LP_less_20 = [];
all_block_performance_LP_less_30 = [];
all_block_performance_LP_greater30 = [];
all_block_performance_no_LP = [];
for sess = 1:num_sessions
    [LP_aligned_block_performance,LP_aligned_search_duration,Learning_Points,...
        block_performance_LP_less_10,block_performance_LP_less_20,block_performance_LP_less_30,...
        block_performance_LP_greater30,block_performance_no_LP] = ...
        CalculateLearningPoint(session_data{sess},all_good_blocks{sess},min_block_len,last_trial_window,...
        forward_smoothing_window,learning_point_threshold,forward_back_trials);
    
    all_Learning_Points = [all_Learning_Points Learning_Points];
    all_LP_aligned_block_performance = [all_LP_aligned_block_performance; LP_aligned_block_performance];
    all_LP_aligned_search_duration = [all_LP_aligned_search_duration; LP_aligned_search_duration];
    
    all_block_performance_LP_less_10 = [all_block_performance_LP_less_10; block_performance_LP_less_10];
    all_block_performance_LP_less_20 = [all_block_performance_LP_less_20; block_performance_LP_less_20];
    all_block_performance_LP_less_30 = [all_block_performance_LP_less_30; block_performance_LP_less_30];
    all_block_performance_LP_greater30 = [all_block_performance_LP_greater30; block_performance_LP_greater30];
    all_block_performance_no_LP = [all_block_performance_no_LP; block_performance_no_LP];
end
all_Learning_Points = laundry(all_Learning_Points);
all_LP_aligned_block_performance = laundry(all_LP_aligned_block_performance);

LP_perform_legend_str = [];
LP_perform_legend_name = {'LP <= 10','LP > 10 & LP <= 20','LP > 20 & LP <= 30','LP > 30','LP DNE'};
for LPtype = 1:length(LP_perform_legend_name)
    if LPtype == 1
        n_str = size(all_block_performance_LP_less_10,1);
    elseif LPtype == 2
        n_str = size(all_block_performance_LP_less_20,1);
    elseif LPtype == 3
        n_str = size(all_block_performance_LP_less_30,1);
    elseif LPtype == 4
        n_str = size(all_block_performance_LP_greater30,1);
    elseif LPtype == 5
        n_str = size(all_block_performance_no_LP,1);
    end
    LP_perform_legend_str{LPtype} = [LP_perform_legend_name{LPtype} ', n = ' num2str(n_str)];
end

figure
subplot(2,2,1)
plot([0 0],[0 100],'k--')
hold on
plot([-forward_back_trials forward_back_trials],[33 33],'k')
plot(-forward_back_trials:forward_back_trials,100*nanmean(all_LP_aligned_block_performance))
hold off
xlabel('Trial from LP')
ylabel('Performance (% Correct)')
box off
axis square
xlim([-forward_back_trials forward_back_trials])
ylim([0 100])
title(['All Switches: Backward Learning Curve, n = ' num2str(size(all_LP_aligned_block_performance,1))])

subplot(2,2,2)
hist(all_Learning_Points,1:max(all_Learning_Points))
xlabel('LP Tria#')
ylabel('Block Count')
axis square
box off
title('All Switches: Learning Point Distribution')

subplot(2,2,3)
plot(-forward_back_trials:forward_back_trials,nanmean(all_LP_aligned_search_duration))
yl = ylim;
hold on
plot([0 0],[yl(1) yl(2)],'k--')
hold off
xlabel('Trial from LP')
ylabel('Search Duaration (sec)')
box off
axis square
xlim([-forward_back_trials forward_back_trials])
title('All Switches: Backwards Search Duration Curve')

subplot(2,2,4)
hold on
plot([0.5 0.5],[0 100],'k--')
plot([-(last_trial_window-1) min_block_len],[33 33],'k--')
plot([-(last_trial_window-1) min_block_len],[66 66],'k--')
p(1) = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_block_performance_LP_less_10,1)',3,2),'g');
p(2) = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_block_performance_LP_less_20,1)',3,2),'b');
p(3) = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_block_performance_LP_less_30,1)',3,2),'m');
p(4) = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_block_performance_LP_greater30,1)',3,2),'r');
p(5) = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_block_performance_no_LP,1)',3,2),'c');
hold off
box off
grid on
axis square
ylim([0 100])
xlabel('Trial # Relative To Switch')
ylabel('Performance (% Correct)')
title('Performance: by "Learning Rate"')
legend(p,LP_perform_legend_str,'Location','NorthEastOutside')

subtitle(monkeyname)

%% Plot Performacne by # of Irrelevant Feature Dimensions
all_LP_by_num_irrel = cell(1,3);
all_LP_aligned_block_performance_by_num_irrel = cell(1,3);
all_block_performance_by_num_irrel = cell(1,3);
all_search_duration_by_num_irrel = cell(1,3);
median_lps = NaN(num_sessions,3);
prop_unlearned_blocks = NaN(num_sessions,3);
for sess = 1:num_sessions
    [block_performance_by_num_irrel,search_duration_by_num_irrel,...
        LP_by_num_irrel,LP_aligned_block_performance_by_num_irrel] = ...
        CalculateBlockPerformancebyNumIrrelDim(session_data{sess},all_good_blocks{sess},min_block_len,last_trial_window,...
        forward_smoothing_window,learning_point_threshold,forward_back_trials);
    
    for num_irrel = 1:3
        median_lps(sess,num_irrel) = nanmedian(LP_by_num_irrel{num_irrel});
        prop_unlearned_blocks(sess,num_irrel) = sum(isnan(LP_by_num_irrel{num_irrel}))/length(LP_by_num_irrel{num_irrel});
        all_LP_by_num_irrel{num_irrel} = [all_LP_by_num_irrel{num_irrel} LP_by_num_irrel{num_irrel}];
        all_LP_aligned_block_performance_by_num_irrel{num_irrel} = ...
            [all_LP_aligned_block_performance_by_num_irrel{num_irrel}; LP_aligned_block_performance_by_num_irrel{num_irrel}];
        all_block_performance_by_num_irrel{num_irrel} = [all_block_performance_by_num_irrel{num_irrel}; ...
            block_performance_by_num_irrel{num_irrel}];
        all_search_duration_by_num_irrel{num_irrel} = [all_search_duration_by_num_irrel{num_irrel}; ...
            search_duration_by_num_irrel{num_irrel}];
    end
end

mean_all_lps = mean(median_lps);
sem_all_lps = std(median_lps)./sqrt(num_sessions);

n_irrel = NaN(1,3);
for num_irrel = 1:3
    n_irrel(num_irrel) = size(all_block_performance_by_num_irrel{num_irrel},1);
end

%---Plot Results by Number of irrelevant featurs
figure
subplot(2,3,1)
hold on
plot([0.5 0.5],[0 100],'k--')
plot([-(last_trial_window-1) min_block_len],[33 33],'k--')
plot([-(last_trial_window-1) min_block_len],[66 66],'k--')
plot([-(last_trial_window-1) min_block_len],[80 80],'k--')
for num_irrel = 1:3
    if n_irrel(num_irrel) > 0
        pnirr(num_irrel) = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_block_performance_by_num_irrel{num_irrel})',3,2));
    else
        pnirr(num_irrel) = plot(0,0);
    end
end
hold off
box off
ylim([0 100])
xlabel('Trial # Relative To Switch')
ylabel('Performance (% Correct)')
title('Performance by # of Irrelevant Dimensions')
legend(pnirr,{['0, n = ' num2str(n_irrel(1))],['1, n = ' num2str(n_irrel(2))],...
    ['2, n = ' num2str(n_irrel(3))]},'Location','SouthEast')

subplot(2,3,2)
hold on
for num_irrel = 1:3
    if n_irrel(num_irrel) > 0
        plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_search_duration_by_num_irrel{num_irrel})',3,2));
    else
        plot(0,1)
    end
end
yl = ylim;
plot([0.5 0.5],[yl(1) yl(2)],'k--')
hold off
box off
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (sec)')
title('Search Duration by # of Irrelevant Dimensions')

subplot(2,3,4)
hold on
plot([0 0],[0 100],'k--')
plot([-forward_back_trials forward_back_trials],[33 33],'k--')
plot([-forward_back_trials forward_back_trials],[66 66],'k--')
for num_irrel = 1:3
    plot(-forward_back_trials:forward_back_trials,100*nanmean(all_LP_aligned_block_performance_by_num_irrel{num_irrel}))
end
ylim([0 100])
xlim([-forward_back_trials forward_back_trials])
hold off
box off
xlabel('Trial from LP')
ylabel('Performance (% Correct)')
title('Backward Learning Curves by # of Irrelevant Dimensions')

subplot(2,3,5)
hold on
for num_irrel = 1:3
    n_dist = hist(all_LP_by_num_irrel{num_irrel},1:2:60);
    n_dist = n_dist./sum(n_dist);
    plot(1:2:60,n_dist);
end
hold off
box off
xlabel('LP trial #')
ylabel('Proportion of Blocks')
title('LP Distribution # of Irrelevant Dimensions')

subplot(2,3,3)
bar(mean_all_lps)
hold on
errorbar(mean_all_lps,sem_all_lps,'k','linestyle','none')
hold off
box off
ylabel('LP')
xlabel('Num Irrel')
xticks(1:3)
xticklabels({'0','1','2'})
title('Mean LP')

subplot(2,3,6)
bar(mean(prop_unlearned_blocks))
hold on
errorbar(mean(prop_unlearned_blocks),std(prop_unlearned_blocks)./sqrt(num_sessions),'k','linestyle','none')
hold off
box off
ylabel('Proportion of Blocks')
xlabel('Num Irrel')
xticks(1:3)
xticklabels({'0','1','2'})
title('Proprotion of Unlearned blocks')

%% Plot performance and search duration by target and distractor dimensions
[block_performance_by_dim,search_duration_by_dim] = ...
    CalculateBlockPerformancebyDimension(session_data,all_good_blocks,min_block_len,...
    last_trial_window);

%% Look for Choice Biases: Spatial & Feature
[spatial_choice_baises,feature_choice_baises] = CalculateChoiceBiases(...
    session_data,min_block_len,forward_smoothing_window,learning_point_threshold);
subtitle(monkeyname)
%%
toc