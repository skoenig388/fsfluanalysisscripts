%Writen 2/5/19 Seth Koenig

clar
%log_dir = 'Z:\MonkeyData\HuamnFluEye\';
%log_dir = 'Z:\MonkeyData\';
%log_dir = 'C:\Users\Wolmesdorf-Lab\Documents\monkeyfixation\logs\';
log_dir = 'Z:\MonkeyData\Wotan\Fixation\';
%log_dir = 'Z:\MonkeyData\Frey\';

Fs = 600;
[Blow,Alow] = butter(8,120/(Fs/2),'low');

screenX = 1920;
screenY = 1080;
sess_file_name = 'Fixation__Wotan__2019_02_12__11_57_01';

trial_data_name = [log_dir sess_file_name '\RuntimeData\TrialData\' sess_file_name '__TrialData.txt'];
frame_data_dir2 = [log_dir sess_file_name '\RuntimeData\FrameData\'];
frame_data_dir = [log_dir sess_file_name '\RuntimeData\FrameData\' sess_file_name];
frame_data_name = '__FrameData__Trial_';

UDP_recv_data_dir = [log_dir sess_file_name '\RuntimeData\UnityUDPRecvData\' sess_file_name];
UDP_recv_name = '__UDPRecv__Trial_';

gaze_data_dir = [log_dir sess_file_name '\RuntimeData\PythonData\GazeData\' sess_file_name];
gaze_data_name = '__GazeData__Trial_';

%% Read in Trial Data
session_data = ImportFLU_Data(sess_file_name,log_dir);

trial_data = session_data.trial_data;
num_trials = size(trial_data,1);

%% Read in Frame Data
all_epoch = cell(1,num_trials);
all_frame = cell(1,num_trials);
all_time = cell(1,num_trials);
all_system_time = cell(1,num_trials);
all_xy = cell(1,num_trials);

if size(dir(frame_data_dir2),1) == 3 %only 1 frame data file
    temp_data = readtable([frame_data_dir frame_data_name(1:end-8) '.txt']);
    trial_nums = temp_data.TrialInExperiment;
    nt = max(trial_nums);
    for t = 1:nt
        these_trials = find(trial_nums == t);
        all_epoch{t} = temp_data.TrialEpoch(these_trials);
        all_frame{t} = temp_data.Frame(these_trials);
        all_time{t} = temp_data.FrameStartUnity(these_trials);
        all_system_time{t} = temp_data.FrameStartSystem(these_trials);
        all_xy{t} = [temp_data.EyetrackerTimeStamp(these_trials) temp_data.EyePositionX(these_trials) temp_data.EyePositionY(these_trials)];
    end
else
    nt = 0;
    for t = 0:num_trials-1
        try
            temp_data = readtable([frame_data_dir frame_data_name num2str(t) '.txt']);
            
            if isempty(temp_data)
                continue
            end
            all_epoch{t+1} = temp_data.TrialEpoch;
            all_frame{t+1} = temp_data.Frame;
            all_time{t+1} = temp_data.FrameStartUnity;
            all_system_time{t+1} = temp_data.FrameStartSystem;
            all_xy{t+1} = [temp_data.EyetrackerTimeStamp temp_data.EyePositionX temp_data.EyePositionY];
            nt = nt+1;
        catch
            continue
        end
    end
    num_trials = nt;
end
%% Read in UDP receive file ...want to know frame received and Eye tracker time stamps
all_UDP_recv_data = cell(1,num_trials);
if size(dir(frame_data_dir2),1) == 3 %only 1 frame data file
    temp_data = ImportUDPRecv([UDP_recv_data_dir UDP_recv_name(1:end-8) '.txt']);
    for t = 1:length(all_frame)
        frame_start = all_frame{t}(1);
        frame_end = all_frame{t}(end);
        frame_start_ind = find(temp_data(:,1) == frame_start);
        frame_end_ind = find(temp_data(:,1) == frame_end);
        all_UDP_recv_data{t} = temp_data(frame_start_ind:frame_end_ind,:);
    end
else
    for t = 0:num_trials-1
        all_UDP_recv_data{t+1} = ImportUDPRecv([UDP_recv_data_dir UDP_recv_name num2str(t) '.txt']);
    end
end
%% Read in Eye tracking data saved by Python at 600 Hz
if size(dir(frame_data_dir2),1) == 3 %only 1 frame data file
    temp_data = readtable([gaze_data_dir gaze_data_name(1:end-8) '__.txt']);
    
    all_pythonEye_data_XY = [temp_data.left_ADCS_validity  temp_data.right_ADCS_validity  ...
        temp_data.left_ADCS_x temp_data.left_ADCS_y ...
        temp_data.right_ADCS_x temp_data.right_ADCS_y];
    
    percent_binocular = sum(~isnan(temp_data.left_ADCS_x) &  ~isnan(temp_data.right_ADCS_x))/...
        sum(~isnan(temp_data.left_ADCS_x) |  ~isnan(temp_data.right_ADCS_x));
    
    average_participant_distance = nanmean(temp_data.left_origin_UCS_z+temp_data.right_origin_UCS_z)/2;
    all_pythonEye_data_time = [temp_data.system_time_stamp temp_data.device_time_stamp];
else
    all_pythonEye_data_time = cell(1,num_trials);
    all_pythonEye_data_XY = cell(1,num_trials);
    percent_binocular = NaN(1,num_trials);
    average_participant_distance = NaN(1,num_trials);
    for t = 0:num_trials-1
        try
            temp_data = readtable([gaze_data_dir gaze_data_name num2str(t) '.txt']);
        catch
            disp('File Missing')
            continue
        end
        
        all_pythonEye_data_XY{t+1} = [temp_data.left_ADCS_validity  temp_data.right_ADCS_validity  ...
            temp_data.left_ADCS_x temp_data.left_ADCS_y ...
            temp_data.right_ADCS_x temp_data.right_ADCS_y];
        
        try
            percent_binocular(t+1) = sum(~isnan(temp_data.left_ADCS_x) &  ~isnan(temp_data.right_ADCS_x))/...
                sum(~isnan(temp_data.left_ADCS_x) |  ~isnan(temp_data.right_ADCS_x));
        catch
            disp('Cant get participant eye data')
        end
        all_pythonEye_data_time{t+1} = [temp_data.system_time_stamp temp_data.device_time_stamp];
        try
            average_participant_distance(t+1) = nanmean(temp_data.left_origin_UCS_z+temp_data.right_origin_UCS_z)/2;
        catch
            disp('Cant get participant distance')
        end
    end
end
%% Determine conversion from ADCS/pixels to DVA
average_eye_distance = nanmean(average_participant_distance)/10;%cm
full_screen_dva_x= atand(43.5/average_eye_distance)*2;
full_screen_dva_y = atand(24/average_eye_distance)*2;
%% Align Eye-Data Python-Eyetracker to Frames/Epochs

trial_aligned_Python_eye_data = cell(1,num_trials);
for t = 1:num_trials
    
    ITI_epoch_start = [trial_data.Epoch0_StartFrame(t) trial_data.Epoch0_StartTimeAbsolute(t) trial_data.Epoch0_StartTimeRelative(t)];
    Reward_epoch_start =  [trial_data.Epoch7_StartFrame(t) trial_data.Epoch7_StartTimeAbsolute(t) trial_data.Epoch7_StartTimeRelative(t)];
    Reward_epoch_duration = trial_data.Epoch7_Duration(t);
    if t ~= num_trials
        reward_epoch_end_frame = all_frame{t+1}(1)-1;
    else
        reward_epoch_end_frame =all_frame{end}(end);
    end
    
    trial_frame_start = ITI_epoch_start(1);
    trial_end_frame = reward_epoch_end_frame;
    
    trial_Unity_time_start = ITI_epoch_start(2);
    trail_Unity_end_time = Reward_epoch_start(2)+Reward_epoch_duration;
    
    
    if isempty(all_UDP_recv_data{t})
        continue
    end
    UDP_start_index = find(all_UDP_recv_data{t}(:,1) == trial_frame_start);
    if length(UDP_start_index) > 1
        UDP_start_index = UDP_start_index(1);
        disp('Found Multiple UDP Start Messages')
    elseif isempty(UDP_start_index)
        if abs(all_UDP_recv_data{t}(1,1)-trial_frame_start) <= 1
            UDP_start_index = 1;
            disp('1 frame offset with UDP')
        else
            disp('Cant find Trial Start Frame in UDP')
            continue
        end
    end
    
    
    UDP_end_index = find(all_UDP_recv_data{t}(:,1) == trial_end_frame);
    if length(UDP_end_index) > 1
        UDP_end_index = UDP_end_index(1);
        disp('Found Multiple UDP End Messages')
    elseif isempty(UDP_end_index)
        disp('Cant find Trial End Frame in UDP')
        continue
    end
    
    %verify frame start time is within 1 frame across files
    if abs(all_UDP_recv_data{t}(UDP_start_index,2)- trial_Unity_time_start) > 0.018
        disp('Fixation Epochs Start: UDP and Frame Start times are off :(')
    end
    if abs(all_UDP_recv_data{t}(UDP_end_index,2)- trail_Unity_end_time) > 0.018
        disp('Fixation Epochs Start: UDP and Frame Start times are off :(')
    end
    
    Eye_tracker_time_stamp_start = all_UDP_recv_data{t}(UDP_start_index,5);
    if isnan(Eye_tracker_time_stamp_start)
        disp('What')
    elseif isempty(Eye_tracker_time_stamp_start)
        disp('What')
    end
    
    Eye_tracker_time_stamp_end = all_UDP_recv_data{t}(UDP_end_index,5);
    if isnan(Eye_tracker_time_stamp_end)
        disp('What')
    elseif isempty(Eye_tracker_time_stamp_end)
        disp('What')
    end
    
    try
        
        if size(dir(frame_data_dir2),1) == 3 %only 1 frame data file
            python_ind  = find(all_pythonEye_data_time(:,2) >= Eye_tracker_time_stamp_start & ...
                all_pythonEye_data_time(:,2) <= Eye_tracker_time_stamp_end);
            
            trial_aligned_Python_eye_data{t} = [all_pythonEye_data_time(python_ind,:) ...
                all_pythonEye_data_XY(python_ind,:)];
        else
            python_ind  = find(all_pythonEye_data_time{t}(:,2) >= Eye_tracker_time_stamp_start & ...
                all_pythonEye_data_time{t}(:,2) <= Eye_tracker_time_stamp_end);
            trial_aligned_Python_eye_data{t} = [all_pythonEye_data_time{t}(python_ind,:) ...
                all_pythonEye_data_XY{t}(python_ind,:)];
        end
    catch
        disp('Eye Data mismatch or alignment issue')
    end
end
%%
all_pct_looking = NaN(num_trials,7);

for t =1:num_trials
    num_objs = length(session_data.trial_def{t}.relevantObjects);
    select_rad = trial_data.selectionRadius(t);
    
    reward_obj  =[];
    object_locations = NaN(2,num_objs);
    allStimDimVals = NaN(num_objs,7);
    allNeutralVals = NaN(num_objs,5);
    for o = 1:num_objs
        if session_data.trial_def{t}.relevantObjects(o).StimTrialRewardProb == 1
            reward_obj = [reward_obj o];
        end
        object_locations(1,o) = round(session_data.trial_def{t}.relevantObjects(o).StimScreenLocation.x);
        object_locations(2,o) = round(session_data.trial_def{t}.relevantObjects(o).StimScreenLocation.y);
        [dimVals,neutralVals] = ParseQuaddleName(session_data.trial_def{t}.relevantObjects(o).StimName);%for now in case not listed
        allStimDimVals(o,:) = [dimVals(1,:) dimVals(2,3) dimVals(2,5)];
        allNeutralVals(o,:) = neutralVals;
    end
    
    rewarded_shape = allStimDimVals(reward_obj,1);
    rewarded_pattern = allStimDimVals(reward_obj,2);
    rewarded_color = [allStimDimVals(reward_obj,3); allStimDimVals(reward_obj,6)];
    rewarded_arms = [allStimDimVals(reward_obj,4); allStimDimVals(reward_obj,7)];
    
    stim_array = -1*ones(screenY,screenX);
    [yy,xx] = find(stim_array == -1);
    xyind = find(stim_array == -1);
    for o = 1:num_objs
        rind = find(sqrt((object_locations(1,o)-xx).^2+(object_locations(2,o)-yy).^2) <= select_rad/2);
        if sum(allNeutralVals(o,:)) == 5 %neutral quaddle
            stim_array(xyind(rind)) = 0;
        elseif reward_obj == o
            stim_array(xyind(rind)) = 6;
        elseif allStimDimVals(o,1) == rewarded_shape && allNeutralVals(o,1) == 0
            stim_array(xyind(rind)) = 2;
        elseif allStimDimVals(o,2) == rewarded_pattern && allNeutralVals(o,2) == 0
            stim_array(xyind(rind)) = 3;
        elseif ((abs(allStimDimVals(o,3)-rewarded_color(1))-1000000) < 3) && allNeutralVals(o,3) == 0
            stim_array(xyind(rind)) = 4;
        elseif allStimDimVals(o,4) == (rewarded_arms(1) && rewarded_arms(2) == allStimDimVals(o,7)) && (allNeutralVals(o,4) == 0 && allNeutralVals(o,5) == 0)
            stim_array(xyind(rind)) = 5;
        else
            disp('Unknown feature value')
        end
    end
    
    
    baseline_end_time = trial_data.Epoch4_StartTimeRelative(t);
    reward_start_time =  trial_data.Epoch7_StartTimeRelative(t);
    eyetime = trial_aligned_Python_eye_data{t}(:,2);
    eyetime = eyetime-eyetime(1);
    eyetime = eyetime/1000000;%convert to seconds
    
    t1 = find(eyetime < baseline_start_time);
    t1 = t1(end);
    t2 = find(eyetime > reward_start_time);
    t2 = t2(1);
    
    eyex = trial_aligned_Python_eye_data{t}(t1:t2,5);
    eyex = filtfilt(Blow,Alow,eyex);
    
    eyey = trial_aligned_Python_eye_data{t}(t1:t2,6);
    eyey = filtfilt(Blow,Alow,eyey);
    
    cumulative_looking_time = zeros(1,7);
    for eyeind = 1:length(eyex)
        x = round(1920*eyex(eyeind));
        if x < 1 || x > 1920 || isnan(x)
            continue
        end
        y = round(1080*(1-eyey(eyeind)));
        if y < 1 || y > 1080 || isnan(y)
            continue
        end
        
        val = stim_array(y,x);
        if val == -1%looking at background
            cumulative_looking_time(1) = cumulative_looking_time(1) + 1;
        elseif val == 0 %neutral quaddle
            cumulative_looking_time(2) = cumulative_looking_time(2) + 1;
        else
            cumulative_looking_time(val+1) = cumulative_looking_time(val+1) + 1;
        end
    end
    all_pct_looking(t,:) = cumulative_looking_time./sum(cumulative_looking_time);
    
%     figure
%     subplot(2,2,1)
%     imagesc(stim_array)
%     hold on
%     plot(1920*eyex,1080*(1-eyey),'w')
%     hold off
%     box off
%     axis equal
%     xlim([1 1920])
%     ylim([1 1080])
%     axis off
%     
%     subplot(2,2,2)
%     plot(eyetime(t1:t2),1920*eyex)
%     hold on
%     plot(eyetime(t1:t2),1080*eyey)
%     hold off
%     xlim([eyetime(t1) eyetime(t2)])
%     ylim([0 1920])
%     legend('X','Y')
%     xlabel('Time (sec)')
%     ylabel('Eye Position (px)')
%     box off
%     
%     subplot(2,2,3)
%     bar(1:7,100*cumulative_looking_time./sum(cumulative_looking_time));
%     set(gca,'Xtick',1:7);
%     set(gca,'XtickLabel',{'Background','Neutral','Shape','Pattern','Color','Arms','Rewarded'})
%     xtickangle(45)
%     ylabel('% of Looking Time')
%     box off
%     subtitle(['Eye data for Trial #' num2str(t)])
end
%%
figure
bar(1:7,100*nanmean(all_pct_looking));
set(gca,'Xtick',1:7);
set(gca,'XtickLabel',{'Background','Neutral','Shape','Pattern','Color','Arms','Rewarded'})
xtickangle(45)
ylabel('% of Looking Time')
box off
subtitle('Average Percent of Looking time by Feature Dim')