function Calculate_FLU2FS_RH_FSonly_anlaysis(session_data,all_FLU_FS_blocks,...
    min_FLU_block_len,min_FS_block_len,min_FS_performance)
%Written by Seth Konig 6/24/19
%Modified from Calculate_FLU2FS_FSonly_anlaysis to handle new FLU2FS_RH anlaysi

%Inputs:
% 1) session_data across all sessions
% 2) all_FLU_FS_blocks: determines which blocks are FLu and which are FS
% 3) min_FLU_block_len: miniumum # of trials in a FLU block
% 4) min_FS_performance: minimum perforamnce to count as "good" FS block

expected_numD = [3 5 7];
if min_FLU_block_len == 20
    expected_numTrials = [20 40 60];
elseif min_FLU_block_len == 30
    expected_numTrials = [30 60 90];
else
    error('Need to know expected number of trials per FLU block')
end

%lp variables
forward_smoothing_window = 10;
learning_point_threshold = 0.8;

num_sessions = length(session_data);

session_average_FLU_performance = NaN(num_sessions,length(expected_numTrials));
session_average_FLU_search_duration = NaN(num_sessions,length(expected_numTrials));
session_average_FS_performance = NaN(num_sessions,length(expected_numD));
session_average_FS_search_dur = NaN(num_sessions,length(expected_numD));

%LMM variables
%value to fit
all_search_durs = [];
%important trial factors
all_numT = [];
all_numD = [];
%important reward factors
all_lps = [];
all_FLU_num_rewards = [];
all_FLU_num_rewards_since_lp = [];
%random effects
all_target_dims = [];
all_sessions = [];
all_trial_nums = [];
all_block_nums = [];

for sess = 1:num_sessions
    num_blocks = length(all_FLU_FS_blocks{sess});
    block = session_data{sess}.trial_data.Block;
    block = block + 1;%index starts at 0 in C++
    rewarded = strcmpi(session_data{sess}.trial_data.isRewarded,'true');
    search_duration = Calculate_FLU_Search_Duration(session_data{sess});
    
    %uses a little bit more space but makes indexing for LP easier so....
    FLU_performance_last_10_trials = NaN(num_blocks,length(expected_numTrials));
    FLU_search_duration_last_10_trials = NaN(num_blocks,length(expected_numTrials));
    
    %reward variables so can track things later for LMM
    FLU_lps = NaN(1,num_blocks);
    FLU_num_rewards = NaN(1,num_blocks);
    FLU_num_rewards_since_lp = NaN(1,num_blocks);
    FLU_target_dim = NaN(1,num_blocks);
    
    %divide by numD,num irrel in FLU block, whether FS block was good, and
    %whether trial was rewarded
    FS_performance_by_num_trials = cell(length(expected_numD),3);
    FS_search_duration_by_num_trials = cell(length(expected_numD),3,2,2);
    
    for numD = 1:length(expected_numD)
        for num_trials = 1:3
            FS_search_duration_by_num_trials{numD,num_trials} = NaN(num_blocks,min_FS_block_len);
            FS_performance_by_num_trials{numD,num_trials} = NaN(num_blocks,min_FS_block_len);
        end
    end
    
    FLU_blocks = find(all_FLU_FS_blocks{sess} == 1);
    
    for b = 1:length(FLU_blocks)
        this_FLU_block = FLU_blocks(b);
        blockind = find(block == this_FLU_block);
        if isempty(blockind)
            error('Why is this FLU block empty?')
        elseif length(blockind) < min_FLU_block_len
            error('Why is this FLU block too short?')
        end
        
        this_number_FLU_trials = session_data{sess}.block_def(this_FLU_block).TrialRange(1);%20, 40, 60
        this_number_FLU_trials = find(expected_numTrials == this_number_FLU_trials);
        if isempty(this_number_FLU_trials)
            error('Unknown number of trials')
        end
        
        %---Get Performance on last 10 trials of FLU block---%
        blockrewarded = rewarded(blockind(1):blockind(end));
        lp = FindLp(blockrewarded, 'slidingwindow', forward_smoothing_window, learning_point_threshold);
        if isnan(lp)
            disp('No LP found...going to next block')
            continue
        else
            FLU_lps(this_FLU_block) = lp;
            FLU_num_rewards(this_FLU_block) = sum(blockrewarded);
            FLU_num_rewards_since_lp(this_FLU_block) = sum(blockrewarded(lp:end));
            [FLU_target_dim(this_FLU_block),~] = DetermineTargetDimension(session_data{sess},FLU_blocks(b));
        end
        FLU_performance_last_10_trials(this_FLU_block,this_number_FLU_trials) = mean(blockrewarded(end-9:end));
        
        %---Get Search Duration on last 10 trials of FLU block---%
        blocksearchdur = search_duration(blockind(1):blockind(end));
        FLU_search_duration_last_10_trials(this_FLU_block,this_number_FLU_trials) = mean(blocksearchdur(end-9:end));
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%---Data for FS Block---%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if this_FLU_block+1 > length(all_FLU_FS_blocks{sess})
            continue
        elseif all_FLU_FS_blocks{sess}(this_FLU_block+1) == 2
            this_FS_block = this_FLU_block+1;
            FS_blockind = find(block == this_FS_block);
            if isempty(FS_blockind)
                error('Why is this FS block empty?')
            elseif length(FS_blockind) < min_FS_block_len
                %block not finished
                continue
            elseif length(FS_blockind) > min_FS_block_len+1
                error('Why are there so many trials in this FS block?')
            end
            
            FSblockrewarded = rewarded(FS_blockind(1):FS_blockind(end));
            FSblocksearchdur = search_duration(FS_blockind(1):FS_blockind(end));
            if length(FSblockrewarded) > min_FS_block_len
                selected_objects = session_data{sess}.trial_data.SelectedObjectID(FS_blockind(1):FS_blockind(end));
                emptys = find(cellfun(@isempty,selected_objects));
                FSblockrewarded(emptys) = [];
                FSblocksearchdur(emptys) = [];
            end
            
            if sum(FSblockrewarded)/length(FSblockrewarded) < min_FS_performance
                continue
            end
            for bk = 1:length(FSblockrewarded)
                if length(session_data{sess}.trial_def{FS_blockind(bk)}.relevantObjects) == 8
                    %newere version with stimlui on concentric circle
                    numD = 0;
                    for stim = 1:8
                        if sum(session_data{sess}.trial_def{FS_blockind(bk)}.relevantObjects(stim).StimDimVals ~= 0) >= 1
                            numD = numD + 1;
                        end
                    end
                    numD = numD - 1;%target doesn't count
                else
                    error('Why are there not 8 stimuli on this FS trial?');
                end
                numDindex = find(expected_numD == numD);
                if isempty(numDindex)
                    error('Wrong # of Distractors?');
                else
                    FS_performance_by_num_trials{numDindex,this_number_FLU_trials}(this_FS_block,bk) ...
                        = FSblockrewarded(bk);
                    if FSblockrewarded(bk)
                        FS_search_duration_by_num_trials{numDindex,this_number_FLU_trials}(this_FS_block,bk) ...
                            = FSblocksearchdur(bk);
                    end
                end
            end
        else
            disp('Why is there no FS block to anlayze?')
            continue
        end
    end
    
    %compact into average across sessions
    session_average_FLU_performance(sess,:) = nanmean(FLU_performance_last_10_trials);
    session_average_FLU_search_duration(sess,:) = nanmean(FLU_search_duration_last_10_trials);
    for numT = 1:length(expected_numTrials)
        for numD = 1:length(expected_numD)
            session_average_FS_performance(sess,numD,numT) = nanmean(FS_performance_by_num_trials{numD,numT}(:));
            session_average_FS_search_dur(sess,numD,numT) = nanmean(FS_search_duration_by_num_trials{numD,numT}(:));
        end
    end
    %%
    %format for LMM
    for numT = 1:length(expected_numTrials)
        for numD = 1:length(expected_numD)
            
            nonnan_ind = find(~isnan(FS_performance_by_num_trials{numD,numT}));
            [block,trials] = find(~isnan(FS_performance_by_num_trials{numD,numT}));
            
            %value to fit
            all_search_durs = [all_search_durs; FS_search_duration_by_num_trials{numD,numT}(nonnan_ind)];
            
            %important trial factors
            all_numT = [all_numT; expected_numTrials(numT)*ones(length(nonnan_ind),1)];
            all_numD = [all_numD; expected_numD(numD)*ones(length(nonnan_ind),1)];
            
            for bl = 1:length(block)
                %important reward factors
                all_lps = [all_lps; FLU_lps(block(bl)-1)];
                all_FLU_num_rewards = [all_FLU_num_rewards; FLU_num_rewards(block(bl)-1)'];
                all_FLU_num_rewards_since_lp = [all_FLU_num_rewards_since_lp; FLU_num_rewards_since_lp(block(bl)-1)'];
                
                %random effects, target dim...likley a big factor
                all_target_dims = [all_target_dims; FLU_target_dim(block(bl)-1)'];
            end
            
            %more random effects
            all_sessions = [all_sessions; sess*ones(length(nonnan_ind),1)];
            all_trial_nums = [all_trial_nums; trials];
            all_block_nums = [all_block_nums; block];
        end
    end
    
    %%
end
%% Calculate Search slope and intercept by sessions
session_intercepts = NaN(num_sessions,length(expected_numTrials));
session_slopes = NaN(num_sessions,length(expected_numTrials));

for sess = 1:num_sessions
    for numT = 1:length(expected_numTrials)
        these_durs = session_average_FS_search_dur(sess,:,numT);
        P = polyfit(expected_numD,these_durs,1);
        session_slopes(sess,numT) = P(1);
        session_intercepts(sess,numT) = P(2);
    end
end

mean_session_slopes = 1000*nanmean(session_slopes);
sem_session_slopes = 1000*nanstd(session_slopes)./sqrt(num_sessions);
mean_session_intercepts = 1000*nanmean(session_intercepts);
sem_session_intercepts = 1000*nanstd(session_intercepts)./sqrt(num_sessions);
%%
%---Plot Main Results---%
figure
subplot(2,3,1)
bar(100*mean(session_average_FLU_performance))
hold on
errorb(1:3,100*mean(session_average_FLU_performance),100*std(session_average_FLU_performance)./sqrt(num_sessions))
hold off
box off
xticks([1:3])
if min_FLU_block_len == 20
    xticklabels({'20','40','60'})
elseif min_FLU_block_len == 30
    xticklabels({'30','60','90'})
end
xlabel('# of FLU Trials')
ylabel('Average Performance')
title('FLU performance last 10 trials')
box off
ylim([75 100])

subplot(2,3,2)
bar(mean(session_average_FLU_search_duration))
hold on
errorb(1:3,mean(session_average_FLU_search_duration),std(session_average_FLU_search_duration)./sqrt(num_sessions))
hold off
box off
xticks([1:3])
if min_FLU_block_len == 20
    xticklabels({'20','40','60'})
elseif min_FLU_block_len == 30
    xticklabels({'30','60','90'})
end
xlabel('# of FLU Trials')
ylabel('Search Duration (sec)')
title('FLU Search Duration last 10 trials')
box off
yl = ylim;
if yl(1) == 0
    ylim([min(mean(session_average_FLU_search_duration))-max(std(session_average_FLU_search_duration)...
        ./sqrt(num_sessions)) yl(2)])
end

across_session_avg = NaN(3,length(expected_numD));
across_session_sem = NaN(3,length(expected_numD));

for numT = 1:3
    for numD = 1:length(expected_numD)
        session_average_FS_performance(sess,numD,numT) = nanmean(FS_performance_by_num_trials{numD,numT}(:));
        across_session_avg(numT,numD) = nanmean(session_average_FS_performance(:,numD,numT));
        across_session_sem(numT,numD) = nanstd(session_average_FS_performance(:,numD,numT))./sqrt(num_sessions);
    end
end

clrs = 'rgb';
subplot(2,3,3)
hold on
for irrel = 1:3
    errorbar(expected_numD,100*across_session_avg(irrel,:),100*across_session_sem(irrel,:),clrs(irrel),'linewidth',2)
end
hold off
box off
xlabel('# FLU Trials')
ylabel('Performance')
xticks([expected_numD])
ylim([75 100])
xlim([expected_numD(1)-1 expected_numD(end)+1])

if min_FLU_block_len == 20
    legend({'20','40','60'},'location','northEastOutside')
elseif min_FLU_block_len == 30
    legend({'30','60','90'},'location','northEastOutside')
end
title('FS Performance')

across_session_avg_correct_trials_good_blocks = NaN(3,length(expected_numD));
across_session_sem_correct_trials_good_blocks = NaN(3,length(expected_numD));

good = 1;
rw = 1;
for numT = 1:3
    for numD = 1:length(expected_numD)
        across_session_avg_correct_trials_good_blocks(numT,numD) = ...
            nanmean(session_average_FS_search_dur(:,numD,numT,good,rw));
        across_session_sem_correct_trials_good_blocks(numT,numD) = ...
            nanstd(session_average_FS_search_dur(:,numD,numT,good,rw))./sqrt(num_sessions);
    end
end

clrs = 'rgb';
subplot(2,3,4)
hold on
for irrel = 1:3
    errorbar(expected_numD,across_session_avg_correct_trials_good_blocks(irrel,:),...
        across_session_sem_correct_trials_good_blocks(irrel,:),clrs(irrel),'linewidth',2)
end
hold off
box off
xlabel('# Distractors')
ylabel('Search Duration (sec)')
xticks(expected_numD)
xlim([expected_numD(1)-1 expected_numD(end)+1])
title('FS Search Duration')

% subplot(2,3,5)
% hold on
% for irrel = 1:2
%     plot(expected_numD,across_session_avg_correct_trials_good_blocks(irrel,:)-across_session_avg_correct_trials_good_blocks(3,:))
% end
% plot(expected_numD,across_session_avg_correct_trials_good_blocks(2,:)-across_session_avg_correct_trials_good_blocks(1,:))
% plot([2.5 7.5],[0 0],'k--')
% hold off
% legend({'20 vs 60','20 vs 40','40 vs 60'},'location','Northeastoutside')
% ylabel('Search Duration (sec)')
% xlabel('# Distractors')
% xticks([expected_numD])
% xlim([expected_numD(1)-1 expected_numD(end)+1])
% title('Difference in Search Duration by # FLU Trials')

subplot(2,3,5)
bar(expected_numTrials,mean_session_slopes)
hold on
errorbar(expected_numTrials,mean_session_slopes,sem_session_slopes,'k','linestyle','none');
hold off
box off
xlabel('# of FLU Trials')
ylabel('Search Slope, m (ms/distractor)')
title('Search Efficiency')

subplot(2,3,6)
bar(expected_numTrials,mean_session_intercepts)
hold on
errorbar(expected_numTrials,mean_session_intercepts,sem_session_intercepts,'k','linestyle','none');
hold off
box off
xlabel('# of FLU Trials')
ylabel('Search Intercept (ms)')
title('Search Intercept')

%% Build LMM Model

%---Make Model table---%
mainFStbl = table(all_search_durs,all_numD,all_numT,...
    all_lps,all_FLU_num_rewards,all_FLU_num_rewards_since_lp,...
    all_target_dims,all_sessions,all_trial_nums,all_block_nums,...
    'VariableNames',{'SearchDuration','numD','numT',...
    'LP','NumRewards','NumRewardsLP',...
    'TargetDim','Session','FSTrialNum','Block'});


%---Made Models---%
random_effects_str = '+ (numD|TargetDim) + (numD|Session) + (numD|FSTrialNum) + (numD|Block)';
models = cell(1,4);
model_names = {'numT','LP','NumRewards','NumRewardsLP'};
models{1} = fitlme(mainFStbl,['SearchDuration ~ 1 + numD + numT ' random_effects_str]);
models{2} = fitlme(mainFStbl,['SearchDuration ~ 1 + numD + LP + (LP|TargetDim)' random_effects_str]);
models{3} = fitlme(mainFStbl,['SearchDuration ~ 1 + numD + NumRewards + (NumRewards|TargetDim)' random_effects_str]);
models{4} = fitlme(mainFStbl,['SearchDuration ~ 1 + numD + NumRewardsLP + (NumRewardsLP|TargetDim)' random_effects_str]);

%---Calculate Model Fitness---%
r2s = NaN(1,length(models));
AICs = NaN(1,length(models));
slopes = cell(1,length(models));
slope_names = cell(1,length(models));
alt_comparison_p_values = NaN(length(models),length(models));
for m = 1:length(models)
    
    r2s(m) = models{m}.Rsquared.Ordinary;
    AICs(m) = models{m}.ModelCriterion.AIC;
    slopes{m} = models{m}.Coefficients.Estimate(2:end)*1000;
    slope_names{m} = models{m}.CoefficientNames(2:end);
    for mm = 1:length(models)
        if m > mm
            if models{m}.DFE > models{mm}.DFE%simple model should alsways be first
                comparison_stats = compare(models{m},models{mm});
            else
                comparison_stats = compare(models{mm},models{m});
            end
            alt_comparison_p_values(m,mm) = comparison_stats.pValue(2);
        end
    end
end

%%
[~,~,randomeffects_stats] = covarianceParameters(models{1});

random_interecept_effect_names = {'TargetDim','Session','FSTrialNum','Block'};

intercept_random_effects_var = NaN(1,length(randomeffects_stats)-1);
slope_random_effects_var = NaN(1,length(randomeffects_stats)-1);

intercept_random_effects_var(1) = randomeffects_stats{1}.Estimate(1);%Target Dim intercept
intercept_random_effects_var(2) = randomeffects_stats{2}.Estimate(1);%Session intercept
intercept_random_effects_var(3) = randomeffects_stats{3}.Estimate(1);%FS Trial num intercept
intercept_random_effects_var(4) = randomeffects_stats{4}.Estimate(1);%Block intercept

slope_random_effects_var(1) = randomeffects_stats{1}.Estimate(3);%Target Dim slope
slope_random_effects_var(2) = randomeffects_stats{2}.Estimate(3);%Session slope
slope_random_effects_var(3) = randomeffects_stats{3}.Estimate(3);%FS Trial num slope
slope_random_effects_var(4) = randomeffects_stats{4}.Estimate(3);%Block slope

%% PLot LMM results
sig_threshold = 0.05/sum(~isnan(alt_comparison_p_values(:)));
non_sig_comparisons = find(alt_comparison_p_values > 0.05/sum(~isnan(alt_comparison_p_values(:))));%bonferroni correction

%Creat p-value colormap
crange = [0 sig_threshold/100; sig_threshold/100 sig_threshold/10; sig_threshold/10 sig_threshold; ...
    sig_threshold 1];
cmap = copper(size(crange,1));

colormappingind = NaN(size(alt_comparison_p_values));
for i = 1:size(crange,1)
    colormappingind(alt_comparison_p_values <= crange(i,2) & alt_comparison_p_values >= crange(i,1)) = i;
end

figure
subplot(2,3,1)
imagesc(colormappingind,'AlphaData',~isnan(colormappingind))
xticks(1:length(model_names))
xticklabels(model_names);
xtickangle(30);
yticks(1:length(model_names))
yticklabels(model_names);
axis square
colormap(cmap)
cbh = colorbar;
cbh.Ticks = 1:size(crange,1);
cbh.TickLabels = num2cell(crange(:,2));    %Replace the labels of these 8 ticks with the numbers 1 to 8
title('Log-likelhood ratio test, pvalues')

subplot(2,3,2)
bar(r2s)
xticks(1:length(models))
xticklabels(model_names)
xtickangle(30)
ylabel('R^2')
title('Model Fits')
box off
yl = ylim;
ylim([0.15 yl(2)])

for m = 1:length(models)
    subplot(2,3,2+m)
    bar(slopes{m})
    box off
    xticks(1:length(slope_names{m}))
    xticklabels(slope_names{m})
    xtickangle(30)
    ylabel('ms/factor')
    title(model_names{m})
end

figure
subplot(1,2,1)
bar(1000*intercept_random_effects_var)
box off
xticks(1:length(intercept_random_effects_var))
xticklabels(random_interecept_effect_names)
xtickangle(30)
ylabel('ms^2')
title('Random Effects: Intercepts')

subplot(1,2,2)
bar(1000*slope_random_effects_var)
box off
xticklabels(random_interecept_effect_names)
xtickangle(30)
ylabel('(ms/factor)^2')
title('Random Effects: Slopes')

%%
end