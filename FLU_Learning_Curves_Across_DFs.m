%Written by Seth Konig 3/9/19
%Code grabs data across different Difficulty modes to look at how learning
%curves are affected by task structure

clar
num_shuffs = 1000;


monkeyname = 'Wotan';
log_dir = 'Z:\MonkeyData\Wotan\FLU Task\';

%all "good" sets with > ~80% performance on last 10 trials in a block
sess_file_names = {'FLU__Wotan__04_11_2018__12_25_57','FLU__Wotan__05_11_2018__13_19_07',...
    'FLU__Wotan__06_11_2018__12_54_38','FLU__Wotan__07_11_2018__11_41_06',...
    'FLU__Wotan__08_11_2018__10_41_47','FLU__Wotan__13_11_2018__11_06_25',...
    'FLU__Wotan__19_11_2018__11_45_33','FLU__Wotan__15_11_2018__11_38_51',...
    'FLU__Wotan__20_11_2018__10_47_16','FLU__Wotan__21_11_2018__10_57_12',...
    'FLU__Wotan__22_11_2018__12_58_45','FLU__Wotan__23_11_2018__13_48_36',...
    'FLU__Wotan__26_11_2018__11_09_58','FLU__Wotan__27_11_2018__12_25_21',...
    'FLU__Wotan__28_11_2018__14_46_10','FLU__Wotan__30_11_2018__11_44_41',...
    'FLU__Wotan__01_12_2018__13_58_02',...
    'FLU__Wotan__03_12_2018__15_18_08','FLU__Wotan__04_12_2018__10_27_26',...
    'FLU__Wotan__05_12_2018__11_23_55','FLU__Wotan__08_12_2018__12_02_13',...
    'FLU__Wotan__09_12_2018__13_43_05','FLU__Wotan__10_12_2018__15_48_08',...
    'FLU__Wotan__11_12_2018__11_20_40','FLU__Wotan__12_12_2018__11_19_32',...
    'FLU__Wotan__13_12_2018__11_20_58','FLU__Wotan__14_12_2018__12_01_09',...
    'FLU__Wotan__07_01_2019__14_04_19','FLU__Wotan__08_01_2019__14_28_14',...
    'FLU__Wotan__09_01_2019__13_56_16',...
    'FLU__Wotan__10_01_2019__14_41_35','FLU__Wotan__11_01_2019__15_10_41',...
    'FLU__Wotan__14_01_2019__12_44_40','FLU__Wotan__16_01_2019__13_29_45',...
    'FLU__Wotan__17_01_2019__14_27_36','FLU__Wotan__18_01_2019__12_09_39',...
    'FLU__Wotan__21_01_2019__12_36_27','FLU__Wotan__22_01_2019__12_02_23',...
    'FLU__Wotan__23_01_2019__12_40_21','FLU__Wotan__27_01_2019__13_47_33',...
    'FLU__Wotan__29_01_2019__11_55_24',...
    };

session_type = {'Reversal','Reversal',...
    'Reversal','Reversal',...
    'Reversal','Reversal',...
    'Reversal','Reversal',...
    '0 Irrelevant','0 Irrelevant',...
    '0 Irrelevant','0 Irrelevant',...
    '0 Irrelevant','0 Irrelevant',...
    '0 Irrelevant','0 Irrelevant',...
    '0 Irrelevant',...
    '1 Irrelevant','1 Irrelevant',...
    '1 Irrelevant','1 Irrelevant',...
    '1 Irrelevant','1 Irrelevant',...
    '1 Irrelevant','1 Irrelevant',...
    '1 Irrelevant','1 Irrelevant',...
    '1 Irrelevant','1 Irrelevant',...
    '1 Irrelevant',...
    '2 Irrelevant','2 Irrelevant',...
    '2 Irrelevant','2 Irrelevant',...
    '2 Irrelevant','2 Irrelevant',...
    '2 Irrelevant','2 Irrelevant',...
    '2 Irrelevant','2 Irrelevant',...
    '2 Irrelevant',...
    };



%% Import Session Data
num_session = length(sess_file_names);
session_data = cell(1,num_session);
for sess = 1:num_session
    disp(['Loading dat from session #' num2str(sess) '/' num2str(num_session)])
    session_data{sess} = ImportFLU_DataV2(sess_file_names{sess},log_dir);
end

%% All block peformance by session

all_block_performance = cell(1,num_session);
all_block_search_duration = cell(1,num_session);

for sess = 1:num_session
    
    search_duration = session_data{sess}.trial_data.Epoch4_Duration;
    rewarded = strcmpi(session_data{sess}.trial_data.isRewarded,'true');
    
    block = session_data{sess}.trial_data.Block;
    block = block + 1;%index starts at 0 in C++
    
    block_ind = 1;
    this_session_search_durs = NaN(max(block),40);
    this_session_performance = NaN(max(block),40);
    
    for b =2:max(block)
        
        blockind = find(block == b);
        blockrewarded = rewarded(blockind(1)-10:blockind(end));
        blocksearchdur = search_duration(blockind(1)-10:blockind(end));
        
        if length(blockind) < 15
            continue
        end
        blocksearchdur(blocksearchdur >= 2.5) = NaN;%timed out or not paying attention
        blocksearchdur(blocksearchdur <= 0.25) = NaN;
        
        
        if length(blockrewarded) >= 40
            this_session_performance(block_ind,1:40) = blockrewarded(1:40);
            this_session_search_durs(block_ind,1:40) = blocksearchdur(1:40);
        else
            this_session_performance(block_ind,1:length(blockrewarded)) = blockrewarded;
            this_session_search_durs(block_ind,1:length(blocksearchdur)) = blocksearchdur;
        end
        
        block_ind = block_ind + 1;
    end
    
    all_block_performance{sess} = this_session_performance;
    all_block_search_duration{sess} = this_session_search_durs;
    
end
% Remove extra NaNs
all_block_performance = laundry(all_block_performance);
all_block_search_duration = laundry(all_block_search_duration);
%% Collapse Block Performances Across Task Type
all_session_types = unique(session_type);

type_learning_curves = cell(1,length(all_session_types));
type_search_duration = cell(1,length(all_session_types));
for type = 1:length(all_session_types)
    these_sessions = find(contains(session_type,all_session_types{type}));
    
    for ts = 1:length(these_sessions)
        type_learning_curves{type} = [type_learning_curves{type}; all_block_performance{these_sessions(ts)}];
        type_search_duration{type} = [type_search_duration{type}; all_block_search_duration{these_sessions(ts)}];
    end
end
%%
n_learn_curves = cellfun(@(s) size(s,1),type_learning_curves);

average_type_learning_curves = NaN(length(all_session_types),40);
average_type_search_duration = NaN(length(all_session_types),40);
for type = 1:length(all_session_types)
    average_type_learning_curves(type,:) = nanmean(type_learning_curves{type});
    average_type_search_duration(type,:) = nanmean(type_search_duration{type});
end
%% Stats by number of irrelevant features
all_curves = [];
all_types = [];
for type = 1:3
    all_curves = [all_curves; type_learning_curves{type}];
    all_types = [all_types; type*ones(size(type_learning_curves{type},1),1)];
end

stat_win = [4 20]+10;%trials 4-20 after the shift igore 1st since could be at chance

observed_diff01 = sum(average_type_learning_curves(1,stat_win(1):stat_win(2))-average_type_learning_curves(2,stat_win(1):stat_win(2)));
observed_diff02 = sum(average_type_learning_curves(1,stat_win(1):stat_win(2))-average_type_learning_curves(3,stat_win(1):stat_win(2)));
observed_diff12 = sum(average_type_learning_curves(2,stat_win(1):stat_win(2))-average_type_learning_curves(3,stat_win(1):stat_win(2)));

shuffled_diff = NaN(3,num_shuffs);
for shuff = 1:num_shuffs
    shuff_ind = all_types(randperm(length(all_types)));
    shuff1 = nanmean(all_curves(shuff_ind == 1,:));
    shuff2 = nanmean(all_curves(shuff_ind == 2,:));
    shuff3 = nanmean(all_curves(shuff_ind == 3,:));
    
    shuffled_diff(1,shuff) = sum(shuff1(stat_win(1):stat_win(2))-shuff2(stat_win(1):stat_win(2)));
    shuffled_diff(2,shuff) = sum(shuff1(stat_win(1):stat_win(2))-shuff3(stat_win(1):stat_win(2)));
    shuffled_diff(3,shuff) = sum(shuff2(stat_win(1):stat_win(2))-shuff3(stat_win(1):stat_win(2)));
end

prct01 = 100*sum(observed_diff01 > shuffled_diff(1,:))/num_shuffs;
prct02 = 100*sum(observed_diff02 > shuffled_diff(2,:))/num_shuffs;
prct12 = 100*sum(observed_diff12 > shuffled_diff(3,:))/num_shuffs;
%%
figure

figure
subplot(1,2,1)
hold on
for type = 1:length(all_session_types)-1
    plot(-9:30,100*FilterLearningCurves(average_type_learning_curves(type,:)',3))
end
plot([0.5 0.5],[0 100],'k--')
plot([-10 30],[80 80],'r--')
hold off
ylim([50 100])
xlabel('Trial # Relative To Switch')
ylabel('Performance %')
box off
axis square
legend(all_session_types,'Location','SouthEast')
title(sprintf(['Average Smoothed Performance: ' ...
     'percentile_{0 vs 1, trials 4-20} = ' num2str(prct01,3) '%%, \n' ...
     'percentile_{0 vs 2, trials 4-20} = ' num2str(prct02,3) '%%,' ...
      'percentile_{1 vs 2, trials 4-20} = ' num2str(prct12,3) '%%']))


subplot(1,2,2)
hold on
for type = 1:length(all_session_types)-1
    plot(-9:30,FilterLearningCurves( average_type_search_duration(type,:)',3))
end
yl = ylim;
hold on
plot([0.5 0.5],[yl(1) yl(2)],'k--')
hold off
axis square
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (seconds)')
box off
title('Average Smoothed Search Duration')

subtitle([monkeyname ': Performance and Search Durations across Task types'])



