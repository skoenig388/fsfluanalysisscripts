%written by Seth Koenig 
%4/19/2019

clar
import_option = 'slim';
min_acceptable_block_perform = 0.5;%set to 0 if you don't want to remove blocks

%---Bard---%
% monkey_name = 'Bard 0 irrelevant';
% sess_file_names = {'FLU__Bard__25_03_2019__09_33_24','FLU__Bard__26_03_2019__09_43_48',...
%     'FLU__Bard__27_03_2019__09_57_12','FLU__Bard__28_03_2019__09_40_11',...
%     'FLU__Bard__29_03_2019__11_04_20','FLU__Bard__01_04_2019__10_26_51',...
%     'FLU__Bard__02_04_2019__11_54_27','FLU__Bard__03_04_2019__10_09_40',...
%     'FLU__Bard__04_04_2019__10_40_09','FLU__Bard__05_04_2019__09_07_58',...
%     'FLU__Bard__08_04_2019__09_09_58','FLU__Bard__09_04_2019__10_38_44',...
%     'FLU__Bard__10_04_2019__11_00_18','FLU__Bard__11_04_2019__11_28_50',...
%     'FLU__Bard__15_04_2019__09_55_12','FLU__Bard__16_04_2019__09_39_53',...
%     'FLU__Bard__17_04_2019__10_33_13','FLU__Bard__18_04_2019__09_34_07',...
%     'FLU__Bard__19_04_2019__09_12_27',...
%     };


%---Frey---%
monkey_name = 'Frey: 1 irrelevant';
sess_file_names = {'FLU__Frey__14_03_2019__10_21_27','FLU__Frey__15_03_2019__11_42_45',...
    'FLU__Frey__17_03_2019__11_42_18','FLU__Frey__18_03_2019__10_49_59',...
    'FLU__Frey__19_03_2019__11_02_07','FLU__Frey__20_03_2019__10_30_40',...
    'FLU__Frey__21_03_2019__11_36_54','FLU__Frey__22_03_2019__10_25_23',...
    'FLU__Frey__23_03_2019__13_12_02','FLU__Frey__25_03_2019__10_01_40',...
    'FLU__Frey__26_03_2019__10_03_13','FLU__Frey__27_03_2019__10_18_42',...
    'FLU__Frey__28_03_2019__10_20_57','FLU__Frey__29_03_2019__11_55_16',...
    'FLU__Frey__30_03_2019__11_25_34','FLU__Frey__01_04_2019__10_49_32',...
    'FLU__Frey__02_04_2019__10_20_01','FLU__Frey__03_04_2019__10_53_30',...
    'FLU__Frey__04_04_2019__10_25_21','FLU__Frey__05_04_2019__10_56_11',...
    'FLU__Frey__06_04_2019__11_45_32','FLU__Frey__07_04_2019__10_41_40',...
    'FLU__Frey__08_04_2019__11_28_39','FLU__Frey__10_04_2019__10_26_52',...
    'FLU__Frey__11_04_2019__10_59_26','FLU__Frey__12_04_2019__10_22_15',...
    'FLU__Frey__13_04_2019__11_21_51','FLU__Frey__16_04_2019__10_13_48',...
    'FLU__Frey__17_04_2019__09_41_04','FLU__Frey__18_04_2019__10_30_20',...
    'FLU__Frey__19_04_2019__10_36_02','FLU__Frey__20_04_2019__11_30_00',...
    'FLU__Frey__22_04_2019__12_08_08','FLU__Frey__23_04_2019__10_38_39',...
};

%---Reider---%
% monkey_name = 'Reider 0 irrelevant';
% sess_file_names = {'FLU__Reider__19_03_2019__08_35_43','FLU__Reider__20_03_2019__08_55_13',...
%     'FLU__Reider__21_03_2019__11_15_49','FLU__Reider__22_03_2019__09_55_30',...
%     'FLU__Reider__25_03_2019__08_55_48','FLU__Reider__26_03_2019__10_44_44',...
%     'FLU__Reider__27_03_2019__09_04_07','FLU__Reider__28_03_2019__08_11_58',...
%     'FLU__Reider__29_03_2019__10_38_20','FLU__Reider__01_04_2019__10_31_48',...
%     'FLU__Reider__02_04_2019__10_03_58','FLU__Reider__03_04_2019__09_48_55',...
%     'FLU__Reider__04_04_2019__08_59_18','FLU__Reider__05_04_2019__10_40_14',...
%     'FLU__Reider__08_04_2019__10_45_08','FLU__Reider__09_04_2019__11_33_04',...
%     };


% monkey_name = 'Reider 1 irrelevant';
% sess_file_names = {'FLU__Reider__10_04_2019__11_04_28','FLU__Reider__11_04_2019__11_22_21',...
%     'FLU__Reider__12_04_2019__11_06_39','FLU__Reider__15_04_2019__09_06_29',...
%     'FLU__Reider__18_04_2019__09_32_55','FLU__Reider__17_04_2019__10_57_56',...
%     'FLU__Reider__19_04_2019__11_23_57',...
%     };


%% Import Session Data
num_sessions = length(sess_file_names);
session_data = cell(1,num_sessions);
for sess =1:num_sessions
    
    disp(['Loading data from session #' num2str(sess) '/' num2str(length(sess_file_names))])
    log_dir = FindDataDirectory(sess_file_names{sess});
    if ~isempty(log_dir)
        session_data{sess} = ImportFLU_DataV2(sess_file_names{sess},log_dir,import_option);
    else
        error(['Cant Find Log Dir for '  sess_file_names{sess}])
    end
end
%% Calculate Averege Performance by Block
%so can remove bad blocks at begining and end if motivational issues
average_block_performance = NaN(num_sessions,25);
for sess = 1:num_sessions
    rewarded = strcmpi(session_data{sess}.trial_data.isRewarded,'true');
    block = session_data{sess}.trial_data.Block;
    block = block + 1;%index starts at 0 in C++
    
    
    for b =1:max(block)
        blockind = find(block == b);
        if isempty(blockind)
            continue
        elseif length(blockind) < 15
            continue
        end
        blockrewarded = rewarded(blockind(1):blockind(end));
        average_block_performance(sess,b) = sum(blockrewarded)/length(blockrewarded);
    end
end

first_last_block_to_anlayze = NaN(num_sessions,2);
for sess = 1:num_sessions
   num_blocks = sum(~isnan(average_block_performance(sess,:)));
   bad_blocks =  find(average_block_performance(sess,:) < min_acceptable_block_perform);
   if isempty(bad_blocks)
       first_last_block_to_anlayze(sess,1) = 2; %ignore 1 anyay
       first_last_block_to_anlayze(sess,2) = num_blocks; %last block
       continue
   end
   gapped_bad_blocks = findgaps(bad_blocks);
   if bad_blocks(1) == 1
       one_gaps = gapped_bad_blocks(1,:);
       one_gaps(one_gaps == 0) = [];
       first_last_block_to_anlayze(sess,1) = one_gaps(end)+1;
   else
       first_last_block_to_anlayze(sess,1) = 2; %ignore 1 anyay
   end
   if bad_blocks(end) == num_blocks
       last_gaps = gapped_bad_blocks(end,:);
       last_gaps(last_gaps == 0) = [];
       first_last_block_to_anlayze(sess,2) = last_gaps(1)-1;
   else
       first_last_block_to_anlayze(sess,2) = num_blocks; %last block
   end
end
%% All block peformance

all_block_performance = NaN(10*length(sess_file_names),40);
all_block_search_duration = NaN(10*length(sess_file_names),40);
all_block_ind = 1;

for sess = 1:num_sessions
    
    if isfield(session_data{sess}.configdata,'objectSelectionMode')
        if strcmpi(session_data{sess}.configdata.objectSelectionMode,'2')
            search_start_time = session_data{sess}.trial_data.Epoch3_StartTimeRelative+session_data{sess}.trial_data.baselineDuration;
            search_end_time =  session_data{sess}.trial_data.Epoch7_StartTimeRelative...
                -session_data{sess}.trial_data.Epoch6_Duration-session_data{sess}.trial_data.Epoch5_Duration;
            search_duration = search_end_time-search_start_time;
        else
            search_duration = session_data{sess}.trial_data.Epoch4_Duration;
        end
    else
        search_duration = session_data{sess}.trial_data.Epoch4_Duration;
    end
    rewarded = strcmpi(session_data{sess}.trial_data.isRewarded,'true');
    
    block = session_data{sess}.trial_data.Block;
    block = block + 1;%index starts at 0 in C++
    
    
    for b =first_last_block_to_anlayze(sess,1):first_last_block_to_anlayze(sess,2)
        
        blockind = find(block == b);
        if isempty(blockind)
            continue
        elseif length(blockind) < 30
            continue
        end
        blockrewarded = rewarded(blockind(1)-10:blockind(end));
        blocksearchdur = search_duration(blockind(1)-10:blockind(end));
        

        blocksearchdur(blocksearchdur >= 2.5) = NaN;%timed out or not paying attention
        blocksearchdur(blocksearchdur <= 0) = NaN;
        
        
        if length(blockrewarded) > 40
            all_block_performance(all_block_ind,1:40) = blockrewarded(1:40);
            all_block_search_duration(all_block_ind,1:40) = blocksearchdur(1:40);
        else
            all_block_performance(all_block_ind,1:length(blockrewarded)) = blockrewarded;
            all_block_search_duration(all_block_ind,1:length(blocksearchdur)) = blocksearchdur;
        end
        
        all_block_ind = all_block_ind + 1;
    end
    
end
% Remove extra NaNs
all_block_performance = laundry(all_block_performance);
all_block_search_duration = laundry(all_block_search_duration);
% Calculate Average Performance and Search Duration
total_num_blocks = size(all_block_performance,1);
num_not_nans = sum(~isnan(all_block_performance));

average_performance = nansum(all_block_performance)./num_not_nans;
average_search_duration = nansum(all_block_search_duration)./num_not_nans;

%% Smooth performance 

smoothed_all_block_peform = NaN(size(all_block_performance));
for b = 1:total_num_blocks
    smoothed_all_block_peform(b,:) = FilterLearningCurves(all_block_performance(b,:)',3,1);
end

smoothing_window = 20;
for col = 1:size(smoothed_all_block_peform,2)
    coldat = smoothed_all_block_peform(:,col);
    coldat  = [coldat(smoothing_window:-1:1); coldat ; coldat(end:-1:end-(smoothing_window-1))];
    coldat  = filtfilt(1/smoothing_window*ones(1,smoothing_window),1,coldat );
    coldat  = coldat ((smoothing_window+1):end-smoothing_window);
    smoothed_all_block_peform(:,col) =  coldat;
end
%%
figure
subplot(2,2,1)
plot(-9:30,100*average_performance)
hold on
plot(-9:30,100*nanmean(smoothed_all_block_peform))
plot([0.5 0.5],[0 100],'k--')
plot([-10 30],[80 80],'r--')
plot([-10 30],[33 33],'k--')
plot([-10 30],[66 66],'k--')
hold off
ylim([0 100])
xlabel('Trial # Relative To Switch')
ylabel('Performance %')
legend('Raw','Smoothed')
box off
title('Average Performance')

prct2_5 = prctile(smoothed_all_block_peform(:),2.5);
prct97_5 = prctile(smoothed_all_block_peform(:),97.5);

subplot(2,2,3)
hold on
plot(100*mean(smoothed_all_block_peform(:,1:9),2))
plot(100*mean(smoothed_all_block_peform(:,31:40),2))
plot(100*mean(smoothed_all_block_peform(:,21:29),2))
plot(100*mean(smoothed_all_block_peform(:,11:19),2))

hold off
xlabel('Block #')
xlim([0 total_num_blocks+1])
ylim([25 100])
ylabel('Average Performance (% Correct)')
legend({'Last 10 Trials','Trials 20-30','Trials 11-20','1^{st} 10 Trials'},'location','NorthWest')
box off
title('Smoothed Performance by Block/Session')

subplot(2,2,[2 4])
imagesc(-9:30,1:total_num_blocks,smoothed_all_block_peform)
hold on
plot([0.5 0.5],[0 total_num_blocks],'w--')
hold off
xlabel('Trial # Relative To Switch')
ylabel('Block#')
box off
title('Smoothed Performance by Block/Session')
caxis([prct2_5 prct97_5])
colormap('viridis')
colorbar

subtitle([monkey_name ', n_{sessions} = ' num2str(num_sessions) ', n_{blocks} = ' num2str(total_num_blocks)])