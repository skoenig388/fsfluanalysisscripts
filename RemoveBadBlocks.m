function [goodblocks,block_avg_peformance] = RemoveBadBlocks(session_data,...
    miniumum_performance,max_block_cut,min_block_len,max_time_out)
%Code written by Seth Konig 5/8/19
%adopted from other places

%Inputs:
% 1) session_data: all the session data
% 2) miniumum_performance: minimump proportion correct to consider as cutoff
% 3) max_block_cut: the maximum # of blocks that can be cut from start and end of sesssion

%Outputs:
% 1) block_avg_peformance: average proportion correct by block
% 2) goodblocks: blocks with acceptable performance

block = session_data.trial_data.Block;
block = block + 1;%index starts at 0 in C++
num_blocks = max(block);
block_avg_peformance = NaN(1,num_blocks);
rewarded = strcmpi(session_data.trial_data.isRewarded,'true');

%---Remove Blocks Based on Performance----%
for b =1:max(block)
    blockind = find(block == b);
    if isempty(blockind)
        continue
    elseif length(blockind) < min_block_len
        continue
    end
    blockrewarded = rewarded(blockind(1):blockind(end));
    block_avg_peformance(b) = sum(blockrewarded)/length(blockrewarded);
end

%don't remove if average performance is bad or below criterion
%the nan should also remove blocks without enought data
if mean(block_avg_peformance) < miniumum_performance
    goodblocks = find(~isnan(block_avg_peformance));
    goodblocks(goodblocks == 1) = [];%don't want the 1st block
else
    bad_blocks =  find(block_avg_peformance < miniumum_performance);
    if isempty(bad_blocks)
        goodblocks = find(~isnan(block_avg_peformance));
        %goodblocks(goodblocks == 1) = [];%don't want the 1st block
    else
        gapped_bad_blocks = findgaps(bad_blocks);
        
        if bad_blocks(1) == 1 || bad_blocks(end) == num_blocks%bad blocks at start and finish
            if bad_blocks(1) == 1
                one_gaps = gapped_bad_blocks(1,:);
                one_gaps(one_gaps == 0) = [];
                first_block_to_analyze = one_gaps(end)+1;
                if first_block_to_analyze > max_block_cut
                    first_block_to_analyze = max_block_cut;
                end
            else
                first_block_to_analyze = 1;
                %first_block_to_analyze = 2; %ignore 1 anyay
            end
            if bad_blocks(end) == num_blocks
                last_gaps = gapped_bad_blocks(end,:);
                last_gaps(last_gaps == 0) = [];
                last_block_to_anlayze = last_gaps(1)-1;
                if last_block_to_anlayze < num_blocks - max_block_cut
                    last_block_to_anlayze = num_blocks - max_block_cut;
                end
            else
                last_block_to_anlayze = num_blocks; %last block
            end
            goodblocks = find(~isnan(block_avg_peformance));
            goodblocks(goodblocks > last_block_to_anlayze) = [];
            goodblocks(goodblocks < first_block_to_analyze) = [];
        else %only bad blocks are those in the middle
            goodblocks = find(~isnan(block_avg_peformance));
            %goodblocks(goodblocks == 1) = [];%don't want the 1st block
        end
    end
end

%---Remove Blocks based on Long Breaks---%
%should mostly apply to kiosk monkeys and last blocks in general
%looks for performance that is worse following a break otherwise we ignore
%the breaks for now
inter_trial_duration = diff(session_data.trial_data.Epoch0_StartTimeAbsolute);
if any(inter_trial_duration > max_time_out)
    long_breaks = find(inter_trial_duration > max_time_out);
    long_breaks_trial_in_block = session_data.trial_data.TrialInBlock(long_breaks);
    block_breaks = block(long_breaks);
    unique_block_breaks = unique(block_breaks);
    blocks_to_remove = [];
    for bb = 1:length(unique_block_breaks)
        blockind = find(block == unique_block_breaks(bb));
        blockrewarded = rewarded(blockind(1):blockind(end));
        these_breaks = long_breaks_trial_in_block(block_breaks == unique_block_breaks(bb));
        if length(these_breaks) > 2
            blocks_to_remove = [blocks_to_remove unique_block_breaks(bb)];%% too many breaks to want to analyze
        else
            if length(these_breaks) == 1
                if these_breaks > 5 && these_breaks < length(blockrewarded) - 5
                    performance_before_break = blockrewarded(1:these_breaks-1);
                    performance_after_break = blockrewarded(these_breaks+1:end);
                    if mean(performance_after_break(1:5))-mean(performance_before_break) < -0.2
                        blocks_to_remove = [blocks_to_remove unique_block_breaks(bb)];%% too many breaks to want to analyze
                    else
                        continue
                    end
                else
                    continue
                end
            else
                for brk = 1:length(these_breaks)
                    if these_breaks(brk) <  5 && these_breaks(brk) < length(blockrewarded) - 5
                        performance_before_break = blockrewarded(1:these_breaks(brk)-1);
                        performance_after_break = blockrewarded(these_breaks(brk)+1:end);
                        if mean(performance_after_break(1:5))-mean(performance_before_break) < -0.2
                            blocks_to_remove = [blocks_to_remove unique_block_breaks(bb)];%% too many breaks to want to analyze
                        else
                            continue
                        end
                    else
                        continue
                    end
                end
            end
        end
    end
    if ~isempty(blocks_to_remove)
        disp('removing bad blocks due to long breaks in time')
        blocks_to_remove = unique(blocks_to_remove);
        for badblock = 1:length(blocks_to_remove)
            goodblocks(goodblocks == blocks_to_remove(badblock)) = [];
        end
    end
end

end