%Scripts to Process Feature Search (FS) training
%written by Seth Konig 10/25/18

clear,clc,close all%fclose all

% session_names = {'FS__Frey__28_09_2018__11_48_39','FS__Frey__29_09_2018__13_06_28','FS__Frey__30_09_2018__12_50_58',...
%     'FS__Frey__02_10_2018__12_47_44','FS__Frey__03_10_2018__11_19_12','FS__Frey__04_10_2018__11_35_17',...
%     'FS__Frey__05_10_2018__11_08_26','FS__Frey__06_10_2018__14_27_20','FS__Frey__07_10_2018__13_28_08',...
%     'FS__Frey__08_10_2018__12_13_00','FS__Frey__09_10_2018__10_43_50','FS__Frey__10_10_2018__13_16_37',...
%     'FS__Wotan__03_10_2018__12_04_19','FS__Wotan__04_10_2018__11_06_59','FS__Wotan__05_10_2018__12_02_31',...
%     'FS__Wotan__06_10_2018__15_07_21','FS__Wotan__07_10_2018__14_06_28','FS__Wotan__08_10_2018__12_51_05',...
%     'FS__Wotan__09_10_2018__11_25_49','FS__Wotan__10_10_2018__14_00_28','FS__Wotan__11_10_2018__11_39_37',...
%     'FS__Wotan__12_10_2018__13_07_31','FS__Wotan__13_10_2018__10_21_41','FS__Wotan__14_10_2018__15_32_48'};
% session_type = {'Shape1','Shape2','Shape3',...
%     'Arm1','Arm2','Arm3',...
%     'Pattern1','Pattern2','Pattern3',...
%     'Color1','Color2','Color3',...
%     'Shape1','Shape2','Shape3',...
%     'Arm3','Arm2','Arm1',...
%     'Pattern1','Pattern2','Pattern3',...
%     'Color1','Color2','Color3'};

% session_names = {'FS__Igor__22_01_2019__08_58_42','FS__Igor__23_01_2019__08_37_46','FS__Igor__24_01_2019__09_12_33',...
%     'FS__Igor__25_01_2019__11_29_51','FS__Igor__28_01_2019__08_56_57','FS__Igor__29_01_2019__10_08_58',...
%     'FS__Igor__30_01_2019__08_25_17','FS__Igor__31_01_2019__09_13_49','FS__Igor__01_02_2019__08_56_55',...
%     'FS__Igor__04_02_2019__09_09_18','FS__Igor__05_02_2019__09_06_41','FS__Igor__06_02_2019__12_59_50'};
%
% session_type = {'Shape1','Shape2','Shape3',...
%     'Arm3','Arm2','Arm1',...
%     'Pattern1','Pattern2','Pattern3',...
%     'Color1','Color2','Color3'};

% session_names = {'FS__Reider__25_01_2019__10_02_37','FS__Reider__28_01_2019__10_16_56','FS__Reider__29_01_2019__11_08_40',...
%     'FS__Reider__30_01_2019__09_30_06','FS__Reider__31_01_2019__10_05_38','FS__Reider__01_02_2019__14_41_14',...
%     'FS__Reider__04_02_2019__10_02_09','FS__Reider__05_02_2019__10_04_19','FS__Reider__06_02_2019__08_59_15',...
%     'FS__Reider__07_02_2019__09_08_07','FS__Reider__08_02_2019__09_53_14'};
%
% session_type = {'Shape1','Shape2','Shape3',...
%     'Arm3','Arm2','Arm1',...
%     'Pattern1','Pattern2','Pattern3',...
%     'Color1','Color2'};

% session_names = {'FS__Igor__22_01_2019__08_58_42','FS__Igor__23_01_2019__08_37_46','FS__Igor__24_01_2019__09_12_33',...
%     'FS__Igor__25_01_2019__11_29_51','FS__Igor__28_01_2019__08_56_57','FS__Igor__29_01_2019__10_08_58',...
%     'FS__Igor__30_01_2019__08_25_17','FS__Igor__31_01_2019__09_13_49','FS__Igor__01_02_2019__08_56_55',...
%     'FS__Igor__04_02_2019__09_09_18','FS__Igor__05_02_2019__09_06_41','FS__Igor__06_02_2019__12_59_50',...
%     'FS__Reider__25_01_2019__10_02_37','FS__Reider__28_01_2019__10_16_56','FS__Reider__29_01_2019__11_08_40',...
%     'FS__Reider__30_01_2019__09_30_06','FS__Reider__31_01_2019__10_05_38','FS__Reider__01_02_2019__14_41_14',...
%     'FS__Reider__04_02_2019__10_02_09','FS__Reider__05_02_2019__10_04_19','FS__Reider__06_02_2019__08_59_15',...
%     'FS__Reider__07_02_2019__09_08_07','FS__Reider__08_02_2019__09_53_14'};
%
% session_type = {'Shape1','Shape2','Shape3',...
%     'Arm3','Arm2','Arm1',...
%     'Pattern1','Pattern2','Pattern3',...
%     'Color1','Color2','Color3',...
%     'Shape1','Shape2','Shape3',...
%      'Arm3','Arm2','Arm1',...
%     'Pattern1','Pattern2','Pattern3',...
%     'Color1','Color2','Color3'};


session_names = {'FS__Bard__28_01_2019__10_08_14','FS__Bard__29_01_2019__09_00_35','FS__Bard__30_01_2019__08_19_22',...
    'FS__Bard__31_01_2019__10_12_38','FS__Bard__01_02_2019__09_02_15','FS__Bard__04_02_2019__09_14_44',...
    'FS__Bard__05_02_2019__09_15_14','FS__Bard__06_02_2019__09_05_38','FS__Bard__07_02_2019__08_37_30',...
    'FS__Bard__08_02_2019__10_07_40','FS__Bard__11_02_2019__08_56_11','FS__Bard__12_02_2019__09_01_53',...
    'FS__Sindri__04_02_2019__10_25_17','FS__Sindri__05_02_2019__10_11_34','FS__Sindri__06_02_2019__10_06_41',...
    'FS__Sindri__07_02_2019__09_27_55','FS__Sindri__08_02_2019__08_59_53','FS__Sindri__11_02_2019__09_57_48',...
    'FS__Sindri__12_02_2019__10_08_19','FS__Sindri__13_02_2019__09_07_12','FS__Sindri__14_02_2019__10_50_06',...
    'FS__Sindri__15_02_2019__10_25_05','FS__Sindri__18_02_2019__10_07_07','FS__Sindri__19_02_2019__12_28_31',...
    'FS__Igor__22_01_2019__08_58_42','FS__Igor__23_01_2019__08_37_46','FS__Igor__24_01_2019__09_12_33',...
    'FS__Igor__25_01_2019__11_29_51','FS__Igor__28_01_2019__08_56_57','FS__Igor__29_01_2019__10_08_58',...
    'FS__Igor__30_01_2019__08_25_17','FS__Igor__31_01_2019__09_13_49','FS__Igor__01_02_2019__08_56_55',...
    'FS__Igor__04_02_2019__09_09_18','FS__Igor__05_02_2019__09_06_41','FS__Igor__06_02_2019__12_59_50',...
    'FS__Reider__25_01_2019__10_02_37','FS__Reider__28_01_2019__10_16_56','FS__Reider__29_01_2019__11_08_40',...
    'FS__Reider__30_01_2019__09_30_06','FS__Reider__31_01_2019__10_05_38','FS__Reider__01_02_2019__14_41_14',...
    'FS__Reider__04_02_2019__10_02_09','FS__Reider__05_02_2019__10_04_19','FS__Reider__06_02_2019__08_59_15',...
    'FS__Reider__07_02_2019__09_08_07','FS__Reider__08_02_2019__09_53_14','FS__Reider__11_02_2019__10_10_17'...
    'FS__Frey__28_09_2018__11_48_39','FS__Frey__29_09_2018__13_06_28','FS__Frey__30_09_2018__12_50_58',...
    'FS__Frey__02_10_2018__12_47_44','FS__Frey__03_10_2018__11_19_12','FS__Frey__04_10_2018__11_35_17',...
    'FS__Frey__05_10_2018__11_08_26','FS__Frey__06_10_2018__14_27_20','FS__Frey__07_10_2018__13_28_08',...
    'FS__Frey__08_10_2018__12_13_00','FS__Frey__09_10_2018__10_43_50','FS__Frey__10_10_2018__13_16_37',...
    'FS__Wotan__03_10_2018__12_04_19','FS__Wotan__04_10_2018__11_06_59','FS__Wotan__05_10_2018__12_02_31',...
    'FS__Wotan__06_10_2018__15_07_21','FS__Wotan__07_10_2018__14_06_28','FS__Wotan__08_10_2018__12_51_05',...
    'FS__Wotan__09_10_2018__11_25_49','FS__Wotan__10_10_2018__14_00_28','FS__Wotan__11_10_2018__11_39_37',...
    'FS__Wotan__12_10_2018__13_07_31','FS__Wotan__13_10_2018__10_21_41','FS__Wotan__14_10_2018__15_32_48'
    };

session_type = {'Shape1','Shape2','Shape3',...
    'Arm3','Arm2','Arm1',...
    'Pattern1','Pattern2','Pattern3',...
    'Color1','Color2','Color3',...
    'Shape1','Shape2','Shape3',...
    'Arm3','Arm2','Arm1',...
    'Pattern1','Pattern2','Pattern3',...
    'Color1','Color2','Color3',...
    'Shape1','Shape2','Shape3',...
    'Arm3','Arm2','Arm1',...
    'Pattern1','Pattern2','Pattern3',...
    'Color1','Color2','Color3',...
    'Shape1','Shape2','Shape3',...
    'Arm3','Arm2','Arm1',...
    'Pattern1','Pattern2','Pattern3',...
    'Color1','Color2','Color3',...
    'Shape1','Shape2','Shape3',...
    'Arm1','Arm2','Arm3',...
    'Pattern1','Pattern2','Pattern3',...
    'Color1','Color2','Color3',...
    'Shape1','Shape2','Shape3',...
    'Arm3','Arm2','Arm1',...
    'Pattern1','Pattern2','Pattern3',...
    'Color1','Color2','Color3',...
    };

session_data = cell(1,length(session_names));
which_monkey = NaN(1,length(session_names));
for sn = 1:length(session_names)
    session_data{sn} = ImportFLU_Data(session_names{sn});
    
    if contains(session_names{sn},'Bard')
        which_monkey(sn) = 1;
    elseif contains(session_names{sn},'Frey')
        which_monkey(sn) = 2;
    elseif contains(session_names{sn},'Igor')
        which_monkey(sn) = 3;
    elseif contains(session_names{sn},'Reider')
        which_monkey(sn) = 4;
    elseif contains(session_names{sn},'Sindri')
        which_monkey(sn) = 5;
    elseif contains(session_names{sn},'Wotan')
        which_monkey(sn) = 6;
    else
        error('Name Not found')
    end
end

%%
trial_count = NaN(1,length(session_names));
session_performance = cell(1,length(session_names));
for sn = 1:length(session_names)
    
    trial_count(sn) = size(session_data{sn}.trial_data,1);
    
    search_duration = session_data{sn}.trial_data.Epoch4_Duration;
    sd = search_duration;
    sd = [sd(10:-1:1); sd; sd(end:-1:end-9)];
    sd = filtfilt(1/10*ones(1,10),1,sd);
    sd = sd(10:end-10);
    
    session_performance{sn}.search_duration = search_duration;
    session_performance{sn}.smoothed_search_duration = sd;
    
    trial_duration = session_data{sn}.trial_data.TrialTime;
    td = trial_duration;
    td = [td(10:-1:1); td; td(end:-1:end-9)];
    td = filtfilt(1/10*ones(1,10),1,td);
    td = td(10:end-10);
    
    session_performance{sn}.trial_duration = trial_duration;
    session_performance{sn}.smoothed_trial_duration = td;
    
    rewarded = strcmpi(session_data{sn}.trial_data.isRewarded,'true');
    rwd = 100*rewarded;
    rwd = [rwd(10:-1:1); rwd; rwd(end:-1:end-9)];
    rwd = filtfilt(1/10*ones(1,10),1,rwd);
    rwd = rwd(10:end-10);
    
    session_performance{sn}.rewarded = rewarded;
    session_performance{sn}.smoothed_rewarded = rwd;
    
end

%%
performance_by_trial_number = NaN(length(session_names),floor(median(trial_count)));
search_dur_by_trial_number = NaN(length(session_names),floor(median(trial_count)));
last_block_all_search_durs = NaN(length(session_names),405);
last_block_time_between_trials = NaN(length(session_names),405);
all_blocks_time_to_start_trial = NaN(length(session_names),floor(median(trial_count)));
for sn = 1:length(session_names)
    
    block_nums = session_data{sn}.trial_data.Block;%index starts at 0 in C++
    num_trials_in_last_block = sum(block_nums == 5);
    if num_trials_in_last_block > 400 %restarted Igor on last block for some demo
        num_trials_in_last_block = 400;
    end
    perform = session_performance{sn}.search_duration(block_nums == 5);
    perform = perform(1:num_trials_in_last_block);
    last_block_all_search_durs(sn,1:num_trials_in_last_block) = perform;
    
    tbt = session_data{sn}.trial_data.Epoch0_StartTimeAbsolute(block_nums == 5);
    tbt = tbt(1:num_trials_in_last_block);
    last_block_time_between_trials(sn,1:num_trials_in_last_block) = tbt;
    
    if trial_count(sn) >=  floor(median(trial_count))
        performance_by_trial_number(sn,1:end) = session_performance{sn}.rewarded(1:floor(median(trial_count)));
        search_dur_by_trial_number(sn,1:end) = session_performance{sn}.search_duration(1:floor(median(trial_count)));
        
        if ~isempty(find(contains(session_data{sn}.trial_data.Properties.VariableNames,'CentralCueSelectionDuration')))
            all_blocks_time_to_start_trial(sn,1:end) =  session_data{sn}.trial_data.Epoch1_Duration(1:floor(median(trial_count)))...
                - session_data{sn}.trial_data.CentralCueSelectionDuration(1:floor(median(trial_count)));
        else %old names
            all_blocks_time_to_start_trial(sn,1:end) =  session_data{sn}.trial_data.Epoch1_Duration(1:floor(median(trial_count)))...
                -session_data{sn}.trial_data.fixationDuration(1:floor(median(trial_count)));
        end
    else
        performance_by_trial_number(sn,1:trial_count(sn)) = session_performance{sn}.rewarded;
        search_dur_by_trial_number(sn,1:trial_count(sn)) = session_performance{sn}.search_duration;
        all_blocks_time_to_start_trial(sn,1:trial_count(sn)) =  session_data{sn}.trial_data.Epoch1_Duration....
            -session_data{sn}.trial_data.fixationDuration;
    end
end
%%
all_blocks_time_to_start_trial(:,1) = [];%1st trial doesn't count
all_blocks_time_to_start_trial = laundry(all_blocks_time_to_start_trial);

last_block_all_search_durs = laundry(last_block_all_search_durs);
last_block_all_search_durs(last_block_all_search_durs < 0.4) = NaN;%%these are usually multiple selections

last_block_time_between_trials = laundry(last_block_time_between_trials);

time_outs = last_block_all_search_durs;
time_outs = (time_outs >= 5);
time_outs = sum(time_outs);

figure
subplot(2,2,1)
plot(1:length(time_outs),time_outs/size(last_block_all_search_durs,1));
xlim([0 length(time_outs)])
xlabel('Trial #')
ylabel('Proportion of Time Outs')
title('Time outs in Last Block (12D)')
box off

lbasd = nanmean(last_block_all_search_durs);
lbasd = [lbasd(10:-1:1) lbasd lbasd(end:-1:end-9)];
lbasd = filtfilt(1/10*ones(1,10),1,lbasd);
lbasd = lbasd(10:end-10);

[r,p] = corrcoef(1:length(time_outs),nanmean(last_block_all_search_durs));
P = polyfit(1:length(time_outs),nanmean(last_block_all_search_durs),1);
subplot(2,2,2)
plot(lbasd);
xlabel('Trial #')
ylabel('Average Search Duration (ms)')
title(sprintf(['Average Search Duration in Last Block (12D) \n' ...
    'r = ' num2str(r(2),2) ', p = ' num2str(p(2),2) ', m = ' num2str(1000*P(1),2) ' ms/trial']))
xlim([0 length(time_outs)])
box off

lbtbt = nanmean(diff(last_block_time_between_trials,[],2));
lbtbt = [lbtbt(10:-1:1) lbtbt lbtbt(end:-1:end-9)];
lbtbt = filtfilt(1/10*ones(1,10),1,lbtbt);
lbtbt = lbtbt(10:end-10);

[r,p] = corrcoef(1:length(lbtbt)-1,nanmean(diff(last_block_time_between_trials,[],2)));
P = polyfit(1:length(lbtbt)-1,nanmean(diff(last_block_time_between_trials,[],2)),1);
subplot(2,2,3)
plot(lbtbt);
xlabel('Trial #')
ylabel('Average Time Between Trials (ms)')
title(sprintf(['Average Time  between trials in Last Block (12D) \n' ...
    'r = ' num2str(r(2),2) ', p = ' num2str(p(2),2) ', m = ' num2str(1000*P(1),2) ' ms/trial']))
xlim([0 length(lbtbt)])
box off


abtst = nanmean(all_blocks_time_to_start_trial);
abtst = [abtst(10:-1:1) abtst abtst(end:-1:end-9)];
abtst = filtfilt(1/10*ones(1,10),1,abtst);
abtst = abtst(10:end-10);

[r,p] = corrcoef(1:length(all_blocks_time_to_start_trial),nanmean(all_blocks_time_to_start_trial));
P = polyfit(1:length(all_blocks_time_to_start_trial),nanmean(all_blocks_time_to_start_trial),1);
subplot(2,2,4)
plot(abtst);
xlabel('Trial #')
ylabel('Average Time to Touch Blue Square')
title(sprintf(['Average Time to Touch Blue Square All blocks \n' ...
    'r = ' num2str(r(2),2) ', p = ' num2str(p(2),2) ', m = ' num2str(1000*P(1),2) ' ms/trial']))
xlim([0 length(abtst)])
box off

%%

perform = nanmean(performance_by_trial_number);
perform = [perform(10:-1:1) perform perform(end:-1:end-9)];
perform = filtfilt(1/10*ones(1,10),1,perform);
perform = perform(10:end-10);


search = nanmean(search_dur_by_trial_number);
search = [search(10:-1:1) search search(end:-1:end-9)];
search = filtfilt(1/10*ones(1,10),1,search);
search = search(10:end-10);

figure
subplot(1,2,1)
plot(100*perform);
xlabel('Trial #')
ylabel('Performance %')
title('Smoothed Performance Across Sessions')
box off
axis square

subplot(1,2,2)
plot(search);
xlabel('Trial #')
ylabel('Search Duration (seconds)')
title('Smoothed Search Duration Across Sessions')
box off
axis square

%%
num_distractors = [0 1 3 5 9 12];
dims = {'Shape','Pattern','Color','Arm'};
average_performance1D =  NaN(length(dims),length(num_distractors));
average_search_duration1D =  NaN(length(dims),length(num_distractors));
for d = 1:length(dims)
    these_sets = find(contains(session_type,dims{d}));
    
    performance =  NaN(length(these_sets),length(num_distractors));
    search_duration = NaN(length(these_sets),length(num_distractors));
    
    
    for set = 1:length(these_sets)
        block_nums = session_data{these_sets(set)}.trial_data.Block;%index starts at 0 in C++
        for b = 1:max(block_nums)+1
            if contains(session_data{sn}.block_def(b).BlockID,'D0')
                block_ind = 1;
            elseif  contains(session_data{these_sets(set)}.block_def(b).BlockID,'D12')
                block_ind = 6;
            elseif  contains(session_data{these_sets(set)}.block_def(b).BlockID,'D1')
                block_ind = 2;
            elseif  contains(session_data{these_sets(set)}.block_def(b).BlockID,'D3')
                block_ind = 3;
            elseif  contains(session_data{these_sets(set)}.block_def(b).BlockID,'D5')
                block_ind = 4;
            elseif  contains(session_data{these_sets(set)}.block_def(b).BlockID,'D9')
                block_ind = 5;
                
            else
                disp('BlockID not recognized')
            end
            
            if block_ind ~= b
                disp('block # mismatch for some reason')
            end
            
            these_trials =  find(session_data{these_sets(set)}.trial_data.Block == b-1);
            
            rewarded = session_performance{these_sets(set)}.rewarded(these_trials);
            
            if mean(rewarded) > 0.8
                search_durs = session_performance{these_sets(set)}.search_duration(these_trials);
                search_durs = search_durs(rewarded == 1);
                search_duration(set,block_ind) = mean(search_durs);
            end
            performance(set,block_ind) = mean(rewarded);
        end
    end
    average_performance1D(d,:)  = nanmean(performance);
    average_search_duration1D(d,:) =  nanmean(search_duration);
end
%% Divide Search Duration effect of # Distractors by Monkey
average_search_duration_monkey = cell(1,max(which_monkey));
for monk = 1:max(which_monkey)
    these_sets = find(which_monkey == monk);
    
    search_duration = NaN(length(these_sets),length(num_distractors),4);
    
    for set = 1:length(these_sets)
        block_nums = session_data{these_sets(set)}.trial_data.Block;%index starts at 0 in C++
        this_dim = find(contains(dims,session_type{these_sets(set)}(1:end-1)));
        for b = 1:max(block_nums)+1
            if contains(session_data{sn}.block_def(b).BlockID,'D0')
                block_ind = 1;
            elseif  contains(session_data{these_sets(set)}.block_def(b).BlockID,'D12')
                block_ind = 6;
            elseif  contains(session_data{these_sets(set)}.block_def(b).BlockID,'D1')
                block_ind = 2;
            elseif  contains(session_data{these_sets(set)}.block_def(b).BlockID,'D3')
                block_ind = 3;
            elseif  contains(session_data{these_sets(set)}.block_def(b).BlockID,'D5')
                block_ind = 4;
            elseif  contains(session_data{these_sets(set)}.block_def(b).BlockID,'D9')
                block_ind = 5;
            else
                disp('BlockID not recognized')
            end
            
            if block_ind ~= b
                disp('block # mismatch for some reason')
            end
            
            these_trials =  find(session_data{these_sets(set)}.trial_data.Block == b-1);
            
            rewarded = session_performance{these_sets(set)}.rewarded(these_trials);
            
            if mean(rewarded) > 0.8
                search_durs = session_performance{these_sets(set)}.search_duration(these_trials);
                search_durs = search_durs(rewarded == 1);
                search_duration(set,block_ind,this_dim) = mean(search_durs);
            end
        end
    end
    average_search_duration_monkey{monk} = search_duration;
end

slope_by_monkey = NaN(4,max(which_monkey));
for monk = 1:max(which_monkey)
    monkey_dim_search_dur = NaN(4,6);
    for dim = 1:4
        monkey_dim_search_dur = nanmean(average_search_duration_monkey{monk}(:,:,dim));
        p = polyfit(num_distractors,monkey_dim_search_dur,1);
        slope_by_monkey(dim,monk) = p(1);
    end
end

%%
figure
subplot(2,2,1)
bar(100*average_performance1D')
xticks(1:length(num_distractors))
xticklabels(num2cell(num_distractors))
xlabel('# of Distractors')
ylabel('Peformance %')
box off
%legend(dims)
ylim([30 100])
title('Search Duration: Dimension vs. # Distractors')

subplot(2,2,2)
bar(average_search_duration1D')
xticks(1:length(num_distractors))
xticklabels(num2cell(num_distractors))
xlabel('# of Distractors')
ylabel('Search Duration (seconds)')
box off
legend(dims)
yl = ylim;
ylim([0.5 yl(2)])
title('Search Duration: Dimension vs. # Distractors')

subplot(2,2,3)
bar(nanmean(average_search_duration1D))
xticks(1:length(num_distractors))
xticklabels(num2cell(num_distractors))
xlabel('# of Distractors')
ylabel('Search Duration (seconds)')
box off
title('Average Search Duration by # Distractors')
ylim([0.5 1])


average_slope = NaN(1,length(dims));
for d = 1:length(dims)
    p = polyfit(num_distractors,average_search_duration1D(d,:),1);
    average_slope(d) = p(1);
end

subplot(2,2,4)
bar(1000*average_slope)
xticks(1:length(dims))
xticklabels(dims)
xlabel('Dimension')
ylabel('Slope (Search Duration (ms)/EDistractor)')
title('Slope: Effect of # of Distractors')
box off

save('1DFSTraining','average_performance1D','average_search_duration1D');

%% Plot Average Search Duration by Dimension Across Multiple Monkeys

figure
bar(1000*slope_by_monkey)
box off
xticks(1:4);
xticklabels(dims)
xlabel('Dimension')
ylabel('Slope (Search Duration (ms)/#Distractor)')
title('Slope: Effect of # of Distractors by Monkey')
legend('Bard','Frey','Igor','Reider','Sindri','Wotan','Location','NortheastOutside')