%Draft Scripts to do basic parsing of output of FLU task with primary
%purpose for debugging
%Seth Koenig 9/10/18
%Updated SDK 2/11/19 to handle trial by trial stored data

clar

interewarddur = 0.2;
sess_file_name = 'VDS__Wotan__12_02_2019__12_02_22';
log_dir = 'Z:\MonkeyData\Wotan\VDM\';

%% Read in Stored Frame, Trial and Serial Data
frame_dir = [log_dir sess_file_name '\RuntimeData\FrameData\'];
frame_name = '__FrameData';

trial_dir = [log_dir sess_file_name '\RuntimeData\TrialData\'];
trial_name = '__TrialData.txt';
trialdef_name = '__trialdef_on_trial_';

serial_dir =[log_dir sess_file_name '\RuntimeData\UnitySerialData\'];
serial_Recv_name = '__SerialRecv';
serial_Send_name = '__SerialSent';

gaze_data_dir = [log_dir sess_file_name '\RuntimeData\PythonData\GazeData\' sess_file_name];
gaze_data_name = '__GazeData__Trial_';
%% Load Config Variables
config_dir = [log_dir sess_file_name '\RuntimeData\ConfigCopy\'];

block_json = ReadJsonFile([config_dir sess_file_name '__block_defs.json']);
list = dir(config_dir);
for l = 1:length(list)
    if list(l).isdir
        continue
    else
        if contains(list(l).name,'BlockDef') && contains(list(l).name,'.txt')
            %still need to read in data correctly for this but got block_json for now
            blockData =  ReadBlockText([config_dir list(l).name]);
        elseif contains(list(l).name,'ExptParameters')
            ExptParamsData = ReadExperimentalParameters([config_dir list(l).name]);
            num_epochs = length(ExptParamsData.TrialSequenceEpochs);
        elseif contains(list(l).name,'config') && contains(list(l).name,'session')
            configdata = ReadFLUConfigFile([config_dir list(l).name]);
        end
    end
end

%% Read in Trial Data
trial_data = readtable([trial_dir sess_file_name trial_name]);
if trial_data.Epoch0_Duration(end) == -1
    trial_data(end,:) = [];
end
num_trials = max(trial_data.TrialInExperiment);
%% Read in Frame Data

tic
all_frame_data = cell(1,num_trials);
if length(ls(serial_dir)) < 4
    all_frame_data = readtable([frame_dir sess_file_name frame_name]);
else
    for t = 1:num_trials
        frame_data = readtable([frame_dir sess_file_name frame_name '__Trial_' num2str(t) '.txt']);
        if ~isempty(frame_data)
            all_frame_data{t+1} = frame_data;
        end
    end
end
empts = find(cellfun(@isempty,all_frame_data));
disp([num2str(length(empts)) ' frame data files have no data'])
all_frame_data(empts) = [];
frame_data = [];
for t = 1:length(all_frame_data)
    frame_data = [frame_data; all_frame_data{t}];
end
num_frames = frame_data.Frame(end);
toc

%% Read in Serial Receive Data
tic
all_serial_Recv_data = cell(1,num_trials);
if length(ls(serial_dir)) < 5
    error('No complete method for integrating single serial file yet')
    serial_Recv_data = readtable([serial_dir sess_file_name serial_Recv_name]);
    serial_Recv_data = ParseNeurarduinoData3(serial_Recv_data);
else
    for t = 1:num_trials
        serial_Recv_data = readtable([serial_dir sess_file_name serial_Recv_name '__Trial_' num2str(t) '.txt']);
        if ~isempty(serial_Recv_data)
            all_serial_Recv_data{t+1} = ParseNeurarduinoData3(serial_Recv_data);
        else
            %some older version of FLU task save data only on ever 2-3 trials
            disp(['No data found in ' sess_file_name serial_Recv_name '__Trial_' num2str(t) '.txt'])
        end
    end
end

empts = find(cellfun(@isempty,all_serial_Recv_data));
disp([num2str(length(empts)) ' serial data files have no data'])
all_serial_Recv_data(empts) = [];

serial_JoyAndPhotoDetails = [];
serial_event = [];
for asr = 1:length(all_serial_Recv_data)
    serial_JoyAndPhotoDetails = [serial_JoyAndPhotoDetails; all_serial_Recv_data{asr}.JoyAndPhotoDetails];
    if size(all_serial_Recv_data{asr}.EventCodeDetails,2) > 1
        serial_event = [serial_event; all_serial_Recv_data{asr}.EventCodeDetails];
    end
end
toc


%serial_send_data = readtable([serial_dir sess_file_name serial_Send_name]);
%serial_send_data = ParseNeurarduinoData3(serial_send_data);
%% Read in TrialDefs
tic
num_blocks = trial_data.Block(end)+1;
TrialDefs = cell(1,num_trials);
for td = 1:num_trials
    if exist([trial_dir sess_file_name trialdef_name num2str(td) '.json'])
        TrialDefs{td} = ReadJsonFile([trial_dir sess_file_name trialdef_name num2str(td) '.json']);
    else
        disp(['Missing Trial Def for ' num2str(td)]);
    end
end
toc
%% Read  Item and Condition Files...if Available
if isfield(block_json,'TrialDefPath')
    ItmData = cell(1,num_blocks);
    CndData = cell(1,num_blocks);
    for b = 1:num_blocks
        if ~isempty(block_json(b).TrialDefPath)
            [ItmData{b},CndData{b}] = ReadItmCndText(block_json(b).TrialDefPath);
        end
    end
end

%% Process Frame/Time Data from Arudino/Serial Data file
Frames = serial_JoyAndPhotoDetails.FrameReceived;
lastFrame = max(Frames);
UnityTime = serial_JoyAndPhotoDetails.FrameStart;
ArduinoTime = serial_JoyAndPhotoDetails.ArduinoTimestamp;

frame_start_time = NaN(2,lastFrame);%use first time stamp per frame
for f = 1:lastFrame
    frameindex = find(Frames == f);
    if isempty(frameindex)
        continue
    end
    frameindex = frameindex(1);
    frame_start_time(1,f) = UnityTime(frameindex);
    frame_start_time(2,f) = ArduinoTime(frameindex);
end

nanind = find(isnan(frame_start_time(1,:)) | isnan(frame_start_time(2,:)));
firstnan = find(diff(nanind) > 1);
nanind(1:firstnan(1)-1) = [];

%% Determine ~ When Block Transition Occured
trial_start_frame = trial_data.Epoch0_StartFrame;
Blocknum = trial_data.Block;
changed_blocks = find(diff(Blocknum) ~=0);
if isempty(changed_blocks)
    changed_blocks = length(Blocknum)-1;
end

%%
disp(['There are ' num2str(sum(diff(frame_data.Frame) ~= 1)) ' missing Frames!'])
%% Plot Basic Frame Data Plots
figure
p1 = plot(frame_data.Frame(1:end-1),diff(frame_data.Frame));
hold on
yl = ylim;
for b = 1:length(changed_blocks)
    p2 = plot([trial_start_frame(changed_blocks(b)+1) trial_start_frame(changed_blocks(b)+1)],[yl(1) yl(2)],'r');
end
p3 = plot(trial_start_frame(changed_blocks),0.75,'g*');
hold off
xlabel('Frame #')
ylabel('Difference in Frame #')
legend([p1,p2,p3(1)],'Data','Block Transition','ITI Start')
box off
title('Unity Frame From Frame Data')

diffs = diff(frame_data.FrameStartUnity);
%diffs(diffs > 0.1) = 0.016;%useful for looking at certain things
figure
plot(frame_data.Frame(1:end-1),diffs)
hold on
yl = ylim;
for b = 1:length(changed_blocks)
    plot([trial_start_frame(changed_blocks(b)+1) trial_start_frame(changed_blocks(b)+1)],[yl(1) yl(2)],'r')
end
plot(trial_start_frame(changed_blocks),0.075,'g*')
hold off
xlabel('Frame #')
ylabel('Difference in Frame Start in Unity Time (sec)')
box off
title('Unity Time From Frame Data')

figure
plot(frame_data.Frame(1:end-1),diff(frame_data.FrameStartSystem/10^7))
hold on
yl = ylim;
for b = 1:length(changed_blocks)
    plot([trial_start_frame(changed_blocks(b)+1) trial_start_frame(changed_blocks(b)+1)],[yl(1) yl(2)],'r')
end
plot(trial_start_frame(changed_blocks),0.075,'g*')
hold off
xlabel('Frame #')
ylabel('Difference in Frame Start in System Time (sec)')
box off
title('System Time From Frame Data')

figure
p1 = plot(1:lastFrame-1,diff(frame_start_time(1,:)));
hold on
p2 = plot(1:lastFrame-1,diff(frame_start_time(2,:)));
p3 = plot(nanind,100,'r*');
p4 = plot(trial_start_frame(changed_blocks),75,'g*');
hold off
box off
xlabel('Frame #')
ylabel('Time between Frames (ms)')
legend('Unity','Arduino')
legend([p1, p2, p3(1), p4(1)],'Unity','Arduino')
title('Data From Serial Recv')

%%
figure
plot(diff(frame_data.EyetrackerTimeStamp/10^3))
box off
xlabel('Frame #')
ylabel('Time between Samples (ms)')
title('Eye Tracker Time Stamp Data From Frame Data')

%% Plot Basic Frame Duration Estimations on From Every File On Same pLot
figure
plot(frame_data.Frame(1:end-1),filtfilt(1/7*ones(1,7),1,1000*diff(frame_data.FrameStartUnity)))
hold on
plot(frame_data.Frame(1:end-1),filtfilt(1/7*ones(1,7),1,1000*diff(frame_data.FrameStartSystem/10^7)))
dat = diff(frame_start_time(1,:));
dat(isnan(dat)) = nanmean(dat);
plot(1:lastFrame-1,filtfilt(1/7*ones(1,7),1,dat));
dat = diff(frame_start_time(2,:));
dat(isnan(dat)) = nanmean(dat);
plot(1:lastFrame-1,filtfilt(1/7*ones(1,7),1,dat))
hold off
xlabel('Frame #')
ylabel('Difference in Frame Start (ms)')
legend('UnityTime FrameData','SystemTime FrameData','UnityTime SerialRecv','AruduinoTime SerialRecv')
box off
title('All timing data')
%% Read in Eye tracking data saved by Python at 600 Hz
if strcmpi(configdata.eyeTrackType,'2');
    all_pythonEye_data_time = cell(1,num_trials);
    all_pythonEye_data_XY = cell(1,num_trials);
    percent_binocular = NaN(1,num_trials);
    average_participant_distance = NaN(1,num_trials);
    for t = 1:num_trials
        try
            temp_data = readtable([gaze_data_dir gaze_data_name num2str(t) '.txt']);
        catch
            disp(['File Missing for trial #' num2str(t)])
            continue
        end
        
        all_pythonEye_data_XY{t} = [temp_data.left_ADCS_validity  temp_data.right_ADCS_validity  ...
            temp_data.left_ADCS_x temp_data.left_ADCS_y ...
            temp_data.right_ADCS_x temp_data.right_ADCS_y ...
            temp_data.left_pupil_diameter temp_data.right_pupil_diameter];
        
        try
            percent_binocular(t) = sum(~isnan(temp_data.left_ADCS_x) &  ~isnan(temp_data.right_ADCS_x))/...
                sum(~isnan(temp_data.left_ADCS_x) |  ~isnan(temp_data.right_ADCS_x));
        catch
            disp('Cant get participant eye data')
        end
        all_pythonEye_data_time{t} = [temp_data.system_time_stamp temp_data.device_time_stamp];
        try
            average_participant_distance(t) = nanmean(temp_data.left_origin_UCS_z+temp_data.right_origin_UCS_z)/2;
        catch
            disp('Cant get participant distance')
        end
    end
    % Plot EYe Tracker Time stamps
    all_python_time = [];
    for t = 1:num_trials
        all_python_time = [all_python_time; all_pythonEye_data_time{t}];
    end
    figure
    plot(diff(all_python_time/10^3));
    xlabel('Sample #')
    box off
    ylabel('Time between Samples (ms)')
    title('Eye Tracker Time Stamps')
end
%% Plot Eye &/or touch position
if any(contains(frame_data.Properties.VariableNames,'EyePositionX'))
    
    x = frame_data.EyePositionX;
    y = frame_data.EyePositionY;
    
    figure
    plot(frame_data.FrameStartUnity,x-1920)
    hold on
    plot(frame_data.FrameStartUnity,y)
    hold off
    xlabel('Time (ms)')
    ylabel('Position (px)')
    box off
    legend('EyeX','EyeY')
end

if any(contains(frame_data.Properties.VariableNames,'TouchX'))
    Tx = frame_data.TouchX;
    Tx(Tx == -99999) = NaN;
    Tx = Tx-1920;
    
    Ty = frame_data.TouchX;
    Ty(Ty == -99999) = NaN;
    
    figure
    plot(frame_data.FrameStartUnity,Tx)
    hold on
    plot(frame_data.FrameStartUnity,Ty)
    hold off
    xlabel('Time (ms)')
    ylabel('Position (px)')
    box off
    legend('TouchX','TouchY')
end

%% PLot Trial Durations

figure
subplot(2,2,1)
hist(trial_data.TrialTime,25);
xlabel('Trial Duration (sec)')
ylabel('Trail Count')
box off
title(['Mean Trial Duration: ' num2str(mean(trial_data.TrialTime),2) ' seconds'])

subplot(2,2,2)
plot(trial_data.TrialTime)
xlabel('Trial #')
ylabel('Trial Duration (sec)')
title('Trial Duration by Trial')
box off


end_search_time = trial_data.TimeofQuaddleSelected;
end_search_time(end_search_time == -99999) = NaN;

subplot(2,2,3)
hist(end_search_time-trial_data.Epoch4_StartTimeAbsolute,25);
xlabel('Search Duration (sec)')
ylabel('Trail Count')
box off
title(['Mean Search Duration: ' num2str(nanmean(end_search_time-trial_data.Epoch4_StartTimeAbsolute),2) ' seconds'])

subplot(2,2,4)
plot(end_search_time-trial_data.Epoch4_StartTimeAbsolute)
xlabel('Trial #')
ylabel('Search Duration (sec)')
box off
title('Search Duration by Trial')




%% Check lagging issues & other timing issues According to Trial Data

num_trials = size(trial_data,1);
Epoch_StartFrame = NaN(num_trials,num_epochs);
Epoch_StartTime =  NaN(num_trials,num_epochs);
Epoch_StartAbsTime = NaN(num_trials,num_epochs);
Epoch_Durations = NaN(num_trials,num_epochs);

Epoch_names = ExptParamsData.TrialSequenceEpochs;
selected_epoch_duration_index = NaN(1,num_epochs);

selected_duration_name = trial_data.Properties.VariableNames(44:end);
for epoch = 1:num_epochs
    Epoch_StartFrame(:,epoch) = getfield(trial_data,['Epoch' num2str(epoch-1) '_StartFrame']);
    Epoch_StartTime(:,epoch) = getfield(trial_data,['Epoch' num2str(epoch-1) '_StartTimeRelative']);
    Epoch_StartAbsTime(:,epoch) = getfield(trial_data,['Epoch' num2str(epoch-1) '_StartTimeAbsolute']);
    Epoch_Durations(:,epoch) = getfield(trial_data,['Epoch' num2str(epoch-1) '_Duration']);
    
    if strcmpi(Epoch_names(epoch),'ITI')
        selected_epoch_duration_index(epoch) = find(contains(trial_data.Properties.VariableNames,'itiDuration'));
    elseif strcmpi(Epoch_names(epoch),'Blink')
        selected_epoch_duration_index(epoch) = find(contains(trial_data.Properties.VariableNames,'blinkOnDuration'));
    elseif strcmpi(Epoch_names(epoch),'Fixation')
        selected_epoch_duration_index(epoch) = find(contains(trial_data.Properties.VariableNames,'CentralCueSelectionDuration'));
    elseif contains(Epoch_names(epoch),'Baseline')
        selected_epoch_duration_index(epoch) = find(contains(trial_data.Properties.VariableNames,'baselineDuration'));
    elseif strcmpi(Epoch_names(epoch),'CovertPrep')
        selected_epoch_duration_index(epoch) = find(contains(trial_data.Properties.VariableNames,'covertPrepDurat'));
    elseif strcmpi(Epoch_names(epoch),'freeGaze')
        selected_epoch_duration_index(epoch) = find(contains(trial_data.Properties.VariableNames,'freeGazeDuration'));
    elseif strcmpi(Epoch_names(epoch),'SelectObject')
        selected_epoch_duration_index(epoch) = find(contains(trial_data.Properties.VariableNames,'goDuration'));
    elseif strcmpi(Epoch_names(epoch),'ChoiceToFB')
        selected_epoch_duration_index(epoch) = find(contains(trial_data.Properties.VariableNames,'ObjectSelectionDuration'));
    elseif strcmpi(Epoch_names(epoch),'Feedback')
        selected_epoch_duration_index(epoch) = find(contains(trial_data.Properties.VariableNames,'fBDuration'));
    end
end

%% Plot Epoch Durations and Times
figure
subplot(2,3,1)
plot(Epoch_StartTime)
box off
yl = ylim;
ylim([-0.1 yl(2)])
xlim([0 num_trials])
xlabel('Trial #')
ylabel('Relative Start Time (s)')
legend(Epoch_names,'Location','NorthEastOutside')
title('Relative Start time of Each Epoch')

subplot(2,3,2)
plot(Epoch_Durations)
box off
xlabel('Trial #')
ylabel('Duration (s)')
xlim([0 num_trials])
title('Duration of Each Epoch')

subplot(2,3,4)
plot(Epoch_StartFrame-Epoch_StartFrame(:,1)*ones(1,num_epochs))
box off
xlabel('Trial #')
ylabel('# of Frames')
xlim([0 num_trials])
yl = ylim;
ylim([-0.1 yl(2)])
title('Relative Start frame of Each Epoch')

subplot(2,3,5)
plot(diff(Epoch_StartFrame')')
box off
xlabel('Trial #')
ylabel('# of Frames')
xlim([0 num_trials])
yl = ylim;
ylim([-0.1 yl(2)])
title('Relative # of Frames in Each Epoch')

subplot(2,3,3)
hist(Epoch_StartAbsTime(2:end,1)-(Epoch_StartAbsTime(1:end-1,end)+Epoch_Durations(1:end-1,end)))
xlabel('Time (sec)')
ylabel('Trial Count')
box off
title('Amount of Time Between Trials')
%% Plot Epoch Durations and Times vs. Selected Durations
figure
for epoch = 1:num_epochs
    if strcmpi(Epoch_names(epoch),'Reward')
        timediff = Epoch_Durations(:,epoch)-interewarddur*(block_json(1).RuleArray.RewardMag-1);
    else
        timediff = Epoch_Durations(:,epoch)-trial_data.(trial_data.Properties.VariableNames{selected_epoch_duration_index(epoch)});
    end
    
    subplot(3,3,epoch)
    hist(1000*timediff)
    xlabel('Observed-Selected (ms)')
    ylabel('Trial Count')
    title(Epoch_names(epoch))
    box off
end
%% Plot Stored Config Data Values And Extra values at end of trial data
figure
for col = selected_epoch_duration_index(1):length(trial_data.Properties.VariableNames)
    subplot(4,4,col-selected_epoch_duration_index(1)+1)
    plot(table2array(trial_data(:,col)))
    xlabel('Trial #')
    %ylabel('Unit?')
    yl = ylim;
    if yl(1) < 0
        ylim([0 yl(2)])
    end
    box off
    title(trial_data.Properties.VariableNames{col})
    
end

%% Compare Trial Data, Frame Data, and Serial Data
frame_trial_data = [];
frame_trial_data.Epoch_StartFrame = NaN(num_trials,num_epochs);
frame_trial_data.Epoch_StartTime =  NaN(num_trials,num_epochs);
frame_trial_data.Epoch_StartAbsTime = NaN(num_trials,num_epochs);
frame_trial_data.Epoch_Durations = NaN(num_trials,num_epochs);

for t = 1:num_trials
    trialindex = find(frame_data.TrialInExperiment == t);
    
    this_trial_dat = frame_data(trialindex,:);
    
    for epoch = 1:num_epochs
        epoch_index = find(this_trial_dat.TrialEpoch == epoch-1);
        
        if length(epoch_index) < 3
            continue
        end
        
        frame_trial_data.Epoch_StartFrame(t,epoch) = this_trial_dat.Frame(epoch_index(1));
        frame_trial_data.Epoch_StartTime(t,epoch) = this_trial_dat.FrameStartUnity(epoch_index(1)+1);
        frame_trial_data.Epoch_StartAbsTime(t,epoch) = this_trial_dat.FrameStartSystem(epoch_index(1));
        frame_trial_data.Epoch_Durations(t,epoch) = this_trial_dat.FrameStartUnity(epoch_index(end))-this_trial_dat.FrameStartUnity(epoch_index(1));
    end
end
%%
framediff = frame_trial_data.Epoch_StartFrame-Epoch_StartFrame;
figure
subplot(2,2,1)
imAlpha=ones(size(framediff));
imAlpha(isnan(framediff))=0;
imagesc(framediff,'AlphaData',imAlpha);
set(gca,'color',[1 1 1]);
xlabel('Epoch #')
ylabel('Trial #')
colorbar
%caxis([-250 15])
box off
title('Difference in Frame Start From FrameData to TrialData')

timediff = frame_trial_data.Epoch_StartTime-Epoch_StartAbsTime;
subplot(2,2,2)
imAlpha=ones(size(timediff));
imAlpha(isnan(timediff))=0;
imagesc(timediff,'AlphaData',imAlpha);
set(gca,'color',[1 1 1]);
xlabel('Epoch #')
ylabel('Trial #')
colorbar
%caxis([-4 0.2])
box off
title('Difference in Time (sec) Start From FrameData to TrialData')

durationdiff = frame_trial_data.Epoch_Durations-Epoch_Durations;
subplot(2,2,3)
imAlpha=ones(size(durationdiff));
imAlpha(isnan(durationdiff))=0;
imagesc(durationdiff,'AlphaData',imAlpha);
set(gca,'color',[1 1 1]);
xlabel('Epoch #')
ylabel('Trial #')
colorbar
%caxis([-0.2 6])
box off
title('Difference in Time (sec) Start From FrameData to TrialData')
%% Plot ITI triggered Average Frame Duration
twin = 50;
frame_dur_around_iti_start = NaN(num_trials,2*twin+1);

framediff = diff(frame_data.FrameStartUnity);
frame_num = frame_data.Frame(1:end-1);
for i = 2:num_trials
    frame_num_index = find(frame_data.Frame ==  frame_trial_data.Epoch_StartFrame(i,1));
    frame_dur_around_iti_start(i,:) = framediff(frame_num_index-twin:frame_num_index+twin);
end

figure
plot(-twin:twin,1000*nanmean(frame_dur_around_iti_start))
yl = ylim;
hold on
plot([0 0],[yl(1) yl(2)])
hold off
box off
xlabel('Frame from ITI start')
ylabel('Average Frame Duration (ms)')
title('Average Frame Duration Around the Start of the ITI')

%% Look at timing of frames across different files

figure
%subplot(1,2,1)
plot(1000*(diff(frame_data.FrameStartSystem)/10^7-diff(frame_data.FrameStartUnity)));
xlabel('Frame #')
ylabel('System-Unity Frame Duration (ms)')
box off
xlim([0 num_frames])
title('System Time vs Unity Time')
yl = ylim;
ylim([-32 yl(2)])

% subplot(1,2,2)
% plot(filtfilt(1/7*ones(1,7),1,1000*(diff(frame_data.FrameStartSystem)/10^7-diff(frame_data.FrameStartUnity))));
% xlabel('Frame #')
% ylabel('System-Unity Frame Duration (ms)')
% box off
% xlim([0 num_frames])
% title('Smoothed(7 Frames): System Time vs Unity Time')
% yl = ylim;
% ylim([-10 yl(2)])


%%
frameFrame_serialFrame = NaN(1,num_frames);
frameFrame_trialFrame = NaN(1,num_frames);

for f = 1:num_frames
    
    frameindex = find(frame_data.Frame == f);
    if isempty(frameindex)
        continue
    end
    
    serialindex = find(serial_JoyAndPhotoDetails.FrameReceived == f);
    if ~isempty(serialindex)
        serialtime = serial_JoyAndPhotoDetails.FrameStart(serialindex(1))/1000;
        frameFrame_serialFrame(f) = frame_data.FrameStartUnity(frameindex)- serialtime;
    end
    trialindex = find(Epoch_StartFrame == f);
    if ~isempty(trialindex)
        frameFrame_trialFrame(f) = frame_data.FrameStartUnity(frameindex)-Epoch_StartAbsTime(trialindex);
    end
end
frameFrame_serialFrame(isnan(frameFrame_serialFrame)) = [];
frameFrame_trialFrame(isnan(frameFrame_trialFrame)) = [];

figure
subplot(1,2,1)
hist(frameFrame_serialFrame)
box off
xlabel('Difference in Frame Time (ms)')
ylabel('Frame Count')
box off
title('Serial vs Frame Data')

subplot(1,2,2)
hist(frameFrame_trialFrame)
box off
xlabel('Difference in Frame Time (ms)')
ylabel('Frame Count')
box off
title('Trial vs Frame Data')

%% Check TrialDefs vs BlockDefs.TrialDefs vs TrialData vs ITM/CND file

for t = 1:num_trials
    
    blk = trial_data.Block(t);
    TrlBlock = trial_data.TrialInBlock(t);
    trialcode = trial_data.TrialCode(t);
    trialcode = trialcode-1000*(blk+1);
    contextname  = trial_data.ContextName(t);
    contextnum = trial_data.ContextNum(t);
    
    this_trial = TrialDefs{t};
    
    block_trial = block_json(blk+1).TrialDefs(trialcode);
    if ~isequaln(this_trial,block_trial)
        disp('TrialDefs in Block and  TrialDefs in TrialData are not equal')
        disp(['Block# ' num2str(blk) ', Trial #' num2str(trialcode)])
    end
end
%%
for t = 1:num_trials
    
    blk = trial_data.Block(t);
    TrlBlock = trial_data.TrialInBlock(t);
    trialcode = trial_data.TrialCode(t);
    contextname  = trial_data.ContextName(t);
    contextnum = trial_data.ContextNum(t);
    
    this_trial = TrialDefs{t};
    block_trial = block_json(blk+1).TrialDefs(trialcode);
    
    this_CND = CndData{blk+1}(trialcode,:);
    these_items = cell(1,size(this_CND,2)-4);
    for stim = 5:size(this_CND,2)
        these_items{stim-4} = ItmData{blk+1}(this_CND{1,stim},:);
        if stim == 1
            if  ~contains(this_trial.relevantObjects(stim-4).StimID,'rel')
                disp('Why is object not relevant')
            end
        end
        this_trial.relevantObjects(stim-4).StimID = [];  %sometimes field is not set
        block_trial.relevantObjects(stim-4).StimID = []; %sometimes field is not set
    end
    
    objects = this_trial.relevantObjects;
    for stim = 1:length(these_items)
        
        if objects(stim).StimName ~=  these_items{stim}.StimName{1}
            disp('Stimulus Name is not the same')
            disp(['Stim #' num2str(stim)])
        end
        
        if objects(stim).StimPath ~=  these_items{stim}.StimPath{1}
            disp('Stimulus Path is not the same')
            disp(['Stim #' num2str(stim)])
        end
        
        if objects(stim).StimCode ~=  these_items{stim}.StimCode
            disp('Stimulus Code is not the same')
            disp(['Stim #' num2str(stim)])
        end
        
        if objects(stim).StimTrialRewardMag ~=  these_items{stim}.StimTrialRewardMag
            disp('Stimulus RewardMag is not the same')
            disp(['Stim #' num2str(stim)])
        end
        
        
        if objects(stim).StimTrialRewardProb ~=  these_items{stim}.StimTrialRewardProb
            disp('Stimulus RewardProb is not the same')
            disp(['Stim #' num2str(stim)])
        end
        
        if strcmpi(these_items{stim}.isRelevant{1},'true')
            val = true;
        else
            val = false;
        end
        if objects(stim).isRelevant ~=  val
            disp('Stimulus Relevance is not the same')
            disp(['Stim #' num2str(stim)])
        end
        
        if (objects(stim).StimLocation.x ~= these_items{stim}.StimLocationX) || ...
                (objects(stim).StimLocation.y ~=  these_items{stim}.StimLocationY) || ...
                (objects(stim).StimLocation.z ~=  these_items{stim}.StimLocationZ)
            disp('Stimulus Location is not the same')
            disp(['Stim #' num2str(stim)])
        end
        
        if (objects(stim).StimRotation.x ~=  these_items{stim}.StimRotationX) || ...
                (objects(stim).StimRotation.y ~=  these_items{stim}.StimRotationY) || ...
                (objects(stim).StimRotation.z ~=  these_items{stim}.StimRotationZ)
            disp('Stimulus Location is not the same')
            disp(['Stim #' num2str(stim)])
        end
        
        for i = 1:5
            if objects(stim).StimDimVals ~= these_items{stim}.(['StimDimVals' num2str(i)])
                disp('Stimulus Value is not the same')
                disp(['Stim #' num2str(stim)])
            end
        end
    end
    
    
    if ~strcmpi(this_trial.TrialName,block_trial.TrialName) || (this_trial.TrialCode ~= block_trial.TrialCode) ...
            || (this_trial.ContextNum ~= block_trial.ContextNum) || ~strcmpi(this_trial.ContextName,block_trial.ContextName) ...
            || ~isequal(this_trial,block_trial)
        disp('TrialDefs in Block and  TrialDefs in TrialData are not equal')
        disp(['Block# ' num2str(blk) ', Trial #' num2str(trialcode)])
    end
end
%% Plot X & Y of Touch Position

mean_touch = NaN(2,num_trials);
item_location = NaN(2,num_trials);
item_locationXY = NaN(2,num_trials);
feedback_epoch = find(contains(Epoch_names,'ChoiceToFB'))-1;
for t = 1:num_trials
    trialindex = find(frame_data.TrialInExperiment == t);
    
    this_trial_dat = frame_data(trialindex,:);
    selection = trial_data.TimeofQuaddleSelected(t);
    selection_index = find(this_trial_dat.FrameStartUnity == selection);
    feedback_index = find(this_trial_dat.TrialEpoch == feedback_epoch);
    
    
    if any(contains(frame_data.Properties.VariableNames,'TouchX'))
        touchonX = this_trial_dat.TouchX(selection_index:feedback_index-1);
        touchonY = this_trial_dat.TouchY(selection_index:feedback_index-1);
        if any(touchonX == -99999)
            disp('Touch Invalid Duration Selection?')
        end
        if any(touchonY == -99999)
            disp('Touch Invalid Duration Selection?')
        end
        mean_touch(1,t) = touchonX(end)-1920;
        mean_touch(2,t) = touchonY(end);
    elseif  any(contains(frame_data.Properties.VariableNames,'EyePositionX'))
        touchonX = this_trial_dat.EyePositionX(selection_index:feedback_index-1);
        touchonY = this_trial_dat.EyePositionY(selection_index:feedback_index-1);
        if any(touchonX == -99999)
            disp('Touch Invalid Duration Selection?')
        end
        if any(touchonY == -99999)
            disp('Touch Invalid Duration Selection?')
        end
        mean_touch(1,t) = touchonX(end)-1920;
        mean_touch(2,t) = touchonY(end);
        
    end
    
    touched_item = this_trial_dat.TouchedObjectId{selection_index(1)+1};
    touched_item = str2double(erase(touched_item,'rel'));
    
    item_location(1,t) = TrialDefs{t}.relevantObjects(touched_item).StimLocation.x;
    item_location(2,t) = TrialDefs{t}.relevantObjects(touched_item).StimLocation.z;
    
    item_locationXY(1,t) = TrialDefs{t}.relevantObjects(touched_item).StimScreenLocation.x;
    item_locationXY(2,t) = TrialDefs{t}.relevantObjects(touched_item).StimScreenLocation.y;
end
%%
figure
subplot(2,2,1)
plot(item_location(1,:),item_location(2,:),'.k','MarkerSize',14)
hold on
plot(mean_touch(1,:)/1920*24-12,mean_touch(2,:)/1080*13-6.5,'*r');%estimated scaling
hold off
axis equal
box off
title('World Space')

subplot(2,2,2)
plot(item_locationXY(1,:),item_locationXY(2,:),'.k','MarkerSize',14)
hold on
plot(mean_touch(1,:),mean_touch(2,:),'*r');%estimated scaling
hold off
axis equal
box off
title('Screen Space')