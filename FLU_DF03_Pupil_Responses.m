%Written Seth Koenig 8/8/19

tic
clar

%data import options
pre_processed_data_dir = '\Processed_Data\'; %for pre-saved session data files
reimport_new = false;%will import session data that is already processsed
import_option = 'all';
max_time_out = 5*60;

%figure_dir = 'C:\Users\Seth-PC\Documents\MATLAB\CFS DC Figures\';
figure_dir = 'C:\Users\Wolmesdorf-Lab\Documents\MATLAB\CFS DC Figures\';

%average acceptable performance across blocks
miniumum_performance = 0.5;%set to 0 if you don't want to remove blocks
max_block_cut = 3;%of of 1st and last blocks to cut if performance is very bad
min_block_len = 30;%miniumum # of trials in a block
last_trial_window = 10;%window to look at before end of block

%---Wotan 0-2---%
monkeyname = 'Wotan';
sess_file_names = {'FLU__Wotan__13_08_2019__12_59_30','FLU__Wotan__14_08_2019__13_11_27',...
    'FLU__Wotan__15_08_2019__13_33_35'};

%% Import Session Data
num_sessions = length(sess_file_names);
session_data = cell(1,num_sessions);
which_monkey = NaN(1,num_sessions);
for sess =1:num_sessions
    which_monkey(sess) = DetermineWhichMonkey(sess_file_names{sess});
    
    disp(['Loading data from session #' num2str(sess) '/' num2str(num_sessions)])
    log_dir = FindDataDirectory(sess_file_names{sess});
    if ~isempty(log_dir)
        session_data_file_name = [log_dir sess_file_names{sess} ...
            pre_processed_data_dir sess_file_names{sess} '_session_data.mat'];
        if ~exist(session_data_file_name,'file') && ~reimport_new
            session_data{sess} = ImportFLU_DataV2(sess_file_names{sess},log_dir,import_option,true);
        else
            load(session_data_file_name,'all_session_data');
            session_data{sess} = all_session_data;
        end
    else
        error(['Cant Find Log Dir for '  sess_file_names{sess}])
    end
end

%%
trial_aligned_Python_eye_data = cell(1,num_sessions);
for sess = 1:num_sessions
    if ~isempty(session_data{sess}.gaze_data)
        trial_aligned_Python_eye_data{sess} = GetTrialAlignedPythonGazeData(session_data{sess});
    end
end
%% Calculate Averege Performance by Block and Remove "Bad" Blocks
all_good_blocks = cell(1,num_sessions);
all_block_performance = cell(1,num_sessions);
all_session_block_performance = NaN(num_sessions,25);
for sess = 1:num_sessions
    [all_good_blocks{sess},all_block_performance{sess}] = ...
        RemoveBadBlocks(session_data{sess},miniumum_performance,max_block_cut,min_block_len,max_time_out);
    all_session_block_performance(sess,1:length(all_block_performance{sess})) = ...
        all_block_performance{sess};
end

figure
subplot(1,2,1)
plot(nanmean(all_session_block_performance))
xlabel('Block #')
ylabel('Performance-Proportion Correct')
title('Average Performance by Block')
box off

subplot(1,2,2)
hist(all_session_block_performance(:),20)
xlabel('Performance-Proportion Correct')
ylabel('Block Count')
title('Distribution of Block Performance')
box off

subtitle([monkeyname ': Performance by Block #'])
box off
%% Calculate perofrmance across all blocks
all_block_performance = [];
all_trial_count = NaN(length(sess_file_names),25);
all_extra_trial_count = NaN(length(sess_file_names),25);
all_block_end_performance = NaN(length(sess_file_names),25);

for sess = 1:num_sessions
    [block_performance,trial_count,extra_trial_count,block_end_performance] = ...
        CalculateBlockPerformance(session_data{sess},all_good_blocks{sess},min_block_len,last_trial_window);
    
    all_block_performance = [all_block_performance; block_performance];
    all_trial_count(sess,1:length(trial_count)) = trial_count;
    all_extra_trial_count(sess,1:length(extra_trial_count)) = extra_trial_count;
    all_block_end_performance(sess,1:length(block_end_performance)) = block_end_performance;
end

figure
subplot(2,2,1)
hold on
plot([0.5 0.5],[0 100],'k--')
plot([-(last_trial_window-1) min_block_len],[80 80],'k--')
plot([-(last_trial_window-1) min_block_len],[33 33],'k--')
plot([-(last_trial_window-1) min_block_len],[66 66],'k--')
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_block_performance)',3,2),'r')
plot(-(last_trial_window-1):min_block_len,100*nanmean(all_block_performance))
hold off
ylim([0 100])
xlabel('Trial # Relative To Switch')
ylabel('Performance (% Correct)')
box off
title(sprintf(['Average Performance-All blocks \n' ...
    'Average_{20-30} = ' num2str(100*mean(nanmean(all_block_performance(:,end-9:end))),3) '%%, ' ...
    'Average_{Last 10} = ' num2str(100*mean(nanmean(all_block_performance(:,1:10))),3) '%%']));

subplot(2,2,2)
histogram(all_trial_count(:),30:60)
xlabel('Trials in Block')
ylabel('Block Count')
title('Total Trial Count per Block')
box off

subplot(2,2,3)
histogram(all_extra_trial_count(:),0:30)
xlabel('Extra Trials in Block')
ylabel('Block Count')
title('Extra Trial Count per block')
box off

subplot(2,2,4)
histogram(100*all_block_end_performance(:),-5:10:95)
xlabel('Performance (% Correct)')
ylabel('Block Count')
box off
title('Peformance at End of Block')

subtitle(monkeyname)
