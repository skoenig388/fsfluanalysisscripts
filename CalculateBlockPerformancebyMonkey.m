function [block_performance_by_monkey,all_search_duration_by_monkey] = ...
    CalculateBlockPerformancebyMonkey(session_data,goodblocks,min_block_len,...
    last_trial_window,which_monkeys)
%adapted from code else where, written 5/10/19

%Inputs:
% 1) session_data across all sessions
% 2) goodblocks: blocks with acceptable performance across all sessions
% 3) min_block_len: miniumum # of trials in a block
% 4) last_trial_window: # of trials to look at end of block
% 5) which_monkeys: which monkey;

%Outputs:
% 1) block_performance_by_monkey: block transition-aligned performance by monkey
% 2) all_search_duration_by_monkey: search duration by monkey
% *) Figures with performance by monkeys


if nargin < 2
    error('Not enough input arguments')
elseif nargin < 3
    min_block_len = 30;
    last_trial_window = 10;
elseif nargin < 4
    last_trial_window = 10;
end

if length(unique(which_monkeys)) == 1
    %only 1 monkey
    block_performance_by_monkey = [];
    all_search_duration_by_monkey = [];
    return
end

num_sessions = length(session_data);
num_monkeys = max(which_monkeys);%not unique because number order matters for naming

block_performance_by_monkey = cell(1,num_monkeys);
all_search_duration_by_monkey = cell(1,num_monkeys);

correct_n_minus_one_incorrect_search_duration_by_monkey = cell(1,num_monkeys);
correct_n_minus_one_correct_search_duration_by_monkey = cell(1,num_monkeys);
incorrect_n_minus_one_incorrect_search_duration_by_monkey = cell(1,num_monkeys);
incorrect_n_minus_one_correct_search_duration_by_monkey = cell(1,num_monkeys);
    
for sess = 1:num_sessions
    
    this_monkey = which_monkeys(sess);
    num_blocks = length(goodblocks{sess});
    
    block = session_data{sess}.trial_data.Block;
    block = block + 1;%index starts at 0 in C++
    rewarded = strcmpi(session_data{sess}.trial_data.isRewarded,'true');
    search_duration = Calculate_FLU_Search_Duration(session_data{sess});
    
    block_performance = NaN(num_blocks,min_block_len+last_trial_window);
    block_search_duration =  NaN(num_blocks,min_block_len+last_trial_window);

    correct_n_minus_one_incorrect_search_duration = NaN(num_blocks,min_block_len+last_trial_window);
    correct_n_minus_one_correct_search_duration = NaN(num_blocks,min_block_len+last_trial_window);
    incorrect_n_minus_one_incorrect_search_duration = NaN(num_blocks,min_block_len+last_trial_window);
    incorrect_n_minus_one_correct_search_duration = NaN(num_blocks,min_block_len+last_trial_window);
        
    for b = 1:num_blocks
        blockind = find(block == goodblocks{sess}(b));
        if isempty(blockind)
            error('why is block empty?')
        elseif length(blockind) < min_block_len %didn't finish
            error('why is block too short?')
        end
        
        %---Get Performance---%
        blockrewarded = rewarded(blockind(1):blockind(end));
        blockrewarded = [blockrewarded(end-(last_trial_window-1):end); blockrewarded];
        if length(blockrewarded) > min_block_len+last_trial_window
            block_performance(b,1:min_block_len+last_trial_window) = blockrewarded(1:min_block_len+last_trial_window);
        else
            block_performance(b,1:length(blockrewarded)) = blockrewarded;
        end
        
        %---Get Search Duration---%
        blocksearchdur = search_duration(blockind(1):blockind(end));
        blocksearchdur = [blocksearchdur(end-(last_trial_window-1):end); blocksearchdur];
        if length(blocksearchdur) > min_block_len+last_trial_window
            block_search_duration(b,1:min_block_len+last_trial_window) = blocksearchdur(1:min_block_len+last_trial_window);
        else
            block_search_duration(b,1:length(blocksearchdur)) = blocksearchdur;
        end
        
        %---Get Search Duration based on performance---%
        blockrewarded = rewarded(blockind(1):blockind(end));
        blockrewarded = [blockrewarded(end-(last_trial_window-1):end); blockrewarded];
        if blockind(1) == 1
            prior_block_trial_rewarded = false;
        else
            prior_block_trial_rewarded = rewarded(blockind(1)-1); %trial 1 should not exist since don't analyze block 1
        end
    
        blocksearchdur_corr1corr = blocksearchdur;
        blocksearchdur_corr1corr([~prior_block_trial_rewarded; ~blockrewarded(1:end-1)] & ~blockrewarded) = NaN;
        if length(blocksearchdur_corr1corr) > min_block_len+last_trial_window
            correct_n_minus_one_correct_search_duration(b,1:min_block_len+last_trial_window) = blocksearchdur_corr1corr(1:min_block_len+last_trial_window);
        else
            correct_n_minus_one_correct_search_duration(b,1:length(blocksearchdur_corr1corr)) = blocksearchdur_corr1corr;
        end
        
        blocksearchdur_corr1incorr = blocksearchdur;
        blocksearchdur_corr1incorr([prior_block_trial_rewarded; blockrewarded(1:end-1)] & ~blockrewarded) = NaN;
        if length(blocksearchdur_corr1incorr) > min_block_len+last_trial_window
            correct_n_minus_one_incorrect_search_duration(b,1:min_block_len+last_trial_window) = blocksearchdur_corr1incorr(1:min_block_len+last_trial_window);
        else
            correct_n_minus_one_incorrect_search_duration(b,1:length(blocksearchdur_corr1incorr)) = blocksearchdur_corr1incorr;
        end
        
        blocksearchdur_incorr1corr = blocksearchdur;
        blocksearchdur_incorr1corr([~prior_block_trial_rewarded; ~blockrewarded(1:end-1)] & blockrewarded) = NaN;
        if length(blocksearchdur_corr1corr) > min_block_len+last_trial_window
            incorrect_n_minus_one_correct_search_duration(b,1:min_block_len+last_trial_window) = blocksearchdur_incorr1corr(1:min_block_len+last_trial_window);
        else
            incorrect_n_minus_one_correct_search_duration(b,1:length(blocksearchdur_incorr1corr)) = blocksearchdur_incorr1corr;
        end
        
        blocksearchdur_incorr1incorr = blocksearchdur;
        blocksearchdur_incorr1incorr([prior_block_trial_rewarded; blockrewarded(1:end-1)] & blockrewarded) = NaN;
        if length(blocksearchdur_incorr1incorr) > min_block_len+last_trial_window
            incorrect_n_minus_one_incorrect_search_duration(b,1:min_block_len+last_trial_window) = blocksearchdur_incorr1incorr(1:min_block_len+last_trial_window);
        else
            incorrect_n_minus_one_incorrect_search_duration(b,1:length(blocksearchdur_incorr1incorr)) = blocksearchdur_incorr1incorr;
        end
        
    end
    block_performance_by_monkey{this_monkey} = [block_performance_by_monkey{this_monkey}; block_performance];
    all_search_duration_by_monkey{this_monkey} = [all_search_duration_by_monkey{this_monkey}; block_search_duration];
    
    correct_n_minus_one_incorrect_search_duration_by_monkey{this_monkey} = [correct_n_minus_one_incorrect_search_duration_by_monkey{this_monkey}; correct_n_minus_one_incorrect_search_duration];
    correct_n_minus_one_correct_search_duration_by_monkey{this_monkey} = [correct_n_minus_one_correct_search_duration_by_monkey{this_monkey}; correct_n_minus_one_correct_search_duration];
    incorrect_n_minus_one_incorrect_search_duration_by_monkey{this_monkey} = [incorrect_n_minus_one_incorrect_search_duration_by_monkey{this_monkey}; incorrect_n_minus_one_incorrect_search_duration];
    incorrect_n_minus_one_correct_search_duration_by_monkey{this_monkey} = [incorrect_n_minus_one_correct_search_duration_by_monkey{this_monkey}; incorrect_n_minus_one_correct_search_duration];
end
block_performance_by_monkey = laundry(block_performance_by_monkey);
all_search_duration_by_monkey = laundry(all_search_duration_by_monkey);

%----Calculate Mean and standard deviation by monkey so can Normalize---%
mean_search_durations = NaN(1,num_monkeys);
std_search_durations = NaN(1,num_monkeys);
for monk = 1:num_monkeys
    mean_search_durations(monk) = nanmean(all_search_duration_by_monkey{monk}(:));
    std_search_durations(monk) = nanstd(all_search_duration_by_monkey{monk}(:));
end

%---Make the Legend---%
name_str = {'Bard','Frey','Igor','Reider','Sindri','Wotan','All'};
legend_str = cell(1,length(name_str));
for name = 1:length(name_str)
    if name == length(name_str)
        legend_str{name} = [name_str{name} ', n = ' num2str(size(cell2mat(block_performance_by_monkey'),1))];
    else
        legend_str{name} = [name_str{name} ', n = ' num2str(size(block_performance_by_monkey{name},1))];
    end
end

%---Plot Learning Curves and Search Duration by Monkey---%
figure
subplot(2,2,1)
hold on
for monk = 1:num_monkeys
    if ~isempty(block_performance_by_monkey{monk})
        plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(block_performance_by_monkey{monk})',3,2));
    end
end
plot(-(last_trial_window-1):min_block_len,100*nanmean(cell2mat(block_performance_by_monkey')),'k','linewidth',4)
plot([0.5 0.5],[0 100],'k--')
plot([-10 30],[80 80],'r--')
plot([-10 30],[33 33],'k--')
plot([-10 30],[66 66],'k--')
hold off
ylim([0 100])
xlabel('Trial # Relative To Switch')
ylabel('Performance %')
box off
title('All Switches: Average Performance')
legend(legend_str,'Location','SouthEast')

subplot(2,2,2)
hold on
for monk = 1:num_monkeys
    if ~isempty(all_search_duration_by_monkey{monk})
        plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_search_duration_by_monkey{monk})',3,2));
    end
end
plot(-(last_trial_window-1):min_block_len,nanmean(cell2mat(all_search_duration_by_monkey')),'k','linewidth',4)
yl = ylim;
plot([0.5 0.5],[yl(1) yl(2)],'k--')
hold off
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (sec)')
box off
title('All Switches: Average Search Duration')

%---Plot Z-scored Search Duration---%
subplot(2,2,3)
hold on
for monk = 1:num_monkeys
    if ~isempty(all_search_duration_by_monkey{monk})
        this_search_duration = (all_search_duration_by_monkey{monk}-mean_search_durations(monk))./std_search_durations(monk);
        plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(this_search_duration)',3,2));
    end
end
this_search_duration = cell2mat(all_search_duration_by_monkey');
this_search_duration = (this_search_duration-nanmean(this_search_duration(:)))./nanstd(this_search_duration(:));
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(this_search_duration)',3,2),'k','linewidth',4);      
yl = ylim;
plot([0.5 0.5],[yl(1) yl(2)],'k--')
hold off
xlabel('Trial # Relative To Switch')
ylabel('Normalized Search Duration (sec)')
box off
title('All Switches: Z-Scored Search Duration')

subplot(2,2,4)
hold on
%all search durations
this_search_duration = cell2mat(all_search_duration_by_monkey');
this_search_duration = (this_search_duration-nanmean(this_search_duration(:)))./nanstd(this_search_duration(:));
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(this_search_duration)',3,2),'k','linewidth',4);

%all correct minus 1 correct search durations
all_norm_correct1correct = [];
for monk = 1:num_monkeys
    this_search_duration = (correct_n_minus_one_correct_search_duration_by_monkey{monk}-mean_search_durations(monk))./std_search_durations(monk);
    all_norm_correct1correct = [all_norm_correct1correct; this_search_duration];
end
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_norm_correct1correct)',3,2),'g');

%all correct minus 1 incorrect search durations
all_norm_correct1incorrect = [];
for monk = 1:num_monkeys
    this_search_duration = (correct_n_minus_one_incorrect_search_duration_by_monkey{monk}-mean_search_durations(monk))./std_search_durations(monk);
    all_norm_correct1incorrect = [all_norm_correct1incorrect; this_search_duration];
end
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_norm_correct1incorrect)',3,2),'b');

%all incorrect minus 1 correct search durations
all_norm_incorrect1correct = [];
for monk = 1:num_monkeys
    this_search_duration = (incorrect_n_minus_one_correct_search_duration_by_monkey{monk}-mean_search_durations(monk))./std_search_durations(monk);
    all_norm_incorrect1correct = [all_norm_incorrect1correct; this_search_duration];
end
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_norm_incorrect1correct)',3,2),'m');


%all incorrect minus 1 incorrect search durations
all_norm_incorrect1incorrect = [];
for monk = 1:num_monkeys
    this_search_duration = (incorrect_n_minus_one_incorrect_search_duration_by_monkey{monk}-mean_search_durations(monk))./std_search_durations(monk);
    all_norm_incorrect1incorrect = [all_norm_incorrect1incorrect; this_search_duration];
end
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_norm_incorrect1incorrect)',3,2),'r');

yl = ylim;
plot([0.5 0.5],[yl(1) yl(2)],'k--')
hold off
legend('All','Correct & N-1 Correct','Correct & N-1 Incorrect','Incorrect & N-1 Correct','Incorrect & N-1 Incorrect')
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (sec)')
box off
title('All Switches: Z-Scored Search Duration by Performance')

subtitle('Shift Learning Performance by Monkey')
end