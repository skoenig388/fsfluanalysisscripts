%Scripts to Process Feature Search (FS) training
%written by Seth Konig 10/25/18

clear,clc,close all%fclose all


%VDM/VDS
session_names = {'VDM__Wotan__25_09_2018__12_27_46','VDM__Wotan__26_09_2018__12_04_00'};
%session_names = {'VDM__Frey__18_09_2018__11_18_26','VDM__Frey__19_09_2018__11_07_11'};%missing 1st 7 trials
session_data = cell(1,length(session_names));

for sn = 1:length(session_names)
    session_data{sn} = ImportFLU_Data(session_names{sn});
end

%%
session_performance = cell(1,length(session_names));
for sn = 1:length(session_names)
    
    
    search_duration = session_data{sn}.trial_data.Epoch4_Duration;
    sd = search_duration;
    sd = [sd(10:-1:1); sd; sd(end:-1:end-9)];
    sd = filtfilt(1/10*ones(1,10),1,sd);
    sd = sd(10:end-10);
    
    session_performance{sn}.search_duration = search_duration;
    session_performance{sn}.smoothed_search_duration = sd;
    
    trial_duration = session_data{sn}.trial_data.TrialTime;
    td = trial_duration;
    td = [td(10:-1:1); td; td(end:-1:end-9)];
    td = filtfilt(1/10*ones(1,10),1,td);
    td = td(10:end-10);
    
    session_performance{sn}.search_duration = trial_duration;
    session_performance{sn}.smoothed_trial_duration = td;
    
    rewarded = strcmpi(session_data{sn}.trial_data.isRewarded,'true');
    rwd = 100*rewarded;
    rwd = [rwd(10:-1:1); rwd; rwd(end:-1:end-9)];
    rwd = filtfilt(1/10*ones(1,10),1,rwd);
    rwd = rwd(10:end-10);
    
    session_performance{sn}.rewarded = rewarded;
    session_performance{sn}.smoothed_rewarded = rwd;
    
end

%%

for sn = 1:length(session_names)
    

figure

subplot(1,3,1)
plot(session_performance{sn}.smoothed_trial_duration)
xlabel('Trial #')
ylabel('Duration (seconds)')
title('Smoothed Trial Duration')
box off
axis square

subplot(1,3,2)
plot(session_performance{sn}.smoothed_search_duration)
xlabel('Trial #')
ylabel('Duration (seconds)')
title('Smoothed Search Duration')
box off
axis square

subplot(1,3,3)
plot(session_performance{sn}.smoothed_rewarded)
xlabel('Trial #')
ylabel('Performance(%)')
title('Smoothed Performance')
box off
axis square

%supertitle(session_names{sn})
end
