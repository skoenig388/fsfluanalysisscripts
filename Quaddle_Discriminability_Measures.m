%Determine number of Times, Sessions, and Blocks a monkey experienced a
%Quaddle.
%Written by Seth Koenig 11/23/18
%Updated to newer code syntax (particularly importing) Seth Koenig 4/24/19

clar

%%

% %---FLU DF01: Discrimination Task---%
% sess_file_names = {...
%    'FLU__Wotan__30_10_2018__12_26_15','FLU__Wotan__31_10_2018__12_56_17',....
%    'FLU__Wotan__01_11_2018__13_00_01','FLU__Wotan__02_11_2018__13_29_23',...
%    'FLU__Wotan__03_11_2018__13_12_22',...
%    'FLU__Frey__27_10_2018__12_13_57','FLU__Frey__29_10_2018__11_25_38',...
%    'FLU__Frey__30_10_2018__11_42_22','FLU__Frey__31_10_2018__12_11_10',...
%    'FLU__Frey__01_11_2018__12_06_45',...
%    'FLU__Igor__22_02_2019__10_19_48','FLU__Igor__25_02_2019__09_10_13',...
%    'FLU__Igor__26_02_2019__11_21_40','FLU__Igor__27_02_2019__09_14_10',...
%    'FLU__Igor__28_02_2019__09_55_09',...
%    'FLU__Reider__27_02_2019__10_29_52','FLU__Reider__28_02_2019__10_54_16',...
%    'FLU__Reider__01_03_2019__09_40_58','FLU__Reider__04_03_2019__10_27_14',...
%    'FLU__Reider__05_03_2019__09_50_08',...
%    'FLU__Bard__28_02_2019__10_18_26','FLU__Bard__01_03_2019__09_43_10',...
%    'FS__Bard__04_03_2019__09_08_34','FLU__Bard__05_03_2019__10_39_09',...
%    'FLU__Bard__06_03_2019__08_51_15',...
%    'FLU__Sindri__07_03_2019__09_22_42','FLU__Sindri__08_03_2019__09_35_37',...
%    'FLU__Sindri__11_03_2019__08_23_48','FLU__Sindri__12_03_2019__08_04_20',...
%    'FLU__Sindri__13_03_2019__07_19_45',...
%    };

% %---For FLU 0 irrelevant features---%
% sess_file_names = {'FLU__Frey__15_02_2019__11_59_59','FLU__Frey__16_02_2019__12_31_20',...
%     'FLU__Frey__18_02_2019__11_09_46','FLU__Frey__25_02_2019__10_59_03','FLU__Frey__26_02_2019__11_09_32',...
%     'FLU__Frey__28_02_2019__10_31_32','FLU__Frey__01_03_2019__11_40_45','FLU__Frey__02_03_2019__12_50_48',...
%     'FLU__Frey__07_03_2019__11_00_10','FLU__Frey__08_03_2019__10_15_22','FLU__Frey__09_03_2019__10_41_20',...
%     'FLU__Frey__11_03_2019__11_39_47','FLU__Frey__12_03_2019__10_02_12','FLU__Frey__13_03_2019__10_21_17',...
%     'CFS__Wotan__20_03_2019__12_46_17','CFS__Wotan__21_03_2019__13_42_40','FLU__Wotan__22_03_2019__12_51_44',...
%     'FLU__Wotan__23_03_2019__15_57_44','FLU__Wotan__25_03_2019__12_20_35',...
%     'FLU__Sindri__27_03_2019__08_20_29',...
%     'FLU__Sindri__28_03_2019__08_06_39','FLU__Sindri__29_03_2019__09_05_46',...
%     'FLU__Sindri__04_04_2019__08_52_30',...
%     'FLU__Sindri__05_04_2019__10_06_16','FLU__Sindri__08_04_2019__10_14_16',...
%     'FLU__Sindri__10_04_2019__09_08_13','FLU__Sindri__10_04_2019__09_08_13',...
%     'FLU__Igor__29_03_2019__08_57_59','FLU__Igor__01_04_2019__09_00_46',...
%     'FLU__Igor__02_04_2019__08_21_16','FLU__Igor__03_04_2019__08_04_24',...
%     'FLU__Igor__05_04_2019__08_59_11','FLU__Igor__08_04_2019__09_14_38',...
%     'FLU__Reider__21_03_2019__11_15_49','FLU__Reider__27_03_2019__09_04_07',...
%     'FLU__Reider__28_03_2019__08_11_58','FLU__Reider__01_04_2019__10_31_48',...
%     'FLU__Reider__03_04_2019__09_48_55','FLU__Reider__04_04_2019__08_59_18',...
%     'FLU__Reider__05_04_2019__10_40_14','FLU__Reider__09_04_2019__11_33_04',...
%     'FLU__Bard__03_04_2019__10_09_40',...
%     'FLU__Bard__04_04_2019__10_40_09','FLU__Bard__05_04_2019__09_07_58',...
%     'FLU__Bard__09_04_2019__10_38_44',...
%     'FLU__Bard__11_04_2019__11_28_50','FLU__Bard__15_04_2019__09_55_12',...
%     'FLU__Bard__16_04_2019__09_39_53','FLU__Bard__17_04_2019__10_33_13',...
%     'FLU__Bard__18_04_2019__09_34_07','FLU__Bard__19_04_2019__09_12_27',...
%     };

%---1 irrelevant feature value---%
monkeyname = 'All 1 Irreleveant';
sess_file_names = {'FLU__Wotan__28_03_2019__12_38_16','FLU__Wotan__01_04_2019__13_18_43',...
    'FLU__Wotan__03_04_2019__13_00_04',...
    'FLU__Wotan__07_04_2019__12_33_56','FLU__Wotan__08_04_2019__13_05_27',...
    'FLU__Wotan__10_04_2019__12_47_14','FLU__Wotan__11_04_2019__13_18_53',...
    'FLU__Wotan__12_04_2019__12_57_02','FLU__Wotan__13_04_2019__13_26_54',...
    ...
    'FLU__Frey__14_03_2019__10_21_27',...
    'FLU__Frey__05_04_2019__10_56_11','FLU__Frey__08_04_2019__11_28_39',...
    'FLU__Frey__17_04_2019__09_41_04','FLU__Frey__18_04_2019__10_30_20',...
    'FLU__Frey__19_04_2019__10_36_02','FLU__Frey__23_04_2019__10_38_39',...
    'FLU__Frey__24_04_2019__10_51_15','FLU__Frey__25_04_2019__10_22_35',...
    ...
    'FLU__Reider__10_04_2019__11_04_28','FLU__Reider__11_04_2019__11_22_21',...
    'FLU__Reider__18_04_2019__09_32_55','FLU__Reider__17_04_2019__10_57_56',...
    'FLU__Reider__19_04_2019__11_23_57','FLU__Reider__22_04_2019__10_19_05',...
    'FLU__Reider__23_04_2019__10_06_09','FLU__Reider__24_04_2019__08_08_42',...
    'FLU__Reider__25_04_2019__08_32_24','FLU__Reider__26_04_2019__07_38_04',...
    ...
    'FLU__Igor__09_04_2019__09_52_58','FLU__Igor__10_04_2019__09_04_06',...
    'FLU__Igor__12_04_2019__09_17_30','FLU__Igor__17_04_2019__09_03_24',...
    'FLU__Igor__18_04_2019__07_43_34','FLU__Igor__23_04_2019__12_07_06',...
    'FLU__Igor__25_04_2019__10_36_02','FLU__Igor__26_04_2019__09_48_04',...
    ...
    'FLU__Bard__22_04_2019__09_38_21','FLU__Bard__23_04_2019__09_51_00',...
    'FLU__Bard__24_04_2019__10_11_14','FLU__Bard__25_04_2019__09_36_36',...
    ...
    'FLU__Sindri__15_04_2019__11_27_35','FLU__Sindri__16_04_2019__11_51_56',...
    'FLU__Sindri__17_04_2019__09_06_47','FLU__Sindri__18_04_2019__07_38_40',...
    'FLU__Sindri__22_04_2019__07_46_05','FLU__Sindri__23_04_2019__07_47_33',...
    'FLU__Sindri__24_04_2019__08_15_25','FLU__Sindri__25_04_2019__07_26_19',...
    'FLU__Sindri__26_04_2019__07_43_02',...
    };



%%
shapes = {'S00','S01', 'S02', 'S03', 'S04', 'S05', 'S06', 'S07', 'S08', 'S09'};
patterns = {'P00','P01', 'P02', 'P03', 'P04', 'P05', 'P06', 'P07', 'P08', 'P09'};
patternless_colors = {'C6000000_6000000', 'C7070014_7070014', 'C7070059_7070059', 'C7070106_7070106', 'C7070148_7070148', 'C7070194_7070194', 'C7070286_7070286', 'C7070335_7070335','C7070240_7070240'};
patterned_colors =   {'C7000000_5000000', 'C7070014_5000000', 'C7070059_5000000', 'C7070106_5000000', 'C7070148_5000000', 'C7070194_5000000', 'C7070286_5000000', 'C7070335_5000000','C7070240_5000000'};
arms = {'A00_E00','A01_E00', 'A02_E00','A00_E01', 'A00_E02', 'A00_E03','A01_E01', 'A02_E01','A01_E02', 'A01_E03','A02_E02','A02_E03'};

num_features = length(shapes)-1+length(patterns)-1 + length(patternless_colors)-1 + length(arms)-1;
dim_indexes = [1 length(shapes)-1;
    length(shapes) length(shapes)+length(patterns)-2;
    length(shapes)+length(patterns) length(shapes)+length(patterns)+length(patternless_colors)-3;
    length(shapes)+length(patterns)+length(patternless_colors) length(shapes)+length(patterns)+length(patternless_colors)+length(arms)-4];


%%

%all trials
q_count_all = zeros(num_features);
performance_all = zeros(num_features);
search_duration_all= zeros(num_features);

%trials 1-10
q_count_0110 = zeros(num_features);
performance_0110 = zeros(num_features);
search_duration_0110 = zeros(num_features);

%trials 11-20
q_count_1120 = zeros(num_features);
performance_1120 = zeros(num_features);
search_duration_1120 = zeros(num_features);

%trials 21-30
q_count_2130 = zeros(num_features);
performance_2130 = zeros(num_features);
search_duration_2130 = zeros(num_features);

%last 10 trials
q_count_last = zeros(num_features);
performance_last = zeros(num_features);
search_duration_last = zeros(num_features);

trials_analyzed = zeros(1,length(sess_file_names));
for sess = 1:length(sess_file_names)
    disp(['Importing Session ' num2str(sess) '/' num2str(length(sess_file_names))])
    log_dir = FindDataDirectory(sess_file_names{sess});
    if ~isempty(log_dir)
        session_data = ImportFLU_DataV2(sess_file_names{sess},log_dir,'slim');
    else
        disp(['Cant Find Log Dir for '  sess_file_names{sess}])
        continue
    end
    
    rewarded = strcmpi(session_data.trial_data.isRewarded,'true');
    
    search_start_time = session_data.trial_data.Epoch3_StartTimeRelative+session_data.trial_data.baselineDuration;
    search_end_time =  session_data.trial_data.Epoch7_StartTimeRelative...
        -session_data.trial_data.Epoch6_Duration-session_data.trial_data.Epoch5_Duration;
    search_duration = search_end_time-search_start_time;
    
    block = session_data.trial_data.Block;
    block = block + 1;%index starts at 0 in C++
    trial_count = length(block);
    
    for b = 1:max(block)
        block_ind = find(block == b);
        
        if length(block_ind) < 15
            continue
        end
        for t = 1:length(block_ind)
            trials_analyzed(sess) = trials_analyzed(sess)+1;
            trial_def = session_data.trial_def{block_ind(t)};
            
            stim_values = NaN(length(trial_def.relevantObjects),4);
            
            if length(trial_def.relevantObjects) == 1
                continue
            end
            for stim = 1:length(trial_def.relevantObjects)
                
                stim_name = trial_def.relevantObjects(stim).StimName;
                
                dimVals= ParseQuaddleName(stim_name,patternless_colors,arms);
                if dimVals(2) ~= 0%patterned object
                    dimVals= ParseQuaddleName(stim_name,patterned_colors,arms);
                end
                stim_values(stim,1) = dimVals(1);
                stim_values(stim,2) = dimVals(2);
                stim_values(stim,3) = dimVals(3)-1;
                stim_values(stim,4) = dimVals(5)-1;
            end
            
            non_neutral_dims = find(stim_values(1,:) ~= 0);%assume target and distractors have same dims active
            for dim1 = 1:length(non_neutral_dims)
                for dim2 = 1:length(non_neutral_dims)
                    if non_neutral_dims(dim1) == 1 %shapes
                        added_ind1 = 0;
                    elseif non_neutral_dims(dim1) == 2 %patterns
                        added_ind1 = length(shapes)-1;
                    elseif non_neutral_dims(dim1) == 3 %colors
                        added_ind1 = length(shapes)-1 + length(patterns)-1;
                    elseif non_neutral_dims(dim1) == 4 %arms
                        added_ind1 = length(shapes)-1 + length(patterns)-1 + length(patterned_colors)-1;
                    end
                    this_dim1 = non_neutral_dims(dim1);
                    
                    if non_neutral_dims(dim2) == 1 %shapes
                        added_ind2 = 0;
                    elseif non_neutral_dims(dim2) == 2 %patterns
                        added_ind2 = length(shapes)-1;
                    elseif non_neutral_dims(dim2) == 3 %colors
                        added_ind2 = length(shapes)-1 + length(patterns)-1;
                    elseif non_neutral_dims(dim2) == 4 %arms
                        added_ind2 = length(shapes)-1 + length(patterns)-1 + length(patterned_colors)-1;
                    end
                    this_dim2 = non_neutral_dims(dim2);
                    
                    for stim = 2:length(trial_def.relevantObjects)
                        
                        
                        q_count_all(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) = ...
                            q_count_all(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2))+1;
                        
                        performance_all(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) = ...
                            performance_all(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) + rewarded(block_ind(t));
                        
                        search_duration_all(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) = ...
                            search_duration_all(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) + search_duration(block_ind(t));
                        
                        if t < 10
                            %trials 1-10
                            q_count_0110(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) = ...
                                q_count_0110(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2))+1;
                            performance_0110(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) = ...
                                performance_0110(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) + rewarded(block_ind(t));
                            search_duration_0110(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) = ...
                                search_duration_0110(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) + search_duration(block_ind(t));
                        elseif t < 21
                            %trials 11-20
                            q_count_1120(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) = ...
                                q_count_1120(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2))+1;
                            performance_1120(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) = ...
                                performance_1120(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) + rewarded(block_ind(t));
                            search_duration_1120(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) = ...
                                search_duration_1120(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) + search_duration(block_ind(t));
                        elseif t < 31
                            %trials 21-30
                            q_count_2130(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) = ...
                                q_count_2130(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2))+1;
                            performance_2130(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) = ...
                                performance_2130(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) + rewarded(block_ind(t));
                            search_duration_2130(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) = ...
                                search_duration_2130(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) + search_duration(block_ind(t));
                        elseif t >= length(block_ind)-9
                            %last 10 trials
                            q_count_last(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) = ...
                                q_count_last(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2))+1;
                            performance_last(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) = ...
                                performance_last(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) + rewarded(block_ind(t));
                            search_duration_last(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) = ...
                                search_duration_last(added_ind1+stim_values(1,this_dim1),added_ind2+stim_values(stim,this_dim2)) + search_duration(block_ind(t));
                            
                        end
                    end
                end
            end
        end
    end
end
%% Peformance
figure
b = imagesc(100*performance_all./q_count_all);
set(b,'AlphaData',~isnan(performance_all./q_count_all));
xlabel('Target Dimension-Value')
ylabel('Distractor Dimension-Value');
box off
axis square
colorbar
title('Average Peformance Across All Trials')
xlim([0 39])
ylim([0 39])

txt = '\leftarrow Shapes';
t = text(10,5,txt);
t.FontSize = 14;

txt = '\leftarrow Patterns';
t1 = text(18.5,13,txt);
t1.FontSize = 14;

txt = '\leftarrow Colors';
t2 = text(26,22,txt);
t2.FontSize = 14;

txt = 'Arms \rightarrow';
t3 = text(20,30,txt);
t3.FontSize = 14;

all_vals = 100*performance_all(:)./q_count_all(:);
all_vals(all_vals == 0) = [];

prct_2_5 = prctile(all_vals,2.5);
prct_97_5 = prctile(all_vals,97.5);
caxis([prct_2_5 prct_97_5])
colormap('viridis')


figure
subplot(2,2,1)
b = imagesc(100*performance_0110./q_count_0110);
set(b,'AlphaData',~isnan(performance_0110./q_count_0110));
xlabel('Target Dimension-Value')
ylabel('Distractor Dimension-Value');
axis square
box off
colorbar
title('Average Peformance on Trials 1-10')
xlim([0 39])
ylim([0 39])
caxis([prct_2_5 prct_97_5])

% txt = '\leftarrow Shapes';
% t = text(10,5,txt);
% t.FontSize = 14;
% 
% txt = '\leftarrow Patterns';
% t1 = text(18.5,13,txt);
% t1.FontSize = 14;
% 
% txt = '\leftarrow Colors';
% t2 = text(26,22,txt);
% t2.FontSize = 14;
% 
% txt = 'Arms \rightarrow';
% t3 = text(20,30,txt);
% t3.FontSize = 14;

% Trials 11-20
subplot(2,2,2)
b = imagesc(100*performance_1120./q_count_1120);
set(b,'AlphaData',~isnan(performance_1120./q_count_1120));
xlabel('Target Dimension-Value')
ylabel('Distractor Dimension-Value');
box off
axis square
colorbar
title('Average Peformance on Trials 11-20')
xlim([0 39])
ylim([0 39])
caxis([prct_2_5 prct_97_5])

% txt = '\leftarrow Shapes';
% t = text(10,5,txt);
% t.FontSize = 14;
% 
% txt = '\leftarrow Patterns';
% t1 = text(18.5,13,txt);
% t1.FontSize = 14;
% 
% txt = '\leftarrow Colors';
% t2 = text(26,22,txt);
% t2.FontSize = 14;
% 
% txt = 'Arms \rightarrow';
% t3 = text(20,30,txt);
% t3.FontSize = 14;

% Trials 21-30
subplot(2,2,3)
b = imagesc(100*performance_2130./q_count_2130);
set(b,'AlphaData',~isnan(performance_2130./q_count_2130));
xlabel('Target Dimension-Value')
ylabel('Distractor Dimension-Value');
box off
axis square
colorbar
title('Average Peformance on Trials 21-30')
xlim([0 39])
ylim([0 39])
caxis([prct_2_5 prct_97_5])

% txt = '\leftarrow Shapes';
% t = text(10,5,txt);
% t.FontSize = 14;


% txt = '\leftarrow Patterns';
% t1 = text(18.5,13,txt);
% t1.FontSize = 14;
% 
% txt = '\leftarrow Colors';
% t2 = text(26,22,txt);
% t2.FontSize = 14;

% txt = 'Arms \rightarrow';
% t3 = text(20,30,txt);
% t3.FontSize = 14;

% Last 10 trials
subplot(2,2,4)
b = imagesc(100*performance_last./q_count_last);
set(b,'AlphaData',~isnan(performance_last./q_count_last));
xlabel('Target Dimension-Value')
ylabel('Distractor Dimension-Value');
box off
axis square
colorbar
title('Average Peformance on Last 10 Trials')
xlim([0 39])
ylim([0 39])
caxis([prct_2_5 prct_97_5])

% txt = '\leftarrow Shapes';
% t = text(10,5,txt);
% t.FontSize = 14;
% 
% txt = '\leftarrow Patterns';
% t1 = text(18.5,13,txt);
% t1.FontSize = 14;
% 
% txt = '\leftarrow Colors';
% t2 = text(26,22,txt);
% t2.FontSize = 14;
% 
% txt = 'Arms \rightarrow';
% t3 = text(20,30,txt);
% t3.FontSize = 14;
% 
colormap('viridis')
%% Search Duration
figure
b = imagesc(1000*search_duration_all./q_count_all);
set(b,'AlphaData',~isnan(search_duration_all./q_count_all));
xlabel('Target Dimension-Value')
ylabel('Distractor Dimension-Value');
box off
axis square
colorbar
title('Average Search Duration Across All Trials')
xlim([0 39])
ylim([0 39])

txt = '\leftarrow Shapes';
t = text(10,5,txt);
t.FontSize = 14;

txt = '\leftarrow Patterns';
t1 = text(18.5,13,txt);
t1.FontSize = 14;

txt = '\leftarrow Colors';
t2 = text(26,22,txt);
t2.FontSize = 14;

txt = 'Arms \rightarrow';
t3 = text(20,30,txt);
t3.FontSize = 14;

all_vals = 1000*search_duration_all(:)./q_count_all(:);
all_vals(all_vals == 0) = [];

prct_2_5 = prctile(all_vals,2.5);
prct_97_5 = prctile(all_vals,97.5);
caxis([prct_2_5 prct_97_5])
colormap('viridis')

figure
% Trials 1-10
subplot(2,2,1)
b = imagesc(1000*search_duration_0110./q_count_0110);
set(b,'AlphaData',~isnan(search_duration_0110./q_count_0110));
xlabel('Target Dimension-Value')
ylabel('Distractor Dimension-Value');
axis square
box off
colorbar
title('Average Search Duration on Trials 1-10')
xlim([0 39])
ylim([0 39])
caxis([prct_2_5 prct_97_5])

txt = '\leftarrow Shapes';
t = text(10,5,txt);
t.FontSize = 14;

txt = '\leftarrow Patterns';
t1 = text(18.5,13,txt);
t1.FontSize = 14;

txt = '\leftarrow Colors';
t2 = text(26,22,txt);
t2.FontSize = 14;

txt = 'Arms \rightarrow';
t3 = text(20,30,txt);
t3.FontSize = 14;

% Trials 11-20
subplot(2,2,2)
b = imagesc(1000*search_duration_1120./q_count_1120);
set(b,'AlphaData',~isnan(search_duration_1120./q_count_1120));
xlabel('Target Dimension-Value')
ylabel('Distractor Dimension-Value');
box off
axis square
colorbar
title('Average Search Duration on Trials 11-20')
xlim([0 39])
ylim([0 39])
caxis([prct_2_5 prct_97_5])

txt = '\leftarrow Shapes';
t = text(10,5,txt);
t.FontSize = 14;

txt = '\leftarrow Patterns';
t1 = text(18.5,13,txt);
t1.FontSize = 14;

txt = '\leftarrow Colors';
t2 = text(26,22,txt);
t2.FontSize = 14;

txt = 'Arms \rightarrow';
t3 = text(20,30,txt);
t3.FontSize = 14;

% Trials 21-30
subplot(2,2,3)
b = imagesc(1000*search_duration_2130./q_count_2130);
set(b,'AlphaData',~isnan(search_duration_2130./q_count_2130));
xlabel('Target Dimension-Value')
ylabel('Distractor Dimension-Value');
box off
axis square
colorbar
title('Average Search Duration on Trials 21-30')
xlim([0 39])
ylim([0 39])
caxis([prct_2_5 prct_97_5])

txt = '\leftarrow Shapes';
t = text(10,5,txt);
t.FontSize = 14;

txt = '\leftarrow Patterns';
t1 = text(18.5,13,txt);
t1.FontSize = 14;

txt = '\leftarrow Colors';
t2 = text(26,22,txt);
t2.FontSize = 14;

txt = 'Arms \rightarrow';
t3 = text(20,30,txt);
t3.FontSize = 14;

subplot(2,2,4)
b = imagesc(1000*search_duration_last./q_count_last);
set(b,'AlphaData',~isnan(search_duration_last./q_count_last));
xlabel('Target Dimension-Value')
ylabel('Distractor Dimension-Value');
box off
axis square
colorbar
title('Average Search Duration on Last 10 Trials')
xlim([0 39])
ylim([0 39])
caxis([prct_2_5 prct_97_5])

txt = '\leftarrow Shapes';
t = text(10,5,txt);
t.FontSize = 14;

txt = '\leftarrow Patterns';
t1 = text(18.5,13,txt);
t1.FontSize = 14;

txt = '\leftarrow Colors';
t2 = text(26,22,txt);
t2.FontSize = 14;

txt = 'Arms \rightarrow';
t3 = text(20,30,txt);
t3.FontSize = 14;

%% Make Collapsed plot for performance on all trials
collapsed_avg_perform = NaN(4,4);
all_avg_peform = 100*performance_all./q_count_all;
for dim = 1:4
    for dimdim = 1:4
        vals = all_avg_peform(dim_indexes(dim,1):dim_indexes(dim,2),...
            dim_indexes(dimdim,1):dim_indexes(dimdim,2));
        collapsed_avg_perform(dim,dimdim) = nanmean(vals(:));
    end
end
figure
b = imagesc(collapsed_avg_perform);
set(b,'AlphaData',~isnan(collapsed_avg_perform))
xlabel('Target Dimension')
ylabel('Distractor Dimension');
box off
axis square
colorbar
title('Average Peformance Across All Trials by Dimension')
xticks(1:4)
yticks(1:4);
xticklabels({'Shape','Pattern','Color','Arms'});
yticklabels({'Shape','Pattern','Color','Arms'})
colormap('viridis')
%% Make Collapsed plot for trials by groups of 10
collapsed_avg_perform10 = NaN(4,4);
collapsed_avg_perform20 = NaN(4,4);
collapsed_avg_perform30 = NaN(4,4);
all_avg_peform10 = 100*performance_0110./q_count_0110;
all_avg_peform20 = 100*performance_1120./q_count_1120;
all_avg_peform30 = 100*performance_2130./q_count_2130;
all_avg_peformlast = 100*performance_last./q_count_last;
for dim = 1:4
    for dimdim = 1:4
        vals = all_avg_peform10(dim_indexes(dim,1):dim_indexes(dim,2),...
            dim_indexes(dimdim,1):dim_indexes(dimdim,2));
        collapsed_avg_perform10(dim,dimdim) = nanmean(vals(:));
        
        vals = all_avg_peform20(dim_indexes(dim,1):dim_indexes(dim,2),...
            dim_indexes(dimdim,1):dim_indexes(dimdim,2));
        collapsed_avg_perform20(dim,dimdim) = nanmean(vals(:));
        
        vals = all_avg_peform30(dim_indexes(dim,1):dim_indexes(dim,2),...
            dim_indexes(dimdim,1):dim_indexes(dimdim,2));
        collapsed_avg_perform30(dim,dimdim) = nanmean(vals(:));
        
        vals = all_avg_peformlast(dim_indexes(dim,1):dim_indexes(dim,2),...
            dim_indexes(dimdim,1):dim_indexes(dimdim,2));
        collapsed_avg_performlast(dim,dimdim) = nanmean(vals(:));
    end
end

clims = NaN(4,2);
figure
subplot(2,2,1)
imagesc(collapsed_avg_perform10);
xlabel('Target Dimension')
ylabel('Distractor Dimension');
title('Average Peformance Trials 1-10')
box off
axis square
colorbar
xticks(1:4)
yticks(1:4);
xticklabels({'Shape','Pattern','Color','Arms'});
yticklabels({'Shape','Pattern','Color','Arms'})
colormap('viridis')
clims(1,:) = caxis;


subplot(2,2,2)
imagesc(collapsed_avg_perform20);
xlabel('Target Dimension')
ylabel('Distractor Dimension');
title('Average Peformance Trials 11-20')
box off
axis square
colorbar
xticks(1:4)
yticks(1:4);
xticklabels({'Shape','Pattern','Color','Arms'});
yticklabels({'Shape','Pattern','Color','Arms'})
colormap('viridis')
clims(2,:) = caxis;

subplot(2,2,3)
imagesc(collapsed_avg_perform30);
xlabel('Target Dimension')
ylabel('Distractor Dimension');
title('Average Peformance Trials 21-30')
box off
axis square
colorbar
xticks(1:4)
yticks(1:4);
xticklabels({'Shape','Pattern','Color','Arms'});
yticklabels({'Shape','Pattern','Color','Arms'})
colormap('viridis')
clims(3,:) = caxis;


subplot(2,2,4)
imagesc(collapsed_avg_performlast);
xlabel('Target Dimension')
ylabel('Distractor Dimension');
title('Average Peformance Last Trials')
box off
axis square
colorbar
xticks(1:4)
yticks(1:4);
xticklabels({'Shape','Pattern','Color','Arms'});
yticklabels({'Shape','Pattern','Color','Arms'})
colormap('viridis')
clims(4,:) = caxis;


minc = min(clims(:));
maxc = max(clims(:));

subplot(2,2,1)
caxis([minc maxc])
subplot(2,2,2)
caxis([minc maxc])
subplot(2,2,3)
caxis([minc maxc])
subplot(2,2,4)
caxis([minc maxc])


% minc = prct_2_5;
% maxc = prct_97_5;
% subplot(2,2,1)
% caxis([minc maxc])
% subplot(2,2,2)
% caxis([minc maxc])
% subplot(2,2,3)
% caxis([minc maxc])
