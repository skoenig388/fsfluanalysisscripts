function [block_performance_by_num_irrel,search_duration_by_num_irrel,...
    LP_by_num_irrel,LP_aligned_block_performance_by_num_irrel] = ...
    CalculateBlockPerformancebyNumIrrelDim(session_data,goodblocks,min_block_len,last_trial_window,...
    forward_smoothing_window,learning_point_threshold,forward_back_trials)
%adapted from code else where, written 5/10/19

%Inputs:
% 1) session_data across all sessions
% 2) goodblocks: blocks with acceptable performance across all sessions
% 3) min_block_len: miniumum # of trials in a block
% 4) last_trial_window: # of trials to look at end of block
% 5) forward_smoothing_window: window width for forward smoothing filter
% 6) learning_point_threshold: proportion correct to consider having "learned"
% 7) forward_back_trials: for performance aligned LP, how far forward and back to look

%Outputs:
% 1) block_performance_by_num_irrel: block transition-aligned performance by # of irrelevant feature dimensions
% 2) search_duration_by_num_irrel: search duration by # of irrelevant feature dimensions
% 3) LP_by_num_irrel: trial # of learning point by # of irrelevant feature dimensions
% 4) LP_aligned_block_performance_by_num_irrel: learning point aligned performance by # of irrelevant feature dimensions


if nargin < 3
    error('Not enough input arguments')
elseif nargin < 4
    forward_smoothing_window = 10;
elseif nargin < 5
    forward_smoothing_window = 10;
    learning_point_threshold = 0.8;
elseif nargin < 6
    forward_smoothing_window = 10;
    learning_point_threshold = 0.8;
    forward_back_trials = 5;
end

if learning_point_threshold > 10
    %assuming thrteshold is meant to be proportion correct not 100%
    learning_point_threshold = learning_point_threshold/100;
end

num_blocks = length(goodblocks);
block = session_data.trial_data.Block;
block = block + 1;%index starts at 0 in C++
rewarded = strcmpi(session_data.trial_data.isHighestProbReward,'true');
search_duration = Calculate_FLU_Search_Duration(session_data);

%uses a little bit more space but makes indexing for LP easier so....
block_performance_by_num_irrel = cell(1,3);
search_duration_by_num_irrel = cell(1,3);
LP_by_num_irrel = cell(1,3);
LP_aligned_block_performance_by_num_irrel = cell(1,3);
for num_irrel = 1:3
    block_performance_by_num_irrel{num_irrel} = NaN(num_blocks,min_block_len+last_trial_window);
    search_duration_by_num_irrel{num_irrel} = NaN(num_blocks,min_block_len+last_trial_window);
    LP_aligned_block_performance_by_num_irrel{num_irrel} = NaN(num_blocks,2*forward_back_trials+1);
end

number_of_irrelevant_dimensions = ParseBlockDef4NumIrrel(session_data.block_def);
for b = 1:num_blocks
    blockind = find(block == goodblocks(b));
    if isempty(blockind)
        error('why is block empty?')
    elseif length(blockind) < 15%didn't finish
        error('why is block too short?')
    end
    
    this_number_irrel = number_of_irrelevant_dimensions(goodblocks(b))+1;
    
    %---Get Performance---%
    blockrewarded = rewarded(blockind(1):blockind(end));
    blockrewarded = [blockrewarded(end-(last_trial_window-1):end); blockrewarded];
    block_performance_by_num_irrel{this_number_irrel}(b,:)= blockrewarded(1:min_block_len+last_trial_window);
    
    %---Get Search Duration---%
    blocksearchdur = search_duration(blockind(1):blockind(end));
    blocksearchdur = [blocksearchdur(end-(last_trial_window-1):end); blocksearchdur];
    search_duration_by_num_irrel{this_number_irrel}(b,:) = blocksearchdur(1:min_block_len+last_trial_window);
    
    %---Get Learning Point---%
    blockrewarded = rewarded(blockind(1):blockind(end));
    trials_in_block = length(blockrewarded);
    lp = FindLp(blockrewarded, 'slidingwindow', forward_smoothing_window, learning_point_threshold);
    if ~isempty(lp) && ~isnan(lp) && lp ~= length(blockrewarded)
        LP_by_num_irrel{this_number_irrel} = [LP_by_num_irrel{this_number_irrel} lp];
        
        if lp <= forward_back_trials
            start_index = forward_back_trials-lp+1;
            LP_aligned_block_performance_by_num_irrel{this_number_irrel}(b,start_index+1:end) = blockrewarded(1:lp+forward_back_trials);
        elseif lp >= trials_in_block-forward_back_trials
            end_index = (lp+forward_back_trials)-trials_in_block;
            LP_aligned_block_performance_by_num_irrel{this_number_irrel}(b,1:end-end_index) = blockrewarded(lp-forward_back_trials:end);
        else
            LP_aligned_block_performance_by_num_irrel{this_number_irrel}(b,:) = blockrewarded(lp-forward_back_trials:lp+forward_back_trials);
        end
    else
        LP_by_num_irrel{this_number_irrel} = [LP_by_num_irrel{this_number_irrel} NaN];
    end
end

%---remove excess NaNs---%
block_performance_by_num_irrel = laundry(block_performance_by_num_irrel,1);
search_duration_by_num_irrel = laundry(search_duration_by_num_irrel,1);
try
    LP_by_num_irrel = laundry(LP_by_num_irrel,1);
end
LP_aligned_block_performance_by_num_irrel = laundry(LP_aligned_block_performance_by_num_irrel,1);

end