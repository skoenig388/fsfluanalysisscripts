clar

log_dir = 'Z:\MonkeyData\Wotan\Context Feature Search Task\';
sess_file_name = 'CFS__Wotan__07_03_2019__15_53_46';


Fs = 600;
[Blow,Alow] = butter(8,120/(Fs/2),'low');
%% Feature Dimensions and values
shapes = {'S01', 'S02', 'S03', 'S04', 'S05', 'S06', 'S07', 'S08', 'S09'};
patterns = {'P01', 'P02', 'P03', 'P04', 'P05', 'P06', 'P07', 'P08', 'P09'};

patternless_colors = {'C6070014_6070014', 'C6070059_6070059', 'C6070106_6070106', 'C6070148_6070148', 'C6070194_6070194', 'C6070287_6070287', 'C6070335_6070335'};
%patterned_colors =   {'C7000000_5000000', 'C7070014_5000000', 'C7070059_5000000', 'C7070106_5000000', 'C7070148_5000000', 'C7070194_5000000', 'C7070286_5000000', 'C7070335_5000000'};
gray_pattern_color = 'C7000000_5000000';

arms = {'A00_E01', 'A00_E02','A00_E03', 'A01_E00', 'A01_E01', 'A01_E02', 'A01_E03', 'A02_E00', 'A02_E01', 'A02_E02', 'A02_E03'};
%% Import Data (takes a while)
session_data = ImportFLU_DataV2(sess_file_name,log_dir);
%% Get Screen Details
[pixelsize,cmsize] = ReadMonitordetails(session_data.configdata.monitorDetails);
screenX = pixelsize(1);
screenY = pixelsize(2);

%% Get Trial Segemented Python Eye Data

num_trials = session_data.num_trials;
trial_aligned_Python_eye_data = cell(1,num_trials);
for t = 1:num_trials


    ITI_epoch_start = [session_data.trial_data.Epoch0_StartFrame(t) session_data.trial_data.Epoch0_StartTimeAbsolute(t) session_data.trial_data.Epoch0_StartTimeRelative(t)];
    Reward_epoch_start =  [session_data.trial_data.Epoch7_StartFrame(t) session_data.trial_data.Epoch7_StartTimeAbsolute(t) session_data.trial_data.Epoch7_StartTimeRelative(t)];
    Reward_epoch_duration = session_data.trial_data.Epoch7_Duration(t);

    this_trial_ind = find(session_data.frame_data.TrialInExperiment == t);
    reward_epoch_end_frame = session_data.frame_data.Frame(this_trial_ind(end));

    trial_frame_start = ITI_epoch_start(1);
    trial_end_frame = reward_epoch_end_frame;

    trial_Unity_time_start = ITI_epoch_start(2);
    trail_Unity_end_time = Reward_epoch_start(2)+Reward_epoch_duration;

    UDP_start_index = find(session_data.UDP_recv_data.Frame == trial_frame_start);
    if length(UDP_start_index) > 1
        UDP_start_index = UDP_start_index(1);
        disp('Found Multiple UDP Start Messages')
    elseif isempty(UDP_start_index)
        error('Cant find Trial Start Frame in UDP')
        continue
    end


    UDP_end_index = find(session_data.UDP_recv_data.Frame == trial_end_frame);
    if length(UDP_end_index) > 1
        UDP_end_index = UDP_end_index(1);
        disp('Found Multiple UDP End Messages')
    elseif isempty(UDP_end_index)
        if t == num_trials
            if abs(session_data.UDP_recv_data.Frame(end)-trial_end_frame) <= 1
                UDP_end_index = length(session_data.UDP_recv_data.Frame);
                trial_end_frame = session_data.UDP_recv_data.Frame(end);
            else
                error('Cant find Trial Start Frame in UDP')
            end
        else
            error('Cant find Trial Start Frame in UDP')
        end
    end

    %verify frame start time is within 1 frame across files
    if abs(session_data.UDP_recv_data.FrameStartUnity(UDP_start_index)- trial_Unity_time_start) > 0.1
        disp(['Trial Start: UDP and Frame Start times are off :( by ' ...
            num2str(abs(session_data.UDP_recv_data.FrameStartUnity(UDP_start_index)- trial_Unity_time_start))])
    end
    if abs(session_data.UDP_recv_data.FrameStartUnity(UDP_end_index)- trail_Unity_end_time) > 0.1
        disp(['Trial End: UDP and Frame Start times are off :( by ' ...
            num2str(abs(session_data.UDP_recv_data.FrameStartUnity(UDP_end_index)- trail_Unity_end_time))])
    end

    Eye_tracker_time_stamp_start = session_data.UDP_recv_data.EyeTrackerTimeStamp(UDP_start_index);
    if isnan(Eye_tracker_time_stamp_start)
        disp('What, Trial Start ET is a NaN')
    elseif isempty(Eye_tracker_time_stamp_start)
        disp('What, Trial Start ET is Empty')
    end

    Eye_tracker_time_stamp_end = session_data.UDP_recv_data.EyeTrackerTimeStamp(UDP_end_index);
    if isnan(Eye_tracker_time_stamp_end)
        disp('What, Trial End ET is a NaN')
    elseif isempty(Eye_tracker_time_stamp_end)
        disp('What, Trial End ET is Empty')
    end

    python_ind  = find(session_data.gaze_data.EyeTrackerTimeStamp >= Eye_tracker_time_stamp_start & ...
        session_data.gaze_data.EyeTrackerTimeStamp <= Eye_tracker_time_stamp_end);

    trial_aligned_Python_eye_data{t} = [session_data.gaze_data.PythonTimeStamp(python_ind,:) ...
        session_data.gaze_data.EyeTrackerTimeStamp(python_ind,:) ...
        session_data.gaze_data.Lx(python_ind,:) session_data.gaze_data.Ly(python_ind,:) ...
        session_data.gaze_data.Rx(python_ind,:) session_data.gaze_data.Ry(python_ind,:) ...
        session_data.gaze_data.PupilL(python_ind,:) session_data.gaze_data.PupilR(python_ind,:)];
end
%% Determine which Quaddles where shown

target_Quaddle_identitys = NaN(20,5);
which_context = NaN(1,20);
for t = 1:20 %block 0, loop through all fam block to make sure got target
    dimVals= ParseQuaddleName(session_data.trial_def{t}.relevantObjects(1).StimName,patternless_colors,arms);
    %dimVals = session_data.trial_def{t}.relevantObjects(1).StimDimVals';
    target_Quaddle_identitys(t,:) = dimVals;
    which_context(t) = session_data.trial_def{t}.ContextNum;
end
contexts = unique(which_context);

target1 = target_Quaddle_identitys(which_context == contexts(1),:);
target1 = target1(1,:);
target2 = target_Quaddle_identitys(which_context == contexts(2),:);
target2 = target2(1,:);
total_num_features = sum(target1 ~= 0) + sum(target2 ~= 0); %+1 for neutral

if total_num_features == 2
    all_Quaddles = [target1; target2; zeros(1,5)];
elseif total_num_features == 4
    all_Quaddles = [target1; target2];
    all_Quaddle_types = [{'T1'};{'T2'}];
    %1D decomposition
    for t = 1:2
        if t == 1
            this_target = target1;
        elseif t == 2
            this_target = target2;
        end
        non_neutral_features = find(this_target ~=0);
        for nnf = 1:length(non_neutral_features)
            distractor = zeros(1,5);
            distractor(non_neutral_features(nnf)) = this_target(non_neutral_features(nnf));
            all_Quaddles = [all_Quaddles; distractor];
            all_Quaddle_types = [all_Quaddle_types; {['T' num2str(t) char('a'+nnf-1)]}];
        end
    end
    
    %2D recombination across targets
    non_neutral_features1 = find(target1 ~=0);
    non_neutral_features2 = find(target2 ~=0);
    for t = 1:length(non_neutral_features1)
        for tt = 1:length(non_neutral_features1)
            if t ~= tt
                distractor = zeros(1,5);
                distractor(non_neutral_features1(t)) = target1(non_neutral_features(t));
                distractor(non_neutral_features2(tt)) = target2(non_neutral_features2(tt));
                all_Quaddles = [all_Quaddles; distractor];
                all_Quaddle_types = [all_Quaddle_types; {['T1' char('a'+t-1) 'T2' char('a' + tt-1)]}];
            end
        end
    end
    
    all_Quaddles =[all_Quaddles; zeros(1,5)];
    all_Quaddle_types = [all_Quaddle_types; {'neutral'}];
elseif total_num_features == 6%not redundant for 2D (4 total features) since 2D decomposition options
    
else
    error('Found too many unique features')
end
%%
all_pct_looking = NaN(num_trials,size(all_Quaddles,1)+1);
all_trial_contexts = NaN(1,num_trials);
all_trial_num_objects = NaN(1,num_trials);
all_trial_search_durations = NaN(1,num_trials);
for t =1:num_trials
    all_trial_contexts(t) = session_data.trial_def{t}.ContextNum;
    num_objs = length(session_data.trial_def{t}.relevantObjects);
    all_trial_num_objects(t) = num_objs;
    select_rad = session_data.trial_data.ObjectselectionRadius(t);
    
    object_locations = NaN(2,num_objs);
    allStimDimVals = NaN(num_objs,5);
    for o = 1:num_objs
        object_locations(1,o) = round(session_data.trial_def{t}.relevantObjects(o).StimScreenLocation.x);
        object_locations(2,o) = round(session_data.trial_def{t}.relevantObjects(o).StimScreenLocation.y);
        dimVals= ParseQuaddleName(session_data.trial_def{t}.relevantObjects(o).StimName,patternless_colors,arms);
        allStimDimVals(o,:) = dimVals;
    end
    
    stim_array = -1*ones(screenY,screenX);
    [yy,xx] = find(stim_array == -1);
    xyind = find(stim_array == -1);
    for o = 1:num_objs
        rind = find(sqrt((object_locations(1,o)-xx).^2+(object_locations(2,o)-yy).^2) <= select_rad);
        equiv = (all_Quaddles == allStimDimVals(o,:));
        identity = find(all(equiv'));
        if isempty(identity)
            error('Unknown feature value')
        end
        stim_array(xyind(rind)) = identity;
    end
    
    
    search_start_time = session_data.trial_data.Epoch3_StartTimeRelative(t)+session_data.trial_data.baselineDuration(t);
    search_end_time =  session_data.trial_data.Epoch7_StartTimeRelative(t)...
        -session_data.trial_data.fBDuration(t)-session_data.trial_data.ObjectSelectionDuration(t);
    if search_end_time < search_start_time
        disp('What')
    end
    eyetime = trial_aligned_Python_eye_data{t}(:,2);
    if isempty(eyetime)
        continue
    end
    eyetime = eyetime-eyetime(1);
    eyetime = eyetime/1e6;%convert to seconds
    
    all_trial_search_duration(t) = search_end_time-search_start_time;
    t1 = find(eyetime < search_start_time);
    t1 = t1(end);
    t2 = find(eyetime > search_end_time);
    t2 = t2(1);
    
    eyex = trial_aligned_Python_eye_data{t}(t1:t2,5);
    eyey = trial_aligned_Python_eye_data{t}(t1:t2,6);
    
    cumulative_looking_time = zeros(1,size(all_Quaddles,1)+1);
    for eyeind = 1:length(eyex)
        x = round(1920*eyex(eyeind));
        if x < 1 || x > 1920 || isnan(x)
            continue
        end
        y = round(1080*(1-eyey(eyeind)));
        if y < 1 || y > 1080 || isnan(y)
            continue
        end
        
        val = stim_array(y,x);
        if val == -1%looking at background
            cumulative_looking_time(1) = cumulative_looking_time(1) + 1;
        else
            cumulative_looking_time(val+1) = cumulative_looking_time(val+1) + 1;
        end
    end
    all_pct_looking(t,:) = cumulative_looking_time./sum(cumulative_looking_time);
    
    %     figure
    %     subplot(2,2,1)
    %     imagesc(stim_array)
    %     hold on
    %     plot(1920*eyex,1080*(1-eyey),'w')
    %     hold off
    %     box off
    %     axis equal
    %     xlim([1 1920])
    %     ylim([1 1080])
    %     axis off
    %
    %     subplot(2,2,2)
    %     plot(eyetime(t1:t2),1920*eyex)
    %     hold on
    %     plot(eyetime(t1:t2),1080*eyey)
    %     hold off
    %     xlim([eyetime(t1) eyetime(t2)])
    %     ylim([0 1920])
    %     legend('X','Y')
    %     xlabel('Time (sec)')
    %     ylabel('Eye Position (px)')
    %     box off
    %
    %     subplot(2,2,3)
    %     bar(1:length(cumulative_looking_time),100*cumulative_looking_time./sum(cumulative_looking_time));
    %     set(gca,'Xtick',1:length(cumulative_looking_time));
    %     xtickangle(45)
    %     ylabel('% of Looking Time')
    %     box off
    %     subtitle(['Eye data for Trial #' num2str(t)])
end
%% Split Data by Context


contxt1_pct = all_pct_looking(all_trial_contexts == contexts(1),:);
contxt2_pct = all_pct_looking(all_trial_contexts == contexts(2),:);

figure
subplot(2,2,1)
bar([nanmean(contxt1_pct(1:10,:)) nanmean(contxt2_pct(1:10,:))])
box off
xlabel('Quaddle Indentity')
xticks(1:size(contxt1_pct,2)+size(contxt2_pct,2))
if total_num_features == 2
    xticklabels({'C1 Bckgrnd','C1 Target1','C1 Target 2','C1 Neutral',...
        'C2 Bckgrnd','C2 Target1','C2 Target 2','C2 Neutral'})
elseif total_num_features == 4
    xticklabels({'C1 Bckgrnd','C1 Target1','C1 Target 2','C1 T1A','C1 T1B',...
        'C1 T2A','C1 T2B','C1 T1A + T2B','C1 T1B + T2A','C1 Neutral',...
        'C2 Bckgrnd','C2 Target1','C2 Target 2','C2 T1A','C2 T1B',...
        'C2 T2A','C2 T2B','C2 T1A + T2B','C2 T1B + T2A','C2 Neutral'})
end
xtickangle(45)
ylabel('Proportion of Looking Time')
title('Familiarization Trials')

all_pct_looking5 = all_pct_looking(all_trial_num_objects < 5,:);
all_trial_contexts5 = all_trial_contexts(all_trial_num_objects < 5);
contxt1_pct5 = all_pct_looking5(all_trial_contexts5 == contexts(1),:);
contxt2_pct5 = all_pct_looking5(all_trial_contexts5 == contexts(2),:);

subplot(2,2,2)
bar([nanmean(contxt1_pct5) nanmean(contxt2_pct5)])
box off
xlabel('Quaddle Indentity')
xticks(1:size(contxt1_pct,2)+size(contxt2_pct,2))
if total_num_features == 2
    xticklabels({'C1 Bckgrnd','C1 Target1','C1 Target 2','C1 Neutral',...
        'C2 Bckgrnd','C2 Target1','C2 Target 2','C2 Neutral'})
elseif total_num_features == 4
    xticklabels({'C1 Bckgrnd','C1 Target1','C1 Target 2','C1 T1A','C1 T1B',...
        'C1 T2A','C1 T2B','C1 T1A + T2B','C1 T1B + T2A','C1 Neutral',...
        'C2 Bckgrnd','C2 Target1','C2 Target 2','C2 T1A','C2 T1B',...
        'C2 T2A','C2 T2B','C2 T1A + T2B','C2 T1B + T2A','C2 Neutral'})
end
xtickangle(45)
ylabel('Proportion of Looking Time')
title('Trials with < 5 Quaddles')

subplot(2,2,3)
bar([nanmean(contxt1_pct) nanmean(contxt2_pct)])
box off
xlabel('Quaddle Indentity')
xticks(1:size(contxt1_pct,2)+size(contxt2_pct,2))
if total_num_features == 2
    xticklabels({'C1 Bckgrnd','C1 Target1','C1 Target 2','C1 Neutral',...
        'C2 Bckgrnd','C2 Target1','C2 Target 2','C2 Neutral'})
elseif total_num_features == 4
    xticklabels({'C1 Bckgrnd','C1 Target1','C1 Target 2','C1 T1A','C1 T1B',...
        'C1 T2A','C1 T2B','C1 T1A + T2B','C1 T1B + T2A','C1 Neutral',...
        'C2 Bckgrnd','C2 Target1','C2 Target 2','C2 T1A','C2 T1B',...
        'C2 T2A','C2 T2B','C2 T1A + T2B','C2 T1B + T2A','C2 Neutral'})
end
xtickangle(45)
ylabel('Proportion of Looking Time')
title('All Trials')

all_pct_looking6 = all_pct_looking(all_trial_num_objects > 5,:);
all_trial_contexts6 = all_trial_contexts(all_trial_num_objects > 5);
contxt1_pct6 = all_pct_looking6(all_trial_contexts6 == contexts(1),:);
contxt2_pct6 = all_pct_looking6(all_trial_contexts6 == contexts(2),:);

subplot(2,2,4)
bar([nanmean(contxt1_pct6) nanmean(contxt2_pct6)])
box off
xlabel('Quaddle Indentity')
xticks(1:size(contxt1_pct,2)+size(contxt2_pct,2))
if total_num_features == 2
    xticklabels({'C1 Bckgrnd','C1 Target1','C1 Target 2','C1 Neutral',...
        'C2 Bckgrnd','C2 Target1','C2 Target 2','C2 Neutral'})
elseif total_num_features == 4
    xticklabels({'C1 Bckgrnd','C1 Target1','C1 Target 2','C1 T1A','C1 T1B',...
        'C1 T2A','C1 T2B','C1 T1A + T2B','C1 T1B + T2A','C1 Neutral',...
        'C2 Bckgrnd','C2 Target1','C2 Target 2','C2 T1A','C2 T1B',...
        'C2 T2A','C2 T2B','C2 T1A + T2B','C2 T1B + T2A','C2 Neutral'})
end
xtickangle(45)
ylabel('Proportion of Looking Time')
title('Trials with 5+ Quaddles')

subtitle([sess_file_name ' ' session_data.blockjson(1).BlockID(9:end-3)])

%% Figure for Higher Dimensional Tasks
if total_num_features == 4
    contxt1_pct = all_pct_looking(all_trial_contexts == contexts(1),:);
    contxt1_pct = contxt1_pct(11:end,:);%ignore fam block
    contxt2_pct = all_pct_looking(all_trial_contexts == contexts(2),:);
    contxt2_pct(11:end,:);%ignore fam block
    
    background = [contxt1_pct(:,1); contxt2_pct(:,1)];
    neutral =  [contxt1_pct(:,end); contxt2_pct(:,2)];
    T1ind = find(cell2mat(cellfun(@(s) strcmpi(s,'T1'),all_Quaddle_types,'UniformOutput',false)));
    T2ind = find(cell2mat(cellfun(@(s) strcmpi(s,'T2'),all_Quaddle_types,'UniformOutput',false)));
    T1aind = find(cell2mat(cellfun(@(s) strcmpi(s,'T1a'),all_Quaddle_types,'UniformOutput',false)));
    T1bind = find(cell2mat(cellfun(@(s) strcmpi(s,'T1b'),all_Quaddle_types,'UniformOutput',false)));
    T2aind = find(cell2mat(cellfun(@(s) strcmpi(s,'T2a'),all_Quaddle_types,'UniformOutput',false)));
    T2bind = find(cell2mat(cellfun(@(s) strcmpi(s,'T2b'),all_Quaddle_types,'UniformOutput',false)));
    T1aT2bind = find(cell2mat(cellfun(@(s) strcmpi(s,'T1aT2b'),all_Quaddle_types,'UniformOutput',false)));
    T1bT2aind = find(cell2mat(cellfun(@(s) strcmpi(s,'T1bT2a'),all_Quaddle_types,'UniformOutput',false)));
    
    
    target = [contxt1_pct(:,T1ind+1); contxt2_pct(:,T2ind+1)];
    wrong_target = [contxt1_pct(:,T2ind+1); contxt2_pct(:,T1ind+1)];
    targetpartial = [contxt1_pct(:,T1aind+1); contxt1_pct(:,T1bind+1); ...
        contxt2_pct(:,T2aind+1); contxt2_pct(:,T2bind+1)];
    wrong_target_partial = [contxt1_pct(:,T2aind+1); contxt1_pct(:,T2bind+1); ...
        contxt2_pct(:,T1aind+1); contxt2_pct(:,T1bind+1)];
    target_plus_wrong_target_2D = [contxt1_pct(:,T1aT2bind+1); contxt1_pct(:,T1bT2aind+1); ...
        contxt2_pct(:,T1aT2bind+1); contxt2_pct(:,T1bT2aind+1)];
    
    figure
    bar(1:7,[nanmean(background) nanmean(target) nanmean(wrong_target) ....
        nanmean(targetpartial) nanmean(wrong_target_partial) nanmean(target_plus_wrong_target_2D) nanmean(neutral)])
    ylim([0 1.25*nanmean(target)])
    xticks(1:7)
    xticklabels({'Background','Correct Target','Wrong Target',...
        'Target 1D Decomp','Wrong Target 1D Decomp','Target + Wrong Target 2D','Neutral'})
    xtickangle(30)
    ylabel('Proportion of Looking Time')
    box off
    title([sess_file_name ' ' session_data.blockjson(1).BlockID(9:end-3) ', All trials, Data Collapsed Across Contexts'])
    
end
%% Plot search duration by block
num_distractors = unique(all_trial_num_objects)-1;
average_search_duration = NaN(1,length(num_distractors));
for dist = 1:length(num_distractors)
    average_search_duration(dist) = mean(all_trial_search_duration(all_trial_num_objects-1 == num_distractors(dist)));
end

figure
bar(1:length(num_distractors),average_search_duration)
xlabel('# of Distractors')
ylabel('Average Search Duration (sec)')
xticks([1:length(num_distractors)])
xticklabels(cellfun(@(s) num2str(s),num2cell(num_distractors),'UniformOutput',false))
box off

p = polyfit(num_distractors,average_search_duration,1);
title([sess_file_name ' ' session_data.blockjson(1).BlockID(9:end-3) ', m = ' num2str(1000*p(1),3) ' ms/distractor'])

%% Determine Effect of Previous Trial Context on Search Duration
num_back_search_dur = NaN(num_trials,4);
%1) number of preceeding trials with same context
%2) Percent change relative to expected for block
%3) Absolute change relative to expected for block
%4) Absolute
for t = 1:num_trials
    if t > 3 %looking 3 trials back
        numobjs = all_trial_num_objects(t)-1;
        expected_search_dur = average_search_duration(num_distractors == numobjs);
        
        if all_trial_contexts(t-1) == all_trial_contexts(t)
            if all_trial_contexts(t-2) == all_trial_contexts(t)
                if all_trial_contexts(t-3) == all_trial_contexts(t)
                    num_back_search_dur(t,1) = 3;
                    num_back_search_dur(t,2) =  (all_trial_search_duration(t)-expected_search_dur)/expected_search_dur;
                    num_back_search_dur(t,3) =  all_trial_search_duration(t)-expected_search_dur;
                    num_back_search_dur(t,4) =  all_trial_search_duration(t)-expected_search_dur;
                else
                    num_back_search_dur(t,1) = 2;
                    num_back_search_dur(t,2) =  (all_trial_search_duration(t)-expected_search_dur)/expected_search_dur;
                    num_back_search_dur(t,3) =  all_trial_search_duration(t)-expected_search_dur;
                    num_back_search_dur(t,4) =  all_trial_search_duration(t)-expected_search_dur;
                end
            else
                num_back_search_dur(t,1) = 1;
                num_back_search_dur(t,2) =  (all_trial_search_duration(t)-expected_search_dur)/expected_search_dur;
                num_back_search_dur(t,3) =  all_trial_search_duration(t)-expected_search_dur;
                num_back_search_dur(t,4) =  all_trial_search_duration(t)-expected_search_dur;
            end
        else
            num_back_search_dur(t,1) = 0;
            num_back_search_dur(t,2) =  (all_trial_search_duration(t)-expected_search_dur)/expected_search_dur;
            num_back_search_dur(t,3) =  all_trial_search_duration(t)-expected_search_dur;
            num_back_search_dur(t,4) =  all_trial_search_duration(t)-expected_search_dur;
        end
    end
end

average_search_dur_by_preceeding_contexts = NaN(3,4);
for pre = 0:3
    these_ind = find(num_back_search_dur(:,1) == pre);
    average_search_dur_by_preceeding_contexts(1,pre+1) = nanmean(num_back_search_dur(these_ind,2));
    average_search_dur_by_preceeding_contexts(2,pre+1) = nanmean(num_back_search_dur(these_ind,3));
    average_search_dur_by_preceeding_contexts(3,pre+1) = nanmean(num_back_search_dur(these_ind,4));
end

figure
subplot(2,2,1)
bar(0:3,100*average_search_dur_by_preceeding_contexts(1,:))
xlabel('# of Preceeding Trials with Same Context')
ylabel('% Change in Search Duration')
box off
title('% Change Compared to Expected by # Distractors')

subplot(2,2,2)
bar(0:3,1000*average_search_dur_by_preceeding_contexts(2,:))
xlabel('# of Preceeding Trials with Same Context')
ylabel('Change in Search Duration (ms)')
box off
title('Absolute Change Compared to Expected by # Distractors')

subplot(2,2,3)
bar(0:3,1000*average_search_dur_by_preceeding_contexts(3,:))
xlabel('# of Preceeding Trials with Same Context')
ylabel('Change in Search Duration (ms)')
box off
title('Absolute Change')

subtitle([sess_file_name ' ' session_data.blockjson(1).BlockID(9:end-3)])