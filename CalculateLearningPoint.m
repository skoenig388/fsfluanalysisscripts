function [LP_aligned_block_performance,LP_aligned_block_search_duration,Learning_Points,....
    block_performance_LP_less_10,block_performance_LP_less_20,...
    block_performance_LP_less_30,block_performance_LP_greater30,block_performance_no_LP] = ...
    CalculateLearningPoint(session_data,goodblocks,min_block_len,last_trial_window,...
    forward_smoothing_window,learning_point_threshold,forward_back_trials)
%adapted from code else where, written 5/10/19

%Inputs:
% 1) session_data: all the session data
% 2) goodblocks: blocks with acceptable performance
% 3) min_block_len: miniumum # of trials in a block
% 4) last_trial_window: # of trials to look at end of block
% 5) forward_smoothing_window: window width for forward smoothing filter
% 6) learning_point_threshold: proportion correct to consider having "learned"
% 7) forward_back_trials: for performance aligned LP, how far forward and back to look

%Outputs:
% 1) LP_aligned_block_performance: learning point aligned performance
% 2) LP_algined_block_search_duration: learnning point aligned search duration
% 3) Learning_Points: trial in which "learning" occured
% 4) block_performance_LP_less_10: block performance on blocks in which LP occurs on a trial <= trial 10
% 5) block_performance_LP_less_30: block performance on blocks in which LP occurs on a trial < trial 30
% 6) block_performance_LP_greater30: block performance on blocks in which LP occurs on a trial > 30
% 7) block_performance_no_LP: block performance on blocks with no LP identified

if nargin < 3
    error('Not enough input arguments')
elseif nargin < 4
    forward_smoothing_window = 10;
elseif nargin < 5
    forward_smoothing_window = 10;
    learning_point_threshold = 0.8;
elseif nargin < 6
    forward_smoothing_window = 10;
    learning_point_threshold = 0.8;
    forward_back_trials = 5;
end

if learning_point_threshold > 1
    %assuming thrteshold is meant to be proportion correct not 100%
    learning_point_threshold = learning_point_threshold/100;
end

num_blocks = length(goodblocks);
block = session_data.trial_data.Block;
block = block + 1;%index starts at 0 in C++
rewarded = strcmpi(session_data.trial_data.isHighestProbReward,'true');
search_duration = Calculate_FLU_Search_Duration(session_data);

LP_aligned_block_performance = NaN(num_blocks,2*forward_back_trials+1);
LP_aligned_block_search_duration = NaN(num_blocks,2*forward_back_trials+1);

Learning_Points = NaN(1,num_blocks);

block_performance_LP_less_10 = [];
block_performance_LP_less_20 = [];
block_performance_LP_less_30 = [];
block_performance_LP_greater30 = [];
block_performance_no_LP = [];
for b = 1:num_blocks
    blockind = find(block == goodblocks(b));
    if isempty(blockind)
        error('why is block empty?')
    elseif length(blockind) < min_block_len%didn't finish
        error('why is block too short?')
    end
    
    %---Get Learning Point---%
    blockrewarded = rewarded(blockind(1):blockind(end));
    blocksearchdur = search_duration(blockind(1):blockind(end));
    trials_in_block = length(blockrewarded);
    lp = FindLp(blockrewarded, 'slidingwindow', forward_smoothing_window, learning_point_threshold);
    if ~isempty(lp) && ~isnan(lp) && lp ~= length(blockrewarded)
        Learning_Points(b) = lp;
        
        if lp <= forward_back_trials
            start_index = forward_back_trials-lp+1;
            LP_aligned_block_performance(b,start_index+1:end) = blockrewarded(1:lp+forward_back_trials);
            LP_aligned_block_search_duration(b,start_index+1:end) = blocksearchdur(1:lp+forward_back_trials);
        elseif lp >= trials_in_block-forward_back_trials
            end_index = (lp+forward_back_trials)-trials_in_block;
            LP_aligned_block_performance(b,1:end-end_index) = blockrewarded(lp-forward_back_trials:end);
            LP_aligned_block_search_duration(b,1:end-end_index) = blocksearchdur(lp-forward_back_trials:end);
        else
            LP_aligned_block_performance(b,:) = blockrewarded(lp-forward_back_trials:lp+forward_back_trials);
            LP_aligned_block_search_duration(b,:) = blocksearchdur(lp-forward_back_trials:lp+forward_back_trials);
        end
    end
    
    %add last trial group for this part of the analysis
    blockrewarded = rewarded(blockind(1):blockind(end));
    blockrewarded = [blockrewarded(end-(last_trial_window-1):end); blockrewarded];
    if lp <= 10
        block_performance_LP_less_10 = [block_performance_LP_less_10; blockrewarded(1:min_block_len+last_trial_window)'];
    elseif lp <= 20
        block_performance_LP_less_20 = [block_performance_LP_less_20; blockrewarded(1:min_block_len+last_trial_window)'];
    elseif lp <= 30
        block_performance_LP_less_30 = [block_performance_LP_less_30; blockrewarded(1:min_block_len+last_trial_window)'];
    elseif isnan(lp) || isempty(lp)
        block_performance_no_LP = [block_performance_no_LP;  blockrewarded(1:min_block_len+last_trial_window)'];
    else %lp > 30
        block_performance_LP_greater30 = [block_performance_LP_greater30; blockrewarded(1:min_block_len+last_trial_window)'];
    end
end

end