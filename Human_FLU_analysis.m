%Written by Seth Konig 3/2/19
%Similar to FLU_DF04_Combined_Data_Across_Sets but updated to work with
%newer scripts and new task structure

tic
clar
num_shuffs = 1000;
import_option = 'slim';
max_time_out = 3*60;

%average acceptable performance across blocks
miniumum_performance = 0.5;%set to 0 if you don't want to remove blocks
max_block_cut = 3;%of of 1st and last blocks to cut if performance is very bad
min_block_len = 20;%miniumum # of trials in a block
last_trial_window = 10;%window to look at before end of block

learning_point_threshold = 80;
forward_back_trials = 7;
forward_smoothing_window = 10; %size of the smoothing window for forward filter

%data import options
pre_processed_data_dir = '\Processed_Data\'; %for pre-saved session data files
reimport_new = false;%will import session data that is already processsed

%%

monkeyname = 'Human';
sess_file_names = {'MFLU__001__17_07_2019__15_08_09','MFLU__002__18_07_2019__13_01_41',...
    'MFLU__003__18_07_2019__14_38_38','MFLU__004__22_07_2019__15_01_51',...
    'MFLU__005__23_07_2019__15_30_21','MFLU__006__23_07_2019__16_21_44',...
    'MFLU__007__24_07_2019__10_05_16','MFLU__008__24_07_2019__11_18_16',...
    'MFLU__009__24_07_2019__13_04_35','MFLU__010__24_07_2019__14_02_49',...
    'MFLU__011__24_07_2019__16_03_31','MFLU__012__29_07_2019__10_00_42',...
    'MFLU__013__31_07_2019__11_28_30','MFLU__014__16_08_2019__13_24_20',...
    'MFLU__015__21_08_2019__14_12_59','MFLU__016__22_08_2019__13_07_25',...
    };
    
%% Import Session Data
num_sessions = length(sess_file_names);
session_data = cell(1,num_sessions);
which_monkey = NaN(1,num_sessions);
for sess =1:num_sessions
    which_monkey(sess) = DetermineWhichMonkey(sess_file_names{sess});
    
    disp(['Loading data from session #' num2str(sess) '/' num2str(num_sessions)])
    log_dir = FindDataDirectory(sess_file_names{sess});
    if ~isempty(log_dir)
        session_data_file_name = [log_dir sess_file_names{sess} ...
            pre_processed_data_dir sess_file_names{sess} '_session_data.mat'];
        if ~exist(session_data_file_name,'file') && ~reimport_new
            session_data{sess} = ImportFLU_DataV2(sess_file_names{sess},log_dir,import_option);
        else
            load(session_data_file_name,'all_session_data');
            session_data{sess} = all_session_data;
        end
    else
        error(['Cant Find Log Dir for '  sess_file_names{sess}])
    end
end

%% Calculate Averege Performance by Block and Remove "Bad" Blocks
all_good_blocks = cell(1,num_sessions);
all_block_performance = cell(1,num_sessions);
all_session_block_performance = NaN(num_sessions,25);
for sess = 1:num_sessions
    [all_good_blocks{sess},all_block_performance{sess}] = ...
        RemoveBadBlocks(session_data{sess},miniumum_performance,max_block_cut,min_block_len,max_time_out);
    all_session_block_performance(sess,1:length(all_block_performance{sess})) = ...
        all_block_performance{sess};
end

figure
subplot(1,2,1)
plot(nanmean(all_session_block_performance))
xlabel('Block #')
ylabel('Performance-Proportion Correct')
title('Average Performance by Block')
box off

subplot(1,2,2)
hist(all_session_block_performance(:),20)
xlabel('Performance-Proportion Correct')
ylabel('Block Count')
title('Distribution of Block Performance')
box off

subtitle([monkeyname ': Performance by Block #'])
box off
%% Calculate perofrmance across all blocks
all_block_performance = [];
all_trial_count = NaN(length(sess_file_names),25);
all_extra_trial_count = NaN(length(sess_file_names),25);
all_block_end_performance = NaN(length(sess_file_names),25);

for sess = 1:num_sessions
    [block_performance,trial_count,extra_trial_count,block_end_performance] = ...
        CalculateBlockPerformance(session_data{sess},all_good_blocks{sess},min_block_len,last_trial_window);
    
    all_block_performance = [all_block_performance; block_performance];
    all_trial_count(sess,1:length(trial_count)) = trial_count;
    all_extra_trial_count(sess,1:length(extra_trial_count)) = extra_trial_count;
    all_block_end_performance(sess,1:length(block_end_performance)) = block_end_performance;
end

figure
subplot(2,2,1)
hold on
plot([0.5 0.5],[0 100],'k--')
plot([-(last_trial_window-1) min_block_len],[80 80],'k--')
plot([-(last_trial_window-1) min_block_len],[33 33],'k--')
plot([-(last_trial_window-1) min_block_len],[66 66],'k--')
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_block_performance)',3,2),'r')
plot(-(last_trial_window-1):min_block_len,100*nanmean(all_block_performance))
hold off
ylim([0 100])
xlabel('Trial # Relative To Switch')
ylabel('Performance (% Correct)')
box off
title(sprintf(['Average Performance-All blocks \n' ...
    'Average_{20-30} = ' num2str(100*mean(nanmean(all_block_performance(:,end-9:end))),3) '%%, ' ...
    'Average_{Last 10} = ' num2str(100*mean(nanmean(all_block_performance(:,1:10))),3) '%%']));

subplot(2,2,2)
histogram(all_trial_count(:),30:60)
xlabel('Trials in Block')
ylabel('Block Count')
title('Total Trial Count per Block')
box off

subplot(2,2,3)
histogram(all_extra_trial_count(:),0:30)
xlabel('Extra Trials in Block')
ylabel('Block Count')
title('Extra Trial Count per block')
box off

subplot(2,2,4)
histogram(100*all_block_end_performance(:),-5:10:95)
xlabel('Performance (% Correct)')
ylabel('Block Count')
box off
title('Peformance at End of Block')

subtitle(monkeyname)

%% Calculate Search Duration/Response Time Measurements for All Blocks
all_search_duration = [];
all_correct_search_duration = [];
all_incorrect_search_duration = [];
all_incorrect_last_trial_search_duration = [];
all_correct_last_trial_search_duration = [];

all_correct_n1_incorrect_search_duration = [];
all_correct_n1_correct_search_duration = [];
all_incorrect_n1_incorrect_search_duration = [];
all_incorrect_n1_correct_search_duration = [];

for sess = 1:num_sessions
    [block_search_duration,block_correct_search_duration,block_incorrect_search_duration,...
        block_incorrect_last_trial,block_correct_last_trial,...
        correct_incorrect_minus_one_search_duration,correct_correct_minus_one_search_duration,...
        incorrect_incorrect_minus_one_search_duration,incorrect_correct_minus_one_search_duration] = ...
        CalculateBlockSearchDuration(session_data{sess},all_good_blocks{sess},min_block_len,last_trial_window);
    
    all_search_duration = [all_search_duration; block_search_duration];
    all_correct_search_duration = [all_correct_search_duration; block_correct_search_duration];
    all_incorrect_search_duration = [all_incorrect_search_duration; block_incorrect_search_duration];
    all_incorrect_last_trial_search_duration = [all_incorrect_last_trial_search_duration; block_incorrect_last_trial];
    all_correct_last_trial_search_duration = [all_correct_last_trial_search_duration; block_correct_last_trial];
    
    all_correct_n1_incorrect_search_duration = [all_correct_n1_incorrect_search_duration; correct_incorrect_minus_one_search_duration];
    all_correct_n1_correct_search_duration = [all_correct_n1_correct_search_duration; correct_correct_minus_one_search_duration];
    all_incorrect_n1_incorrect_search_duration = [all_incorrect_n1_incorrect_search_duration; incorrect_incorrect_minus_one_search_duration];
    all_incorrect_n1_correct_search_duration = [all_incorrect_n1_correct_search_duration; incorrect_correct_minus_one_search_duration];
end

figure
subplot(2,2,1)
hold on
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_search_duration)',3,2),'r')
plot(-(last_trial_window-1):min_block_len,nanmean(all_search_duration))
yl = ylim;
plot([0.5 0.5],[yl(1) yl(2)],'k--')
hold off
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (sec)')
box off
title(sprintf(['Average Search Duration-All blocks \n' ...
    'Average_{20-30} = ' num2str(mean2(all_search_duration(:,end-9:end)),3) ' sec, ' ...
    'Average_{Last 10} = ' num2str(mean2(all_search_duration(:,1:10)),3) ' sec']))


subplot(2,2,2)
hold on
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_search_duration)',3,2),'k')
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_correct_search_duration)',3,2),'g')
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_incorrect_search_duration)',3,2),'r')
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_incorrect_last_trial_search_duration)',3,2),'b')
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_correct_last_trial_search_duration)',3,2),'m')
yl = ylim;
plot([0.5 0.5],[yl(1) yl(2)],'k--')
hold off
legend('All','Correct','Incorrect','Incorrect Last Trial','Correct Last Trial')
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (sec)')
box off
title('Search Duration by Performance')

subplot(2,2,3)
hold on
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_search_duration)',3,2),'k')
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_correct_n1_correct_search_duration)',3,2),'g')
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_correct_n1_incorrect_search_duration)',3,2),'b')
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_incorrect_n1_correct_search_duration)',3,2),'m')
plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_incorrect_n1_incorrect_search_duration)',3,2),'r')
yl = ylim;
plot([0.5 0.5],[yl(1) yl(2)],'k--')
hold off
legend('All','Correct & N-1 Correct','Correct & N-1 Incorrect','Incorrect & N-1 Correct','Incorrect & N-1 Incorrect')
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (sec)')
box off
title('Search Duration by Performance')

subplot(2,2,4)
bar([nanmean(all_search_duration(:)),...
    nanmean(all_correct_search_duration(:)),nanmean(all_correct_n1_correct_search_duration(:)),nanmean(all_correct_n1_incorrect_search_duration(:)),...
    nanmean(all_incorrect_search_duration(:)),nanmean(all_incorrect_n1_correct_search_duration(:)),nanmean(all_incorrect_n1_incorrect_search_duration(:))])
xticks(1:7)
xticklabels({'All',...
    'All Correct','Correct & N-1 Correct','Correct & N-1 InCorrect',...
    'All Incorrect','Incorrect & N-1 Correct','Incorrect & N-1 Incorrect'})
xtickangle(30)
ylim(yl)
ylabel('Search Duration (sec)')
box off
title('Search Duration by Performance Across All Trials')

subtitle(monkeyname)
%% Calculate Performance for Intra-vs Extra-dimensional shifts and Cued vs Uncued Shifts
all_intra_performance = [];
all_extra_performance = [];

all_cued_intra_performance = [];
all_cued_extra_performance = [];
all_uncued_intra_performance = [];
all_uncued_extra_performance = [];

all_intra_search_duration = [];
all_extra_search_duration = [];

all_cued_intra_search_duration = [];
all_cued_extra_search_duration = [];
all_uncued_intra_search_duration = [];
all_uncued_extra_search_duration = [];

for sess = 1:num_sessions
    [intra_extra_block_performance,intra_extra_search_duration,...
        intra_extra_shift,cued_uncued_shift] = CalculateBlockPerformanceIntra_vs_Extra_Dim(...
        session_data{sess},all_good_blocks{sess},min_block_len,last_trial_window);
    
    all_intra_performance = [all_intra_performance; intra_extra_block_performance(intra_extra_shift == 1,:)];
    all_extra_performance = [all_extra_performance; intra_extra_block_performance(intra_extra_shift == 2,:)];
    
    all_cued_intra_performance = [all_cued_intra_performance; intra_extra_block_performance(intra_extra_shift == 1 & cued_uncued_shift == 1,:)];
    all_cued_extra_performance = [all_cued_extra_performance; intra_extra_block_performance(intra_extra_shift == 2 & cued_uncued_shift == 1,:)];
    all_uncued_intra_performance = [all_uncued_intra_performance; intra_extra_block_performance(intra_extra_shift == 1 & cued_uncued_shift == 0,:)];
    all_uncued_extra_performance = [all_uncued_extra_performance; intra_extra_block_performance(intra_extra_shift == 2 & cued_uncued_shift == 0,:)];
    
    all_intra_search_duration = [all_intra_search_duration; intra_extra_search_duration(intra_extra_shift == 1,:)];
    all_extra_search_duration = [all_extra_search_duration; intra_extra_search_duration(intra_extra_shift == 2,:)];
    
    all_cued_intra_search_duration = [all_cued_intra_search_duration; intra_extra_search_duration(intra_extra_shift == 1 & cued_uncued_shift == 1,:)];
    all_cued_extra_search_duration = [all_cued_extra_search_duration; intra_extra_search_duration(intra_extra_shift == 2 & cued_uncued_shift == 1,:)];
    all_uncued_intra_search_duration = [all_uncued_intra_search_duration; intra_extra_search_duration(intra_extra_shift == 1 & cued_uncued_shift == 0,:)];
    all_uncued_extra_search_duration = [all_uncued_extra_search_duration; intra_extra_search_duration(intra_extra_shift == 2 & cued_uncued_shift == 0,:)];
    
end

if size(all_cued_intra_search_duration,1) ~= 0 && size(all_uncued_intra_search_duration,1) ~= 0
    %---Plots for Intradimensional vs Extradimensional---%
    figure
    subplot(2,2,1)
    hold on
    plot([0.5 0.5],[0 100],'k--')
    plot([-(last_trial_window-1) min_block_len],[80 80],'k--')
    plot([-(last_trial_window-1) min_block_len],[33 33],'k--')
    plot([-(last_trial_window-1) min_block_len],[66 66],'k--')
    p1 = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_intra_performance)',3,2),'r');
    p2 = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_extra_performance)',3,2),'b');
    hold off
    ylim([0 100])
    xlabel('Trial # Relative To Switch')
    ylabel('Performance (% Correct)')
    box off
    title('Performance: Intra vs Extra-Dim Shifts')
    legend([p1 p2],{'Intra','Extra'},'Location','SouthEast')
    
    subplot(2,2,3)
    plot(-(last_trial_window-1):min_block_len,FilterLearningCurves((100*nanmean(all_extra_performance)-100*nanmean(all_intra_performance))',3,2),'m');
    hold on
    yl = ylim;
    plot([0 0],[yl(1) yl(2)],'k--')
    hold off
    xlabel('Trial # Relative To Switch')
    ylabel('Performance (% Correct)')
    box off
    title('Performance Difference: Extra-Intra')
    
    subplot(2,2,2)
    hold on
    plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_intra_search_duration)',3,2),'r');
    plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_extra_search_duration)',3,2),'b');
    yl = ylim;
    plot([0 0],[yl(1) yl(2)],'k--')
    hold off
    xlabel('Trial # Relative To Switch')
    ylabel('Search Duration (sec)')
    box off
    title('Search Duration: Intra vs Extra-Dim Shifts')
    
    subplot(2,2,4)
    plot(-(last_trial_window-1):min_block_len,FilterLearningCurves((nanmean(all_extra_search_duration)-nanmean(all_intra_search_duration))',3,1),'m');
    hold on
    yl = ylim;
    plot([0 0],[yl(1) yl(2)],'k--')
    hold off
    xlabel('Trial # Relative To Switch')
    ylabel('Search Duration (Sec)')
    box off
    title('Search Duration Difference: Extra-Intra')
    
    subtitle(monkeyname)
    
    %---Plots for Cued vs Uncued & Intradimensional vs Extradimensional---%
    figure
    subplot(2,2,1)
    hold on
    plot([0.5 0.5],[0 100],'k--')
    plot([-(last_trial_window-1) min_block_len],[80 80],'k--')
    plot([-(last_trial_window-1) min_block_len],[33 33],'k--')
    plot([-(last_trial_window-1) min_block_len],[66 66],'k--')
    p1 = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_cued_intra_performance)',3,2),'r');
    p2 = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_uncued_intra_performance)',3,2),'m');
    p3= plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_cued_extra_performance)',3,2),'b');
    p4 = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_uncued_extra_performance)',3,2),'g');
    hold off
    ylim([0 100])
    xlabel('Trial # Relative To Switch')
    ylabel('Performance (% Correct)')
    box off
    title('Performance: Intra/Extra & Cued/Uncued Shifts')
    legend([p1 p2 p3 p4],{'Cued Intra','Uncued Intra','Cued Extra','Uncued Extra'},'Location','SouthEast')
    
    subplot(2,2,3)
    hold on
    plot(-(last_trial_window-1):min_block_len,FilterLearningCurves((100*nanmean(all_cued_intra_performance)-100*nanmean(all_uncued_intra_performance))',3,1),'c');
    plot(-(last_trial_window-1):min_block_len,FilterLearningCurves((100*nanmean(all_cued_extra_performance)-100*nanmean(all_uncued_extra_performance))',3,1),'k');
    yl = ylim;
    plot([0 0],[yl(1) yl(2)],'k--')
    hold off
    xlabel('Trial # Relative To Switch')
    ylabel('Performance (% Correct)')
    box off
    title('Performance Difference: Cued-Uncued')
    legend({'Intra','Extra'},'Location','SouthEast')
    
    subplot(2,2,2)
    hold on
    p1 = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_cued_intra_search_duration)',3,2),'r');
    p2 = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_uncued_intra_search_duration)',3,2),'m');
    p3= plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_cued_extra_search_duration)',3,2),'b');
    p4 = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_uncued_extra_search_duration)',3,2),'g');
    yl = ylim;
    plot([0 0],[yl(1) yl(2)],'k--')
    hold off
    xlabel('Trial # Relative To Switch')
    ylabel('Search Duration (sec)')
    box off
    title('Search Duration: Intra/Extra & Cued/Uncued Shifts')
    
    subplot(2,2,4)
    hold on
    plot(-(last_trial_window-1):min_block_len,FilterLearningCurves((nanmean(all_cued_intra_search_duration)-nanmean(all_uncued_intra_search_duration))',3,1),'c');
    plot(-(last_trial_window-1):min_block_len,FilterLearningCurves((nanmean(all_cued_extra_search_duration)-nanmean(all_uncued_extra_search_duration))',3,1),'k');
    yl = ylim;
    plot([0 0],[yl(1) yl(2)],'k--')
    hold off
    xlabel('Trial # Relative To Switch')
    ylabel('Search Duration (sec)')
    box off
    title('Search Duration Difference: Cued-Uncued')
    
    subtitle(monkeyname)
end
%% Stats shift type intra-vs-extra
% stat_win = [4 20]+10;%trials 4-20 after the shift igore 1st since could be at chance
% observed_diff = sum(average_extra_performance(stat_win(1):stat_win(2))-average_intra_performance(stat_win(1):stat_win(2)));
% observed_diff_time = FilterLearningCurves(average_extra_performance',3,2)-FilterLearningCurves(average_intra_performance',3,2);
%
% shuffled_diff = NaN(1,num_shuffs);
% shuffled_diff_time = NaN(num_shuffs,40);
% all_shift_data = [all_extra_block_performance; all_intra_block_performance];
% all_shift_ind = [ones(total_num_extra_blocks,1); 2*ones(total_num_intra_blocks,1)];
% for shuff = 1:num_shuffs
%     shuff_ind = all_shift_ind(randperm(length(all_shift_ind)));
%     shuff_intra = nanmean(all_shift_data(shuff_ind == 1,:));
%     shuff_extra = nanmean(all_shift_data(shuff_ind == 2,:));
%     shuffled_diff(shuff) = sum(shuff_extra(stat_win(1):stat_win(2))-shuff_intra(stat_win(1):stat_win(2)));
%
%     shuffled_diff_time(shuff,:) = FilterLearningCurves(shuff_extra',3,2)-FilterLearningCurves(shuff_intra',3,2);
% end
% prct_intra_extra = 100*sum(observed_diff > shuffled_diff)/num_shuffs;
%
% time_95pct = NaN(40,1);
% for tp = 1:40
%     time_95pct(tp) = prctile(shuffled_diff_time(:,tp),95);
% end
%
% sig_time_points = find(observed_diff_time > time_95pct);
% sig_time_points(sig_time_points <= 14) = [];

%% Plot performance and search duration by monkey
[block_performance_by_monkey,search_duration_by_monkey] = ...
    CalculateBlockPerformancebyMonkey(session_data,all_good_blocks,min_block_len,...
    last_trial_window,which_monkey);

%% Plot performance and search duration by target and distractor dimensions
[block_performance_by_dim,search_duration_by_dim] = ...
    CalculateBlockPerformancebyDimension(session_data,all_good_blocks,min_block_len,...
    last_trial_window);

%% Learning Point anlayses
all_Learning_Points = [];
all_LP_aligned_block_performance = [];
all_LP_aligned_search_duration = [];
all_block_performance_LP_less_10 = [];
all_block_performance_LP_less_20 = [];
all_block_performance_LP_less_30 = [];
all_block_performance_LP_greater30 = [];
all_block_performance_no_LP = [];
for sess = 1:num_sessions
    [LP_aligned_block_performance,LP_aligned_search_duration,Learning_Points,...
        block_performance_LP_less_10,block_performance_LP_less_20,block_performance_LP_less_30,...
        block_performance_LP_greater30,block_performance_no_LP] = ...
        CalculateLearningPoint(session_data{sess},all_good_blocks{sess},min_block_len,last_trial_window,...
        forward_smoothing_window,learning_point_threshold,forward_back_trials);
    
    all_Learning_Points = [all_Learning_Points Learning_Points];
    all_LP_aligned_block_performance = [all_LP_aligned_block_performance; LP_aligned_block_performance];
    all_LP_aligned_search_duration = [all_LP_aligned_search_duration; LP_aligned_search_duration];
    
    all_block_performance_LP_less_10 = [all_block_performance_LP_less_10; block_performance_LP_less_10];
    all_block_performance_LP_less_20 = [all_block_performance_LP_less_20; block_performance_LP_less_20];
    all_block_performance_LP_less_30 = [all_block_performance_LP_less_30; block_performance_LP_less_30];
    all_block_performance_LP_greater30 = [all_block_performance_LP_greater30; block_performance_LP_greater30];
    all_block_performance_no_LP = [all_block_performance_no_LP; block_performance_no_LP];
end
all_Learning_Points = laundry(all_Learning_Points);
all_LP_aligned_block_performance = laundry(all_LP_aligned_block_performance);

LP_perform_legend_str = [];
LP_perform_legend_name = {'LP <= 10','LP > 10 & LP <= 20','LP > 20 & LP <= 30','LP > 30','LP DNE'};
for LPtype = 1:length(LP_perform_legend_name)
    if LPtype == 1
        n_str = size(all_block_performance_LP_less_10,1);
    elseif LPtype == 2
        n_str = size(all_block_performance_LP_less_20,1);
    elseif LPtype == 3
        n_str = size(all_block_performance_LP_less_30,1);
    elseif LPtype == 4
        n_str = size(all_block_performance_LP_greater30,1);
    elseif LPtype == 5
        n_str = size(all_block_performance_no_LP,1);
    end
    LP_perform_legend_str{LPtype} = [LP_perform_legend_name{LPtype} ', n = ' num2str(n_str)];
end

figure
subplot(2,2,1)
plot([0 0],[0 100],'k--')
hold on
plot([-forward_back_trials forward_back_trials],[33 33],'k')
plot(-forward_back_trials:forward_back_trials,100*nanmean(all_LP_aligned_block_performance))
hold off
xlabel('Trial from LP')
ylabel('Performance (% Correct)')
box off
axis square
xlim([-forward_back_trials forward_back_trials])
ylim([0 100])
title(['All Switches: Backward Learning Curve, n = ' num2str(size(all_LP_aligned_block_performance,1))])

subplot(2,2,2)
hist(all_Learning_Points,1:max(all_Learning_Points))
xlabel('LP Tria#')
ylabel('Block Count')
axis square
box off
title('All Switches: Learning Point Distribution')

subplot(2,2,3)
plot(-forward_back_trials:forward_back_trials,nanmean(all_LP_aligned_search_duration))
yl = ylim;
hold on
plot([0 0],[yl(1) yl(2)],'k--')
hold off
xlabel('Trial from LP')
ylabel('Search Duaration (sec)')
box off
axis square
xlim([-forward_back_trials forward_back_trials])
title('All Switches: Backwards Search Duration Curve')

% subplot(2,2,4)
% hold on
% plot([0.5 0.5],[0 100],'k--')
% plot([-(last_trial_window-1) min_block_len],[33 33],'k--')
% plot([-(last_trial_window-1) min_block_len],[66 66],'k--')
% p(1) = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_block_performance_LP_less_10)',3,2),'g');
% p(2) = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_block_performance_LP_less_20)',3,2),'b');
% p(3) = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_block_performance_LP_less_30)',3,2),'m');
% p(4) = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_block_performance_LP_greater30)',3,2),'r');
% p(5) = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_block_performance_no_LP)',3,2),'c');
% hold off
% box off
% grid on
% axis square
% ylim([0 100])
% xlabel('Trial # Relative To Switch')
% ylabel('Performance (% Correct)')
% title('Performance: by "Learning Rate"')
% legend(p,LP_perform_legend_str,'Location','NorthEastOutside')

subtitle(monkeyname)

%% Plot Performacne by # of Irrelevant Feature Dimensions
all_LP_by_num_irrel = cell(1,3);
all_LP_aligned_block_performance_by_num_irrel = cell(1,3);
all_block_performance_by_num_irrel = cell(1,3);
all_search_duration_by_num_irrel = cell(1,3);
for sess = 1:num_sessions
    [block_performance_by_num_irrel,search_duration_by_num_irrel,...
        LP_by_num_irrel,LP_aligned_block_performance_by_num_irrel] = ...
        CalculateBlockPerformancebyNumIrrelDim(session_data{sess},all_good_blocks{sess},min_block_len,last_trial_window,...
        forward_smoothing_window,learning_point_threshold,forward_back_trials);
    
    for num_irrel = 1:3
        all_LP_by_num_irrel{num_irrel} = [all_LP_by_num_irrel{num_irrel} LP_by_num_irrel{num_irrel}];
        all_LP_aligned_block_performance_by_num_irrel{num_irrel} = ...
            [all_LP_aligned_block_performance_by_num_irrel{num_irrel}; LP_aligned_block_performance_by_num_irrel{num_irrel}];
        all_block_performance_by_num_irrel{num_irrel} = [all_block_performance_by_num_irrel{num_irrel}; ...
            block_performance_by_num_irrel{num_irrel}];
        all_search_duration_by_num_irrel{num_irrel} = [all_search_duration_by_num_irrel{num_irrel}; ...
            search_duration_by_num_irrel{num_irrel}];
    end
end

n_irrel = NaN(1,3);
for num_irrel = 1:3
    n_irrel(num_irrel) = size(all_block_performance_by_num_irrel{num_irrel},1);
end

%---Plot Results by Number of irrelevant featurs

figure
subplot(2,2,1)
hold on
plot([0.5 0.5],[0 100],'k--')
plot([-(last_trial_window-1) min_block_len],[33 33],'k--')
plot([-(last_trial_window-1) min_block_len],[66 66],'k--')
plot([-(last_trial_window-1) min_block_len],[80 80],'k--')
for num_irrel = 1:3
    if n_irrel(num_irrel) > 0
        pnirr(num_irrel) = plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(100*nanmean(all_block_performance_by_num_irrel{num_irrel})',3,2));
    else
        pnirr(num_irrel) = plot(0,0);
    end
end
hold off
box off
ylim([0 100])
xlabel('Trial # Relative To Switch')
ylabel('Performance (% Correct)')
title('Performance by # of Irrelevant Dimensions')
legend(pnirr,{['0, n = ' num2str(n_irrel(1))],['1, n = ' num2str(n_irrel(2))],...
    ['2, n = ' num2str(n_irrel(3))]},'Location','SouthEast')

subplot(2,2,2)
hold on
for num_irrel = 1:3
    if n_irrel(num_irrel) > 0
        plot(-(last_trial_window-1):min_block_len,FilterLearningCurves(nanmean(all_search_duration_by_num_irrel{num_irrel})',3,2));
    else
        plot(0,1)
    end
end
yl = ylim;
plot([0.5 0.5],[yl(1) yl(2)],'k--')
hold off
box off
xlabel('Trial # Relative To Switch')
ylabel('Search Duration (sec)')
title('Search Duration by # of Irrelevant Dimensions')

subplot(2,2,3)
hold on
plot([0 0],[0 100],'k--')
plot([-forward_back_trials forward_back_trials],[33 33],'k--')
plot([-forward_back_trials forward_back_trials],[66 66],'k--')
for num_irrel = 1:3
    plot(-forward_back_trials:forward_back_trials,100*nanmean(all_LP_aligned_block_performance_by_num_irrel{num_irrel}))
end
ylim([0 100])
xlim([-forward_back_trials forward_back_trials])
hold off
box off
xlabel('Trial from LP')
ylabel('Performance (% Correct)')
title('Backward Learning Curves by # of Irrelevant Dimensions')

subplot(2,2,4)
hold on
for num_irrel = 1:3
    n_dist = hist(all_LP_by_num_irrel{num_irrel},1:2:60);
    n_dist = n_dist./sum(n_dist);
    plot(1:2:60,n_dist);
end
hold off
box off
xlabel('LP trial #')
ylabel('Proportion of Blocks')
title('LP Distribution # of Irrelevant Dimensions')
%% Look for Choice Biases: Spatial & Feature

[spatial_choice_baises,feature_choice_baises] = CalculateChoiceBiases(...
    session_data,min_block_len,forward_smoothing_window,learning_point_threshold);
subtitle(monkeyname)

%%
toc